# - Try to find CPLEX
# Once done this will define
#  CPLEX_FOUND - System has CPlex
#  CPLEX_INCLUDE_DIRS - The CPlex include directories
#  CPLEX_LIBRARIES - The libraries needed to use CPlex

if (CPLEX_INCLUDE_DIR)
  # in cache already
  set(CPLEX_FOUND TRUE)
  set(CPLEX_INCLUDE_DIRS "${CPLEX_C_INCLUDE_DIR};${CPLEX_C_XX_INCLUDE_DIR}" )
  set(CPLEX_LIBRARIES "${CPLEX_CXX_LIBRARY};${CPLEX_C_LIBRARY}" )
else (CPLEX_INCLUDE_DIR)

find_path(CPLEX_C_INCLUDE_DIR 
          NAMES ilcplex/cplex.h
          PATHS "$ENV{CPLEX_HOME}/cplex/include"
                "$ENV{CPLEX_HOME}\\cplex\\include"
          )

find_path(CPLEX_CXX_INCLUDE_DIR 
          NAMES ilconcert/iloenv.h
          PATHS "$ENV{CPLEX_HOME}/concert/include"
                "$ENV{CPLEX_HOME}\\concert\\include"
          )

find_library( CPLEX_C_LIBRARY 
              NAMES cplex1290
              PATHS "$ENV{CPLEX_HOME}/cplex/lib" 
                    "$ENV{CPLEX_HOME}\\cplex\\lib\\x64_windows_vs2017\\stat_mda"
              )

find_library( CPLEX_CXX_LIBRARY 
              NAMES concert
              PATHS "$ENV{CPLEX_HOME}/concert/lib" 
                    "$ENV{CPLEX_HOME}\\concert\\lib\\x64_windows_vs2017\\stat_mda"
              )

# Used in debugging file
#message(${CPLEX_C_INCLUDE_DIR})
#message(${CPLEX_CXX_INCLUDE_DIR})
#message(${CPLEX_C_LIBRARY})
#message(${CPLEX_CXX_LIBRARY})

set(CPLEX_INCLUDE_DIRS "${CPLEX_C_INCLUDE_DIR};${CPLEX_CXX_INCLUDE_DIR}" )
set(CPLEX_LIBRARIES "${CPLEX_C_LIBRARY};${CPLEX_CXX_LIBRARY}" )

# use c++ headers as default
set(CPLEX_COMPILER_DEFS "-DIL_STD" CACHE STRING "CPlex Compiler Definitions")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBCPLEX_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(CPLEX  DEFAULT_MSG
                                  CPLEX_C_LIBRARY CPLEX_CXX_LIBRARY
                                  CPLEX_C_INCLUDE_DIR CPLEX_CXX_INCLUDE_DIR)

mark_as_advanced(CPLEX_C_INCLUDE_DIR CPLEX_CXX_INCLUDE_DIR 
				 CPLEX_C_LIBRARY CPLEX_CXX_LIBRARY)

endif(CPLEX_INCLUDE_DIR)