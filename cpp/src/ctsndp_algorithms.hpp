
#pragma once

#include "ctsndp_defs.hpp"
#include <memory>
#include <vector>
#include <set>
#include <map>
#include <iostream>

class cServiceNetwork;
class cServiceLink;
class cCommodityShipments;
class cPartiallyTimeExpandedNetwork;
class cPartiallyTimeExpandedNetworkArc;
class cPartiallyTimeExpandedNetworkNode;
class cPartiallyTimeExpandedNetworkDelayNode;

namespace ctsndp
{
    /**
     * This is algorithm 2 listed on page 1311
     * Requires: Directed network 𝒟 = (𝒩, 𝒜), commodity set 𝒦
     * Returns: A partially time expanded network 𝒟t = (𝒩t, 𝒜t U ℋt), commodity set 𝒦
     **/
    std::unique_ptr<cPartiallyTimeExpandedNetwork> create_initial(const cServiceNetwork& service_network,
                                                                  const cCommodityShipments& shipments,
                                                                  std::ostream* debug = nullptr);

    /**
     * This is algorithm 3 listed on page 1312
     * Requires: Arc((i,t),(j,t')) in 𝒜t
     * Modifies: The partially time expanded network 
     **/
    void lengthen_arc(cPartiallyTimeExpandedNetwork& network,
                      std::set<cPartiallyTimeExpandedNetworkArc*>&& arcs,
                      std::ostream* debug = nullptr);

    /**
     * This is algorithm 4 listed on page 1312
     * Requires: Node i in 𝒩 and t_k < t_new
     * Modifies: The partially time expanded network; time point t_new added to 𝒯i
     * Node: t_k_1 should satify t_new < t_k+1, but it is not required.
     **/
    void refine(cPartiallyTimeExpandedNetwork& network,
                const cPartiallyTimeExpandedNetworkNode& node,
                double t_k, double t_new, double t_k_1,
                std::ostream* debug = nullptr);

    /**
     * This is algorithm 5 listed on page 1312
     * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_k < t_new
	 *
	 * Note: we return the arcs that will be deleted in algorithm 3
     **/
	std::vector<cPartiallyTimeExpandedNetworkArc*> restore(cPartiallyTimeExpandedNetwork& network,
                 const cPartiallyTimeExpandedNetworkNode& node,
                 double t_k, double t_new,
                std::ostream* debug = nullptr);

    /**
     * This is algorithm 6, an extension to the ctsndp paper
     * Requires: Arc((i,t),(j.t')) in 𝒜t
     * Modifies: The partially time expanded network
     **/
    void ensure_capacities(cPartiallyTimeExpandedNetwork& network,
                      arcs_over_capacity_t&& over_capacity_arcs,
					  double consolidation_time,
					  std::ostream* debug = nullptr);

	/**
	 * This is algorithm 7, an extension to algorithm 4 listed on page 1312
	 * Requires: Node i in 𝒩 and t_k < t_new
	 * Modifies: The partially time expanded network; time point t_new added to 𝒯i
	 *			  node is update to the newly inserted node
	 * Node: t_k_1 should satify t_new < t_k+1, but it is not required.
	 **/
	std::vector<cPartiallyTimeExpandedNetworkArc*> refine(cPartiallyTimeExpandedNetwork& network,
		cPartiallyTimeExpandedNetworkNode*& node,
		double t_k, double t_new, double& t_k_1, double step_time, unsigned int capacity,
		const cServiceLink* const link,
		std::ostream* debug = nullptr);

	/**
	 * Similiar algorithm to algorithm 3 listed on page 1312, but is used to shorten delay arcs
	 * Requires: Arc((i,t),(j,t')) in 𝒜t
	 * Modifies: The partially time expanded network
	 **/
	void shorten_arc(cPartiallyTimeExpandedNetwork& network,
		ctsndp::arcs_to_shorten_t&& arcs,
		std::ostream* debug = nullptr);

	/**
	 * Similiar algorithm to algorithm 4 listed on page 1312, but is used to refine delay nodes
	 * Requires: Node i in 𝒩 and t_new < t_k_1
	 * Modifies: The partially time expanded network; time point t_new added to 𝒯i
	 * Node: t_k should satify t_k < t_new.
	 **/
	void refine_delays(cPartiallyTimeExpandedNetwork& network,
		const cPartiallyTimeExpandedNetworkDelayNode& node,
		double t_k, double t_new, double t_k_1,
		std::ostream* debug = nullptr);

	/**
	 * Similiar algorithm to algorithm 5 listed on page 1312
	 * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_k < t_new
	 *
	 * Note: we return the arcs that will be deleted in algorithm 8
	 **/
	std::vector<cPartiallyTimeExpandedNetworkArc*> restore_delays(cPartiallyTimeExpandedNetwork& network,
		const cPartiallyTimeExpandedNetworkDelayNode& node,
		double t_k, double t_new,
		std::ostream* debug = nullptr);
}
