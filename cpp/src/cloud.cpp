
#include "json_utils.hpp"
#include "ctsndp.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "onf_utils.hpp"
#include "heartland_utils.hpp"

#include "cpprest/json.h"
#include "cpprest/http_listener.h"
#include "cpprest/uri.h"
#include "cpprest/asyncrt_utils.h"
#include "cpprest/json.h"
#include "cpprest/filestream.h"
#include "cpprest/containerstream.h"
#include "cpprest/producerconsumerstream.h"

#pragma warning ( push )
#pragma warning ( disable : 4457 )
#pragma warning ( pop )
#include <locale>
#include <ctime>
#include <clara.hpp>
#include <string>
#include <iostream>


class cHandler
{
public:
    cHandler(const utility::string_t& ip_address, const utility::string_t& port)
    {
        web::uri_builder uri(U("http://") + ip_address + U(":") + port);
        
        auto address = uri.to_uri().to_string();
//        httpHandler.reset(new cCloudRequests(address));
//        httpHandler->open().wait();
        
        ucout << utility::string_t(U("Listening for requests at: ")) << address << std::endl;
    }
    
    ~cHandler()
    {
//        httpHandler->close().wait();
    }
    
private:
//    std::unique_ptr<cCloudRequests> httpHandler;
};

int main(int argc, char** argv)
{
    // Command line parameters
    using namespace clara;

	// Set defaults
    std::string strIpAddress;
    std::string strPort;
    bool showHelp = false;

    auto cli = 
        Opt( strIpAddress, "IP Address" )
        ["-a"]["--address"]
        ("The IP address the server should be listen on for requests.") |
        Opt( strPort, "Port" )
        ["-p"]["--port"]
        ("The port number to use when listening for requests (1024 to 65535).") |
        Help(showHelp);

    auto result = cli.parse( Args( argc, argv ) );
    if( !result ) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    if (showHelp)
    {
        cli.writeToStream(std::cout);
        exit(0);
    }

    utility::string_t port = U("34568");
    if (!port.empty())
    {
        long port_num = std::atol(strPort.c_str());
        if ((port_num > 1024) && (port_num < 65536))
        {
            port = U(strPort);
        }
        else
        {
            cli.writeToStream(std::cout);
            exit(0);
        }
    }
    
    utility::string_t ipAddress = U("127.0.0.1");
    if (!strIpAddress.empty())
    {
        
    }

    cHandler handler(ipAddress, port);
    
    std::cout << "Press ENTER to exit." << std::endl;
    
    std::string line;
    std::getline(std::cin, line);
    
/*
    try
    {
        cCTSNDP ctsndp;
        ctsndp.printPartiallyTimeExpandedNetwork(true);
        auto results = ctsndp.solve(std::move(network), std::move(shipments));
        
        print_result(std::cout, *results.get());
        
        if (!results->mSuccess)
        {
            std::cerr << results->mErrorMessage << std::endl;
        }
        else
        {
            auto jsonDoc = onf::convert_routes_to_json_format(*results);
            write_json_file(routeFileName, jsonDoc);
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
 */
    
    return 0;
}
