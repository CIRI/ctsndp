//
//  MinimizeCostsModel_CPlex.hpp
//
//  Implements the "Minimize Costs" problem
//  using the CPlex optimizer.
//
//  Created by Brett Feddersen on 03/24/20.
//

#pragma once
#ifndef MinimizeCostsModel_CPlex_hpp
#define MinimizeCostsModel_CPlex_hpp

#include "MinimizeCostsModel.hpp"
#include "CPlexModel.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include <string>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 3 on page 1307 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cMinimizeCostsModel_CPlex final : public cMinimizeCostsModel, 
										private cCPlexModel
{
public:
	cMinimizeCostsModel_CPlex(int pass_number, 
								const cPartiallyTimeExpandedNetwork& expandedNetwork,
								const cServiceNetwork& serviceNetwork, 
								bool verboseMode, bool printTimingInfo);
    ~cMinimizeCostsModel_CPlex();

	void setNumericFocus(int numericFocus) override;
	void setTimeLimit_sec(double timeLimit_sec) override;
	void logToConsole(bool log) override;
	void outputDebugMsgs(bool msgs) override;
	void logFilename(const std::string& filename) override;
	void readTuningParameters(const std::string& filename) override;

    bool Optimize() override;

	bool RelaxConstraints() override;
    void FindConstraintViolations(std::ostream& out) override;
	std::string ComputeIIS() override;

    /*
     * Returns both the costs and delay costs, in that order.
     */
    std::tuple<double, double> getMinimalFixedCost() const override;
	std::tuple<double, double> getMinimalVariableCost() const override;

private:
	void ConstructOptimizatonVariables() override;
	void BuildOptimizatonProblem() override;

    void FindCommoditiesPaths();

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
    void constraint1_ConservationOfFlow(std::ostream* debug) override;
    void constraint2_EnsureSufficientCapacityPerFlow(std::ostream* debug) override;
    void constraint2a_EnsureSufficientCapacityPerArc(std::ostream* debug) override;
    void constraint5_TravelTime(std::ostream* debug) override;
    void constraint5a_RestrictionOfFlow(std::ostream* debug) override;
    void constraint5b_RequiredFlows(std::ostream* debug) override;

	bool isUsed(const ctsndp::arc_ptr_t& arc) const;

    //
    // Debug Methods
    //
    void DumpX_kij(std::ostream& out) const;
    void DumpX_kij(std::ostream& out, const cCommodityBundle* const k) const;
    void DumpY_ij(std::ostream& out) const;

    /**
     * Decision Variables: minimize_fixed_costs
     */
	IloIntVarArray				y_kij;	// the number of times arc (i,j) must be used to transport commodity k
	IloArray<IloBoolVarArray>	x_kij;  // does commodity k use the arc (i,j) in its transport
};

#endif /* MinimizeCostsModel_CPlex_hpp */
