//
//  GurobiModel.hpp
//  An abstract Gurobi Model class
//
//  Created by Brett Feddersen on 11/22/19.
//

#pragma once
#ifndef GurobiModel_hpp
#define GurobiModel_hpp

#include <gurobi_c++.h>
#include <string>

/*
 * An abstract base class to support any GUROBI optimization problem.
 */
class cGurobiModel
{
public:

    void setNumericFocus(int numericFocus);
    void setTimeLimit_sec(double timeLimit_sec);
    void logToConsole(bool log);
	void readTuningParameters(const std::string& filename);

	/*
     * Modifies the Gurobi Model object to create a feasibility relaxation.
     * Note that you need to call optimize on the result to compute the actual relaxed solution.
     * The feasibility relaxation is a model that, when solved, minimizes the amount by which the
     * solution violates the bounds and linear constraints of the original model.
     *
     * \return true if the relaxation could be created
     */
    bool RelaxConstraints();
    
    /*
     * Compute an Irreducible Inconsistent Subsystem (IIS). An IIS is a subset of the constraints and variable bounds with the following properties:
     *      1. the subsystem represented by the IIS is infeasible, and
     *      2. if any of the constraints or bounds of the IIS is removed, the subsystem becomes feasible.
     *
     * \return a string of all the constraints or bounds that could not be satisfied
     */
    std::string ComputeIIS();

protected:
    cGurobiModel(const char* model_name);
    ~cGurobiModel() = default;

    void logFilename(const std::string& filename, const char* model_type);

    //
    // The GUROBI model
	GRBEnv	 mEnvironment;
	GRBModel mModel;

    //
    // Debug Variables
    //
    double mTimeLimit_sec;
};

#endif /* GurobiModel_hpp */
