//
//  ctsndp_utils.hpp
//  ctsndp
//
//  Created by Brett Feddersen on 8/7/19.
//

#pragma once
#ifndef ctsndp_utils_h
#define ctsndp_utils_h

#include "ctsndp_defs.hpp"
#include <string>
#include <chrono>

class cServiceNetwork;
class cCommodityShipments;

namespace ctsndp
{
    /*!
     * \brief Apply any fixups to the shipments due to implicity
     *          created nodes in the service network.
     */
    bool validate_inputs(cServiceNetwork& service_network,
                         cCommodityShipments& shipments,
                         bool debug_mode);

    /*!
     * \brief Apply any fixups to the shipments due to implicity
     *          created nodes in the service network.
     */
    void apply_implicit_node_fixups(
                                    cServiceNetwork& service_network,
                                    cCommodityShipments& shipments,
                                    bool debug_mode);

    /*!
     * \brief Apply any fixups to the shipments due to implicity
     *          created nodes in the service network.
     */
    void apply_shortest_path_fixups(
                        const cServiceNetwork& service_network,
                        const cCommodityShipments& shipments);

	bool validate_paths(const ctsndp::commodity_path_node_t& p_ki,
		const ctsndp::commodity_path_arc_t& p_ka);

    /*!
     * \brief Converts a time_t to a string, removing the new line character
     *          that is added by default.
     */
    //std::string to_string(const std::time_t& t);
	std::string to_string(const time_t& t);
}

#endif /* ctsndp_utils_h */
