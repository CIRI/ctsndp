//
//  MinimizeCostsModel_Gurobi.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 03/24/20.
//

#include "MinimizeCostsModel_Gurobi.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_defs.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include "gurobi_utils_priv.hpp"
#include <iostream>
#include <chrono>
#include <cmath>
#include <cassert>
#include <fstream>

namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;
    
    arc_id_t to_index(cPartiallyTimeExpandedNetworkArc* const& arc, unsigned int offset)
    {
        return arc->getId() + offset;
    }
}


using namespace ctsndp;

std::unique_ptr<cMinimizeCostsModel_Gurobi> cMinimizeCostsModel_Gurobi::create(int pass_number,
	const cPartiallyTimeExpandedNetwork& expandedNetwork,
	const cServiceNetwork& serviceNetwork,
	bool verboseMode, bool printTimingInfo)
{
	try
	{
		std::unique_ptr<cMinimizeCostsModel_Gurobi> model(new cMinimizeCostsModel_Gurobi(pass_number,
			expandedNetwork, serviceNetwork, verboseMode, printTimingInfo));

		return model;
	}
	catch(const GRBException& e)
	{
		throw cOptimizationException(e.getMessage());
	}
}

cMinimizeCostsModel_Gurobi::cMinimizeCostsModel_Gurobi(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         bool verboseMode, bool printTimingInfo)
:
    cGurobiModel("MinimizeCosts"),
	cMinimizeCostsModel(pass_number, expandedNetwork, serviceNetwork, verboseMode, printTimingInfo)
{
}

cMinimizeCostsModel_Gurobi::~cMinimizeCostsModel_Gurobi()
{
    y_kij.clear();
    x_kij.clear();
}

void cMinimizeCostsModel_Gurobi::setNumericFocus(int numericFocus)
{
	cGurobiModel::setNumericFocus(numericFocus);

}

void cMinimizeCostsModel_Gurobi::setTimeLimit_sec(double timeLimit_sec)
{
	cGurobiModel::setTimeLimit_sec(timeLimit_sec);
}

void cMinimizeCostsModel_Gurobi::logToConsole(bool log)
{
	cGurobiModel::logToConsole(log);
}

void cMinimizeCostsModel_Gurobi::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cMinimizeCostsModel_Gurobi::logFilename(const std::string& filename)
{
    cGurobiModel::logFilename(filename, ".min_cost");
}

void cMinimizeCostsModel_Gurobi::readTuningParameters(const std::string& filename)
{
	cGurobiModel::readTuningParameters(filename);
}

std::tuple<double, double> cMinimizeCostsModel_Gurobi::getMinimalFixedCost() const
{
	if (mpProgress)
	{
		*mpProgress << "  getMinimalFixedCost..." << std::flush;
	}

	double fixedCost = 0.0;
    double delayCost = 0.0;
    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    for (auto& arc : arcs)
    {
		if (arc->isDelayArc())
		{
			for (auto k : static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc)->getCommodities())
			{
				if (!k->hasDelayCost())
					continue;

				auto kij = to_kij_index(arc, k);

				try
				{
					delayCost += lower_limit(f_kij[kij], 0.0) * as_double(y_kij[kij]);
				}
				catch (const GRBException& e)
				{
					std::string msg = "getMinimalFixedCost (delay cost): ";
					msg += "[ " + to_string(*arc) + ", " + k->getName() + " ] ";
					throw cOptimizationException(msg + e.getMessage());
				}
			}
		}
		else
		{
			auto ij = to_index(arc);

			try
			{
				fixedCost += lower_limit(f_kij[ij], 0.0) * as_double(y_kij[ij]);
			}
			catch (const GRBException& e)
			{
				std::string msg = "getMinimalFixedCost (fixed cost): ";
				msg += "[ " + to_string(*arc) + " ] ";
				throw cOptimizationException(msg + e.getMessage());
			}
		}
    }

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}
	
	return std::make_tuple(fixedCost,delayCost);
}

std::tuple<double, double> cMinimizeCostsModel_Gurobi::getMinimalVariableCost() const
{
	if (mpProgress)
	{
		*mpProgress << "  getMinimalVariableCost..." << std::flush;
	}

	auto& commodities = mPartiallyTimeExpandedNetwork.commodities();
    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    
	double variableCost = 0.0;
	for (auto& commodity : commodities)
    {
        auto k = to_index(commodity);
        for (auto& arc : arcs)
        {
			if (arc->isDelayArc())
				continue;

			auto ij = to_index(arc);

			try
			{
				variableCost += lower_limit(c_kij[ij], 0.0) * q_k[k] * as_double(x_kij[k][ij]);
			}
			catch (const GRBException& e)
			{
				std::string msg = "getMinimalVariableCost (variable cost): ";
				msg += "[ " + commodity->getName() + ", " + to_string(*arc) + " ] ";
				throw cOptimizationException(msg + e.getMessage());
			}
        }
    }

	double delayCost = 0.0;
	for (auto& arc : arcs)
	{
		if (!arc->isDelayArc())
			continue;

		auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
		for (auto commodity : delay_arc->getCommodities())
		{
			if (!commodity->hasDelayPenalty())
				continue;

			auto ij = to_index(arc);
			auto k = to_index(commodity);
			auto kij = to_kij_index(arc, commodity);

			try
			{
				delayCost += lower_limit(c_kij[kij], 0.0) * q_k[k] * as_double(x_kij[k][ij]);
			}
			catch (const GRBException& e)
			{
				std::string msg = "getMinimalVariableCost (delay cost): ";
				msg += "[ " + commodity->getName() + ", " + to_string(*arc) + " ] ";
				throw cOptimizationException(msg + e.getMessage());
			}
		}
	}

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	return std::make_tuple(variableCost, delayCost);
}

bool cMinimizeCostsModel_Gurobi::Optimize()
{
    auto start = std::chrono::system_clock::now();


    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Minimize Costs Problem\"";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	int status = 0;

	try
	{
		if (mpProgress)
		{
			*mpProgress << "  Optimizing \"Minimize Costs Problem\"..." << std::flush;
		}

		mModel.optimize();
		status = mModel.get(GRB_IntAttr_Status);

		if (mpProgress)
		{
			*mpProgress << "done" << std::endl;
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("Minimize Costs Problem Optimize: " + e.getMessage());
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
        case GRB_OPTIMAL:
		{
			SaveSolution();
			return true;
		}

        case GRB_INF_OR_UNBD:
        case GRB_INFEASIBLE:
        {
			if (mpProgress)
			{
				*mpProgress << "  Minimize Costs model is infeasible!" << std::endl;
			}

			std::string msg = "Minimize Costs model is infeasible!";
            throw ctsndp::cInfeasibleSolution("MinimizeCosts", msg);
            break;
        }
                   
        case GRB_TIME_LIMIT:
        {
			std::string msg = "Exceeded time limit (";
            msg += std::to_string(mTimeLimit_sec);
            msg += "):\n";

			try
			{
				auto count = mModel.get(GRB_IntAttr_SolCount);
				if (count == 0)
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    No solution was found!! Terminating" << std::endl;
					}

					msg += "No solution was found!";
				}
				else
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    Feasible solution count = " << count << std::endl;
					}

					SaveSolution();
					return true;
				}
			}
			catch (const GRBException& e)
			{
				msg += e.getMessage();
			}
                       
            throw ctsndp::cTimeLimit("MinimizeCosts", msg);
            break;
        }
    }

    return false;
}

bool cMinimizeCostsModel_Gurobi::RelaxConstraints()
{
	return cGurobiModel::RelaxConstraints();
}

std::string cMinimizeCostsModel_Gurobi::ComputeIIS()
{
	return cGurobiModel::ComputeIIS();
}

void cMinimizeCostsModel_Gurobi::FindConstraintViolations(std::ostream& out)
{
	if (mpProgress)
	{
		*mpProgress << "  \"Minimize Costs Problem\" FindConstraintViolations..." << std::flush;
	}

	unsigned int numArcs = mPartiallyTimeExpandedNetwork.getNumArcs();
    const auto& nodes = mPartiallyTimeExpandedNetwork.nodes();
    const auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    bool printNone = true;

    //*****  constraint1_ConservationOfFlow()  *****
    out << "\nScanning for constraint 1 \"Conservation of Flow\" violations:\n";

    // for all commodities
    for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto k = commodity->getId();
        const auto eat = commodity->getEarliestAvailableTime();
                
        auto source_node =
                    mPartiallyTimeExpandedNetwork.findSourceNode(commodity);
        auto destination_node =
                    mPartiallyTimeExpandedNetwork.findDestinationNode(commodity);

        for (auto& node : nodes)
        {
            int d = 0;
            if (!node->isDelayNode() && (node->hasCommodity(commodity)))
            {
                if (node == source_node)
                {
                    d = 1;
                }
                if (node == destination_node)
                {
                    d = -1;
                }
            }
            else if ((eat == 0) && node->partial_match(*source_node) && !node->hasCommodity(commodity))
            {
                continue;
            }

            int outflows = 0;
            int inflows = 0;
            
            for (auto& arc : arcs)
            {
                if (arc->isSource(node))
                {
                    outflows += as_int(x_kij[k][to_index(arc)]);
                }
                if (arc->isDestination(node))
                {
                    inflows += as_int(x_kij[k][to_index(arc)]);
                }
            }
            
            /*
             * Delay node can not have holdover arcs as in/out flows!
             */
            if (!node->isDelayNode())
            {
                for (auto& arc : mPartiallyTimeExpandedNetwork.holdover_arcs())
                {
                    if (arc->isSource(node))
                    {
                        outflows += as_int(x_kij[k][to_index(arc, numArcs)]);
                    }
                    if (arc->isDestination(node))
                    {
                        inflows += as_int(x_kij[k][to_index(arc, numArcs)]);
                    }
                }
            }
            
            auto delta = std::abs((outflows - inflows) - d);
            if (delta > CONSTRAINT_TOLERANCE)
            {
                out << "For commodity " << commodity->getFullyQualifiedName() << ", ";
                if (d == 1)
                {
                    out << "source node " << node->getName() << "(t:" << node->getTime() << ") ";
                    out << "outflows " << outflows << " are not one!  The inflows are " << inflows << ".\n";
                }
                else if (d == -1)
                {
                    out << "destination node " << node->getName() << "(t:" << node->getTime() << ") ";
                    out << "inflows " << inflows << " are not negative one!  The outflows are " << outflows << ".\n";
                }
                else
                {
                    out << "the outflows " << outflows << " are not inline with the inflows for " << inflows;
                    out << " for node " << node->getName() << "(t:" << node->getTime() << ")\n";

                }
                printNone = false;
            }
        }
    }

    if (printNone)
        out << "None\n";
    printNone = true;

    //*****  constraint2_EnsureSufficientCapacityPerFlow();  *****
    out << "\nScanning for constraint 2 \"q_k * x_kij <= u_ij * y_ij\" violations:\n";
    
    // for all arcs(i, j) in 𝒜t
    for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
    {
        auto ij = to_index(arc);
        // TERM: Sum over commodity k, q_k * x_kij
        double lhs = 0.0;
        for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
        {
            auto k = to_index(commodity);
            lhs += q_k[k] * as_double(x_kij[k][ij]);
        }
        
        // TERM: u_ij * y_ij
		double rhs = 0.0;
		if (arc->isDelayArc())
		{
			auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
			for (auto k : delay_arc->getCommodities())
			{
				auto kij = to_kij_index(arc, k);
				rhs += u_kij[kij] * as_double(y_kij[kij]);
			}
		}
		else
			rhs = u_kij[ij] * as_double(y_kij[ij]);

        double delta = lhs - rhs;

        if (delta > CONSTRAINT_TOLERANCE)
        {
            out << "For arc " << to_string(*arc) << ", ";
            out << "the arc capacity " << rhs << " is less than the demand " << lhs;
            out << ", delta = " << delta << "\n";
            printNone = false;
        }
    }
    
    if (printNone)
        out << "None\n";
    printNone = true;

    //*****  constraint2a_EnsureSufficientCapacityPerArc();  *****
    out << "\nScanning for constraint 2a \"y_ij <= w_ij\" violations:\n";
    
    // for all arcs(i, j) in 𝒜t
    for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
    {
		if (arc->isDelayArc())
		{
			auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
			for (auto k : delay_arc->getCommodities())
			{
				auto kij = to_kij_index(arc, k);

				if (w_kij[kij] < static_cast<unsigned int>(-1))
					continue;

				double lhs = as_double(y_kij[kij]);
				double delta = lhs - w_kij[kij];

				if (delta > CONSTRAINT_TOLERANCE)
				{
					out << "For arc " << to_string(*arc) << ", ";
					out << "the number of arc instantiations " << lhs << " exceeds the capacity " << w_kij[kij];
					out << ", delta = " << delta << "\n";
					printNone = false;
				}
			}
		}
		else
		{
			auto ij = to_index(arc);

			if (w_kij[ij] < static_cast<unsigned int>(-1))
				continue;

			double lhs = as_double(y_kij[ij]);
			double delta = lhs - w_kij[ij];

			if (delta > CONSTRAINT_TOLERANCE)
			{
				out << "For arc " << to_string(*arc) << ", ";
				out << "the number of arc instantiations " << lhs << " exceeds the capacity " << w_kij[ij];
				out << ", delta = " << delta << "\n";
				printNone = false;
			}
		}
    }
    
    if (printNone)
        out << "None\n";
    printNone = true;

    //*****  constraint5_TravelTime(); *****
    out << "\nScanning for constraint 5 \"tau_ij * x_kij <= l_k - e_k\" violations:\n";

    // for all commodities
    for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto k = to_index(commodity);
        auto t = commodity->getLatestDeliveryTime();
        t = t - commodity->getEarliestAvailableTime();
        
        // for all arcs(i, j) in 𝒜t
        double lhs = 0.0;
        for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
        {
            // TERM: tau_ij * x_kij
            lhs += tau_ij[to_index(arc)] * as_int(x_kij[k][to_index(arc)]);
        }

        double delta = lhs - t;
        if (delta > CONSTRAINT_TOLERANCE)
        {
            out << "For commodity " << commodity->getName() << ", ";
            out << "the travel time, " << lhs << ", exceeds the time allotted, " << t;
            out << ", delta = " << delta << "\n";
            printNone = false;
        }
    }
    
    if (printNone)
        out << "None\n";
    printNone = true;

#if 0
    //*****  constraint5a_RestrictionOfFlow();  *****
    out << "\nScanning for constraint 5a \"Restriction of Flow\" violations:\n";
    
#endif
    
	if (mIgnoreRequireLocations)
		return;

    //*****  constraint5b_RequiredFlows();  *****
    out << "\nScanning for constraint 5b \"x_kij == 1\" violations:\n";
    
    for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
    {
        if (!commodity->getShipment().hasRequiredLocations())
           continue;

        auto k = to_index(commodity);

        auto& locations = commodity->getShipment().getRequiredLocations();

        for (auto& location : locations)
        {
            const auto& link = mServiceNetwork.findLink(location + ctsndp::PROCESSING);
           
            const auto& srcs = mPartiallyTimeExpandedNetwork.findNodes(link->getSource());
            const auto& dsts = mPartiallyTimeExpandedNetwork.findNodes(link->getDestination());
           
            for (auto& src : srcs)
            {
                for (auto& dst : dsts)
                {
                    const auto& arc = mPartiallyTimeExpandedNetwork.findArc(src, dst);
                    if (!arc) continue;
                   
                    auto delta = std::abs(as_int(x_kij[k][to_index(arc)]) - 1);
                    
                    if (delta > CONSTRAINT_TOLERANCE)
                    {
                        out << "For commodity " << commodity->getName() << ", ";
                        out << "is required to flow through node \"" << location << "\" but does not!\n";
                        printNone = false;
                    }
                }
           }
        }
    }
    
    if (printNone)
        out << "None\n";

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}
		}


//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cMinimizeCostsModel_Gurobi::SaveSolution()
{
	mMinimalCost = mModel.get(GRB_DoubleAttr_ObjVal);

	// Round down to nearest penny
	mMinimalCost = std::floor(mMinimalCost*100.0) / 100.0;

	if (mOutputDebugMsgs)
	{
		DumpX_kij(*mpDebugOutput);
		DumpY_ij(*mpDebugOutput);
	}

	if (mLogX_kij)
	{
		std::string filename = mLogPath + "MinimizeCosts_X_kij_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpX_kij(file);
		file.close();
	}

	if (mLogY_ij)
	{
		std::string filename = mLogPath + "MinimizeCosts_Y_ij_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpY_ij(file);
		file.close();
	}


	// Find all pair of commodities sharing an arc
	FindCommoditiesPaths();
}

/**
 * Find the path that commodity, k, follows from its source to its sink
 */
void cMinimizeCostsModel_Gurobi::FindCommoditiesPaths()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tFinding Commodity Paths";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		auto& commodities = mPartiallyTimeExpandedNetwork.commodities();
		const unsigned int numArcs = mPartiallyTimeExpandedNetwork.getNumArcs();
		for (auto& commodity : commodities)
		{
			auto node = mPartiallyTimeExpandedNetwork.findSourceNode(commodity);
			auto destination = mPartiallyTimeExpandedNetwork.findDestinationNode(commodity);
			auto k = to_index(commodity);
			double delta_t = commodity->getLatestDeliveryTime() - commodity->getEarliestAvailableTime();

			mP_ki.insert(std::make_pair(commodity, ctsndp::path_node_t()));
			mP_ka.insert(std::make_pair(commodity, ctsndp::path_arc_t()));

			auto& p_ki = mP_ki[commodity];
			auto& p_ka = mP_ka[commodity];

			double travel_time = 0.0;
			bool path_complete = false;
			while (!path_complete)
			{
				bool arc_found = false;
				auto& arcs = node->getOutflows();
				for (auto& arc : arcs)
				{
					auto arc_used = false;

					// We don't care about the holdover arcs in our path, but we
					// do care about hold arcs as we must account for the hold time
					if (arc->isHoldoverArc())
						arc_used = as_bool(x_kij[k][to_index(arc, numArcs)]);
					else
						arc_used = as_bool(x_kij[k][to_index(arc)]);

					if (arc_used)
					{
						if (!arc->isHoldoverArc())
						{
							p_ka.push_back(arc);
							travel_time += arc->getActualTravelTime();
						}
						node = arc->getDestination();
						if (node == destination)
						{
							if (travel_time > delta_t)
								std::cout << "Invalid path for commodity: " << *commodity << ", ldt-eat =" << delta_t << ", travel time = " << travel_time << "\n";
							path_complete = true;
						}

						arc_found = true;
						break;
					}
				}

				// Make sure we are advancing in our path!
				if (!path_complete && !arc_found)
				{
					if (mOutputDebugMsgs)
					{
						*mpDebugOutput << "PATH NOT FOUND!" << std::endl;
						DumpX_kij(*mpDebugOutput, commodity);
					}
					mP_ki[commodity].clear();
					mP_ka[commodity].clear();
					if (mP_ki.find(commodity) == mP_ki.end())
					{
						mP_ki.erase(commodity);
						mP_ka.erase(commodity);
					}
					break;
				}
			}

			if (path_complete)
			{
				for (auto& arc : p_ka)
				{
					p_ki.push_back(arc->getSource());
				}

				p_ki.push_back(p_ka.back()->getDestination());
			}
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("FindCommoditiesPaths (x_kij): " + e.getMessage());
	}

    // We need to save off the y_ij values as they will dissappear with deleting
    // this object.  The x_kij values are already saved in the path info.
	try
	{
		for (auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
		{
			auto& path = it->second;
			if (path.empty())
			{
				continue;
			}

			for (auto& arc : path)
			{
				// We don't care about the utilization of storage (holdover/hold) arcs
				// in our path
				if (arc->isStorageArc())
					continue;

				int u_ij = 0;
				auto ij = to_index(arc);

				if (arc->isDelayArc())
				{
					auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
					for (auto k : delay_arc->getCommodities())
					{
						auto kij = to_kij_index(arc, k);
						u_ij += as_int(y_kij[kij]);
					}
				}
				else
				{
					u_ij = as_int(y_kij[ij]);
				}

				if (u_ij >= GRB_MAXINT && arc->isDelayArc())
				{
					u_ij = 1;
				}

				mUtilization_ij[ij] = u_ij;
			}
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("FindCommoditiesPaths (y_kij): " + e.getMessage());
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cMinimizeCostsModel_Gurobi::ConstructOptimizatonVariables()
{
	auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
	auto& holdover_arcs = mPartiallyTimeExpandedNetwork.holdover_arcs();

	auto numArcs = arcs.size();
	auto totalNumArcs = numArcs + holdover_arcs.size();

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variable y_ij ";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		y_kij.allocate(mModel, mSizeOfKij);
	
		// The lower bound of zero enforces constraint 4 for the algorithm list on page 1307
		y_kij.set_lower_bound(0);
		y_kij.set_upper_bound(GRB_MAXINT);

#if ADD_DEBUG_NAMES
		for (auto& arc : arcs)
		{
			auto ij = to_index(arc);

			std::string varName = "y_ij:" + to_string(*arc);
			std::replace(varName.begin(), varName.end(), ' ', '_');
			y_ij.set_name(ij, varName);
		}
#endif

		if (mVerboseMode || mPrintTimingInfo)
		{
			*mpDebugOutput << "done";

			if (mPrintTimingInfo)
			{
				print_end_elapse_times(*mpDebugOutput, start);
			}

			*mpDebugOutput << std::endl;

			start = std::chrono::system_clock::now();

			*mpDebugOutput << "\tCreating optimization variable x_kij ";

			if (mPrintTimingInfo)
			{
				auto start_time = std::chrono::system_clock::to_time_t(start);

				*mpDebugOutput << " started at " << to_string(start_time);
			}

			*mpDebugOutput << "... " << std::flush;
		}

		auto& commodities = mPartiallyTimeExpandedNetwork.commodities();

		x_kij.allocate(mModel, commodities.size(), totalNumArcs);

#if ADD_DEBUG_NAMES
		for (auto commodity : commodities)
		{
			auto k = to_index(commodity);

			// Using the BINARY type enforces constraint 3 for the algorithm list on page 1307
			for (auto& arc : arcs)
			{
				std::string varName = "x_kij:" + commodity->getShipmentName() + "," + to_string(*arc);
				std::replace(varName.begin(), varName.end(), ' ', '_');
				x_kij.set_name(k, to_index(arc), varName.c_str());
			}

			for (auto& arc : holdover_arcs)
			{
				std::string varName = "holdover_x_kij:" + commodity->getShipmentName() + "," + to_string(*arc);
				std::replace(varName.begin(), varName.end(), ' ', '_');
				x_kij.set_name(k, to_index(arc, numArcs), varName.c_str());
			}
		}
#endif

		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("ConstructOptimizatonVariables: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

void cMinimizeCostsModel_Gurobi::BuildOptimizatonProblem()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
	auto& commodities = mPartiallyTimeExpandedNetwork.commodities();

	try
	{
		GRBLinExpr fixedCost;
		for (auto& arc : arcs)
		{
			if (arc->isDelayArc())
			{
				auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
				for (auto k : delay_arc->getCommodities())
				{
					auto kij = to_kij_index(arc, k);
					fixedCost += f_kij[kij] * y_kij[kij];
				}
			}
			else
				fixedCost += f_kij[to_index(arc)] * y_kij[to_index(arc)];
		}

		GRBLinExpr variableCost;
		for (auto& commodity : commodities)
		{
			auto k = to_index(commodity);
			for (auto& arc : arcs)
			{
				if (arc->isDelayArc())
					continue;

				variableCost += c_kij[to_index(arc)] * q_k[k] * x_kij[k][to_index(arc)];
			}
		}

		for (auto& arc : arcs)
		{
			if (!arc->isDelayArc())
				continue;

			auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
			for (auto commodity : delay_arc->getCommodities())
			{
				auto k = to_index(commodity);
				auto kij = to_kij_index(arc, commodity);

				variableCost += c_kij[kij] * q_k[k] * x_kij[k][to_index(arc)];
			}
		}

		mModel.setObjective(fixedCost + variableCost, GRB_MINIMIZE);
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("BuildOptimizatonProblem: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================


//
// Perform constraint 1 for the algorithm list on page 1307
//
void cMinimizeCostsModel_Gurobi::constraint1_ConservationOfFlow(std::ostream* debug)
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint1: Conservation Of Flow";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	if (debug)
	{
		*debug << "Constraint 1: Conservation Of Flow\n\n";
	}

	try
	{
		// for all commodities
		auto& commodities = mPartiallyTimeExpandedNetwork.commodities();
		unsigned int numArcs = mPartiallyTimeExpandedNetwork.getNumArcs();
		const auto& nodes = mPartiallyTimeExpandedNetwork.nodes();

		for (auto& commodity : commodities)
		{
			if (debug)
			{
				*debug << commodity->getName() << "\n";
			}

			auto k = commodity->getId();
			const auto eat = commodity->getEarliestAvailableTime();

			auto source_node = mPartiallyTimeExpandedNetwork.findSourceNode(commodity);
			auto destination_node = mPartiallyTimeExpandedNetwork.findDestinationNode(commodity);

			for (auto& node : nodes)
			{
				if (RestrictCommodityFromNode(commodity, node))
				{
					continue;
				}

				int d = 0;
				if (!node->isDelayNode() && (node->hasCommodity(commodity)))
				{
					if (node == source_node)
					{
						d = 1;
					}
					if (node == destination_node)
					{
						d = -1;
					}
				}
				else if ((eat == 0) && node->partial_match(*source_node) && !node->hasCommodity(commodity))
				{
					assert(false);
					continue;
				}

				if (debug)
				{
					*debug << "  " << node->getName() << ", t=" << std::to_string(node->getTime());
					if (d == 1)
						*debug << " (source)";
					if (d == -1)
						*debug << " (sink)";
					*debug << "\n";
				}


				GRBLinExpr outflowVars;
				GRBLinExpr inflowVars;

				if (debug)
				{
					*debug << "    Inflows:\n";
				}

				for (auto& arc : node->getInflows())
				{
					if (RestrictCommodityInflowToNode(commodity, arc))
						continue;

					if (arc->isHoldoverArc())
						inflowVars += x_kij[k][to_index(static_cast<cPartiallyTimeExpandedNetworkHoldoverArc*>(arc), numArcs)];
					else
						inflowVars += x_kij[k][to_index(arc)];

					if (debug)
					{
						*debug << "      " << *arc << "\n";
					}
				}

				if (debug)
				{
					*debug << "    Outflows:\n";
				}

				for (auto& arc : node->getOutflows())
				{
					if (RestrictCommodityOutflowFromNode(commodity, arc))
						continue;

					if (arc->isHoldoverArc())
						outflowVars += x_kij[k][to_index(static_cast<cPartiallyTimeExpandedNetworkHoldoverArc*>(arc), numArcs)];
					else
						outflowVars += x_kij[k][to_index(arc)];

					if (debug)
					{
						*debug << "      " << *arc << "\n";
					}
				}

				if (d == 0)
				{
					if (outflowVars.size() == 0)
					{
						if (mOutputDebugMsgs)
						{
							*mpDebugOutput << node->getName() << ", t=" << std::to_string(node->getTime());
							*mpDebugOutput << " has no outflows!\n";
						}
						if (debug)
						{
							*debug << "  " << node->getName() << ", t=" << std::to_string(node->getTime());
							*debug << " has no outflows!\n";
						}
					}

					if (inflowVars.size() == 0)
					{
						if (mOutputDebugMsgs)
						{
							*mpDebugOutput << node->getName() << ", t=" << std::to_string(node->getTime());
							*mpDebugOutput << " has no inflows!\n";
						}
						
						if (debug)
						{
							*debug << "  " << node->getName() << ", t=" << std::to_string(node->getTime());
							*debug << " has no inflows!\n";
						}
					}
				}

#if ADD_DEBUG_NAMES
				std::string constrName = "C1_ConservationOfFlow(" + commodity->getFullyQualifiedName();
				if (d == 1)
				{
					constrName += " from ";
				}
				else if (d == -1)
					constrName += " to ";
				else
				{
					constrName += " to/from ";
				}
				constrName += node->getName();
				constrName += ", t=" + std::to_string(node->getTime()) + ")";

				mModel.addConstr(outflowVars - inflowVars, GRB_EQUAL, d, constrName.c_str());
#else
				try {
					mModel.addConstr(outflowVars - inflowVars, GRB_EQUAL, d);
				}
				catch (const GRBException& e) {
					std::cerr << e.getMessage() << std::endl;
				}
#endif
			}
		}

		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint1_ConservationOfFlow: " + e.getMessage());
	}
    
	if (debug)
	{
		*debug << "\n\n";
	}

	if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 2 for the algorithm listed on page 1307
//
void cMinimizeCostsModel_Gurobi::constraint2_EnsureSufficientCapacityPerFlow(std::ostream* debug)
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint2: Ensure Sufficient Capacity Per Flow";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	if (debug)
	{
		*debug << "Constraint 2: Ensure Sufficient Capacity Per Flow\n\n";
	}

	try
	{
		// for all arcs(i, j) in 𝒜t
		for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
		{
			auto ij = to_index(arc);
			// TERM: Sum over commodity k, q_k * x_kij
			auto lhs = GRBLinExpr();
			for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
			{
				auto k = to_index(commodity);
				lhs += q_k[k] * x_kij[k][ij];
			}
  
			// TERM: u_ij * y_ij
			auto rhs = GRBLinExpr();
			if (arc->isDelayArc())
			{
				auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
				for (auto k : delay_arc->getCommodities())
				{
					auto kij = to_kij_index(arc, k);
					rhs += u_kij[kij] * y_kij[kij];
				}
			}
			else
				rhs += u_kij[ij] * y_kij[ij];

#if ADD_DEBUG_NAMES
			std::string constrName = "C2_EnsureSufficientCapacityPerFlow(" + to_string(*arc) + ")";
			mModel.addConstr(lhs, GRB_LESS_EQUAL, rhs, constrName.c_str());
#else
			mModel.addConstr(lhs, GRB_LESS_EQUAL, rhs);
#endif
		}
            
		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint2_EnsureSufficientCapacityPerFlow: " + e.getMessage());
	}

	if (debug)
	{
		*debug << "\n\n";
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

void cMinimizeCostsModel_Gurobi::constraint2a_EnsureSufficientCapacityPerArc(std::ostream* debug)
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint2a: Ensure Sufficient Capacity Per Arc";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	if (debug)
	{
		*debug << "Constraint 2a: Ensure Sufficient Capacity Per Arc\n\n";
	}

	try
	{
		// for all arcs(i, j) in 𝒜t
		for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
		{
			// Delay and Storage (hold) arcs don't have capacity constraints
			// by definition
			if (arc->isDelayArc() || arc->isStorageArc())
				continue;

			auto ij = to_index(arc);

			if (w_kij[ij] >= GRB_MAXINT)
				continue;

			y_kij[ij].set(GRB_DoubleAttr_UB, w_kij[ij]);
		}
    
		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint2a_EnsureSufficientCapacityPerArc: " + e.getMessage());
	}

	if (debug)
	{
		*debug << "\n\n";
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 5, page 1311, for the algorithm listed on page 1307
//
void cMinimizeCostsModel_Gurobi::constraint5_TravelTime(std::ostream* debug)
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint5: Travel Time";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	if (debug)
	{
		*debug << "Constraint 5: Travel Time\n\n";
	}

	try
	{
		// for all commodities
		for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
		{
			if (debug)
			{
				*debug << commodity->getFullyQualifiedName() << ":\n";
			}
			auto k = to_index(commodity);
			auto t = commodity->getLatestDeliveryTime();
			t = t - commodity->getEarliestAvailableTime();

			// for all arcs(i, j) in 𝒜t
			auto lhs = GRBLinExpr();
			for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
			{
				if (debug)
				{
					*debug << "  " << *arc << ", tau_ij = " << tau_ij[to_index(arc)] << "\n";
				}

				// TERM: tau_ij * x_kij
				lhs += tau_ij[to_index(arc)] * x_kij[k][to_index(arc)];
			}

			if (debug)
			{
				*debug << "  <= " << t << "\n";
			}

#if ADD_DEBUG_NAMES
			std::string constrName = "C5_TravelTime(" + commodity->getFullyQualifiedName() + ")";
			mModel.addConstr(lhs, GRB_LESS_EQUAL, t, constrName.c_str());
#else
			mModel.addConstr(lhs, GRB_LESS_EQUAL, t);
#endif
			if (debug)
			{
				*debug << "\n";
			}
		}

		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint5_TravelTime: " + e.getMessage());
	}

	if (debug)
	{
		*debug << "\n\n";
	}
	
	if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Restricts which commodities can flow on which arcs
//
void cMinimizeCostsModel_Gurobi::constraint5a_RestrictionOfFlow(std::ostream* debug)
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint5a: Restriction Of Flow";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	if (debug)
	{
		*debug << "Constraint 5a: Restriction Of Flow\n\n";
	}

    unsigned int numArcs = mPartiallyTimeExpandedNetwork.getNumArcs();
	auto& commodities = mPartiallyTimeExpandedNetwork.commodities();

	try
	{
		//for all commodities
		for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
		{
			auto k = to_index(commodity);

			const auto& src = mPartiallyTimeExpandedNetwork.findSourceNode(commodity);
			const auto& dst = mPartiallyTimeExpandedNetwork.findDestinationNode(commodity);

			// for all arcs(i, j) in 𝒜t
			for (auto& arc : mPartiallyTimeExpandedNetwork.arcs())
			{
				if (RestrictCommodityFromArc(commodity, arc))
				{
					auto ij = to_index(arc);

#if ADD_DEBUG_NAMES
					std::string constrName = "C5a_RestrictionOfFlow(" + commodity->getFullyQualifiedName() + ")";
					mModel.addConstr(
								x_kij[k][to_index(arc)], GRB_EQUAL,
								0, constrName.c_str());
#else
					x_kij.set_upper_bound(k, ij, 0);
#endif
				}
			}
       
			// for all arcs(i, j) in ℋt
			for (auto& arc : mPartiallyTimeExpandedNetwork.holdover_arcs())
			{
				if (RestrictCommodityFromArc(commodity, arc))
				{
					auto ij = to_index(arc, numArcs);

#if ADD_DEBUG_NAMES
					std::string constrName = "C5a_RestrictionOfFlow(" + commodity->getFullyQualifiedName() + ")";
					mModel.addConstr(x_kij[k][ij], GRB_EQUAL, 0, constrName.c_str());
#else
					x_kij.set_upper_bound(k, ij, 0);
#endif
				}
			}
		}

		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint5a_RestrictionOfFlow: " + e.getMessage());
	}

	if (debug)
	{
		*debug << "\n\n";
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Forces commodities to flow on their required arcs
//
void cMinimizeCostsModel_Gurobi::constraint5b_RequiredFlows(std::ostream* debug)
{
	if (mIgnoreRequireLocations)
	{
		if (mVerboseMode || mPrintTimingInfo)
		{
			*mpDebugOutput << "\t\t*** Required locations have been turned off! ***";
			*mpDebugOutput << std::endl;
		}

		return;
	}

	auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint5b: Required Flows";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	if (debug)
	{
		*debug << "Constraint 5b: Required Flow\n\n";
	}

	try
	{
		//
		// Some commodities are required to flow through certain nodes.  A "processing" arc
		// has been created in these cases.  Here, we force the commodity to flow through this
		// "processing" arc
		//
		for (auto& commodity : mPartiallyTimeExpandedNetwork.commodities())
		{
			if (!commodity->getShipment().hasRequiredLocations())
				continue;
 
			auto k = to_index(commodity);

			auto& locations = commodity->getShipment().getRequiredLocations();
        
			for (auto& location : locations)
			{
				const auto& link = mServiceNetwork.findLink(location + ctsndp::PROCESSING);
            
				const auto& srcs = mPartiallyTimeExpandedNetwork.findNodes(link->getSource());
				const auto& dsts = mPartiallyTimeExpandedNetwork.findNodes(link->getDestination());
            
				for (auto& src : srcs)
				{
					for (auto& dst : dsts)
					{
						const auto& arc = mPartiallyTimeExpandedNetwork.findArc(src, dst);
						if (!arc)
							continue;
                    
#if ADD_DEBUG_NAMES
						std::string constrName = "C5b_RequiredFlows(" + commodity->getFullyQualifiedName();
						constrName += ", " + to_string(*arc) + ")";
						mModel.addConstr(x_kij[k][to_index(arc)], GRB_EQUAL, 1, constrName.c_str());
#else
						x_kij.set_lower_bound(k, to_index(arc), 1);
#endif
					}
				}
			}
		}

		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint5b_RequiredFlows: " + e.getMessage());
	}

	if (debug)
	{
		*debug << "\n\n";
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

bool cMinimizeCostsModel_Gurobi::isUsed(const arc_ptr_t& arc) const
{
	// for all commodities
	for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
	{
		try
		{
			if (as_bool(x_kij[to_index(k)][to_index(arc)]))
				return true;
		}
		catch (const GRBException& e)
		{
			throw cOptimizationException("isUsed: " + e.getMessage());
		}
	}

	return false;
}


//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

void cMinimizeCostsModel_Gurobi::DumpX_kij(std::ostream& out) const
{
    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        DumpX_kij(out, k);
    }
}

void cMinimizeCostsModel_Gurobi::DumpX_kij(std::ostream& out,
                        const cCommodityBundle* const k) const
{
    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    unsigned int numArcs = mPartiallyTimeExpandedNetwork.getNumArcs();
    auto& holdover_arcs = mPartiallyTimeExpandedNetwork.holdover_arcs();
    out << "\n";
    out << k->getFullyQualifiedName() << " " << k->getSourceName();
    out << " (eat: " << k->getEarliestAvailableTime() << ")";
    out << " -> " << k->getDestinationName();
    out << " (ldt: " << k->getLatestDeliveryTime() << ")\n";
    out << "Uses Arcs:\n";
    for (auto& arc : arcs)
    {
		try
		{
			auto arc_used = as_bool(x_kij[to_index(k)][to_index(arc)]);

			if (arc_used)
			{
				out << *arc;
				if (arc->getServiceLink())
					out << " link: " << arc->getServiceLink()->getName();
				out << "\n";
			}
		}
		catch (const GRBException& e)
		{
			out << "GRBException: (" << e.getErrorCode() << ") " << e.getMessage() << std::endl;
		}
    }
    out << "Holdover Arcs:\n";
    for (auto& arc : holdover_arcs)
    {
		try
		{
			auto arc_used = as_bool(x_kij[to_index(k)][to_index(arc, numArcs)]);
			if (arc_used)
				out << *arc << "\n";
		}
		catch (const GRBException& e)
		{
			out << "GRBException: (" << e.getErrorCode() << ") " << e.getMessage() << std::endl;
		}
	}
    out << std::endl;
}

void cMinimizeCostsModel_Gurobi::DumpY_ij(std::ostream& out) const
{
    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    out << "\n";
    out << "Arc Utilization:\n";
    for (auto& arc : arcs)
    {
		try
		{
			int u = 0;
			if (arc->isDelayArc())
			{
				auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
				for (auto k : delay_arc->getCommodities())
					u += as_int(y_kij[to_kij_index(arc, k)]);
			}
			else
				u = as_int(y_kij[to_index(arc)]);

			if (u > 0)
			{
				out << *arc << " utilization: " << u;
				if (isUsed(arc))
				{
					if (arc->getServiceLink())
						out << " link: " << arc->getServiceLink()->getName();
				}
				else
				{
					out << " not used.";
				}
				out << "\n";
			}
		}
		catch (const GRBException& e)
		{
			out << "GRBException: (" << e.getErrorCode() << ") " << e.getMessage() << std::endl;
		}
    }

	out << std::endl;
}
