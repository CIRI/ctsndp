//
//  CPlexModel.cpp
//
//  Created by Brett Feddersen on 03/24/20.
//

#include "CPlexModel.hpp"
#include <iostream>

namespace
{
    std::string log_filename(std::string filename, const char* type)
    {
        if (filename.empty())
            return filename;
        auto n = filename.rfind('.');
        if (n == std::string::npos)
        {
            filename.append(type);
        }
        else
        {
            filename.insert(n, type);
        }
        return filename;
    }
}

cCPlexModel::cCPlexModel(const char* model_name)
:
	mEnv(),
    mModel(mEnv),
	mSolver(mEnv),
    mTimeLimit_sec(0)
{
	mModel.setName(model_name);
	mSolver.setOut(mEnv.getNullStream());
	mSolver.setParam(IloCplex::Param::Tune::Display, 0);
}

cCPlexModel::~cCPlexModel()
{
	mEnv.end();
}


void cCPlexModel::setNumericFocus(int numericFocus)
{
//    mModel.set(GRB_IntParam_NumericFocus, numericFocus);
}

void cCPlexModel::setTimeLimit_sec(double timeLimit_sec)
{
    if (timeLimit_sec > 0.0)
    {
        mTimeLimit_sec = timeLimit_sec;
		mSolver.setParam(IloCplex::Param::TimeLimit, timeLimit_sec);
    }
}

void cCPlexModel::logToConsole(bool log)
{
    if (log)
    {
		mSolver.setParam(IloCplex::Param::Tune::Display, 1);
    }
	else
	{
		mSolver.setParam(IloCplex::Param::Tune::Display, 0);
	}
}

void cCPlexModel::logFilename(const std::string& filename, const char* model_type)
{
    if (!filename.empty())
    {
//        mModel.set(GRB_StringParam_LogFile, log_filename(filename, model_type));
    }
}

void cCPlexModel::readTuningParameters(const std::string& filename)
{
	//TO DO
}

bool cCPlexModel::RelaxConstraints()
{
//    return mModel.feasRelax(GRB_FEASRELAX_LINEAR, true, false, true) >= 0;
	return false;
}

std::string cCPlexModel::ComputeIIS()
{
    std::string msg = "The following constraint(s) cannot be satisfied:\n";
/*
    auto* constraints = mModel.getConstrs();
    for (int i = 0; i < mModel.get(GRB_IntAttr_NumConstrs); ++i)
    {
        if (constraints[i].get(GRB_IntAttr_IISConstr) == 1)
        {
            msg += "\t" + constraints[i].get(GRB_StringAttr_ConstrName) + "\n";
        }
    }

    delete[] constraints;
*/
    
    return msg;
}

