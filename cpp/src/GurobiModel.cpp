//
//  GurobiModel.cpp
//
//  Created by Brett Feddersen on 11/21/19.
//

#include "GurobiModel.hpp"
#include "ctsndp_exceptions.hpp"
#include <iostream>

namespace
{
    std::string log_filename(std::string filename, const char* type)
    {
        if (filename.empty())
            return filename;
        auto n = filename.rfind('.');
        if (n == std::string::npos)
        {
            filename.append(type);
        }
        else
        {
            filename.insert(n, type);
        }
        return filename;
    }

	bool has_extension(const std::string& filename)
	{
		std::size_t pos = filename.rfind(".");
		if (pos == std::string::npos)
			return false;

		// if pos is zero, then we were given either a hidden
		// file or a . for the current diretory
		if (pos == 0)
			return false;
		// if pos is one, then we to check for .. for the
		// previous diretory
		if (pos == 1 && filename[0] == '.')
			return false;

		return true;
	}

	bool has_prm_extension(const std::string& filename)
	{
		static const std::string prm(".prm");

		if (filename.length() > prm.length())
		{
			return (0 == filename.compare(filename.length() - prm.length(), prm.length(), prm));
		}

		return false;
	}
}

cGurobiModel::cGurobiModel(const char* model_name)
:
	mEnvironment(),
    mModel(mEnvironment),
    mTimeLimit_sec(0)
{
    mModel.set(GRB_StringAttr_ModelName, model_name);
    mModel.set(GRB_IntParam_OutputFlag, 0);
}

void cGurobiModel::setNumericFocus(int numericFocus)
{
    mModel.set(GRB_IntParam_NumericFocus, numericFocus);
}

void cGurobiModel::setTimeLimit_sec(double timeLimit_sec)
{
    if (timeLimit_sec > 0.0)
    {
        mTimeLimit_sec = timeLimit_sec;
        mModel.set(GRB_DoubleParam_TimeLimit, timeLimit_sec);
    }
}

void cGurobiModel::logToConsole(bool log)
{
    if (log)
    {
        mModel.set(GRB_IntParam_OutputFlag, 1);
        mModel.set(GRB_IntParam_LogToConsole, log ? 1 : 0);
    }
    else
        mModel.set(GRB_IntParam_OutputFlag, 0);
}

void cGurobiModel::logFilename(const std::string& filename, const char* model_type)
{
    if (!filename.empty())
    {
        mModel.set(GRB_IntParam_OutputFlag, 1);
        mModel.set(GRB_StringParam_LogFile, log_filename(filename, model_type));
    }
}

void cGurobiModel::readTuningParameters(const std::string& filename)
{
	if (filename.empty())
		return;

	if (!has_extension(filename))
	{
		mModel.read(filename + ".prm");
		return;
	}

	if (!has_prm_extension(filename))
	{
		std::string msg = "Invalid parameter file: ";
		msg += filename;
		msg += "\nFile extension must be \"prm\"";
		throw std::logic_error(msg);
	}

	mModel.read(filename);
}


bool cGurobiModel::RelaxConstraints()
{
    return mModel.feasRelax(GRB_FEASRELAX_LINEAR, true, false, true) >= 0;
}

std::string cGurobiModel::ComputeIIS()
{
    std::string msg = "The following constraint(s) cannot be satisfied:\n";
	try
	{
		auto* constraints = mModel.getConstrs();
		auto n = mModel.get(GRB_IntAttr_NumConstrs);
		for (int i = 0; i < n; ++i)
		{
			if (constraints[i].get(GRB_IntAttr_IISConstr) == 1)
			{
				msg += "\t" + constraints[i].get(GRB_StringAttr_ConstrName) + "\n";
			}
		}

		delete[] constraints;
	}
	catch (GRBException& e)
	{
		throw ctsndp::cOptimizationException(e.getMessage());
	}

    
    return msg;
}

