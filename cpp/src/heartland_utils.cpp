
#include "heartland_utils.hpp"
#include "heartland.schemas"
#include "ServiceNetwork/ServiceLocation.hpp"
#include "ServiceNetwork/ServiceLink.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "json_utils.hpp"
#include "ctsndp_vars.hpp"
#include <nlohmann/json-schema.hpp>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <any>
#include <iostream>
#include <limits>
#include <algorithm>
#ifdef WIN32
	#include <direct.h>
#else
	#include <unistd.h>
#endif


namespace
{
    std::string activeFileName;

    /*
     * Required HEARTLAND JSON entities
     */
    const static char* NODE_ID = "id";
    const static char* NODE_CAPACITY = "capacity";
    const static char* NODE_COST = "cost";
    const static char* NODE_HOLDING_TIME = "holding_time";
    const static char* NODE_HOLDING_COST = "holding_cost";
    const static char* NODE_SERVICE_TIME = "service_time";
    const static char* NODE_STORAGE = "storage";
    const static char* NODE_CYCLE_TIME = "cycle_time";

    const static char* LINK_NAME = "name";
    const static char* LINK_SOURCE = "source";
    const static char* LINK_TARGET = "target";
    const static char* LINK_CAPACITY = "capacity";
    const static char* LINK_FIXED_COST = "cost";
    const static char* LINK_TRAVEL_TIME = "travel_time";
    const static char* LINK_CYCLE_TIME = "cycle_time";
    const static char* LINK_TRANSPORTATION_METHODS = "transportation_methods";

    const static char* SHPT_NAME = "name";
    const static char* SHPT_SOURCE = "source";
    const static char* SHPT_DEST = "destination";
    const static char* SHPT_EAT = "EAT";
    const static char* SHPT_LDT = "LDT";
    const static char* SHPT_SHIPPER = "shipper";
    const static char* SHPT_UUID = "group_uuid";

    const static char* CB_NAME = "name";
    const static char* CB_SOURCE = "source";
    const static char* CB_DEST = "destination";
    const static char* CB_EAT = "EAT";
    const static char* CB_LDT = "LDT";
    const static char* CB_DELAY_COST = "delay_cost";
    const static char* CB_DELAY_PENALTY_RATE = "delay_penalty_rate";
    const static char* CB_DELAY_PENALTY_LIMIT = "delay_penalty_limit";
    const static char* CB_CARGO_CATEGORIES = "cargo_categories";

    bool is_required_node_entry(const std::string& key)
    {
        if ((key == NODE_ID) || (key == NODE_CAPACITY) || (key == NODE_COST)
            || (key == NODE_HOLDING_TIME) || (key == NODE_SERVICE_TIME)
            || (key == NODE_STORAGE) || (key == NODE_CYCLE_TIME))
            return true;

        return false;
    }

    bool is_required_link_entry(const std::string& key)
    {
        if ((key == LINK_NAME) || (key == LINK_SOURCE)
            || (key == LINK_TARGET) || (key == LINK_CAPACITY)
            || (key == LINK_FIXED_COST) 
			|| (key == LINK_TRAVEL_TIME) || (key == LINK_CYCLE_TIME)
            || (key == LINK_TRANSPORTATION_METHODS))
        {
            return true;
        }

        return false;
    }

    bool is_required_shipment_entry(const std::string& key)
    {
        if ((key == SHPT_NAME) || (key == SHPT_SOURCE)
            || (key == SHPT_DEST) || (key == SHPT_EAT)
            || (key == SHPT_LDT))
        {
            return true;
        }

        return false;
    }

    bool is_required_commodity_entry(const std::string& key)
    {
        if ((key == CB_NAME) 
			|| (key == CB_SOURCE) || (key == CB_DEST)
			|| (key == CB_EAT) || (key == CB_LDT)
			|| (key == CB_DELAY_COST) 
			|| (key == CB_DELAY_PENALTY_RATE) || (key == CB_DELAY_PENALTY_LIMIT)
            || (key == CB_CARGO_CATEGORIES) || (key == SHPT_UUID))
        {
            return true;
        }

        return false;
    }

    template<typename T>
    std::vector<T> to_array(const nlohmann::json& json_value)
    {
        std::vector<T> array;
        for (auto& item : json_value.items())
        {
            array.push_back(item.value());
        }
        return array;
    }

    void add_properties(cServiceLocation* pLocation, nlohmann::json& node)
    {
        for (auto property : node.items() )
        {
            std::string key = property.key();
            
            // Skip required entries...
            if (is_required_node_entry(key))
                continue;
            
            std::any value = to_any(property.value());
            
            if (value.has_value())
                pLocation->addProperty(key, value);
        }
    }

    void add_properties(cServiceLink* pLink, nlohmann::json& node)
    {
        for (auto property : node.items() )
        {
            std::string key = property.key();
            
            // Skip required entries...
            if (is_required_link_entry(key))
                continue;
            
            std::any value = to_any(property.value());
            
            if (value.has_value())
                pLink->addProperty(key, value);
        }
    }

    void add_properties(cCommodityShipment* pShipment, nlohmann::json& node)
    {
        for (auto property : node.items() )
        {
            std::string key = property.key();
            
            // Skip required entries...
            if (is_required_shipment_entry(key))
                continue;
            
            std::any value = to_any(property.value());
            
            if (value.has_value())
                pShipment->addProperty(key, value);
        }
    }

    void add_properties(cCommodityBundle* pCommodity, nlohmann::json& node)
    {
        for (auto property : node.items() )
        {
            std::string key = property.key();
            
            // Skip required entries...
            if (is_required_commodity_entry(key))
                continue;
            
            std::any value = to_any(property.value());
            
            if (value.has_value())
                pCommodity->addProperty(key, value);
        }
    }

    void add_commodity_to_shipment_from_json_doc(cCommodityShipment* pShipment, 
		nlohmann::json& node, bool no_delays, bool group)
    {
		std::string name = get_required<std::string>(node, CB_NAME, activeFileName);
		int quantity = 1;
		quantity = std::max(get_optional<int>(node, "nTEU", 1, activeFileName), 1);
		auto categories = node[CB_CARGO_CATEGORIES];

		if (group)
		{
			std::unique_ptr<cCommodityBundle> pBundle(new cCommodityBundle(pShipment, name));

			pBundle->setQuantity(quantity);

			for (auto category : categories)
				pBundle->addCategory(category.get<std::string>());

			if (!no_delays)
			{
				double delay_cost = get_optional<double>(node, CB_DELAY_COST, -1.0, activeFileName);
				pBundle->setDelayCost(delay_cost);

				double penalty_rate = get_optional<double>(node, CB_DELAY_PENALTY_RATE, -1.0, activeFileName);
				double penalty_limit = get_optional<double>(node, CB_DELAY_PENALTY_LIMIT, std::numeric_limits<double>::max(), activeFileName);
				pBundle->setDelayPenalties(penalty_rate, penalty_limit);
			}

			add_properties(pBundle.get(), node);

			pShipment->addCommodityBundle(std::move(pBundle));
		}
		else
		{
			for (int i = 0; i < quantity; ++i)
			{
				std::unique_ptr<cCommodityBundle> pBundle(new cCommodityBundle(pShipment, name + "-" + std::to_string(i)));
				pBundle->setQuantity(1);

				for (auto category : categories)
					pBundle->addCategory(category.get<std::string>());

				if (!no_delays)
				{
					double delay_cost = get_optional<double>(node, CB_DELAY_COST, -1.0, activeFileName);
					pBundle->setDelayCost(delay_cost);

					double penalty_rate = get_optional<double>(node, CB_DELAY_PENALTY_RATE, -1.0, activeFileName);
					double penalty_limit = get_optional<double>(node, CB_DELAY_PENALTY_LIMIT, std::numeric_limits<double>::max(), activeFileName);
					pBundle->setDelayPenalties(penalty_rate, penalty_limit);
				}

				add_properties(pBundle.get(), node);
				pShipment->addCommodityBundle(std::move(pBundle));
			}
		}
    }

    void print_cwd()
    {
#ifdef WIN32
	std::cerr << "The current working directory is: " << _getcwd(nullptr, 0) << std::endl;
#else
	std::cerr << "The current working directory is: " << getcwd(nullptr, 0) << std::endl;
#endif
    }
} // End of Namespace

bool heartland::is_valid_service_network_json(nlohmann::json& jsonDoc, const std::string& fileName)
{
    nlohmann::json_schema::json_validator validator;
    
    try 
    {
        validator.set_root_schema(heartland_schemas::service_network_v1);
    }
    catch (const std::exception &e) 
    {
        std::cerr << "Invalid schema: heartland_schemas::service_network_v1\n";
        std::cerr << "Here is why: " << e.what() << "\n";
	return true;
        //throw;
    }
    
    try {
        validator.validate(jsonDoc);
    } 
    catch (const std::exception& e) 
    {
		std::cerr << "Invalid service network file: " << fileName << "\n";
		std::cerr << e.what();
		return false;
    }
    
    return true;
}

bool heartland::is_valid_shipment_json(nlohmann::json& jsonDoc, const std::string& fileName)
{
    nlohmann::json_schema::json_validator validator;
    
    try {
        validator.set_root_schema(heartland_schemas::commodities_v1);
    } 
    catch (const std::exception &e) 
    {
        std::cerr << "Invalid schema: heartland_schemas::commodities_v1\n";
        std::cerr << "Here is why: " << e.what() << "\n";
        throw;
    }
    
    try 
    {
        validator.validate(jsonDoc);
    }
    catch (const std::exception& e) 
    {
		std::cerr << "Invalid shipment file: " << fileName << "\n";
		std::cerr << e.what();
		return false;
    }
    
    return true;
}

std::unique_ptr<cServiceNetwork> heartland::create_service_network_from_json_doc(nlohmann::json& jsonDoc)
{
    std::unique_ptr<cServiceNetwork> pServiceNetwork(new cServiceNetwork());
    
    try {
        auto nodes = jsonDoc["nodes"];
        for (auto node : nodes)
        {
            std::string id = node[NODE_ID].get<std::string>();

			unsigned int capacity = node[NODE_CAPACITY].get<unsigned int>();
//            "cargo_categories": [
//                                 "Non-hazardous",
//                                 "Perishable",
//                                 "Goods"
//                                 ],
            float cost = get_required<float>(node, NODE_COST, activeFileName);
            double holding_time = get_required<double>(node, NODE_HOLDING_TIME, activeFileName);
            double holding_cost = get_optional(node, NODE_HOLDING_COST, ctsndp::MINIMUM_COST, activeFileName);
            double service_time = get_required<double>(node, NODE_SERVICE_TIME, activeFileName);
			double cycle_time = get_optional(node, NODE_CYCLE_TIME, 0.0, activeFileName);
			
            unsigned int storage = get_required<unsigned int>(node, NODE_STORAGE, activeFileName);
//            "transportation_methods": [
//                                       "Sea"
//                                       ],

            cServiceLocation* pHold = nullptr;
            if ((storage > 0) && (holding_time > 0))
            {
                pHold = pServiceNetwork->addLocation(id + ctsndp::HOLD);
                add_properties(pHold, node);
            }
            
            cServiceLocation* pIn = nullptr;
            cServiceLocation* pOut = nullptr;
            cServiceLocation* pLoc = nullptr;
            if (capacity > 0)
            {
                pIn = pServiceNetwork->addLocation(id + ctsndp::IN);
                add_properties(pIn, node);
                
                pOut = pServiceNetwork->addLocation(id + ctsndp::OUT);
                add_properties(pOut, node);
            }
            else
            {
                pLoc = pServiceNetwork->addLocation(id);
                add_properties(pLoc, node);
            }

            if (pHold)
            {
                cServiceLocation* pDest = nullptr;
                if (pIn)
                    pDest = pIn;
                else
                    pDest = pLoc;
                std::unique_ptr<cServiceLink> pLink(new cServiceLink(id + ctsndp::HOLDING, pHold, pDest));
                pLink->setCapacity(storage);
                pLink->setCommodityBundleCapacity(1);
                pLink->setFixedCost(static_cast<float>(holding_cost));
				pLink->setCommodityBundleCost(ctsndp::MINIMUM_VARIABLE_COST);
                pLink->setTravelTime(holding_time);
				if (cycle_time > 0)
				{
					pLink->setCycleTime(cycle_time);
					cycle_time = 0;
				}
                pServiceNetwork->addLink(std::move(pLink));
            }
            
            if (pIn)
            {
                std::unique_ptr<cServiceLink> pLink(new cServiceLink(id + ctsndp::PROCESSING, pIn, pOut));
                pLink->setCapacity(capacity);
                pLink->setCommodityBundleCapacity(1);
                pLink->setFixedCost(cost);
				pLink->setCommodityBundleCost(ctsndp::MINIMUM_VARIABLE_COST);
				pLink->setTravelTime(service_time);
				if (cycle_time > 0)
				{
					pLink->setCycleTime(cycle_time);
					cycle_time = 0;
				}
				pServiceNetwork->addLink(std::move(pLink));
            }

        }

        auto links = jsonDoc["links"];
        for (auto link : links)
        {
            std::string src_name = get_required<std::string>(link, LINK_SOURCE, activeFileName);
            std::string dst_name = get_required<std::string>(link, LINK_TARGET, activeFileName);
            const cServiceLocation* pSrc = pServiceNetwork->findLocation(src_name + ctsndp::OUT);
            if (!pSrc)
                pSrc = pServiceNetwork->findLocation(src_name);
            
            auto pDst = pServiceNetwork->findLocation(dst_name + ctsndp::HOLD);
            if (!pDst)
            {
                pDst = pServiceNetwork->findLocation(dst_name + ctsndp::IN);
                if (!pDst)
                    pDst = pServiceNetwork->findLocation(dst_name);
            }

            if (!pSrc || !pDst)
            {
                throw std::logic_error("Invalid source or target nodes!");
            }
            std::string name = get_required<std::string>(link, LINK_NAME, activeFileName);
            std::unique_ptr<cServiceLink> pLink(new cServiceLink(name, pSrc, pDst));
            bool capacity_means_arc = false;
            auto transportation_methods = link[LINK_TRANSPORTATION_METHODS];
            for (auto method : transportation_methods.items() )
            {
                if (method.value().get<std::string>() == "Sea")
                    capacity_means_arc = true;
            }

            if (link.contains(LINK_CAPACITY))
            {
                auto& value = link[LINK_CAPACITY];
                if (capacity_means_arc)
                {
                    pLink->setCapacity(value.get<unsigned int>());
                    pLink->setCommodityBundleCapacity(std::numeric_limits<unsigned int>::max());
                }
                else
                {
                    pLink->setCommodityBundleCapacity(value.get<unsigned int>());
                }
            }

            if (link.contains(LINK_FIXED_COST))
            {
                pLink->setFixedCost(link[LINK_FIXED_COST].get<float>());
				pLink->setCommodityBundleCost(ctsndp::MINIMUM_VARIABLE_COST);
            }

            if (link.contains(LINK_TRAVEL_TIME))
            {
                pLink->setTravelTime(link[LINK_TRAVEL_TIME].get<double>());
            }

			if (link.contains(LINK_CYCLE_TIME))
			{
				pLink->setCycleTime(link[LINK_CYCLE_TIME].get<double>());
			}

            add_properties(pLink.get(), link);
            pServiceNetwork->addLink(std::move(pLink));
        }

    }
	catch (nlohmann::detail::parse_error& e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		exit(0);
	}
    
    return pServiceNetwork;
}

std::unique_ptr<cServiceNetwork> heartland::create_service_network_from_json_file(const std::string& jsonFileName)
{
	activeFileName = jsonFileName;

    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;
    if (!is_valid_service_network_json(jsonDoc, jsonFileName))
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());

    return create_service_network_from_json_doc(jsonDoc);
}

std::unique_ptr<cCommodityShipment> create_shipment_from_json_doc(nlohmann::json& jsonDoc)
{
    std::unique_ptr<cCommodityShipment> pShipment;
    
    auto commodities = jsonDoc["commodities"];
    for (auto& commodity : commodities)
    {
        std::string name = commodity[SHPT_NAME].get<std::string>();
        auto src_name = commodity[SHPT_SOURCE].get<std::string>();
        auto dest_name = commodity[SHPT_DEST].get<std::string>();
        std::unique_ptr<cCommodityShipment> pShipment(new cCommodityShipment(name, src_name, dest_name));
        pShipment->setEarliestAvailableTime(std::max(commodity[SHPT_EAT].get<double>(),1.0));
        pShipment->setLatestDeliveryTime(commodity[SHPT_LDT].get<double>());
        std::string type = commodity["cargo_categories"].front();
        add_properties(pShipment.get(), commodity);

        std::unique_ptr<cCommodityBundle> pBundle(new cCommodityBundle(pShipment.get(), type));
        pBundle->setQuantity(1);
        pShipment->addCommodityBundle(std::move(pBundle));
    }
    
    return pShipment;
}

void create_shipments_from_json_doc(cCommodityShipments* pShipments, nlohmann::json& jsonDoc,
                                    const std::string& shipment_filename, const std::string& uuid,
                                    const std::string& shipper, bool no_delays, bool group)
{
    std::unique_ptr<cCommodityShipment> pShipment;
    std::string name;
    std::string source;
    std::string destination;
    double eat = -1.0;
    double ldt = -1.0;
    
    auto commodities = jsonDoc["commodities"];
    if (commodities.empty())
        return;
    
    for (auto& commodity : commodities)
    {
		auto src_name = get_required<std::string>(commodity, SHPT_SOURCE, shipment_filename);
		auto dst_name = get_required<std::string>(commodity, SHPT_DEST, shipment_filename);
		std::string tmp = get_optional(commodity, SHPT_UUID, uuid, shipment_filename);
        double e = std::max(commodity[SHPT_EAT].get<double>(), 0.0);
        double l = commodity[SHPT_LDT].get<double>();
        
        if ((name != tmp) || (source != src_name)
            || (destination != dst_name) || (e != eat)
            || (l != ldt))
        {
            if (pShipment.get())
                pShipments->addShipment(std::move(pShipment));
            
            name = tmp;
            source = src_name;
            destination = dst_name;
            eat = e;
            ldt = l;

            pShipment.reset(new cCommodityShipment(name, source, destination));
            pShipment->setEarliestAvailableTime(eat);
            pShipment->setLatestDeliveryTime(ldt);

            if (commodity.contains(SHPT_SHIPPER) && !commodity[SHPT_SHIPPER].empty())
            {
                pShipment->addRequiredLocation(commodity[SHPT_SHIPPER].get<std::string>());
            }
            else if (!shipper.empty())
                pShipment->addRequiredLocation(shipper);
            
            if (!uuid.empty())
                pShipment->addProperty("shipper_uuid", uuid);
            
            if (!shipment_filename.empty())
                pShipment->addProperty("shipment_file", shipment_filename);
        }

        add_commodity_to_shipment_from_json_doc(pShipment.get(), commodity, no_delays, group);
    }
    pShipments->addShipment(std::move(pShipment));
}

heartland::sModelInputs_t heartland::create_inputs_from_schedule_file(const std::string& jsonFileName, 
	bool no_delays, bool group)
{
    std::string::size_type pos = jsonFileName.rfind("/");
    if (pos == std::string::npos)
    {
        pos = jsonFileName.rfind("\\");
    }
    
    std::string path;
    if (pos != std::string::npos)
    {
        path = jsonFileName.substr(0, pos+1);
    }

    sModelInputs_t inputs;
    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return inputs;
    }
    
    nlohmann::json scheduleDoc;
    in >> scheduleDoc;
    std::string networkFileName = scheduleDoc["network"];
    inputs.network = create_service_network_from_json_file(path + networkFileName);
    inputs.shipments.reset(new cCommodityShipments());
    
    auto shipments = scheduleDoc["shipments"];
    for (auto& shipment : shipments)
    {
		std::string shipment_uuid = get_required<std::string>(shipment, "shipment_uuid", jsonFileName);
		std::string shipper = get_optional(shipment, "shipper", std::string(), jsonFileName);
        std::string fileName = path;
#ifdef _WIN32
		std::string tmp = get_required<std::string>(shipment, "shipment_file", jsonFileName);
		std::replace(tmp.begin(), tmp.end(), ':', '_');
		fileName += tmp;
#else
		fileName += shipment["shipment_file"];
#endif
        std::ifstream in(fileName);
        if (!in.is_open())
        {
            std::cerr << "Could not open " << fileName << " for reading!" << std::endl;
			print_cwd();
            continue;
        }

        nlohmann::json shipmentDoc;
        in >> shipmentDoc;
        create_shipments_from_json_doc(inputs.shipments.get(), shipmentDoc, fileName, 
										shipment_uuid, shipper, no_delays, group);
        in.close();
    }


    return inputs;
}

std::unique_ptr<cCommodityShipments> heartland::create_shipments_from_json_doc(nlohmann::json& jsonDoc, 
	bool no_delays, bool group)
{
    std::unique_ptr<cCommodityShipments> pShipments(new cCommodityShipments());
   
    auto commodities = jsonDoc["commodities"];
    for (auto& commodity : commodities)
    {
        std::string name = commodity["name"].get<std::string>();
        auto src_name = commodity["source"].get<std::string>();
        auto dest_name = commodity["destination"].get<std::string>();
        std::unique_ptr<cCommodityShipment> pShipment(new cCommodityShipment(name, src_name, dest_name));
        pShipment->setEarliestAvailableTime(commodity["EAT"].get<double>());
        pShipment->setLatestDeliveryTime(commodity["LDT"].get<double>());
        std::string type = commodity["cargo_categories"].front();
        std::unique_ptr<cCommodityBundle> pBundle(new cCommodityBundle(pShipment.get(), type));
        pBundle->setQuantity(1);
        pShipment->addCommodityBundle(std::move(pBundle));
        pShipments->addShipment(std::move(pShipment));
    }

    return pShipments;
}

std::unique_ptr<cCommodityShipments> heartland::create_shipments_from_json_file(const std::string& jsonFileName,
	bool no_delays, bool group)
{
	activeFileName = jsonFileName;
    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cCommodityShipments>(new cCommodityShipments());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;

    return create_shipments_from_json_doc(jsonDoc, no_delays, group);
}

nlohmann::json heartland::convert_routes_to_json_format(const cRoutes& routes, std::ostream* debug)
{
	if (debug)
	{
		*debug << "Heartland::convert_routes_to_json_format...\n";
	}
    std::vector<nlohmann::json> commodities;
    for (auto& shipment : routes.mpShipments->getShipments())
    {
		if (debug)
		{
			*debug << "  Processing shipment: " << shipment->getName() << "\n";
		}
		for (auto& commodity : shipment->getCommodityBundles())
        {
			if (debug)
			{
				*debug << "    Processing commodity: " << commodity->getName() << "... ";
			}
			const sCommodityPath* path = nullptr;
            for (auto& commodity_path : routes.mPaths)
            {
                if (commodity_path.pCommodity == commodity)
                {
                    path = &commodity_path;
                    break;
                }
            }
            
			if (!path || path->path.empty())
			{
				if (debug)
				{
					*debug << "path empty.\n";
				}
				continue;
			}
            
            std::string name;
            std::vector<std::string> names;
            std::vector<double> dispatchTimes;
            bool withinNode = false;
            for (auto& leg : path->path)
            {
                if (!leg.pLink)
                    continue;
                name = get_name(*(leg.pLink->getSource()));
                if (name.find(ctsndp::HOLD) != std::string::npos)
                {
                    withinNode = true;
                    continue;
                }
                if (withinNode)
                {
                    if (name.find(ctsndp::OUT) != std::string::npos)
                    {
                        names.push_back(name.substr(0, name.find(ctsndp::OUT)));
                        
                        dispatchTimes.push_back(leg.departureTime);
                        withinNode = false;
                    }
                    continue;
                }
                names.push_back(name);
                
                dispatchTimes.push_back(leg.departureTime);
            }

            auto& last = path->path.back();
            if (last.pLink)
            {
                std::string name = get_name(*(last.pLink->getDestination()));
                names.push_back(name.substr(0, name.find(ctsndp::OUT)));
            }
            else
            {
                auto it = ++(path->path.rbegin());

                // We might have multiple delay arcs.  Walk backwards
                // until we find the first travel arc
				auto last = *it;
                while ((it != path->path.rend()) && (last.pLink == nullptr))
                {
                    ++it;
				    last = *it;
                }
                
				if (it == path->path.rend())
					continue;

				std::string tmp;
				
				if (last.pLink)
					tmp = get_name(*(last.pLink->getDestination()));

				if (tmp != name)
				{
					names.push_back(tmp.substr(0, tmp.find(ctsndp::OUT)));
				}
				else
				{
					names.push_back(name);
				}
			}
            nlohmann::json obj;
            obj.emplace("name", commodity->getName());
            obj.emplace("path", names);
            obj.emplace("preferredDepartureTimes", dispatchTimes);
            commodities.push_back(obj);
            dispatchTimes.clear();
			if (debug)
			{
				*debug << "done.\n";
			}
		}
		if (debug)
		{
			*debug << "  done.\n";
		}
	}
    
    nlohmann::json jsonDoc;
    jsonDoc.emplace("commodities", commodities);

	if (debug)
	{
		*debug << "done\n";
	}
	
	return jsonDoc;
}
