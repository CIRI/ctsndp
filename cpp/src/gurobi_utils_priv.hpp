//
//  gurobi_utils_priv.hpp
//  ctsndp
//
//  Created by Brett Feddersen on 03/25/20.
//

#pragma once
#ifndef gurobi_utils_priv_hpp
#define gurobi_utils_priv_hpp

#include <gurobi_c++.h>

namespace ctsndp
{
    /*!
     * \brief Converts the value stored in a Gurobi variable into the type
     */
    bool   as_bool(const GRBVar& var);
    int    as_int(const GRBVar& var);
    double as_double(const GRBVar& var);
}

 
inline bool ctsndp::as_bool(const GRBVar& var)
{
    double d = var.get(GRB_DoubleAttr_X);
    return d > 0.1;
}

inline int ctsndp::as_int(const GRBVar& var)
{
    return static_cast<int>(var.get(GRB_DoubleAttr_X));
}

inline double ctsndp::as_double(const GRBVar& var)
{
    return static_cast<double>(var.get(GRB_DoubleAttr_X));
}

#endif /* gurobi_utils_priv_hpp */
