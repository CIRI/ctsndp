

#include "ctsndp_exceptions.hpp"


namespace ctsndp
{
	cOptimizationException::cOptimizationException(const std::string& what_args)
		:
		std::runtime_error(what_args)
	{}


	cInfeasibleSolution::cInfeasibleSolution(const std::string& modelName, const std::string& what_args)
    :
        std::runtime_error(what_args),
        mModelName(modelName)
    {}

    const char* cInfeasibleSolution::model() const noexcept
    {
        return mModelName.c_str();
    };


    cTimeLimit::cTimeLimit(const std::string& modelName, const std::string& what_args)
    :
        std::runtime_error(what_args),
        mModelName(modelName)
    {}

    const char* cTimeLimit::model() const noexcept
    {
        return mModelName.c_str();
    };
}
