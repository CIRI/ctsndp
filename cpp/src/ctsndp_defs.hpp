//
//  ctsndp_defs.hpp
//  ctsndp
//
//  Created by Brett Feddersen on 11/21/19.
//

#pragma once
#ifndef ctsndp_defs_hpp
#define ctsndp_defs_hpp

#include "Commodities/CommodityBundle.hpp"
#include <vector>
#include <map>
#include <set>

// Forward Declarations
class cServiceLocation;
class cServiceLink;
class cPartiallyTimeExpandedNetworkNode;
class cPartiallyTimeExpandedNetworkDelayNode;
class cPartiallyTimeExpandedNetworkArc;
class cPartiallyTimeExpandedNetworkHoldoverArc;
class cPartiallyTimeExpandedNetworkDelayArc;

/**
 * Use this define to determine if the ConstructFeasibleSolution should
 * by used as a backup in finding arcs to lengthen
 */
#define CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP

/**
 * Set to one to turn on adding debug names to the optimization variables and
 * constraints.  Warning: the program will require move memory.
 */
#define ADD_DEBUG_NAMES 0

namespace ctsndp
{

    typedef const cCommodityBundle* const               commodity_ptr_t;
    typedef const cServiceLocation*                     loc_ptr_t;
    typedef const cServiceLink*                         link_ptr_t;
    typedef cPartiallyTimeExpandedNetworkArc*           arc_ptr_t;
    typedef cPartiallyTimeExpandedNetworkHoldoverArc*   holdover_arc_ptr_t;
	typedef cPartiallyTimeExpandedNetworkDelayArc*		delay_arc_ptr_t;
	typedef cPartiallyTimeExpandedNetworkNode*          node_ptr_t;
    typedef cPartiallyTimeExpandedNetworkDelayNode*     delay_node_ptr_t;

    typedef std::vector<const cCommodityBundle*>        commodities_t;
    typedef std::vector<loc_ptr_t>  path_loc_t;
    typedef std::vector<node_ptr_t> path_node_t;
    typedef std::vector<arc_ptr_t>  path_arc_t;

    typedef std::pair<commodity_ptr_t, commodity_ptr_t>			commodity_pair_t;

	/**
	 * In the paper, J_ak is the set of the pair of commodities that
	 * are consolidated on an arc.  We expanded this to a group of
	 * commodities that all share the arc for efficiency
	 */
	typedef std::map<arc_ptr_t, std::vector<commodities_t>>		consolidation_groups_t;

    //
    // Typedef used by the Minimize Costs
    //
    struct compareBundleById
    {
        bool operator()(ctsndp::commodity_ptr_t& lhs, ctsndp::commodity_ptr_t& rhs) const
        {
            return lhs->getId() < rhs->getId();
        }
    };

    typedef std::map<ctsndp::commodity_ptr_t, ctsndp::path_node_t, compareBundleById> commodity_path_node_t;
    typedef std::map<ctsndp::commodity_ptr_t, ctsndp::path_arc_t, compareBundleById>  commodity_path_arc_t;
    typedef std::map<int, unsigned int>   utilization_t;

    //
    // Typedef used by the Identify Arcs To Lengthen
    //
    struct sCommodityNode
    {
        commodity_ptr_t k;
        node_ptr_t      n;
    };

    struct compareCommodityNodeByPtrId
    {
        bool operator()(const sCommodityNode& lhs, const sCommodityNode& rhs) const
        {
            if (lhs.n == rhs.n )
                return lhs.k->getId() < rhs.k->getId();
            return lhs.n < rhs.n;
        }
    };

    struct sCommodityArc
    {
        commodity_ptr_t k;
        arc_ptr_t       ij;
    };

    struct compareCommodityArcByPtrId
    {
        bool operator()(const sCommodityArc& lhs, const sCommodityArc& rhs) const
        {
            if (lhs.ij == rhs.ij )
                return lhs.k->getId() < rhs.k->getId();
            return lhs.ij < rhs.ij;
        }
    };

    typedef std::map<sCommodityNode, double, compareCommodityNodeByPtrId> dispatch_time_t;
    typedef std::map<sCommodityArc, double, compareCommodityArcByPtrId>   travel_time_t;

    struct sDualCommodityArc
    {
        commodity_ptr_t k1;
        commodity_ptr_t k2;
        arc_ptr_t       ij;
    };

    struct compareDualCommodityArcByPtrId
    {
        bool operator()(const sDualCommodityArc& lhs, const sDualCommodityArc& rhs) const
        {
            if (lhs.ij == rhs.ij )
            {
                if (lhs.k1 == rhs.k1)
                    return lhs.k2->getId() < rhs.k2->getId();
                return lhs.k1->getId() < rhs.k1->getId();
            }
            return lhs.ij < rhs.ij;
        }
    };

    typedef std::map<sDualCommodityArc, double, compareDualCommodityArcByPtrId> dispatch_mismatch_times_t;

    struct sArcsToShorten
    {
        arc_ptr_t		ij;
		double			t_new;
		commodity_ptr_t	k;
		delay_arc_ptr_t delay_arc;
	};

    struct compareArcsToShorten
    {
        bool operator()(const sArcsToShorten& lhs, const sArcsToShorten& rhs) const
        {
            if (lhs.ij == rhs.ij )
            {
                return lhs.t_new < rhs.t_new;
            }
            return lhs.ij < rhs.ij;
        }
    };

    typedef std::set<sArcsToShorten, compareArcsToShorten> arcs_to_shorten_t;

	struct over_capacity_t
	{
		unsigned int utilization;
		double dispatch_time;
		arc_ptr_t arc;
	};


	typedef std::vector<over_capacity_t> arcs_over_capacity_t;
}

#endif /* ctsndp_defs_h */
