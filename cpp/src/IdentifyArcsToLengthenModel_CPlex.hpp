//
//  IdentifyArcsToLengthenModel_CPlex.hpp
//
//  Implements the "Identify Arcs to Lengthen" problem
//  using the CPlex optimizer.
//
//  Created by Brett Feddersen on 03/25/20.
//

#pragma once
#ifndef IdentifyArcsToLengthenModel_CPlex_hpp
#define IdentifyArcsToLengthenModel_CPlex_hpp

#include "IdentifyArcsToLengthenModel.hpp"
#include "CPlexModel.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include <vector>
#include <map>
#include <set>
#include <iosfwd>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the "Identify Arcs to Lengthen" algorithm listed
 * in section 4.3 on page 1312 of the Continuous Time Service Network
 * Design Problem paper.
 */
class cIdentifyArcsToLengthenModel_CPlex final : public cIdentifyArcsToLengthenModel,
												 private cCPlexModel
{
public:
    cIdentifyArcsToLengthenModel_CPlex(int pass_number,
                                 const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                 const cServiceNetwork& serviceNetwork,
                                 const ctsndp::commodity_path_node_t& p_ki,
                                 const ctsndp::commodity_path_arc_t& p_ka,
								 const ctsndp::consolidation_groups_t& j_ak,
                                 bool verboseMode, bool printTimingInfo);

    ~cIdentifyArcsToLengthenModel_CPlex();

	void setNumericFocus(int numericFocus) override;
	void setTimeLimit_sec(double timeLimit_sec) override;
	void logToConsole(bool log) override;
	void outputDebugMsgs(bool msgs) override;
	void logFilename(const std::string& filename) override;
	void readTuningParameters(const std::string& filename) override;

    bool Optimize() override;

	bool RelaxConstraints() override;
	void FindConstraintViolations(std::ostream& out) override;
	std::string ComputeIIS() override;

private:
	void ConstructOptimizatonVariables() override;
	void BuildOptimizatonProblem() override;

    void FindArcsToLengthen();
    void SaveDispatchAndTravelTimes();

    //
    // Constraint definitions used in the Identify Arcs To Lengthen optimization
    //
    void constraint6_CountArcsWithShortenTravelTimes() override;
    void constraint7_EnsureAllowableDispatchTimes() override;
    void constraint8_EnsureDispatchIsAfterAvailable() override;
    void constraint9_EnsureArrivalBeforeDue() override;
    void constraint10_EnsureConsolidations() override;
//    void constraint10_EnsureConsolidations2() override;
    
    //
    // Debug Methods
    //
    void DumpIdentifyArcsTimeInfo(std::ostream& out);
    void DumpCommoditySchedules(std::ostream& out);
    void DumpCommoditySchedule(std::ostream& out, const cCommodityBundle* const k);

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    //
    // Decision Variables
    //
    
    struct sDualCommodityNode
    {
        ctsndp::commodity_ptr_t k1;
        ctsndp::commodity_ptr_t k2;
        ctsndp::node_ptr_t      n;
    };
    
    struct compareDualCommodityNodeByPtrId
    {
        bool operator()(const sDualCommodityNode& lhs, const sDualCommodityNode& rhs) const
        {
            if (lhs.n == rhs.n )
            {
                if (lhs.k1 == rhs.k1)
                    return lhs.k2->getId() < rhs.k2->getId();
                return lhs.k1->getId() < rhs.k1->getId();
            }
            return lhs.n < rhs.n;
        }
    };

    struct sDualCommodityArc
    {
        ctsndp::commodity_ptr_t k1;
        ctsndp::commodity_ptr_t k2;
        ctsndp::arc_ptr_t       ij;
    };
    
    struct compareDualCommodityArcByPtrId
    {
        bool operator()(const sDualCommodityArc& lhs, const sDualCommodityArc& rhs) const
        {
            if (lhs.ij == rhs.ij )
            {
                if (lhs.k1 == rhs.k1)
                    return lhs.k2->getId() < rhs.k2->getId();
                return lhs.k1->getId() < rhs.k1->getId();
            }
            return lhs.ij < rhs.ij;
        }
    };
    
    // the dispatch time of commodity, k, at node i
    std::map<ctsndp::sCommodityNode, IloNumVar, ctsndp::compareCommodityNodeByPtrId> gamma_ki;
    
    // the travel time of arc, (i, j) when taken by commodity, k
    std::map<ctsndp::sCommodityArc, IloNumVar, ctsndp::compareCommodityArcByPtrId>   theta_kij;
    
    // is the arc allowed to be too short
    std::map<ctsndp::sCommodityArc, IloBoolVar, ctsndp::compareCommodityArcByPtrId>   sigma_kij;
};

#endif /* IdentifyArcsToLengthenModel_hpp */
