//
//  ctsndp_utils.cpp
//  cloud
//
//  Created by Brett Feddersen on 8/7/19.
//

#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkNode.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"

#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <exception>
#include <string>
#include <algorithm>
#include <cassert>

bool ctsndp::validate_inputs(cServiceNetwork& service_network,
                     cCommodityShipments& shipments,
                     bool debug_mode)
{
    for (auto& shipment : shipments.getShipments())
    {
        if (shipment->hasRequiredLocations())
        {
            for (auto loc : shipment->getRequiredLocations())
            {
                if (!service_network.hasLocation(loc))
                {
                    if (!service_network.hasLocation(loc + ctsndp::IN))
                    {
                        std::cout << "Validation Failed: Required location, \"";
                        std::cout << loc << "\" does not exists in service network.";
                        return false;
                    }
                }
            }
        }
    }

    return true;
}


// Try to apply any fixups due to implicity created nodes in the service network!
void ctsndp::apply_implicit_node_fixups(
                            cServiceNetwork& service_network,
                            cCommodityShipments& shipments,
                            bool debug_mode)
{
    // If any shipments have any required locations that they must pass through, we will need
    // to make sure there is a "PROCESSING" arc embedded in the service network.  Later, we
    // force the commodity to use this arc.
    for (auto& shipment : shipments.getShipments())
    {
        if (!shipment->hasRequiredLocations())
            continue;
        
        auto required_locations = shipment->getRequiredLocations();
        
        for (auto& required_location : required_locations)
        {
            // Test to see if a "PROCESSING" arc has already been added.
            if (service_network.findLink(required_location + ctsndp::PROCESSING))
                continue;
            
            // No "PROCESSING" arc exists so we must create it.  First we need the
            // "IN" and "OUT" nodes.
            auto pIn = service_network.findLocation(required_location);
            if (!pIn)
            {
                if (debug_mode)
                {
                    std::clog << "no required location named: " << required_location << "\n";
                }
                
                std::string msg = "Apply Implicit Node Fixups Failed: Unknown required location in service network";
                msg += ": " + required_location;
                throw std::logic_error(msg);
            }
            
            auto pOut = service_network.addLocation(required_location + ctsndp::OUT);
            service_network.moveOutgoingLinksFrom(pIn, pOut);
            service_network.renameLocation(required_location, required_location + ctsndp::IN);
            std::unique_ptr<cServiceLink> pLink(new cServiceLink(required_location + ctsndp::PROCESSING, pIn, pOut));
            service_network.addLink(std::move(pLink));
        }
    }

    
    for (auto& shipment : shipments.getShipments())
    {
        std::string source_name = shipment->getSourceName();
        auto source_node = shipment->getSourceLocation();
        
        // Connect the commodity source to an actual location in service network
        if (!source_node)
        {
            source_node = service_network.findLocation(source_name);
            shipment->setSourceLocation(source_node);
        }

        // OLD: We need to look for implicitly created node (HOLD or IN)
		// NEW: We need to look for implicitly created node (OUT -> IN -> HOLD)
		if (!source_node)
        {
            source_node = service_network.findLocation(source_name + ctsndp::OUT);
            if (!source_node)
            {
                source_node = service_network.findLocation(
                                        source_name + ctsndp::IN);
				if (!source_node)
				{
					source_node = service_network.findLocation(
						source_name + ctsndp::HOLD);
				}
			}
            
            if (!source_node)
            {
                if (debug_mode)
                {
                    std::clog << "no service location named: " << source_name << "\n";
                }
                
                std::string msg = "Apply Implicit Node Fixups Failed: Unknown service location";
                msg += ": " + source_name;
                throw std::logic_error(msg);
            }

            shipment->setSourceName(source_node->getName());
            shipment->setSourceLocation(source_node);
        }
        
        std::string destination_name = shipment->getDestinationName();
        auto destination_node = shipment->getDestinationLocation();
 
        // Connect the commodity destination to an actual location in service network
        if (!destination_node)
        {
            destination_node = service_network.findLocation(destination_name);
            shipment->setDestinationLocation(destination_node);
        }

        // We need to look for implicitly created node (OUT)
        if (!destination_node)
        {
            destination_node = service_network.findLocation(destination_name + ctsndp::OUT);
            
            if (!destination_node)
            {
                if (debug_mode)
                {
                    std::clog << "no service location named: " << destination_name << "\n";
                }
                
                std::string msg = "Apply Implicit Node Fixups Failed: Unknown service location";
                msg += ": " + destination_name;
                throw std::logic_error(msg);
            }

            shipment->setDestinationName(destination_node->getName());
            shipment->setDestinationLocation(destination_node);
        }
    }
}

void ctsndp::apply_shortest_path_fixups(
                        const cServiceNetwork& service_network,
                        const cCommodityShipments& shipments)
{
    std::map<int, std::vector<double> > srcTimes;
    
    // Make sure the difference between EAT and LDT will fit within
    // the shortest path time...
    for (auto& shipment : shipments.getShipments())
    {
        double t = shipment->getLatestDeliveryTime()
                    - shipment->getEarliestAvailableTime();
        
        if (t < 0.0)
        {
            std::string msg = "Apply Shortest Path Failed: Shipment's LDT < EAT";
            msg += ": " + shipment->getName();
            throw std::logic_error(msg);
        }
        
        double shortest_time = 0;
        const auto& src = shipment->getSourceLocation();
        assert(src);
        const auto& dst = shipment->getDestinationLocation();
        assert(dst);
        
        auto it = srcTimes.find(src->getId());
        if (it == srcTimes.end())
        {
            auto times = service_network.shortestTimes(src);
            shortest_time = times[dst->getId()];
            srcTimes.insert(std::make_pair(src->getId(), times));
        }
        else
        {
            shortest_time = (it->second)[dst->getId()];
        }

		shipment->setShortestPathTime(shortest_time);

        if (t < shortest_time)
        {
            for (auto& commodity : shipment->getCommodityBundles())
            {
                if (!commodity->hasDelayCost())
                    commodity->setDelayCost(0);
            }
        }
    }
}

bool ctsndp::validate_paths(const ctsndp::commodity_path_node_t& p_ki,
	const ctsndp::commodity_path_arc_t& p_ka)
{
	if (p_ki.empty() && p_ka.empty())
		return true;

	if (p_ki.empty())
		return false;

	if (p_ka.empty())
		return false;

	// for all commodities
	for (auto[k, arcs] : p_ka)
	{
		if (p_ki.find(k) == p_ki.end())
			return false;

		auto& nodes = p_ki.at(k);

		if (arcs.empty() && nodes.empty())
			continue;

		if (arcs.empty())
			return false;

		if (nodes.empty())
			return false;

		if ((arcs.size() + 1) != nodes.size())
			return false;

		int i = 0;
		for (; i < arcs.size() - 1; ++i)
		{
			auto& arc = arcs[i];
			if (arc->getSource() != nodes[i])
				return false;
		}
		if (arcs.back()->getDestination() != nodes[i + 1])
			return false;
	}

	return true;
}

//std::string ctsndp::to_string(const std::time_t& t)
std::string ctsndp::to_string(const time_t& t)
{
#if _MSC_VER
	char buf[26];
	ctime_s(buf, sizeof buf, &t);
	std::string str(buf);
#else
	std::string str(std::ctime(&t));
#endif
	auto pos = str.find_last_of('\n');
    if (pos != std::string::npos)
        str.erase(pos, 1);
    return str;
}



