
#include "onf_utils.hpp"
#include "onf.schemas"
#include "ServiceNetwork/ServiceLocation.hpp"
#include "ServiceNetwork/ServiceLink.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "ctsndp_vars.hpp"
#include "json_utils.hpp"
#include <nlohmann/json-schema.hpp>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <any>
#include <iostream>
#ifdef WIN32
	#include <direct.h>
#else
	#include <unistd.h>
#endif

namespace
{
    std::string activeFileName;

    /*
     * Required ONF JSON entities
     */
    const static char* NODE_ID = "id";
    const static char* LINK_NAME = "name";
    const static char* LINK_SOURCE = "source";
    const static char* LINK_TARGET = "target";
    std::string LINK_CAPACITY = "onf:capacity";
    std::string LINK_BUNDLE_CAPACITY = "onf:commodity_bundle_capacity";
    std::string LINK_FIXED_COST = "onf:fixed_cost";
    std::string LINK_BUNDLE_COST = "onf:commodity_bundle_cost";
    std::string LINK_TRAVEL_TIME = "onf:travel_time";
    std::string LINK_CYCLE_TIME = "onf:cycle_time";

    const static char* SHPT_NAME = "onf:name";
    const static char* SHPT_SOURCE = "onf:source";
    const static char* SHPT_DEST = "onf:destination";
    const static char* SHPT_EAT	= "onf:EAT";
    const static char* SHPT_LDT = "onf:LDT";
//	const static char* SHPT_DELAY_COST = "onf:delay_cost";
    const static char* CB_NAME = "onf:name";
    const static char* CB_TYPE = "onf:type";
    const static char* CB_QUANTITY = "onf:quantity";
    const static char* CB_DELAY_COST = "onf:delay_cost";
    const static char* CB_DELAY_PENALTY_RATE = "onf:delay_penalty_rate";
    const static char* CB_DELAY_PENALTY_LIMIT = "onf:delay_penalty_limit";
    const static char* CB_DELAY_TIME = "onf:max_delay_time";


    bool is_required_node_entry(const std::string& key)
    {
        if (key == NODE_ID)
            return true;

        return false;
    }
    
    bool is_required_link_entry(const std::string& key)
    {
        if ((key == LINK_NAME) || (key == LINK_SOURCE)
            || (key == LINK_TARGET))
        {
            return true;
        }

        return false;
    }

/*
    bool is_required_shipment_entry(const std::string& key)
    {
        if ((key == SHPT_NAME) || (key == SHPT_SOURCE)
                || (key == SHPT_DEST) || (key == SHPT_EAT))
            return true;

        return false;
    }
*/

    bool is_required_bundle_entry(const std::string& key)
    {
        if ((key == CB_TYPE) || (key == CB_QUANTITY)
            || (key == CB_DELAY_COST) 
			|| (key == CB_DELAY_PENALTY_RATE)
			|| (key == CB_DELAY_PENALTY_LIMIT)
			|| (key == CB_DELAY_TIME))
            return true;

        return false;
    }

    template<typename T>
    std::vector<T> to_array(const nlohmann::json& json_value)
    {
        std::vector<T> array;
        for (auto& item : json_value.items())
        {
            array.push_back(item.value());
        }
        return array;
    }

    void print_cwd()
    {
#ifdef WIN32
        std::cerr << "The current working directory is: " << _getcwd(nullptr, 0) << std::endl;
#else
        std::cerr << "The current working directory is: " << getcwd(nullptr, 0) << std::endl;
#endif
    }

} // End of Anonymous Namespace


bool onf::is_valid_service_network_json(nlohmann::json& jsonDoc, const std::string& fileName)
{
    nlohmann::json_schema::json_validator validator;
    
    try 
    {
        validator.set_root_schema(onf_schemas::service_network_v1);
    } 
    catch (const std::exception &e) 
    {
        std::cerr << "Invalid schema: onf_schemas::service_network_v1\n";
		std::cerr << "Here is why: " << e.what() << "\n";
        throw;
    }

    try 
    {
        validator.validate(jsonDoc);
    }
    catch (const std::exception& e) 
    {
		std::cerr << "Invalid service network file: " << fileName << "\n";
		std::cerr << e.what();
		return false;
    }
    
    return true;
}

bool onf::is_valid_shipment_json(nlohmann::json& jsonDoc, const std::string& fileName)
{
    nlohmann::json_schema::json_validator validator;
    
    try
    {
        validator.set_root_schema(onf_schemas::shipments_v1);
    }
    catch (const std::exception &e)
    {
        std::cerr << "Invalid schema: onf_schemas::shipments_v1\n";
		std::cerr << "Here is why: " << e.what() << "\n";
        throw;
    }
    
    try
    {
        validator.validate(jsonDoc);
    } 
    catch (const std::exception& e) 
    {
		std::cerr << "Invalid shipment file: " << fileName << "\n";
		std::cerr << e.what();
		return false;
    }
    
    return true;
}

std::unique_ptr<cServiceNetwork> onf::create_service_network_from_json_doc(nlohmann::json& jsonDoc)
{
    std::unique_ptr<cServiceNetwork> pServiceNetwork(new cServiceNetwork());
    
    try {
        auto nodes = jsonDoc["nodes"];
        for (auto node : nodes)
        {
			std::string id = get_required<std::string>(node, NODE_ID, activeFileName);
            cServiceLocation* pLocation = pServiceNetwork->addLocation(id);
            for (auto property : node.items() )
            {
                std::string key = property.key();
                
                // Skip required entries...
                if (is_required_node_entry(key))
                    continue;
                
                std::any value = to_any(property.value());
                
                if (value.has_value())
                    pLocation->addProperty(key, value);
            }
        }
    
        auto links = jsonDoc["links"];
        for (auto link : links)
        {
            auto src = pServiceNetwork->findLocation(link[LINK_SOURCE]);
            auto dest = pServiceNetwork->findLocation(link[LINK_TARGET]);
            if (!src || !dest)
            {
                throw std::logic_error("Invalid source or target nodes!");
            }
            std::string name = get_required<std::string>(link, LINK_NAME, activeFileName);
            std::unique_ptr<cServiceLink> pLink(new cServiceLink(name, src, dest));
			
			for (auto property : link.items() )
            {
                std::string key = property.key();
                
                if (key == LINK_CAPACITY)
                    pLink->setCapacity(property.value().get<unsigned int>());
                else if (key == LINK_BUNDLE_CAPACITY)
                    pLink->setCommodityBundleCapacity(property.value().get<unsigned int>());
                else if (key == LINK_FIXED_COST)
                    pLink->setFixedCost(property.value().get<float>());
                else if (key == LINK_BUNDLE_COST)
                    pLink->setCommodityBundleCost(property.value().get<float>());
                else if (key == LINK_TRAVEL_TIME)
                    pLink->setTravelTime(property.value().get<double>());
				else if (key == LINK_CYCLE_TIME)
					pLink->setCycleTime(property.value().get<double>());
                else
                {
                    // Skip required entries...
                    if (is_required_link_entry(key))
                        continue;
                    
                    std::any value = to_any(property.value());
                    
                    if (value.has_value())
                        pLink->addProperty(key, value);
                }
            }
            pServiceNetwork->addLink(std::move(pLink));
        }
    }
    catch (nlohmann::detail::parse_error& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return pServiceNetwork;
}

std::unique_ptr<cServiceNetwork> onf::create_service_network_from_json_file(const std::string& jsonFileName)
{
	activeFileName = jsonFileName;
    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;
    if (!is_valid_service_network_json(jsonDoc, jsonFileName))
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
    
    return create_service_network_from_json_doc(jsonDoc);
}

std::unique_ptr<cCommodityShipments> onf::create_shipments_from_json_doc(nlohmann::json& jsonDoc, 
	bool no_delays, bool group)
{
    std::unique_ptr<cCommodityShipments> pShipments(new cCommodityShipments());

    auto shipments = jsonDoc["onf:shipments"];
    for (auto& shipment : shipments)
    {
        std::string name = shipment[SHPT_NAME].get<std::string>();
        auto src_name = shipment[SHPT_SOURCE].get<std::string>();
        auto dest_name = shipment[SHPT_DEST].get<std::string>();
        std::unique_ptr<cCommodityShipment> pShipment(new cCommodityShipment(name, src_name, dest_name));
        pShipment->setEarliestAvailableTime(shipment[SHPT_EAT].get<double>());
        pShipment->setLatestDeliveryTime(shipment[SHPT_LDT].get<double>());

        //BAF: Make optional        pShipment->setDelayCost(shipment[SHPT_DELAY_COST].get<double>());
        
		//ToDo: Currently we are "grouping" by default!  This should be controlled by
		// the group flag.
        auto commodity_bundles = shipment["commodity_bundles"];
        for (auto& commodity_bundle : commodity_bundles)
        {
            std::string type = commodity_bundle[CB_TYPE].get<std::string>();
            std::unique_ptr<cCommodityBundle> pBundle(new cCommodityBundle(pShipment.get(), type));
            
			double delay_cost = -1.0;
			double delay_penalty_rate = -1.0;
			double delay_penalty_limit = std::numeric_limits<double>::max();;

			for (auto property : commodity_bundle.items() )
            {
                std::string key = property.key();
                
                if (key == CB_QUANTITY)
                    pBundle->setQuantity(property.value().get<unsigned int>());
                else if (key == CB_DELAY_COST)
					delay_cost = property.value().get<double>();
				else if (key == CB_DELAY_PENALTY_RATE)
					delay_penalty_rate = property.value().get<double>();
				else if (key == CB_DELAY_PENALTY_LIMIT)
					delay_penalty_limit = property.value().get<double>();
				else if (key == CB_DELAY_TIME)
                    pBundle->setMaxDelayTime(property.value().get<double>());
                else
                {
                    // Skip required entries...
                    if (is_required_bundle_entry(key))
                        continue;
                    
                    std::any value = to_any(property.value());
                    
                    if (value.has_value())
                        pBundle->addProperty(key, value);
                }
            }

			if (!no_delays)
			{
				if (delay_cost >= 0)
					pBundle->setDelayCost(delay_cost);
				if (delay_penalty_rate >= 0)
					pBundle->setDelayPenalties(delay_penalty_rate, delay_penalty_limit);
			}

            pShipment->addCommodityBundle(std::move(pBundle));
        }
        
        pShipments->addShipment(std::move(pShipment));
    }
    
    return pShipments;
}

std::unique_ptr<cCommodityShipments> onf::create_shipments_from_json_file(const std::string& jsonFileName, 
	bool no_delays, bool group)
{
	activeFileName = jsonFileName;
	std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cCommodityShipments>(new cCommodityShipments());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;
    
    return create_shipments_from_json_doc(jsonDoc, no_delays, group);
}

nlohmann::json onf::convert_routes_to_json_format(const cRoutes& routes, std::ostream* debug)
{
	if (debug)
	{
		*debug << "Onf::convert_routes_to_json_format...\n";
	}
	std::vector<nlohmann::json> shipments;
    std::set<const cServiceLocation*> nodes_used;
    std::set<const cServiceLink*> links_used;
    for (auto& shipment : routes.mpShipments->getShipments())
    {
		if (debug)
		{
			*debug << "  Processing shipment: " << shipment->getName() << "\n";
		}
		
		nlohmann::json shipment_obj;
        shipment_obj.emplace("onf:name", shipment->getName());
        shipment_obj.emplace("onf:source", shipment->getSourceName());
        shipment_obj.emplace("onf:destination", shipment->getDestinationName());
        shipment_obj.emplace("onf:EAT", shipment->getEarliestAvailableTime());
        shipment_obj.emplace("onf:LDT", shipment->getLatestDeliveryTime());
        
        ::add_properties(shipment_obj, *shipment);
        
        std::vector<nlohmann::json> commodityBundles;
        
        bool shipment_has_delay_cost = false;
        double shipment_fixed_cost = 0.0;
        double shipment_variable_cost = 0.0;
        double shipment_delay_cost = 0.0;
		double shipment_variable_delay_cost = 0.0;

        for (auto& commodity : shipment->getCommodityBundles())
        {
			if (debug)
			{
				*debug << "    Processing commodity: " << commodity->getName() << "... ";
			}

			nlohmann::json commodity_obj;
            commodity_obj.emplace(CB_NAME, commodity->getName());
            commodity_obj.emplace(CB_QUANTITY, commodity->getQuantity());
            if (commodity->hasDelayCost())
            {
                commodity_obj.emplace(CB_DELAY_COST, commodity->getDelayCost());
                
                if (commodity->hasMaxDelayTime())
                    commodity_obj.emplace(CB_DELAY_TIME, commodity->getMaxDelayTime());
                
                shipment_has_delay_cost = true;
            }
            
            commodity_obj.emplace("cargo_categories", commodity->getCategories());
            ::add_properties(commodity_obj, *commodity);

            const sCommodityPath* path = nullptr;
            for (auto& commodity_path : routes.mPaths)
            {
                if (commodity_path.pCommodity == commodity)
                {
                    path = &commodity_path;
                    break;
                }
            }
            
			if (!path || path->path.empty())
			{
				if (debug)
				{
					*debug << "path empty.\n";
				}
				continue;
			}

            shipment_fixed_cost += path->fixedCost;
            shipment_variable_cost += path->variableCost;
            shipment_delay_cost += path->delayCost;
			shipment_variable_delay_cost += path->variableDelayCost;

            commodity_obj.emplace("onf:fixed_cost", path->fixedCost);
            commodity_obj.emplace("onf:variable_cost", path->variableCost);
            
			if (commodity->hasDelayCost())
			{
				commodity_obj.emplace("onf:delay_cost", path->delayCost);
				commodity_obj.emplace("onf:variable_delay_cost", path->variableDelayCost);
			}

            commodity_obj.emplace("onf:total_cost", path->fixedCost + path->variableCost + path->delayCost);
            
            std::vector<nlohmann::json> nodes;
            std::vector<nlohmann::json> links;
            
            double arrival = -1;
            for (auto& leg : path->path)
            {
                if (!leg.pLink)
                    continue;
                
                nlohmann::json node_obj;
                const auto& node = leg.pLink->getSource();
                node_obj.emplace("id", get_name(*node));
                node_obj.emplace("departs", leg.departureTime);
                node_obj.emplace("departs via", get_name(*(leg.pLink)));
                if (arrival > 0)
                    node_obj.emplace("arrives", arrival);

                nodes_used.insert(node);

                arrival = leg.departureTime + leg.pLink->getTravelTime();

                nlohmann::json link_obj;
                link_obj.emplace("name", get_name(*(leg.pLink)));
                link_obj.emplace("utilization", leg.utilization);
                link_obj.emplace("source", get_name(*(leg.pLink->getSource())));
                link_obj.emplace("destination", get_name(*(leg.pLink->getDestination())));
                link_obj.emplace("fixed_cost", leg.fixedCost);
                link_obj.emplace("variable_cost", leg.variableCost);
                link_obj.emplace("cost", leg.fixedCost + leg.variableCost);

                links_used.insert(leg.pLink);

                nodes.push_back(node_obj);
                links.push_back(link_obj);
            }
          
            auto& last = path->path.back();
            if (last.pLink)
            {
                nlohmann::json node_obj;
                node_obj.emplace("id", get_name(*(last.pLink->getDestination())));
                node_obj.emplace("arrives", arrival);
                nodes.push_back(node_obj);
            }
			else
			{
				auto it = ++(path->path.rbegin());
				if (it == path->path.rend())
					continue;

				auto& last = *it;

				nlohmann::json node_obj;
				node_obj.emplace("id", get_name(*(last.pLink->getDestination())));
				node_obj.emplace("arrives", arrival);
				nodes.push_back(node_obj);
			}

            nlohmann::json path_obj;
            path_obj.emplace("directed", true);
            path_obj.emplace("graph", nlohmann::json());
            path_obj.emplace("multigraph", true);

            path_obj.emplace("nodes", nodes);
            path_obj.emplace("links", links);
            commodity_obj.emplace("path", path_obj);
            
            commodityBundles.push_back(commodity_obj);
			if (debug)
			{
				*debug << "done.\n";
			}
		}

        shipment_obj.emplace("commodity_bundles", commodityBundles);
        shipment_obj.emplace("onf:fixed_cost", shipment_fixed_cost);
        shipment_obj.emplace("onf:variable_cost", shipment_variable_cost);
		if (shipment_has_delay_cost)
		{
			shipment_obj.emplace("onf:delay_cost", shipment_delay_cost);
			shipment_obj.emplace("onf:variable_delay_cost", shipment_variable_delay_cost);
		}
        shipment_obj.emplace("onf:total_cost", shipment_fixed_cost + shipment_variable_cost + shipment_delay_cost);
        shipments.push_back(shipment_obj);

		if (debug)
		{
			*debug << "  done.\n";
		}
   }
    
    nlohmann::json jsonDoc;
    nlohmann::json results;
	results.emplace("Fixed Cost", routes.mFixedCost);
	results.emplace("Fixed Delay Cost", routes.mDelayCost);
	results.emplace("Variable Cost", routes.mVariableCost);
	results.emplace("Variable Delay Cost", routes.mVariableDelayCost);
	results.emplace("Total Cost", routes.mTotalCost);
    results.emplace("Ideal Fixed Cost", routes.mIdealFixedCost);
	results.emplace("Ideal Fixed Delay Cost", routes.mIdealDelayCost);
	results.emplace("Ideal Variable Cost", routes.mIdealVariableCost);
	results.emplace("Ideal Variable Delay Cost", routes.mIdealVariableDelayCost);
	results.emplace("Ideal Total Cost", routes.mIdealTotalCost);
    jsonDoc.emplace("onf:costs", results);
    jsonDoc.emplace("onf:shipments", shipments);
	jsonDoc.emplace("Processing Time (sec)", routes.mSolutionTime_s.count());

	if (debug)
	{
		*debug << "  Processing nodes... ";
	}

	std::vector<nlohmann::json> nodes;
    for (const auto loc : nodes_used)
    {
        nlohmann::json node_obj;
        node_obj.emplace("id", get_name(*loc));
        add_properties(node_obj, *loc);
        nodes.push_back(node_obj);
    }
    jsonDoc.emplace("nodes", nodes);
	if (debug)
	{
		*debug << "done.\n";
	}

	if (debug)
	{
		*debug << "  Processing links used... ";
	}
	std::vector<nlohmann::json> links;
    for (const auto link : links_used)
    {
        nlohmann::json link_obj;
        std::string name = get_name(*link);
        link_obj.emplace("name", name);
        if (name.rfind(":HOLDING") != std::string::npos)
        {
            link_obj.emplace("hold_time", link->getTravelTime());
        }
        else if (name.rfind(":PROCESSING") != std::string::npos)
        {
            link_obj.emplace("processing_time", link->getTravelTime());
        }
        else
        {
            link_obj.emplace("travel_time", link->getTravelTime());
        }
        add_properties(link_obj, *link);
        links.push_back(link_obj);
    }
    jsonDoc.emplace("links", links);
	if (debug)
	{
		*debug << "done.\n";
		*debug << "done.\n";
	}

    return jsonDoc;
}

