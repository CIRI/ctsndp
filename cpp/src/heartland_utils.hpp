
#pragma once

#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "routes.hpp"
#include <nlohmann/json.hpp>
#include <string>
#include <memory>
#include <iosfwd>


namespace heartland
{
    /*!
     * \brief Test to see if the JSON document represents a service network formatted
     * in the Heartland scheme.
     *
     * @param the filename of the JSON file to test.
     *
     * @return True if the file is a valid Heartland service network,
     * otherwise false
     */
    bool is_valid_service_network_json(nlohmann::json& jsonDoc, const std::string& fileName);

    /*!
     * \brief Test to see if the JSON document represents a shipment data formatted
     * in the Heartland scheme.
     *
     * @param the JSON document to test.
     *
     * @return True if the file is a valid Heartland shipment document,
     * otherwise false
     */
    bool is_valid_shipment_json(nlohmann::json& jsonDoc, const std::string& fileName);

    /*!
     * \brief Create an instance of cServiceNetwork from a JSON document.
     *
     * @param a JSON document formatted in the Heartland scheme representing
     * a service network
     *
     * @return a unique pointer to the class representing the service network
     */
    std::unique_ptr<cServiceNetwork> create_service_network_from_json_doc(nlohmann::json& jsonDoc);

    /*!
     * \brief Create an instance of cServiceNetwork from a Heartland JSON file.
     *
     * @param the filename of the JSON based service network file.
     *
     * @return a unique pointer to the class representing the service network
     */
    std::unique_ptr<cServiceNetwork> create_service_network_from_json_file(const std::string& jsonFileName);
    
    /*!
     * \brief Create an instance of cCommodityShipments from a JSON document.
     *
     * @param a JSON document formatted in the Heartland scheme representing
     * the commodities being shipped arcoss the service network
     *
     * @return a unique pointer to the class representing the commodity shipments
     */
    std::unique_ptr<cCommodityShipments> create_shipments_from_json_doc(nlohmann::json& jsonDoc, 
																		bool no_delays,
																		bool group);

    /*!
     * \brief Create an instance of cCommodityShipments from a Heartland JSON file.
     *
     * @param the filename of the JSON based file.
     *
     * The JSON file in the Heartland scheme
     */
    std::unique_ptr<cCommodityShipments> create_shipments_from_json_file(const std::string& jsonFileName, 
																		bool no_delays,
																		bool group);

    /*!
     * \brief Create an instance of cCommodityShipments from a Heartland JSON file.
     *
     * @param the filename of the JSON based file.
     *
     * The JSON file in the Heartland scheme
     */
    struct sModelInputs_t
    {
        std::unique_ptr<cServiceNetwork> network;
        std::unique_ptr<cCommodityShipments> shipments;
    };
    
    sModelInputs_t create_inputs_from_schedule_file(const std::string& jsonFileName, 
		bool no_delays, bool group);

    /*!
     * \brief Converts the output "routes" data from the CTSNDP optimization
     * to a Heartland JSON based file.
     *
     * @param the "routes" data from the optimization.
     *
     * @return a JSON document formatted in the Heartland scheme
     */
    nlohmann::json convert_routes_to_json_format(const cRoutes& routes, std::ostream* debug);
}
