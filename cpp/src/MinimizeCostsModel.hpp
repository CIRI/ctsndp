//
//  MinimizeCostsModel.hpp
//  commodities_shared
//
//  Created by Brett Feddersen on 11/21/19.
//

#pragma once
#ifndef MinimizeCostsModel_hpp
#define MinimizeCostsModel_hpp

#include "OptimizationProblem.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include <vector>
#include <map>
#include <fstream>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 3 on page 1307 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cMinimizeCostsModel : public cOptimizationProblem
{
public:
    cMinimizeCostsModel(int pass_number, const cPartiallyTimeExpandedNetwork& expandedNetwork,
                        const cServiceNetwork& serviceNetwork, bool verboseMode, bool printTimingInfo);
    ~cMinimizeCostsModel();

	void ignoreRequiredLocations(bool ignore);
    void printX_kij(bool print);

    void BuildModel() override;

    /*
     * Returns the results of the "minimize cost" model.
     */
	double getMinimalCost() const;
	virtual std::tuple<double, double> getMinimalFixedCost() const = 0;
    virtual std::tuple<double, double> getMinimalVariableCost() const = 0;
    const ctsndp::utilization_t& getArcUtilization() const;
    ctsndp::commodity_path_node_t returnCommodityPathsByNode() const;
    ctsndp::commodity_path_arc_t  returnCommodityPathsByArc() const;

protected:
	virtual void ConstructOptimizatonVariables() = 0;
	virtual void BuildOptimizatonProblem() = 0;

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
	virtual void constraint1_ConservationOfFlow(std::ostream* debug) = 0;
	virtual void constraint2_EnsureSufficientCapacityPerFlow(std::ostream* debug) = 0;
	virtual void constraint2a_EnsureSufficientCapacityPerArc(std::ostream* debug) = 0;
	virtual void constraint5_TravelTime(std::ostream* debug) = 0;
	virtual void constraint5a_RestrictionOfFlow(std::ostream* debug) = 0;
	virtual void constraint5b_RequiredFlows(std::ostream* debug) = 0;

	//
	// Helper methods
	//

	/**
	 */
	unsigned int to_kij_index(cPartiallyTimeExpandedNetworkArc* const& arc, ctsndp::commodity_ptr_t commodity) const;

	/**
	 * Return true to stop a commodity from using this node in its possible
	 * path. 
	 *
	 * Called by constraint1_ConservationOfFlow!
	 */
	bool RestrictCommodityFromNode(ctsndp::commodity_ptr_t commodity,
									const ctsndp::node_ptr_t node) const;

	/**
	 * Return true to stop a commodity from using the inflow arc
	 * to a node in its possible path.
	 *
	 * Called by constraint1_ConservationOfFlow!
	 */
	bool RestrictCommodityInflowToNode(ctsndp::commodity_ptr_t commodity,
		const ctsndp::arc_ptr_t arc) const;

	/**
	 * Return true to stop a commodity from using the outflow arc
	 * to a node in its possible path.
	 *
	 * Called by constraint1_ConservationOfFlow!
	 */
	bool RestrictCommodityOutflowFromNode(ctsndp::commodity_ptr_t commodity,
		const ctsndp::arc_ptr_t arc) const;

	/**
	 * Return true to stop a commodity from using the arc in its possible path.
	 *
	 * Called by constraint5a_RestrictionOfFlow!
	 */
	bool RestrictCommodityFromArc(ctsndp::commodity_ptr_t commodity,
									const ctsndp::arc_ptr_t arc) const;

	/**
	 * Return true to stop a commodity from using the holdover arc in its
	 * possible path.
	 *
	 * Called by constraint5a_RestrictionOfFlow!
	 */
	bool RestrictCommodityFromArc(ctsndp::commodity_ptr_t commodity,
									const ctsndp::holdover_arc_ptr_t arc) const;

	//
    // Debug Methods
    //


    //
    // Options
    //
	bool mIgnoreRequireLocations;
    
    //
    // Inputs
    //
    const cPartiallyTimeExpandedNetwork& mPartiallyTimeExpandedNetwork;
    const cServiceNetwork&               mServiceNetwork;

	typedef std::map<ctsndp::sCommodityArc, unsigned int, ctsndp::compareCommodityArcByPtrId>   kij_index_t;

	std::size_t mSizeOfKij;
	kij_index_t mKij_Indexes;

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    std::vector<unsigned int> q_k;      // the quantity of commodity k to transport
    std::vector<unsigned int> w_kij;    // the unit capacity of the arc (i,j) (the arc can hold w units of transportation)
    std::vector<unsigned int> u_kij;    // the variable capacity of the arc (i,j) (a unit of transportation can hold u units of commodity)
    std::vector<double> f_kij;          // the fixed costs of utilizing the arc (i,j) by commodity k
    std::vector<double> c_kij;          // the variable cost of utilizing the arc (i,j) by commodity k
    std::vector<double> tau_ij;         // the actual travel time of a commodity utilizing the arc (i,j)

    //
    // Variables to hold the output data
    //
    double mMinimalCost;

    struct compareBundleById
    {
        bool operator()(ctsndp::commodity_ptr_t& lhs, ctsndp::commodity_ptr_t& rhs) const
        {
            return lhs->getId() < rhs->getId();
        }
    };


    // the path commodities through the service network by nodes visited
    ctsndp::commodity_path_node_t mP_ki;

    // the path commodities through the service network by arc
    ctsndp::commodity_path_arc_t mP_ka;

    // the utilization of arc (i,j).  Note: this is the saved version of y_ij
    ctsndp::utilization_t mUtilization_ij;

    //
    // Debug Variables
    //
	bool mOutputX_kij;
	bool mOutputY_ij;

	bool mVerboseMode;
    bool mPrintTimingInfo;

public:
	std::string mLogPath;
	bool mLogX_kij;
	bool mLogY_ij;
	bool mLogConstraints;
};

#endif /* MinimizeCostsModel_hpp */
