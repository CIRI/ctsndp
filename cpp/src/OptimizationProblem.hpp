//
//  OptimizationProblem.hpp
//  An abstract class to provide the core interface
//  to any of the optimizations used within the
//  CTSNDP
//
//  Created by Brett Feddersen on 03/24/20.
//

#pragma once
#ifndef OptimizationProblem_hpp
#define OptimizationProblem_hpp

#include <iosfwd>
#include <string>

/*
 * An abstract base class to support any optimization problem.
 */
class cOptimizationProblem
{
public:

	virtual void setNumericFocus(int numericFocus) = 0;
	virtual void setTimeLimit_sec(double timeLimit_sec) = 0;
	virtual void logToConsole(bool log) = 0;
	virtual void logFilename(const std::string& filename) = 0;
	virtual void readTuningParameters(const std::string& filename) = 0;

	/*
	 * The output stream for all progress messages
	 */
	void setProgressOutput(std::ostream& out);

	virtual void outputDebugMsgs(bool msgs) = 0;

	/*
     * The output stream for all debug messages
     */
	void setDebugOutput(std::ostream& out);

    /*
     * Build model
     *
     * Builds the optimization model: constraints and objective function.
     */
    virtual void BuildModel() = 0;

    /*
     * Optimize the model
     *
     * \return true if the model could reach a feasible solution.
     */
    virtual bool Optimize() = 0;
    
    /*
     * Modifies the optimizer model object to create a feasibility relaxation.
     * Note: that you need to call optimize on the result to compute the actual relaxed 
	 * solution.  The feasibility relaxation is a model that, when solved, minimizes the
	 * amount by which the solution violates the bounds and linear constraints of the
	 * original model.
     *
     * \return true if the relaxation could be created
     */
	virtual bool RelaxConstraints() = 0;
    
	/*
	 * If the optimizer was able to create a feasibility relaxation, this method will
	 * find all of the constraint violations.
	 */
	virtual void FindConstraintViolations(std::ostream& out) = 0;

    /*
     * Compute an Irreducible Inconsistent Subsystem (IIS). An IIS is a subset of the 
	 * constraints and variable bounds with the following properties:
     *      1. the subsystem represented by the IIS is infeasible, and
     *      2. if any of the constraints or bounds of the IIS is removed, the subsystem
	 *         becomes feasible.
     *
     * \return a string of all the constraints or bounds that could not be satisfied
     */
	virtual std::string ComputeIIS() = 0;

	/*
	 * The path to were the log files should be written
	 */
	std::string mLogPath;

protected:
	explicit cOptimizationProblem(int pass_number);
    ~cOptimizationProblem() = default;

    //
    // Debug Variables
    //
    bool mOutputDebugMsgs;
	std::ostream* mpDebugOutput;

	std::ostream* mpProgress;

	/*
	 * The current pass number
	 */
	const int mPass;
};

#endif /* GurobiModel_hpp */
