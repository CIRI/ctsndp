
#pragma once
#ifndef GUROBI_ARRAY_HPP
#define GUROBI_ARRAY_HPP

#include <gurobi_c++.h>
#include <algorithm>
#include <exception>
#include <string>


// Gurobi Array class
template<char VTYPE>
class GRBArray
{
public:
    typedef GRBVar                                  value_type;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef GRBVar*                                 iterator;
    typedef const iterator                          const_iterator;
    typedef std::size_t                             size_type;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;
    
	GRBArray();
	GRBArray(GRBModel& model, size_type size);
	GRBArray(const GRBArray&) = delete;
	GRBArray(const GRBArray&& other) = delete;
	~GRBArray();

    size_type size() const;
    
//    iterator begin() const;
//    iterator end() const;

	GRBVar& operator[](size_type pos);
    const GRBVar& operator[](size_type pos) const;

    // Modifiers
	void clear();
    void allocate(GRBModel& model, size_type size);

	void set_lower_bound(double lb);
	void set_upper_bound(double ub);
	void set_objective(double obj);

	void set_name(size_type pos, const std::string name);

private:
    GRBVar* mpData;
    size_type mSize;
};



///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

template<char VTYPE>
GRBArray<VTYPE>::GRBArray() : mpData(nullptr), mSize(0)
{
}

template<char VTYPE>
GRBArray<VTYPE>::GRBArray(GRBModel& model, size_type size) : mpData(nullptr), mSize(size)
{
	mpData = model.addVars(size, VTYPE);
}

template<char VTYPE>
GRBArray<VTYPE>::~GRBArray()
{
    clear();
}

template<char VTYPE>
typename GRBArray<VTYPE>::size_type GRBArray<VTYPE>::size() const
{
    return mSize;
}

template<char VTYPE>
GRBVar& GRBArray<VTYPE>::operator[](size_type pos)
{
    return *(mpData + pos);
}

template<char VTYPE>
const GRBVar& GRBArray<VTYPE>::operator[](size_type pos) const
{
    return *(mpData + pos);
}

template<char VTYPE>
void GRBArray<VTYPE>::clear()
{
	delete[] mpData;
	mpData = nullptr;
	mSize = 0;
}

template<char VTYPE>
void GRBArray<VTYPE>::allocate(GRBModel& model, size_type size)
{
	if (mpData)
	{
		throw std::logic_error("GRBArray already allocated!");
	}

    mpData = model.addVars(size, VTYPE);
	mSize = size;
	model.update();
	return;
}

template<char VTYPE>
void GRBArray<VTYPE>::set_lower_bound(double lb)
{
	std::for_each_n(mpData, mSize, 
		[lb](GRBVar& var) {var.set(GRB_DoubleAttr_LB, lb); });
}

template<char VTYPE>
void GRBArray<VTYPE>::set_upper_bound(double ub)
{
	std::for_each_n(mpData, mSize,
		[ub](GRBVar& var) {var.set(GRB_DoubleAttr_UB, ub); });
}

template<char VTYPE>
void GRBArray<VTYPE>::set_objective(double obj)
{
	std::for_each_n(mpData, mSize,
		[obj](GRBVar& var) {var.set(GRB_DoubleAttr_Obj, obj); });
}

template<char VTYPE>
void GRBArray<VTYPE>::set_name(size_type pos, const std::string name)
{
	(mpData + pos)->set(GRB_StringAttr_VarName, name.c_str());
}


#endif