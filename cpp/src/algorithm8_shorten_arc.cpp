
#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"

#include <cassert>
#include <set>

namespace
{
	// Find lower bound such that t <= bound
	double lower_bound(cPartiallyTimeExpandedNetworkNode* node, const cCommodityBundle* k, double bound)
	{
		double t = node->getTime();
		assert(t >= bound);

		if (t == bound)
			return bound;

		// Find lower bound such that t <= bound
		for (auto outflow : node->getOutflows())
		{
			if (!outflow->isDelayArc())
				continue;
			auto arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(outflow);
			if (arc->hasCommodity(k))
			{
				t = arc->getDestinationNode()->getTime();
				if (t <= bound)
					break;
					
				return lower_bound(arc->getDestinationNode(), k, bound);
			}
		}

		return t;
	}

	// Find upper bound such that t >= bound
	double upper_bound(cPartiallyTimeExpandedNetworkNode* node, const cCommodityBundle* k, double bound)
	{
		double t = node->getTime();
		assert(t >= bound);

		if (t == bound)
			return bound;

		// Find upper bound such that t >= bound
		for (auto outflow : node->getOutflows())
		{
			if (!outflow->isDelayArc())
				continue;
			auto arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(outflow);
			if (arc->hasCommodity(k))
			{
				if (arc->getDestinationNode()->getTime() < bound)
					break;
					
				return upper_bound(arc->getDestinationNode(), k, bound);
			}
		}

		return t;
	}

	ctsndp::delay_arc_ptr_t find_delay_arc_inflow(const cPartiallyTimeExpandedNetworkNode* node, 
													ctsndp::commodity_ptr_t k)
	{
		for (auto arc : node->getInflows())
		{
			if (! arc->isDelayArc())
				continue;

			ctsndp::delay_arc_ptr_t a = static_cast<ctsndp::delay_arc_ptr_t>(arc);
			if (a->hasCommodity(k))
				return a;
		}
		return nullptr;
	}
}

/**
 * This algorithm is similar to algorithm 3 listed on page 1312 except to shorten arcs instead
 * of lengthening them.
 * Requires: DelayArc((i,t),(i,t')) in 𝒜t
 * Modifies: The partially time expanded network 
 **/
void ctsndp::shorten_arc(cPartiallyTimeExpandedNetwork& network,
						  ctsndp::arcs_to_shorten_t&& arcs,
                          std::ostream* debug)
{
    if (debug)
        *debug << "Shortening arcs:\n";
	
	std::set<cPartiallyTimeExpandedNetworkArc*> arcs_to_delete;
	std::set<const cCommodityBundle*> commodities_to_fix;

	while (!arcs.empty())
    {
		auto it = arcs.begin();

		cPartiallyTimeExpandedNetworkArc* arc = it->ij;
		double t_new = it->t_new;
		auto k = it->k;
		commodities_to_fix.insert(k);
		arcs.erase(it);

        // arc must be an element of 𝒜t
		assert(!arc->isStorageArc());

        auto node = arc->getDestination();
		assert(node->isDelayNode());
		const auto delay_node = static_cast<cPartiallyTimeExpandedNetworkDelayNode*>(node);

        if (debug)
            *debug << *arc << " to " << t_new << "\n";

        // Does the node at t_new already exists? Yes, we
        // can move our arc to that node
        if (!network.findDelayNode(delay_node->getServiceLocation(), t_new))
        {
			// We need to find the lower bound, t_k, and upper bound, t_k+1.  We can't use
			// the normal set of timepoints as we need to follow the delay arcs which can
			// skip nodes.

			// Find upper bound such that t_new <= t_k_1
			double t_k_1 = upper_bound(delay_node, k, t_new);

            // Find lower bound such that t_k <= t_new
            double t_k = lower_bound(delay_node, k, t_new);
            
			refine_delays(network, *delay_node, t_k, t_new, t_k_1, debug);
			auto tmp = restore_delays(network, *delay_node, t_k, t_new, debug);

			for (auto arc : tmp)
			{
				auto it = arcs.begin();
				for (; it != arcs.end(); ++it)
				{
					if (it->ij == arc)
					{
						arcs.erase(it);
						break;
					}
				}

				arcs_to_delete.insert(arc);
			}
        }
        else
        {
            const auto& dest = network.findDelayNode(arc->getDestinationServiceLocation(), t_new);
            network.addArc(arc->getSource(), dest, arc);
			arcs_to_delete.insert(arc);
        }
    }
    arcs.clear();

	for (auto arc : arcs_to_delete)
	{
		network.deleteArc(arc);
	}
	arcs_to_delete.clear();

    if (debug)
        *debug << "complete!" << std::endl;
}
