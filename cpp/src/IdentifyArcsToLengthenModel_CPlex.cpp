//
//  IdentifyArcsToLengthenModel_CPlex.cpp
//
//  Created by Brett Feddersen on 03/25/20.
//

#include "IdentifyArcsToLengthenModel_CPlex.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_utils_priv.hpp"
#include "cplex_utils_priv.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>
#include <fstream>
#include <cassert>


namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;

#if ADD_DEBUG_NAMES
    std::string strConstraint10(const cCommodityBundle& k1, const cCommodityBundle& k2, const cPartiallyTimeExpandedNetworkArc& arc)
    {
        std::string constrName = "C10_EnsureConsolidations(";
        constrName += k1.getUniqueName();
        constrName += ", ";
        constrName += k2.getUniqueName();
        constrName += ": " + to_string(arc) + ")";
        return constrName;
    }
#endif
}

using namespace ctsndp;

cIdentifyArcsToLengthenModel_CPlex::cIdentifyArcsToLengthenModel_CPlex(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_node_t& p_ki,
                                         const ctsndp::commodity_path_arc_t& p_ka,
										 const ctsndp::consolidation_groups_t& j_ak,
                                         bool verboseMode, bool printTimingInfo)
:
    cCPlexModel("IdentifyArcsToLengthen"),
	cIdentifyArcsToLengthenModel(pass_number, expandedNetwork, serviceNetwork, p_ki, p_ka,
									j_ak, verboseMode, printTimingInfo)
{
//    mModel.set(GRB_DoubleParam_FeasibilityTol, 0.001);
}

cIdentifyArcsToLengthenModel_CPlex::~cIdentifyArcsToLengthenModel_CPlex()
{
}

void cIdentifyArcsToLengthenModel_CPlex::setNumericFocus(int numericFocus)
{
	cCPlexModel::setNumericFocus(numericFocus);

}

void cIdentifyArcsToLengthenModel_CPlex::setTimeLimit_sec(double timeLimit_sec)
{
	cCPlexModel::setTimeLimit_sec(timeLimit_sec);
}

void cIdentifyArcsToLengthenModel_CPlex::logToConsole(bool log)
{
	cCPlexModel::logToConsole(log);
}

void cIdentifyArcsToLengthenModel_CPlex::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cIdentifyArcsToLengthenModel_CPlex::logFilename(const std::string& filename)
{
	cCPlexModel::logFilename(filename, ".identify_arcs");
}

void cIdentifyArcsToLengthenModel_CPlex::readTuningParameters(const std::string& filename)
{
	cCPlexModel::readTuningParameters(filename);
}

bool cIdentifyArcsToLengthenModel_CPlex::Optimize()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Identify Arcs to Lengthen Problem\" ";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	mSolver.extract(mModel);

	mSolver.solve();

	auto status = mSolver.getStatus();

	if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
		case IloAlgorithm::Optimal:
		{
            auto objVal = mSolver.getObjValue();

            if (mOutputIdentifyArcsTimeInfo)
            {
                DumpIdentifyArcsTimeInfo(*mpDebugOutput);
            }

			if (mOutputCommodityScheduleInfo)
            {
                DumpCommoditySchedules(*mpDebugOutput);
            }

			if (mLogIdentifyArcsTimeInfo)
			{
				std::string filename = mLogPath + "IdentifyArcsTimeInfo_";
				filename += std::to_string(mPass) + ".txt";
				std::ofstream file(filename);
				DumpIdentifyArcsTimeInfo(file);
				file.close();
			}

			if (mLogCommodityScheduleInfo)
			{
				std::string filename = mLogPath + "IdentifyArcsCommoditySchedule_";
				filename += std::to_string(mPass) + ".txt";
				std::ofstream file(filename);
				DumpCommoditySchedules(file);
				file.close();
			}

            SaveDispatchAndTravelTimes();

            if (objVal == 0.0)
            {
                if (mOutputDebugMsgs)
                {
                    *mpDebugOutput << "none...";
                }
                
                return true;
            }
            
            FindArcsToLengthen();
   
            if (mOutputDebugMsgs)
            {
                *mpDebugOutput << mArcsToLengthen.size() << "...";
            }
            
            return true;
        }
            
		case IloAlgorithm::InfeasibleOrUnbounded:
		case IloAlgorithm::Infeasible:
		{
            std::string msg = "The following constraint(s) cannot be satisfied:\n";
            throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
            break;
        }
    }

    return false;
}

bool cIdentifyArcsToLengthenModel_CPlex::RelaxConstraints()
{
	return cCPlexModel::RelaxConstraints();
}

std::string cIdentifyArcsToLengthenModel_CPlex::ComputeIIS()
{
	return cCPlexModel::ComputeIIS();
}

void cIdentifyArcsToLengthenModel_CPlex::FindConstraintViolations(std::ostream& out)
{
    bool printNone = true;
	out << "\nScanning for tau constraints \"tau_ij >= tau_bar_kij\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        const auto& path = mP_ka.at(k);
        if (path.size() <= 1)
            continue;
        auto last = path.end();

        for (auto it = path.begin(); it != last; ++it)
        {
            auto& arc = *it;

            double tau = tau_ij[to_index(arc)];
            double tau_bar = tau_bar_kij[{k, arc}];

			double delta = tau - tau_bar;
			if (arc->connectedToDelayNode())
				delta = tau_bar - tau;

            if (delta < -CONSTRAINT_TOLERANCE)
            {
				out << tau << " < " << tau_bar;
				out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*arc);
				out << " delta = " << delta << "\n";
                printNone = false;
            }
        }
    }

    if (printNone)
		out << "None.\n";
    printNone = true;
   
	out << "\nScanning for constraint 6 \"theta_kij >= tau_ij * (1 - sigma_kij)\" violations:\n";

   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       const auto& path = mP_ka.at(k);
       if (path.size() <= 1)
           continue;
       auto last = path.end();
       
	   out << std::setprecision(10);
       
       for (auto it = path.begin(); it != last; ++it)
       {
           auto& arc = *it;

		   // TERM: tau_ij * (1 - sigma_kij)
           double rhs = tau_ij[to_index(arc)] * (1 - as_int(sigma_kij[{k, arc}]));
           double theta = as_double(theta_kij[{k, arc}]);
		   if (arc->isDelayArc())
		   {
			   double delta = rhs - theta;
			   if (delta < -CONSTRAINT_TOLERANCE)
			   {
				   out << theta << " <= " << tau_ij[to_index(arc)];
				   out << " * (1 - " << as_int(sigma_kij[{k, arc}]) << ") for arc " << to_string(*arc) << " delta = ";
				   out << delta << "\n";
				   printNone = false;
			   }
		   }
		   else
		   {
			   double delta = theta - rhs;
			   if (delta < -CONSTRAINT_TOLERANCE)
			   {
				   out << theta << " >= " << tau_ij[to_index(arc)];
				   out << " * (1 - " << as_int(sigma_kij[{k, arc}]) << ") for arc " << to_string(*arc) << " delta = ";
				   out << delta << "\n";
				   printNone = false;
			   }
		   }
       }
   }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 7 \"gamma_ki + theta_kij <= gamma_ki_1\" violations:\n";
   
   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       const auto& path = mP_ka.at(k);
       if (path.size() <= 1)
           continue;

       auto n = path.size()-1;
	   auto& nodes = mP_ki.at(k);
           
       for (std::size_t i = 0; i < n; ++i)
       {
           auto& arc = path[i];
           auto lhs = as_double(gamma_ki[{k, arc->getSource()}]) + as_double(theta_kij[{k, arc}]);
           auto rhs = as_double(gamma_ki[{k, nodes[i+1]}]);
           double delta = lhs - rhs;
           
           if (delta > CONSTRAINT_TOLERANCE)
           {
               out << as_double(gamma_ki[{k, arc->getSource()}]) << " + " << as_double(theta_kij[{k, arc}]);
               out << " <= " << rhs << " for commodity " << k->getFullyQualifiedName() << ", arc " << to_string(*arc) << " delta = ";
               out << delta << "\n";
               printNone = false;
           }
       }
   }
   
    if (printNone)
        out << "None.\n";
    printNone = true;
   
    out << "\nScanning for constraint 8 \"e_k <= gamma_ko\" violations:\n";
   
   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       auto& nodes = mP_ki.at(k);
       if (nodes.empty())
           continue;
           
       auto& loc = nodes.front();
       auto gamma1 = as_double(gamma_ki[{k, loc}]);
       double delta = k->getEarliestAvailableTime() - gamma1;

       if (delta > CONSTRAINT_TOLERANCE)
       {
           out << k->getEarliestAvailableTime() << " <= " << gamma1;
           out << " for commodity " << k->getFullyQualifiedName() << " delta = ";
           out << delta << "\n";
           printNone = false;
       }
   }
   
    if (printNone)
        out << "None.\n";
    printNone = true;

   out << "\nScanning for constraint 9 \"gamma_ki + theta_kij <= l_k\" violations:\n";
   
   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       const auto& arcs = mP_ka.at(k);
       if (arcs.empty())
           continue;
           
       auto& arc = arcs.back();
       auto gamma = as_double(gamma_ki[{k, arc->getSource()}]);
       auto theta = as_double(theta_kij[{k, arc}]);
       double ldt = k->getLatestDeliveryTime();
       double delta = (gamma + theta) - ldt;

       if (delta > CONSTRAINT_TOLERANCE)
       {
           out << gamma << " + " << theta << " <= " << ldt;
		   out << " for commodity " << k->getFullyQualifiedName();
		   out << ", " << *arc << " delta = ";
           out << delta << "\n";
           printNone = false;
       }
   }
   
    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 10 \"gamma_k1i = gamma_k2i\" violations:\n";

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					node_ptr_t i = arc->getSource();

					auto gamma1 = as_double(gamma_ki[{k1, i}]);
					auto gamma2 = as_double(gamma_ki[{k2, i}]);
					double delta = std::fabs(gamma1 - gamma2);

					if (delta > CONSTRAINT_TOLERANCE)
					{
						out << gamma1 << " != " << gamma2;
						out << " at node " << to_string(*i);
						out << ", k1 = " << k1->getName();
						out << " and k2 = " << k2->getName();
						out	<< " delta = ";
						out << delta << "\n";
						printNone = false;
					}
				}
			}
		}
	}

    if (printNone)
        out << "None.\n";
}

//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cIdentifyArcsToLengthenModel_CPlex::FindArcsToLengthen()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tFinding Arcs to Lengthen";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    mArcsToLengthen.clear();

    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        const auto& path = mP_ka.at(k);
        for(int i = 0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            if (as_bool(sigma_kij[{k, arc}]))
            {
				mArcsToLengthen.insert(arc);
            }
        }
    }

    if (mVerboseMode || mPrintTimingInfo)
    {
		if (mArcsToLengthen.size() > 0)
			*mpDebugOutput << "found " << mArcsToLengthen.size() << " arcs, ";

        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

void cIdentifyArcsToLengthenModel_CPlex::SaveDispatchAndTravelTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tSaving Dispatch and Travel Times";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    mDispatchTime_ki.clear();
    
    // gamma_ki represents the dispatch time of commodity k at node i
    for(auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;

		if (path.empty())
			continue;

		/*
		 * For the CPlex solver, we cannot access the gamma at the destination.
		 * That number has no meaning, but CPlex will throw an exception
		 */
		auto last = --(path.end());
        for (auto it = path.begin(); it != last; ++it)
        {
			auto& node = *it;
            mDispatchTime_ki[{k, node}] = as_double(gamma_ki[{k, node}]);
        }
    }

    mTravelTime_kij.clear();
    
    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto& path = mP_ka.at(k);
        if (path.size() <= 1)
            continue;
        auto last = path.end();
        
        for (auto it = path.begin(); it != last; ++it)
        {
            auto& arc = *it;
            mTravelTime_kij[{k, arc}] = as_double(theta_kij[{k, arc}]);
        }
    }
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}


//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cIdentifyArcsToLengthenModel_CPlex::ConstructOptimizatonVariables()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variables";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	// gamma_ki represents the dispatch time of commodity k at node i
	for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
	{
		auto& k = it->first;
		auto& path = it->second;
		for (auto& node : path)
		{
			// Setting the lower bound of gamma to zero enforces
			// constraint 11 list on page 1312.
			gamma_ki[{k, node}] = IloNumVar(mEnv, 0, +IloInfinity);
#if ADD_DEBUG_NAMES
			std::string varName = "gamma_ki{" + k->getUniqueName() + ", "
				+ node->getName() + "}";
			std::replace(varName.begin(), varName.end(), ' ', '_');
			// The lower bound of zero enforces constraint 11 for the algorithm list on page 1312
			gamma_ki[{k, node}].setName(varName.c_str());
#endif
		}
	}

	for (auto k_it = mP_ka.begin(); k_it != mP_ka.end(); ++k_it)
	{
		auto& k = k_it->first;
		auto& path = k_it->second;
		auto last = path.end();

		for (auto p_it = path.begin(); p_it != last; ++p_it)
		{
			auto& arc = *p_it;
			assert(!arc->isHoldoverArc());
			
			/*
			 *  Delay arc connect a node in the future back to our destination
			 *  node resulting in a negative travel time.  We set the upper bound
			 *  to zero because a travel time of greater than zero means there is
			 *  no need to use the delay arc.
			 */
			if (arc->isDelayArc())
			{
				/*
				 * Tau bar is the "time" difference between the source and destination nodes,
				 * and represents lower bound of the theta, "travel time".  However, delay nodes
				 * are placed far into the future.  In these cases, tau bar represents the
				 * maximum limit of theta.
				 */
				double tau_bar = std::min(tau_bar_kij[{k, arc}], 0.0);

				theta_kij[{k, arc}] = IloNumVar(mEnv, -IloInfinity, tau_bar);
			}
			else
			{
				//
				// Perform constraint 12 for the algorithm listed on page 1312
				//
				// Tau bar is the "time" difference between the source and destination nodes,
				// and represents lower bound of the theta, "travel time".
				//
				double tau_bar = std::max(tau_bar_kij[{k, arc}], 0.0);

				theta_kij[{k, arc}] = IloNumVar(mEnv, tau_bar, +IloInfinity);
			}

			sigma_kij[{k, arc}] = IloBoolVar(mEnv);

			//
			// Perform the "speed up" given on page 1313, end of second paragraph
			//
			if (!arc->connectedToDelayNode())
			{
				if (arc->getDeltaTime() == arc->getActualTravelTime())
				{
					sigma_kij[{k, arc}].setUB(0.0);
				}
			}

#if ADD_DEBUG_NAMES
			std::string varName = "theta_kij:" + k->getShipmentName() + "," + to_string(*arc);
			std::replace(varName.begin(), varName.end(), ' ', '_');
			theta_kij[{k, arc}].setName(varName.c_str());

			varName = "sigma_kij:" + k->getShipmentName() + "," + to_string(*arc);
			std::replace(varName.begin(), varName.end(), ' ', '_');
			sigma_kij[{k, arc}].setName(varName.c_str());
#endif
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

void cIdentifyArcsToLengthenModel_CPlex::BuildOptimizatonProblem()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	IloExpr allowArcTooShortVars(mEnv);

	// for all commodities
	for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
	{
		const auto& path = mP_ka.at(k);
		if (path.empty())
			continue;

		for (int i = 0; i < path.size() - 1; ++i)
		{
			allowArcTooShortVars += sigma_kij[{k, path[i]}];
		}
	}

	mModel.add(IloMinimize(mEnv, allowArcTooShortVars));

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================

//
// Perform constraint 6 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_CPlex::constraint6_CountArcsWithShortenTravelTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint6: Count Arcs With Shorten Travel Times";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& path = mP_ka.at(k);
			if (path.size() <= 1)
				continue;

			auto last = path.end();

			for (auto it = path.begin(); it != last; ++it)
			{
				auto& arc = *it;
				// TERM: tau_ij * (1 - sigma_kij)
				IloExpr rhs(mEnv);
				rhs += tau_ij[to_index(arc)] * (1 - sigma_kij[{k, arc}]);

				if (arc->isDelayArc())
				{
					IloConstraint constraint = (theta_kij[{k, arc}] <= rhs);

#if ADD_DEBUG_NAMES
					std::string constrName = "C6_CountArcsWithShortenTravelTimes(" + to_string(*arc) + ")";
					constraint.setName(constrName.c_str());
#endif
					mModel.add(constraint);
				}
				else
				{
					IloConstraint constraint = (theta_kij[{k, arc}] >= rhs);

#if ADD_DEBUG_NAMES
					std::string constrName = "C6_CountArcsWithShortenTravelTimes(" + to_string(*arc) + ")";
					constraint.setName(constrName.c_str());
#endif
					mModel.add(constraint);
				}
			}
		}
	}
	catch (IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 7 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_CPlex::constraint7_EnsureAllowableDispatchTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint7: Ensure Allowable Dispatch Times";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& path = mP_ka.at(k);
			if (path.size() <= 1)
				continue;

			const auto& nodes = mP_ki.at(k);
			auto n = path.size()-1;
   	     	for (std::size_t i = 0; i < n; ++i)
    	    {
        	    auto& arc = path[i];

				IloExpr lhs(mEnv);
				lhs += gamma_ki[{k, arc->getSource()}] + theta_kij[{k, arc}];

	            const auto& next = nodes[i+1];
				IloConstraint constraint = (lhs <= gamma_ki[{k, next}]);

#if ADD_DEBUG_NAMES
				std::string constrName = "C7_EnsureAllowableDispatchTimes(";
				constrName += k->getFullyQualifiedName() + ", " + next->getName() + ")";
				constraint.setName(constrName.c_str());
#endif
				mModel.add(constraint);
			}
		}
	}
	catch (IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 8 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_CPlex::constraint8_EnsureDispatchIsAfterAvailable()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint8: Ensure Dispatch Is After Available";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
	    {
		    const auto& nodes = mP_ki.at(k);
	        if (nodes.empty())
			    continue;
        
		    auto& loc = nodes.front();
	        double eat = k->getEarliestAvailableTime();
        
			const auto& path = mP_ka.at(k);
		    for (auto& arc : path)
	        {
				if (arc->arcCapacityIsLimited())
			        eat = arc->getSource()->getTime();
		        break;
	        }

			gamma_ki[{k, loc}].setLB(eat);
		}
	}
	catch (IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 9 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_CPlex::constraint9_EnsureArrivalBeforeDue()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint9: Ensure Arrival Before Due";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        const auto& arcs = mP_ka.at(k);
        if (arcs.empty())
            continue;
        
        auto& arc = arcs.back();
		IloExpr lhs(mEnv);
		lhs += gamma_ki[{k, arc->getSource()}] + theta_kij[{k, arc}];
        double ldt = k->getLatestDeliveryTime();

		IloConstraint constraint = (lhs <= ldt);

#if ADD_DEBUG_NAMES
        std::string constrName = "C9_EnsureArrivalBeforeDue(";
        constrName += k->getFullyQualifiedName() + ", " + arc->getDestinationServiceName() + ")";
		constraint.setName(constrName.c_str());
#endif
		mModel.add(constraint);
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 10 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_CPlex::constraint10_EnsureConsolidations()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint10: Ensure Consolidations";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

        node_ptr_t i = arc->getSource();

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto k_it = commodities.begin();
			auto& k1 = *k_it;

			auto& gamma_k1i = gamma_ki[{k1, i}];

			for (++k_it; k_it != commodities.end(); ++k_it)
			{
				auto& k2 = *k_it;
				try
				{
					IloConstraint constraint = (gamma_k1i == gamma_ki[{k2, i}]);

#if ADD_DEBUG_NAMES
					std::string constrName = strConstraint10(*k1, *k2, *arc);
					constraint.setName(constrName.c_str());
#endif
					mModel.add(constraint);
				}
				catch (const IloException& e)
				{
					std::string msg(e.getMessage());
					if (gamma_ki.find({ k2, i }) == gamma_ki.end())
					{
						msg += ": gamma_k2i{";
						msg += k2->getUniqueName();
					}
					else if (gamma_ki.find({ k1, i }) == gamma_ki.end())
					{
						msg += ": gamma_k1i{";
						msg += k1->getUniqueName();
					}
					else
					{
						msg += ": unknown{";
						msg += k1->getUniqueName();
						msg += ", ";
						msg += k2->getUniqueName();
					}
					msg += ", " + i->getName() + "}";
					throw std::logic_error(msg);
				}
			}
		}
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}


//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

void cIdentifyArcsToLengthenModel_CPlex::DumpIdentifyArcsTimeInfo(std::ostream& out)
{
    out << std::endl;
    out << "\n*** Times ***\n";
    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        if (path.empty())
            continue;
        
        for (int i = 0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            out << k->getUniqueName() << ", " << *arc;
            out << ": tau_ij= " << tau_ij[to_index(arc)];
            out << ": tau_bar_ij= " << tau_bar_kij[{k, arc}];
            out << ", theta_kij= " << as_double(theta_kij[{k, arc}]);
            out << ", gamma1_ki= " << as_double(gamma_ki[{k, arc->getSource()}]);
            out << ", sigma_kij= " << as_int(sigma_kij[{k, arc}]) << "\n";
        }
        
        out << "\n";
    }
    return;
    
    out << "*** Dispatch times ***\n" << std::endl;
    for(auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        for (auto& node : path)
        {
            out << k->getFullyQualifiedName() << "," << node->getName() << "= " << as_double(gamma_ki[{k, node}]) << std::endl;
        }
    }
    
    out << "\n*** Travel times ***\n" << std::endl;
    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        if (path.empty())
            continue;
        
		auto& nodes = mP_ki.at(k);

        for (int i = 0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            out << k->getFullyQualifiedName() << "," << to_index(arc) << " : " << as_double(theta_kij[{k, arc}]);
            out << " >= " << tau_ij[to_index(arc)] << " and " << tau_bar_kij[{k, arc}] << " @ " << as_int(sigma_kij[{k, arc}]) << std::endl;
            out << k->getFullyQualifiedName() << "," << to_index(arc) << " :  ";
            out << as_double(gamma_ki[{k, arc->getSource()}]) << " + ";
            out << as_double(theta_kij[{k, arc}]) << " <= ";
            out << as_double(gamma_ki[{k, nodes[i+1]}]) << std::endl;
        }
    }
    
    out << "\n*** End times ***\n" << std::endl;
    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        if (path.empty())
            continue;
        
        auto& arc = path.back();
        
        out << k->getFullyQualifiedName() << "," << to_index(arc) << " :  ";
        out << as_double(gamma_ki[{k, arc->getSource()}]) << " + ";
        out << as_double(theta_kij[{k, arc}]) << " <= ";
        out << k->getLatestDeliveryTime() << " and ";
        out << tau_bar_kij[{k, arc}] << std::endl;
    }
}

void cIdentifyArcsToLengthenModel_CPlex::DumpCommoditySchedules(std::ostream& out)
{
    out << "\n";
    
    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        DumpCommoditySchedule(out, k);
    }
}

void cIdentifyArcsToLengthenModel_CPlex::DumpCommoditySchedule(std::ostream& out, const cCommodityBundle* const k)
{
    out << k->getFullyQualifiedName() << " " << k->getSourceName();
    out << " (eat: " << k->getEarliestAvailableTime() << ")";
    out << " -> " << k->getDestinationName();
    out << " (ldt: " << k->getLatestDeliveryTime() << ")\n";
    out << "Schedule:\n";
    const auto& path = mP_ka.at(k);

    if (path.empty())
    {
        out << "\tNONE!\n";
        return;
    }
    
    for(int i = 0; i < path.size()-1; ++i)
    {
        auto& arc = path[i];
        
        double gamma = as_double(gamma_ki[{k, arc->getSource()}]);
        out << "\t" << arc->getSourceServiceName() << ", departs: " << gamma;
		double travel_time = as_double(theta_kij[{k, arc}]);
		
		auto* link = arc->getServiceLink();
		if (link)
		{
			out << " == {" << link->getName() << ", " << travel_time << "} ==> ";
		}
		else
		{
			out << " == {" << arc->getSourceServiceName() << ", " << travel_time << "} ==> ";
		}

		out << arc->getDestinationServiceLocation() << ", arrives: " << gamma + travel_time;
		out << ", tau = " << tau_ij[to_index(arc)];
		out << ", tau bar = " << tau_bar_kij[{k, arc}] << ", sigma = " << as_int(sigma_kij[{k, arc}]);

        if (mJ_ak.count(arc) > 0)
        {
            const auto& commodities = mJ_ak.at(arc);
            if (commodities.empty())
                continue;

            out << ", shared = " << commodities.size();
        }

        out << "\n";
    }
    
    out << "\n";
}
