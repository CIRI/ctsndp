//
//  MinimizeDelaysModel_CPlex.cpp
//
//  Implements the "Minimize Delays" problem
//  using the CPlex optimizer.
//
//  Created by Brett Feddersen on 11/22/19.
//

#include "MinimizeDelaysModel_CPlex.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include "cplex_utils_priv.hpp"
#include <iostream>
#include <chrono>

namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;
}

using namespace ctsndp;


cMinimizeDelaysModel_CPlex::cMinimizeDelaysModel_CPlex(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_arc_t& p_ka,
                                         const ctsndp::dispatch_time_t& dispatchTimes_ki,
                                         bool verboseMode, bool printTimingInfo)
:
    cCPlexModel("MinimizeDelays"),
	cMinimizeDelaysModel(pass_number, expandedNetwork, serviceNetwork, p_ka,
						 dispatchTimes_ki, verboseMode, printTimingInfo)
{
}

cMinimizeDelaysModel_CPlex::~cMinimizeDelaysModel_CPlex()
{
}

void cMinimizeDelaysModel_CPlex::setNumericFocus(int numericFocus)
{
	cCPlexModel::setNumericFocus(numericFocus);

}

void cMinimizeDelaysModel_CPlex::setTimeLimit_sec(double timeLimit_sec)
{
	cCPlexModel::setTimeLimit_sec(timeLimit_sec);
}

void cMinimizeDelaysModel_CPlex::logToConsole(bool log)
{
	cCPlexModel::logToConsole(log);
}

void cMinimizeDelaysModel_CPlex::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cMinimizeDelaysModel_CPlex::logFilename(const std::string& filename)
{
	cCPlexModel::logFilename(filename, ".min_delay");
}

void cMinimizeDelaysModel_CPlex::readTuningParameters(const std::string& filename)
{
	cCPlexModel::readTuningParameters(filename);
}

bool cMinimizeDelaysModel_CPlex::Optimize()
{
    if (a_kij.empty())
        return true;
    
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Minimize Delay Problem\" ";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	mSolver.extract(mModel);

	mSolver.solve();

	auto status = mSolver.getStatus();

	if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
		case IloAlgorithm::Optimal:

			FindArcsToShorten();
                       
            return true;

		case IloAlgorithm::InfeasibleOrUnbounded:
		case IloAlgorithm::Infeasible:
		{
            std::string msg = "Minimize Delays model is infeasible!";
            throw ctsndp::cInfeasibleSolution("MinimizeDelays", msg);
            break;
        }

/*
        case GRB_TIME_LIMIT:
        {
            std::string msg = "Exceeded time limit (";
            msg += std::to_string(mTimeLimit_sec);
            msg += "):\n";
            auto count = mModel.get(GRB_IntAttr_SolCount);
            if (count == 0)
            {
                msg += "No solution was found!";
            }
            else
            {
                auto x = mModel.get(GRB_DoubleAttr_X);
                msg += "Best feasible solution = " + std::to_string(x);
            }
                       
            throw ctsndp::cTimeLimit("MinimizeDelays", msg);
            break;
        }
*/
    }

    return false;
}

bool cMinimizeDelaysModel_CPlex::RelaxConstraints()
{
	return cCPlexModel::RelaxConstraints();
}

std::string cMinimizeDelaysModel_CPlex::ComputeIIS()
{
	return cCPlexModel::ComputeIIS();
}

void cMinimizeDelaysModel_CPlex::FindConstraintViolations(std::ostream& out)
{
    bool printNone = true;
	out << "\nScanning for constraint 18 \"a_kij <= gamma_ki\" violations:\n";

    for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
    {
        auto& k = it->first.k;
        const auto& loc = it->first.ij->getDestination();
        double arrival_time = it->second;

        double delta = as_double(gamma_ki[{k, loc}]) - arrival_time;

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << as_double(gamma_ki[{k, loc}]) << " < " << arrival_time;
            out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*(it->first.ij));
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 19, arrival before max delay time violations:\n";

    // for all commodities
    for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
    {
        auto& k = it->first.k;

        if (!k->hasMaxDelayTime())
            continue;

        const auto& arc = it->first.ij;
        
        double arrival_time = as_double(gamma_ki[{k, arc->getDestination()}]) + tau_ij[to_index(arc)];
        double ldt = k->getLatestDeliveryTime() + k->getMaxDelayTime();
        double delta = ldt - arrival_time;

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << ldt << " < " << arrival_time;
            out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*arc);
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;
}

//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cMinimizeDelaysModel_CPlex::FindArcsToShorten()
{
	mArcsToShorten.clear();

	if (a_kij.empty())
		return;

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Arcs to Shorten";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
	{
		auto& k = it->first.k;
		const auto& arc = it->first.ij;
		double t_min = arc->getSourceTime() + arc->getActualTravelTime();
		const auto& node = arc->getDestination();
		double t_new = std::max(as_double(gamma_ki[{k, node}]), t_min);
		double t_old = mDispatchTimes_ki.at({ k, node });
		if ((t_new < t_old) && (t_new != node->getTime()))
		{
			mArcsToShorten.insert({ arc, t_new });
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}


//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cMinimizeDelaysModel_CPlex::ConstructOptimizatonVariables()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variables";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	for (auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
	{
		auto& k = it->first;

		// Make sure this commodity has a delay cost.
		// If yes, then the path contain a "delay" node that can be shortened
		if (!k->hasDelayCost())
			continue;

		auto& path = it->second;
		if (path.empty())
			continue;

		// We can only minimize delays on paths that pass through a delay node
		if (!path.back()->connectedToDelayNode())
			continue;

		if (path.size() < 3)
		{
			auto& arc = path.front();
			const auto& node = arc->getDestination();
			gamma_ki[{k, node}] = IloNumVar(mEnv, 0, +IloInfinity);
#if ADD_DEBUG_NAMES
			std::string varName = "gamma_ki:" + k->getShipmentName() + "," + node->getName();
			std::replace(varName.begin(), varName.end(), ' ', '_');
			gamma_ki[{k, node}].setName(varName.c_str());
#endif
		}
		else
		{
			auto& arc = path[path.size() - 2];
			const auto& node = arc->getDestination();
			gamma_ki[{k, node}] = IloNumVar(mEnv, 0, +IloInfinity);
#if ADD_DEBUG_NAMES
			std::string varName = "gamma_ki:" + k->getShipmentName() + "," + node->getName();
			std::replace(varName.begin(), varName.end(), ' ', '_');
			gamma_ki[{k, node}].setName(varName.c_str());
#endif
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

void cMinimizeDelaysModel_CPlex::BuildOptimizatonProblem()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	IloExpr shortenDispatchVars(mEnv);

	for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
	{
		auto& k = it->first.k;
		const auto& loc = it->first.ij->getDestination();
		shortenDispatchVars += gamma_ki[{k, loc}] - it->second;
	}

	mModel.add(IloMinimize(mEnv, shortenDispatchVars));

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}


//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================

//
// Perform constraint 8 for the algorithm listed on page 1312
//
void cMinimizeDelaysModel_CPlex::constraint18_EnsureDispatchIsAfterAvailable()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint18: Ensure Dispatch Is After Available";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
    {
        auto& k = it->first.k;
        const auto& loc = it->first.ij->getDestination();
        double t = it->second;

		IloConstraint constraint = (gamma_ki[{k, loc}] >= t);

#if ADD_DEBUG_NAMES
        std::string constrName = "EnsureDispatchIsAfterAvailable(";
        constrName += k->getFullyQualifiedName() + ", " + loc->getName() + ")";
		mModel.setName(constrName.c_str());
#endif

		mModel.add(constraint);
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

void cMinimizeDelaysModel_CPlex::constraint19_EnsureArrivalBeforeMaxLateTime()
{
    return;
    
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint19: Ensure Arrival Before Max Late Time";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    // for all commodities
    for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
    {
        auto& k = it->first.k;

        if (!k->hasMaxDelayTime())
            continue;

        const auto& arc = it->first.ij;
        
		IloExpr lhs(mEnv);
		lhs += gamma_ki[{k, arc->getDestination()}] + tau_ij[to_index(arc)];
        double ldt = k->getLatestDeliveryTime() + k->getMaxDelayTime();

		IloConstraint constraint = (lhs <= ldt);

#if ADD_DEBUG_NAMES
        std::string constrName = "EnsureArrivalBeforeMaxLateTime(";
        constrName += k->getFullyQualifiedName() + ", " + to_string(*arc) + ")";
		constraint.setName(constrName.c_str());
#endif

		mModel.add(constraint);
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

