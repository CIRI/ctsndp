//
//  MinimizeDelaysModel_CPlex.hpp
//
//  Implements the "Minimize Delays" problem
//  using the CPlex optimizer.
//
//  Created by Brett Feddersen on 03/25/20.
//

#pragma once
#ifndef MinimizeDelaysModel_CPlex_hpp
#define MinimizeDelaysModel_CPlex_hpp

#include "MinimizeDelaysModel.hpp"
#include "CPlexModel.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include <vector>
#include <map>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 3 on page 1307 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cMinimizeDelaysModel_CPlex final : public cMinimizeDelaysModel,
										 private cCPlexModel
{
public:
	cMinimizeDelaysModel_CPlex(int pass_number,
                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                         const cServiceNetwork& serviceNetwork,
                         const ctsndp::commodity_path_arc_t& p_ka,
                         const ctsndp::dispatch_time_t& dispatchTimes_ki,
                         bool verboseMode, bool printTimingInfo);
    ~cMinimizeDelaysModel_CPlex();

	void setNumericFocus(int numericFocus) override;
	void setTimeLimit_sec(double timeLimit_sec) override;
	void logToConsole(bool log) override;
	void outputDebugMsgs(bool msgs) override;
	void logFilename(const std::string& filename) override;
	void readTuningParameters(const std::string& filename) override;

    bool Optimize() override;

	bool RelaxConstraints() override;
	void FindConstraintViolations(std::ostream& out) override;
	std::string ComputeIIS() override;

protected:
	void ConstructOptimizatonVariables() override;
	void BuildOptimizatonProblem() override;

private:
    
	void FindArcsToShorten();

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
    void constraint18_EnsureDispatchIsAfterAvailable() override;
    void constraint19_EnsureArrivalBeforeMaxLateTime() override;
    

	//
	// Decision Variables: minimize_delays
	//

	// the dispatch time of commodity, k, at node i
	std::map<ctsndp::sCommodityNode, IloNumVar, ctsndp::compareCommodityNodeByPtrId> gamma_ki;
};

#endif /* MinimizeDelaysModel_CPlex_hpp */
