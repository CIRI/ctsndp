
#include "routes.hpp"

#include <iostream>
#include <algorithm>


sLeg::sLeg()
:
    fixedCost(0),
    variableCost(0),
    departureTime(0),
    utilization(0),
    pLink(nullptr)
{}

sCommodityPath::sCommodityPath(const cCommodityBundle* const commodity)
:
    pCommodity(commodity)
{
    fixedCost = 0;
    variableCost = 0;
    delayCost = 0;
	variableDelayCost = 0;
}


sDispatch::sDispatch(const cCommodityBundle* const k, double time)
:
    pCommodity(k), departureTime(time)
{}

sMismatchedDispatch::sMismatchedDispatch(const cServiceLink* link)
:
    pLink(link)
{}

void sMismatchedDispatch::addDispatch(const cCommodityBundle* const k, double time)
{
    auto it = std::find_if(dispatches.begin(), dispatches.end(),
                           [&](const sDispatch& dispatch){ return dispatch.pCommodity == k;});
    if (it == dispatches.end())
        dispatches.emplace_back(k, time);
}


cRoutes::cRoutes()
{
    mSuccess = false;
    mFixedCost = 0.0;
    mVariableCost = 0.0;
    mDelayCost = 0.0;
	mVariableDelayCost = 0.0;
    mTotalCost = 0.0;
    mIdealFixedCost = 0.0;
    mIdealVariableCost = 0.0;
    mIdealDelayCost = 0.0;
	mIdealVariableDelayCost = 0.0;
	mIdealTotalCost = 0.0;
}


void print_result(std::ostream& out, const cRoutes& routes)
{
    if (!routes.mSuccess)
    {
        out << routes.mErrorMessage << std::endl;
        return;
    }
    
    bool hasDelays = false;
    for(auto& routing : routes.mPaths)
    {
        out << '\n';
        if (routing.path.empty())
        {
            out << "Error: No routing path for "
                << routing.pCommodity->getFullyQualifiedName()
                << std::endl;
            continue;
        }
        out << "Moving " << routing.pCommodity->getFullyQualifiedName() << " (qty = " << routing.pCommodity->getQuantity() << ") : " << std::endl;
        auto first = routing.path.begin();
        sLeg leg = *first;
        out << leg.pLink->getSource()->getName();
        out << " (eat: " << routing.pCommodity->getEarliestAvailableTime();
//        out << ", departs: " << leg.departureTime << ")";

        auto last = routing.path.end();
        if (!routing.path.back().pLink)
        {
            hasDelays = true;
            --last;
        }
        for (; first != last; ++first)
        {
            leg = *first;
			if (!leg.pLink)
				continue;

			out << ", departs: " << leg.departureTime << ")";
			if (leg.pLink)
			{
				out << " =={ " << leg.pLink->getName() << " }==> " << leg.pLink->getDestination()->getName();
				double arrival_time = leg.departureTime + leg.pLink->getTravelTime();
				out << " (arrives: " << arrival_time;
			}
			else
			{
				out << " =={ " << "Holding" << " }==> " << "??";
				out << " (arrives: " << "??";
			}
        }
        
//        double arrival_time = leg.departureTime + leg.pLink->getTravelTime();
//        out << " -> " << leg.pLink->getDestination()->getName();
//        out << " (arrives: " << arrival_time << ", ldt: " << routing.pCommodity->getLatestDeliveryTime() << ")";
        out << ", ldt: " << routing.pCommodity->getLatestDeliveryTime() << ")";
        out <<  std::endl;
		out << "Share of Fixed cost = " << routing.fixedCost;
		out << ", Variable cost = " << routing.variableCost;
		out << ", Delay Cost = " << routing.delayCost;
		out << ", Variable delay cost = " << routing.variableDelayCost;
        out << ", Total cost = " << routing.fixedCost + routing.variableCost
						+ routing.delayCost + routing.variableDelayCost << std::endl;
    }
    out << '\n';

    for(auto& utilization : routes.mLinkUtilizations)
    {
		const std::string& linkName = utilization.first;
		auto it = std::find_if(routes.mMismatches.begin(), routes.mMismatches.end(),
			[&](const sMismatchedDispatch& dispatch)
			{
				return linkName == dispatch.pLink->getName();
			});
		out << linkName << ": utilization = ";
		if (it == routes.mMismatches.end())
		{
			out << utilization.second;
		}
		else
		{
			out << it->dispatches.size() << " (" << utilization.second << ")";
		}
		out << "\n";
    }
    out << '\n';

    if (routes.mMismatches.empty())
    {
        if ((routes.mTotalCost - routes.mIdealTotalCost) == 0.0)
        {
            out << "The total cost of " << routes.mTotalCost << " meets the idea minimal cost of ";
            out << routes.mIdealTotalCost << "!";
        }
        else
        {
            if (hasDelays)
            {
                out << "The total cost of " << routes.mTotalCost << " does not meet the idea minimal cost of ";
                out << routes.mIdealTotalCost << " due to delay costs.";
            }
            else
            {
                out << "The total cost of " << routes.mTotalCost << " does not meet the idea minimal cost of ";
                out << routes.mIdealTotalCost << " with no dispatch mismatches!";
            }
        }
    }
    else
    {
        if ((routes.mTotalCost - routes.mIdealTotalCost) == 0.0)
        {
            out << "The total cost of " << routes.mTotalCost << " meets the idea minimal cost of ";
            out << routes.mIdealTotalCost << " with " << routes.mMismatches.size();
        }
        else
        {
            out << "The total cost of " << routes.mTotalCost << " does not meet the idea minimal cost of ";
            out << routes.mIdealTotalCost << " due to " << routes.mMismatches.size();
        }
        
        if (routes.mMismatches.size() == 1)
            out << " dispatch mismatch!";
        else
            out << " dispatch mismatches!";
    }
    
    out << '\n' << std::endl;

    if (routes.mMismatches.size() > 0)
    {
        out << "Dispatch Mismatches:\n";
        for (auto& mismatch : routes.mMismatches)
        {
            out << "Service Link " << to_string(*(mismatch.pLink)) << ":\n";
            for (auto& dispatch : mismatch.dispatches)
            {
                out << "\t" << dispatch.pCommodity->getFullyQualifiedName();
                out << " dispatched at " << std::to_string(dispatch.departureTime);
                out << '\n';
            }
            out << '\n';
        }
        out << '\n';
    }
}

void write_full_report(std::ostream& out, const cRoutes& routes)
{
	if (!routes.mSuccess)
	{
		out << routes.mErrorMessage << std::endl;
		return;
	}

	out << "Execution time (sec): " << routes.mSolutionTime_s.count() << "\n\n";
	out << "Partially Time Expanded Network Stats:\n";
	out << "\tNumber of commodity bundles: " << routes.mpPartiallyTimeExpandedNetwork->commodities().size() << "\n";
	out << "\tNumber of nodes: " << routes.mpPartiallyTimeExpandedNetwork->nodes().size();
	if (routes.mpPartiallyTimeExpandedNetwork->delay_nodes().size() > 0)
	{
		out << " of which " << routes.mpPartiallyTimeExpandedNetwork->delay_nodes().size() << " are delay nodes";
	}
	out << "\n";
	out << "\tNumber of arcs: " << routes.mpPartiallyTimeExpandedNetwork->arcs().size() << "\n";
	out << "\tNumber of storage arcs: " << routes.mpPartiallyTimeExpandedNetwork->holdover_arcs().size() << "\n";
	out << "\n";

//	routes.mpPartiallyTimeExpandedNetwork->print(out);
//	out << "\n";

	out << "Routing Information:\n";
	bool hasDelays = false;
	for (auto& routing : routes.mPaths)
	{
		out << '\n';
		if (routing.path.empty())
		{
			out << "Error: No routing path for "
				<< routing.pCommodity->getFullyQualifiedName()
				<< std::endl;
			continue;
		}
		out << "Moving " << routing.pCommodity->getFullyQualifiedName() << " (qty = " << routing.pCommodity->getQuantity();
		out << ", eat = " << routing.pCommodity->getEarliestAvailableTime();
		out << ", ldt = " << routing.pCommodity->getLatestDeliveryTime() << ") : " << std::endl;

		auto first = routing.path.begin();
		sLeg leg = *first;
		out << leg.pLink->getSource()->getName();
		out << " (eat: " << routing.pCommodity->getEarliestAvailableTime();

		auto last = routing.path.end();
		if (routing.path.size() > 2)
			--last;

		if (!routing.path.back().pLink)
		{
			hasDelays = true;
			--last;
		}

		bool use_comma = true;
		for (; first != last; ++first)
		{
			leg = *first;
			if (!leg.pLink)
				continue;

			if (use_comma)
				out << ", departs: " << leg.departureTime << ")";
			else
				out << " (departs: " << leg.departureTime << ")";
			use_comma = false;

			if (leg.pLink)
			{
				out << " =={ " << leg.pLink->getName() << " }==> " << leg.pLink->getDestination()->getName();
				double arrival_time = leg.departureTime + leg.pLink->getTravelTime();
				out << " (arrives: " << arrival_time << ")";
				out << " fixed cost = " << leg.fixedCost << ", variable cost = " << leg.variableCost;
				out << ", utilization = " << leg.utilization << "\n";

				out << leg.pLink->getDestination()->getName();
			}
			else
			{
				out << " =={ " << "Holding" << " }==> " << "??";
				out << " (arrives: " << "??";
			}
		}

		leg = *first;
		if (leg.pLink)
		{
			out << " =={ " << leg.pLink->getName() << " }==> " << leg.pLink->getDestination()->getName();
			double arrival_time = leg.departureTime + leg.pLink->getTravelTime();
			out << " (arrives: " << arrival_time << ", ldt: " << routing.pCommodity->getLatestDeliveryTime() << ")";
		}
		else
		{
			out << " (arrives: " << "??" << ", ldt: " << routing.pCommodity->getLatestDeliveryTime() << ")";
		}

		out << " fixed cost = " << leg.fixedCost << ", variable cost = " << leg.variableCost;
		out << ", utilization = " << leg.utilization << std::endl;
		out << "Share of Fixed cost = " << routing.fixedCost;
		out << ", Variable cost = " << routing.variableCost;
		out << ", Delay Cost = " << routing.delayCost;
		out << ", Variable delay cost = " << routing.variableDelayCost;
		out << ", Total cost = " << routing.fixedCost + routing.variableCost
			+ routing.delayCost + routing.variableDelayCost << std::endl;
	}
	out << '\n';

	out << "Service Link Information:\n";
	for (auto& utilization : routes.mLinkUtilizations)
	{
		const std::string& linkName = utilization.first;
		auto it = std::find_if(routes.mMismatches.begin(), routes.mMismatches.end(),
			[&](const sMismatchedDispatch& dispatch)
		{
			return linkName == dispatch.pLink->getName();
		});
		out << linkName << ": utilization = ";
		if (it == routes.mMismatches.end())
		{
			out << utilization.second;
		}
		else
		{
			out << it->dispatches.size() << " (" << utilization.second << ")";
		}
		out << "\n";
	}
	out << '\n';

	out << "Cost Information:\n";
	out << "Fixed Cost     = " << routes.mFixedCost         << ", Ideal Fixed Cost     = " << routes.mIdealFixedCost << "\n";
	out << "Variable Cost  = " << routes.mVariableCost      << ", Ideal Variable Cost  = " << routes.mIdealVariableCost << "\n";
	out << "Delay Cost     = " << routes.mDelayCost         << ", Ideal Delay Cost     = " << routes.mIdealDelayCost << "\n";
	out << "Var Delay Cost = " << routes.mVariableDelayCost << ", Ideal Var Delay Cost = " << routes.mIdealVariableDelayCost << "\n";
	out << '\n';

	if (routes.mMismatches.empty())
	{
		if ((routes.mTotalCost - routes.mIdealTotalCost) == 0.0)
		{
			out << "The total cost of " << routes.mTotalCost << " meets the idea minimal cost of ";
			out << routes.mIdealTotalCost << "!";
		}
		else
		{
			if (hasDelays)
			{
				out << "The total cost of " << routes.mTotalCost << " does not meet the idea minimal cost of ";
				out << routes.mIdealTotalCost << " due to delay costs.";
			}
			else
			{
				out << "The total cost of " << routes.mTotalCost << " does not meet the idea minimal cost of ";
				out << routes.mIdealTotalCost << " with no dispatch mismatches!";
			}
		}
	}
	else
	{
		if ((routes.mTotalCost - routes.mIdealTotalCost) == 0.0)
		{
			out << "The total cost of " << routes.mTotalCost << " meets the idea minimal cost of ";
			out << routes.mIdealTotalCost << " with " << routes.mMismatches.size();
		}
		else
		{
			out << "The total cost of " << routes.mTotalCost << " does not meet the idea minimal cost of ";
			out << routes.mIdealTotalCost << " due to " << routes.mMismatches.size();
		}

		if (routes.mMismatches.size() == 1)
			out << " dispatch mismatch!";
		else
			out << " dispatch mismatches!";
	}

	out << '\n' << std::endl;

	if (routes.mMismatches.size() > 0)
	{
		out << "Dispatch Mismatches:\n";
		for (auto& mismatch : routes.mMismatches)
		{
			out << "Service Link " << to_string(*(mismatch.pLink)) << ":\n";
			for (auto& dispatch : mismatch.dispatches)
			{
				out << "\t" << dispatch.pCommodity->getFullyQualifiedName();
				out << " dispatched at " << std::to_string(dispatch.departureTime);
				out << '\n';
			}
			out << '\n';
		}
		out << '\n';
	}
};
