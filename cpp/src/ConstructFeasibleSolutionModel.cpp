//
//  ConstructFeasibleSolutionModel.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 11/22/19.
//

#include "ConstructFeasibleSolutionModel.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include <iostream>
#include <chrono>


namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;
}

using namespace ctsndp;


cConstructFeasibleSolutionModel::cConstructFeasibleSolutionModel(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_node_t& p_ki,
                                         const ctsndp::commodity_path_arc_t& p_ka,
										 const ctsndp::consolidation_groups_t& j_ak,
										 bool verboseMode, bool printTimingInfo)
:
	cOptimizationProblem(pass_number),
	mPartiallyTimeExpandedNetwork(expandedNetwork),
    mServiceNetwork(serviceNetwork),
    mP_ki(p_ki),
    mP_ka(p_ka),
    mJ_ak(j_ak),
    mVerboseMode(verboseMode),
    mPrintTimingInfo(printTimingInfo)
{
	mLogGamma_ki = false;
	mLogDelta_kk_ij = false;

    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
 
    auto numArcs = arcs.size();
    tau_ij.resize(numArcs);
    
    auto start = std::chrono::system_clock::now();

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\n\tCreating optimization variables";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }
*/

    for (auto& arc : arcs)
    {
        auto ij = to_index(arc);
        tau_ij[ij] = arc->getActualTravelTime();
    }
    
/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
       
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
*/
}

cConstructFeasibleSolutionModel::~cConstructFeasibleSolutionModel()
{
}

std::set<ctsndp::arc_ptr_t> cConstructFeasibleSolutionModel::returnArcsToLengthen()
{
	return mArcsToLengthen;
}

ctsndp::dispatch_time_t cConstructFeasibleSolutionModel::returnDispatchTimes()
{
    return mDispatchTime_ki;
}

ctsndp::dispatch_mismatch_times_t cConstructFeasibleSolutionModel::returnMismatchTimes()
{
    return mMismatches_ki;
}

void cConstructFeasibleSolutionModel::BuildModel()
{
	if (mpProgress)
	{
		*mpProgress << "  ConstructOptimizatonVariables..." << std::flush;
	}

	ConstructOptimizatonVariables();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tAdding constraints";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "...\n" << std::flush;
    }

    // Add constraints
	if (mpProgress)
	{
		*mpProgress << "  constraint13_EnsureAllowableDispatchTimes..." << std::flush;
	}

	constraint13_EnsureAllowableDispatchTimes();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint14_EnsureDispatchIsAfterAvailable..." << std::flush;
	}
	
	constraint14_EnsureDispatchIsAfterAvailable();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint15_EnsureArrivalBeforeDue..." << std::flush;
	}
	
	constraint15_EnsureArrivalBeforeDue();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint16and17_DispatchDelta_k1_k2..." << std::flush;
	}
	
	constraint16and17_DispatchDelta_k1_k2();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tdone";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}

	if (mpProgress)
	{
		*mpProgress << "  BuildOptimizatonProblem..." << std::flush;
	}

	BuildOptimizatonProblem();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}
}

