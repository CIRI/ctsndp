//
//  MinimizeDelaysModel.hpp
//
//  Created by Brett Feddersen on 11/21/19.
//

#pragma once
#ifndef MinimizeDelaysModel_Gurobi_hpp
#define MinimizeDelaysModel_Gurobi_hpp

#include "MinimizeDelaysModel.hpp"
#include "GurobiModel.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"

#include <map>
#include <string>
#include <iosfwd>
#include <memory>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 3 on page 1307 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cMinimizeDelaysModel_Gurobi final : public cMinimizeDelaysModel, private cGurobiModel
{
public:
	static std::unique_ptr<cMinimizeDelaysModel_Gurobi> 
	create(int pass_number,
		const cPartiallyTimeExpandedNetwork& expandedNetwork,
		const cServiceNetwork& serviceNetwork,
		const ctsndp::commodity_path_arc_t& p_ka,
		const ctsndp::dispatch_time_t& dispatchTimes_ki,
		bool verboseMode, bool printTimingInfo);

	~cMinimizeDelaysModel_Gurobi();

	void setNumericFocus(int numericFocus) override;
	void setTimeLimit_sec(double timeLimit_sec) override;
	void logToConsole(bool log) override;
	void outputDebugMsgs(bool msgs) override;
	void logFilename(const std::string& filename) override;
	void readTuningParameters(const std::string& filename) override;

    bool Optimize() override;

	bool RelaxConstraints() override;
	void FindConstraintViolations(std::ostream& out) override;
	std::string ComputeIIS() override;

protected:
	cMinimizeDelaysModel_Gurobi(int pass_number,
		const cPartiallyTimeExpandedNetwork& expandedNetwork,
		const cServiceNetwork& serviceNetwork,
		const ctsndp::commodity_path_arc_t& p_ka,
		const ctsndp::dispatch_time_t& dispatchTimes_ki,
		bool verboseMode, bool printTimingInfo);

	void ConstructOptimizatonVariables() override;
	void BuildOptimizatonProblem() override;

private:
	void SaveSolution();
	void FindArcsToShorten();

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
    void constraint18_EnsureDispatchIsAfterAvailable() override;
    void constraint19_EnsureArrivalBeforeMaxLateTime() override;
    

	//
	// Decision Variables: minimize_delays
	//

	// the "dispatch" time of commodity, k, at node i.  This is the 
	// conceptual dispatch time from the delay node (d_k, t) to
	// destination (d_k, ldt)
	std::map<ctsndp::sCommodityNode, GRBVar, ctsndp::compareCommodityNodeByPtrId> gamma_ki;
};

#endif /* MinimizeDelaysModel_Gurobi_hpp */
