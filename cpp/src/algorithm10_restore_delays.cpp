
#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "ServiceNetwork/ServiceLocation.hpp"

#include <algorithm>
#include <cassert>

namespace
{
	std::ostream& operator<<(std::ostream& out, const timepoints_t& points)
	{
		out << "{";
		if (!points.empty())
		{
			auto last = --(points.end());
			for (auto it = points.begin(); it != last; ++it)
			{
				out << *it << ", ";
			}
			out << *last;
		}
		out << "}";
		return out;
	}
}

/**
 * This algorithm is similar to algorithm 5 listed on page 1312
 * The algorithm restores property 4 of the partially time expanded network
 * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_k < t_new
 **/
std::vector<cPartiallyTimeExpandedNetworkArc*> ctsndp::restore_delays(cPartiallyTimeExpandedNetwork& network,
                     const cPartiallyTimeExpandedNetworkDelayNode& node,
                     double t_k, double t_new,
                     std::ostream* debug)
{
	if (debug)
	{
		*debug << "  Restore: " << node;
		*debug << " t_k=" << t_k;
		*debug << ", t_new=" << t_new;
		*debug << "\n";
	}

    std::vector<cPartiallyTimeExpandedNetworkArc*> arcsToDelete;
    std::vector<cPartiallyTimeExpandedNetworkArc*> arcsToRemove;

	auto node_t_k = network.findNode(node.getServiceLocation(), t_k);
	if (node_t_k == nullptr)
	{
		node_t_k = network.findNode(node.getServiceLocation(), t_k);
		std::cout << node.getName() << " T = ";
		auto time_points = network.timepoints(node.getServiceLocation());
		std::cout << time_points << "\n";

		auto& commodities = node.getCommodities();

		node_t_k = network.findNode(node.getServiceLocation(), t_k);
	}
	assert(node_t_k);

    // For all ((j, t), (i, t_k)) in 𝒜t such that t + tau_ji => t_new do
    auto inflows = network.getAllInflowArcsToNode(node_t_k, false);
    for (auto& inflow : inflows)
    {
		double t = inflow->getSource()->getTime();
		double tau_ij = inflow->getActualTravelTime();
		if (t + tau_ij >= t_new)
        {
			// Delete arc ((j, t), (i, t_k)) from 𝒜t
			arcsToRemove.push_back(inflow);
			if (debug)
			{
				*debug << "    Deleting arc: " << *inflow << "\n\n";
			}
		}
    }
    
    for (auto& arc : arcsToRemove)
    {
        network.removeArc(arc);
        arcsToDelete.push_back(arc);
    }
    arcsToRemove.clear();

    if (debug)
        *debug << "  complete!\n\n" << std::endl;

	return arcsToDelete;
}

