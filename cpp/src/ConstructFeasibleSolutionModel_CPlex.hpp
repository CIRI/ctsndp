//
//  ConstructFeasibleSolutionModel_CPlex.hpp
//
//  Implements the "Construct Feasible Solution" problem
//  using the CPlex optimizer.
//
//  Created by Brett Feddersen on 03/25/20.
//

#pragma once
#ifndef ConstructFeasibleSolutionModel_CPlex_hpp
#define ConstructFeasibleSolutionModel_CPlex_hpp

#include "ConstructFeasibleSolutionModel.hpp"
#include "CPlexModel.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include <map>
#include <string>
#include <iosfwd>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 4.4 on page 1313 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cConstructFeasibleSolutionModel_CPlex final : public cConstructFeasibleSolutionModel, 
													private cCPlexModel
{
public:
    cConstructFeasibleSolutionModel_CPlex(int pass_number,
                                    const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                    const cServiceNetwork& serviceNetwork,
                                    const ctsndp::commodity_path_node_t& p_ki,
                                    const ctsndp::commodity_path_arc_t& p_ka,
//									const ctsndp::consolidation_t& j_ak,
									const ctsndp::consolidation_groups_t& j_ak,
									bool verboseMode, bool printTimingInfo);
    
    ~cConstructFeasibleSolutionModel_CPlex();

	void setNumericFocus(int numericFocus) override;
	void setTimeLimit_sec(double timeLimit_sec) override;
	void logToConsole(bool log) override;
	void outputDebugMsgs(bool msgs) override;
	void logFilename(const std::string& filename) override;
	void readTuningParameters(const std::string& filename) override;

    /**
     * \brief Calls optimize to try and solve the "construct feasible solution" optimization problem.
     */
    bool Optimize() override;

	bool RelaxConstraints() override;
	void FindConstraintViolations(std::ostream& out) override;
	std::string ComputeIIS() override;

    /**
     * \brief Returns all of the dispatch times for the commodities moving through the service network
     */
    ctsndp::dispatch_time_t     returnDispatchTimes();

    /**
     * \brief Returns the mismatch of dispatch times between two commodities from the same location
     */
    ctsndp::dispatch_mismatch_times_t returnMismatchTimes();

private:
	void ConstructOptimizatonVariables() override;
	void BuildOptimizatonProblem() override;

	void FindArcsToLengthen();
	void SaveDispatchAndMismatchTimes();

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
    void constraint13_EnsureAllowableDispatchTimes() override;
    void constraint14_EnsureDispatchIsAfterAvailable() override;
    void constraint15_EnsureArrivalBeforeDue() override;
    void constraint16and17_DispatchDelta_k1_k2() override;
    
    //
    // Debug Methods
    //
    void DumpFeasibleSolutionInfo(std::ostream& out);

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    std::map<ctsndp::sCommodityNode, IloNumVar, ctsndp::compareCommodityNodeByPtrId>       gamma_ki;
    std::map<ctsndp::sDualCommodityArc, IloNumVar, ctsndp::compareDualCommodityArcByPtrId> delta_kk_ij;
};

#endif /* ConstructFeasibleSolutionModel_CPLex_hpp */
