//
//  ConstructFeasibleSolutionModel_CPlex.cpp
//
//  Implements the "Construct Feasible Solution" problem
//  using the CPlex optimizer.
//
//  Created by Brett Feddersen on 03/25/20.
//

#include "ConstructFeasibleSolutionModel_CPlex.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include "cplex_utils_priv.hpp"
#include <iostream>
#include <chrono>


namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;
}

using namespace ctsndp;


cConstructFeasibleSolutionModel_CPlex::cConstructFeasibleSolutionModel_CPlex(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_node_t& p_ki,
                                         const ctsndp::commodity_path_arc_t& p_ka,
//										 const ctsndp::consolidation_t& j_ak,
										 const ctsndp::consolidation_groups_t& j_ak,
										 bool verboseMode, bool printTimingInfo)
:
    cCPlexModel("ConstructFeasibleSolution"),
	cConstructFeasibleSolutionModel(pass_number, 
									expandedNetwork, serviceNetwork,
									p_ki, p_ka, j_ak, verboseMode,
									printTimingInfo)
{
    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    auto& holdover_arcs = mPartiallyTimeExpandedNetwork.holdover_arcs();
 
    auto numArcs = arcs.size();
    auto totalNumArcs = numArcs + holdover_arcs.size();
    tau_ij.resize(totalNumArcs);
}

cConstructFeasibleSolutionModel_CPlex::~cConstructFeasibleSolutionModel_CPlex()
{
}

void cConstructFeasibleSolutionModel_CPlex::setNumericFocus(int numericFocus)
{
	cCPlexModel::setNumericFocus(numericFocus);
}

void cConstructFeasibleSolutionModel_CPlex::setTimeLimit_sec(double timeLimit_sec)
{
	cCPlexModel::setTimeLimit_sec(timeLimit_sec);
}

void cConstructFeasibleSolutionModel_CPlex::logToConsole(bool log)
{
	cCPlexModel::logToConsole(log);
}

void cConstructFeasibleSolutionModel_CPlex::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cConstructFeasibleSolutionModel_CPlex::logFilename(const std::string& filename)
{
	cCPlexModel::logFilename(filename, ".construct_solution");
}

void cConstructFeasibleSolutionModel_CPlex::readTuningParameters(const std::string& filename)
{
	cCPlexModel::readTuningParameters(filename);
}

bool cConstructFeasibleSolutionModel_CPlex::Optimize()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Construct Feasible Solution\" ";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		mSolver.extract(mModel);
		mSolver.solve();
	}
	catch (const IloException& e)
	{
		throw cOptimizationException(e.getMessage());
	}

	auto status = mSolver.getStatus();

	if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
		case IloAlgorithm::Optimal:
		{
			auto objVal = mSolver.getObjValue();

			SaveDispatchAndMismatchTimes();

			if (objVal == 0.0)
			{
				if (mOutputDebugMsgs)
				{
					*mpDebugOutput << "none...";
				}

				return true;
			}

			FindArcsToLengthen();

			if (mOutputDebugMsgs)
			{
				*mpDebugOutput << mArcsToLengthen.size() << "...";
			}

			return true;
		}
		case IloAlgorithm::InfeasibleOrUnbounded:
		case IloAlgorithm::Infeasible:
		{
            std::string msg = "Construct Feasible Solution model is infeasible!";
            throw ctsndp::cInfeasibleSolution("ConstructFeasibleSolution", msg);
            break;
        }
    }

    return false;
}

bool cConstructFeasibleSolutionModel_CPlex::RelaxConstraints()
{
	return cCPlexModel::RelaxConstraints();
}

std::string cConstructFeasibleSolutionModel_CPlex::ComputeIIS()
{
	return cCPlexModel::ComputeIIS();
}

void cConstructFeasibleSolutionModel_CPlex::FindConstraintViolations(std::ostream& out)
{
    bool printNone = true;
    out << "\nScanning for constraint 13 \"gamma_ki + tau_ij <= gamma_kj\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto& path = mP_ka.at(k);
        if (path.empty())
            continue;

        for (int i=0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            const auto& loc = arc->getDestination();
            auto arrival_time = as_double(gamma_ki[{k, arc->getSource()}]) + tau_ij[to_index(arc)];
            double delta = as_double(gamma_ki[{k, loc}]) - arrival_time;

            if (delta < -CONSTRAINT_TOLERANCE)
            {
                out << arrival_time << " > " << as_double(gamma_ki[{k, loc}]) ;
                out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*arc);
                out << " delta = " << delta << "\n";
                printNone = false;
            }
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 14 \"eat <= gamma_ko\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto& loc = mP_ki.at(k).front();
        double delta = as_double(gamma_ki[{k, loc}]) - k->getEarliestAvailableTime();

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << k->getEarliestAvailableTime() << " > " << as_double(gamma_ki[{k, loc}]) ;
            out << " for commodity " << k->getFullyQualifiedName() << ", node "<< k->getSourceName();
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }
        
    if (printNone)
        out << "None.\n";
    printNone = true;
     
    out << "\nScanning for constraint 15 \"gamma_ki + tau_ij <= ldt\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        // This constraint is inforcing the fact that we want the dispatch time, gamma,
        // to be early enough that the commodity can be delivered in time.  This is true
        // if we only have the idea of holdover arcs, but with the idea of delay arcs,
        // we must include that last arc segment to the destination node.
        auto& arc = mP_ka.at(k).back();
        double arrival_time = as_double(gamma_ki[{k, arc->getSource()}]) + tau_ij[to_index(arc)];
        auto ldt = k->getLatestDeliveryTime();
        double delta = ldt - arrival_time;

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << arrival_time << " > " << ldt;
            out << " for commodity " << k->getFullyQualifiedName() << ", node "<< k->getDestinationName();
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 16 \"delta_kk_ij >= gamma_k1i - gamma_k2i\" violations:\n";

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					auto rhs = as_double(gamma_ki[{k1, arc->getSource()}])
						- as_double(gamma_ki[{k2, arc->getSource()}]);
					auto lhs = as_double(delta_kk_ij[{k1, k2, arc}]);
					double delta = lhs - rhs;

					if (delta < -CONSTRAINT_TOLERANCE)
					{
						out << lhs << " < " << rhs;
						out << " for commodities " << k1->getFullyQualifiedName() << " and " << k2->getFullyQualifiedName();
						out << ", node " << arc->getSourceServiceName();
						out << " delta = " << delta << "\n";
						printNone = false;
					}
				}
			}
		}
	}

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 17 \"delta_kk_ij >= gamma_k2i - gamma_k1i\" violations:\n";

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					auto rhs = as_double(gamma_ki[{k2, arc->getSource()}])
						- as_double(gamma_ki[{k1, arc->getSource()}]);
					auto lhs = as_double(delta_kk_ij[{k1, k2, arc}]);
					double delta = lhs - rhs;

					if (delta < -CONSTRAINT_TOLERANCE)
					{
						out << lhs << " < " << rhs;
						out << " for commodities " << k1->getFullyQualifiedName() << " and " << k2->getFullyQualifiedName();
						out << ", node " << arc->getSourceServiceName();
						out << " delta = " << delta << "\n";
						printNone = false;
					}
				}
			}
		}
	}

    if (printNone)
        out << "None.\n";
    printNone = true;
}

//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cConstructFeasibleSolutionModel_CPlex::FindArcsToLengthen()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Arcs to Lengthen";

		if (mPrintTimingInfo)
		{
			std::time_t start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	mArcsToLengthen.clear();

	try
	{
		// delta_kk_ij represents the mismatch in dispatch time between commodity k1 and k2 at node i
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;
						double delta = as_double(delta_kk_ij[{k1, k2, arc}]);
						if (delta > 0)
							mArcsToLengthen.insert(arc);
					}
				}
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		if (mArcsToLengthen.size() > 0)
			*mpDebugOutput << "found " << mArcsToLengthen.size() << " arcs, ";

		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

void cConstructFeasibleSolutionModel_CPlex::SaveDispatchAndMismatchTimes()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Dispatch Times";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		// gamma_ki represents the dispatch time of commodity k at node i
		for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
		{
			auto& k = it->first;
			auto& path = it->second;
			if (path.empty())
				continue;

			auto last = --(path.end());
			for (auto it = path.begin(); it != last; ++it)
			{
				auto& node = *it;
				mDispatchTime_ki[{k, node}] = as_double(gamma_ki[{k, node}]);
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}

	start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Mismatch Times";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		// delta_kk_ij represents the mismatch in dispatch time between commodity k1 and k2 at node i
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;
						double delta = as_double(delta_kk_ij[{k1, k2, arc}]);
						if (delta > 0)
							mMismatches_ki[{k1, k2, arc}] = delta;
					}
				}
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cConstructFeasibleSolutionModel_CPlex::ConstructOptimizatonVariables()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variables";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
		{
			auto k = it->first;
			auto& path = it->second;
			for (auto& node : path)
			{
				gamma_ki[{k, node}] = IloNumVar(mEnv, 0, +IloInfinity);

#if ADD_DEBUG_NAMES
				std::string varName = "gamma_ki:" + k->getShipmentName() + "," + node->getName();
				std::replace(varName.begin(), varName.end(), ' ', '_');
				// The lower bound of zero enforces constraint 18 for the algorithm list on page 1313
				gamma_ki[{k, node}].setName(varName.c_str());
#endif
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error("ConstructOptimizatonVariables (gammas): "s + e.getMessage());
	}

	try
	{
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;

						delta_kk_ij[{k1, k2, arc}] = IloNumVar(mEnv, -IloInfinity, +IloInfinity);
#if ADD_DEBUG_NAMES
						std::string varName = "delta_kk_ij: (" + k1->getShipmentName()
							+ ", " + k2->getShipmentName() + ") " + to_string(*arc);
						std::replace(varName.begin(), varName.end(), ' ', '_');

						delta_kk_ij[{k1, k2, arc}].setName(varName.c_str());
#endif
					}
				}
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error("ConstructOptimizatonVariables (deltas): "s + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}


void cConstructFeasibleSolutionModel_CPlex::BuildOptimizatonProblem()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		IloExpr dispatchMismatchVars(mEnv);
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;
						dispatchMismatchVars += delta_kk_ij[{k1, k2, arc}];
					}
				}
			}
		}

		mModel.add(IloMinimize(mEnv, dispatchMismatchVars));
	}
	catch (const IloException& e)
	{
		throw std::logic_error("BuildOptimizatonProblem: "s + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================

//
// Perform constraint 13 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_CPlex::constraint13_EnsureAllowableDispatchTimes()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\t\tConstraint13: Ensure Allowable Dispatch Times";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			auto& path = mP_ka.at(k);

			if (path.empty())
				continue;

			const auto& nodes = mP_ki.at(k);
			auto n = path.size() - 1;
			for (std::size_t i = 0; i < n; ++i)
			{
				auto& arc = path[i];

				IloExpr lhs(mEnv);
				auto node = arc->getSource();
				lhs += (gamma_ki[{k, node}] + tau_ij[to_index(arc)]);

				const auto& next = nodes[i + 1];
				IloConstraint constraint = (lhs <= gamma_ki[{k, next}]);

#if ADD_DEBUG_NAMES
				const auto& destination = arc->getDestinationServiceLocation();
				std::string constrName = "C13_EnsureAllowableDispatchTimes(";
				constrName += k->getFullyQualifiedName() + ", " + destination->getName() + ")";
				constraint.setName(constrName.c_str());
#endif
				mModel.add(constraint);
			}
		}
	}
	catch (const IloException& e)
	{
		/*
		 * For some reason P_ka and P_ki are out of sync.  It only happens in the CPlex solver!
		 */
		throw std::logic_error("constraint13_EnsureAllowableDispatchTimes: "s + e.getMessage());
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 14 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_CPlex::constraint14_EnsureDispatchIsAfterAvailable()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint14: Ensure Dispatch Is After Available";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			auto& loc = mP_ki.at(k).front();
			double eat = k->getEarliestAvailableTime();

			gamma_ki[{k, loc}].setLB(eat);
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error("constraint14_EnsureDispatchIsAfterAvailable: "s + e.getMessage());
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 15 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_CPlex::constraint15_EnsureArrivalBeforeDue()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint15: Ensure Arrival Before Due";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			// This constraint is enforcing the fact that we want the dispatch time, gamma,
			// to be early enough that the commodity can be delivered in time.  This is true
			// if we only have the idea of holdover arcs, but with the idea of delay arcs,
			// we must include that last arc segment to the destination node.
			auto& path = mP_ka.at(k);
			auto it = path.rbegin();
			while ((*it)->isHoldoverArc())
			{
				++it;
			}
        
			auto& arc = *it;
			IloExpr lhs(mEnv);
			lhs += (gamma_ki[{k, arc->getSource()}] + tau_ij[to_index(arc)]);
			auto ldt = k->getLatestDeliveryTime();

			IloConstraint constraint = (lhs <= ldt);

#if ADD_DEBUG_NAMES
			std::string constrName = "C15_EnsureArrivalBeforeDue(";
			constrName += k->getFullyQualifiedName() + ", " + to_string(*arc) + ")";
			constraint.setName(constrName.c_str());
#endif
			mModel.add(constraint);
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error("constraint15_EnsureArrivalBeforeDue: "s + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 16 and 17 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_CPlex::constraint16and17_DispatchDelta_k1_k2()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint16 and 17: Dispatch Delta between k1 and k2";
        
        if (mPrintTimingInfo)
        {
            auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;
						IloExpr rhs16(mEnv);
						rhs16 += (gamma_ki[{k1, arc->getSource()}]
							- gamma_ki[{k2, arc->getSource()}]);

						IloExpr rhs17(mEnv);
						rhs16 += (gamma_ki[{k2, arc->getSource()}]
							- gamma_ki[{k1, arc->getSource()}]);

						IloConstraint constraint16 = (delta_kk_ij[{k1, k2, arc}] >= rhs16);
						IloConstraint constraint17 = (delta_kk_ij[{k1, k2, arc}] >= rhs17);

#if ADD_DEBUG_NAMES
						std::string constrName = "C16_DispatchDelta_{";
						constrName += k1->getFullyQualifiedName() + " - ";
						constrName += k2->getFullyQualifiedName() + "} at ";
						constrName += arc->getSource()->getName();
						constraint16.setName(constrName.c_str());

						constrName = "C17_DispatchDelta_{";
						constrName += k2->getFullyQualifiedName() + " - ";
						constrName += k1->getFullyQualifiedName() + "} at ";
						constrName += arc->getSource()->getName();
						constraint17.setName(constrName.c_str());
#endif
						mModel.add(constraint16);
						mModel.add(constraint17);
					}
				}
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error("constraint16and17_DispatchDelta_k1_k2: "s + e.getMessage());
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}


//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

void cConstructFeasibleSolutionModel_CPlex::DumpFeasibleSolutionInfo(std::ostream& out)
{
    out << std::endl;

    out << "\n*** Dispatch times ***\n";

	try
	{
		for(auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
		{
			auto& k = it->first;
			auto& path = it->second;
			for (auto& node : path)
			{
				out << k->getFullyQualifiedName() << "," << node->getName() << "= " << as_double(gamma_ki[{k, node}]) << "\n";
			}
        
			out << "\n";
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

    if (mJ_ak.empty())
    {
        out << "No shared arcs!\n" << std::endl;
        return;
    }
    
    out << "*** Difference in Dispatch times ***\n";
	
	try
	{
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
 				   continue;

				auto last = --commodities.end();
 			   for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
    				auto& k1 = *k1_it;

        			auto k2_it = k1_it;
            		for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
    					auto& k2 = *k2_it;
        				out << arc->getSource()->getName() << " {";
            			out << k1->getFullyQualifiedName() << ", " << k2->getFullyQualifiedName() << "} = ";
	               		out << as_double(delta_kk_ij[{k1, k2, arc}]) << "\n";
    				}
				}
			}
		}
	}
	catch (const IloException& e)
	{
		throw std::logic_error(e.getMessage());
	}

}
