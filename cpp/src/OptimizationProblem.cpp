//
//  OptimizationProblem.cpp
//
//  Created by Brett Feddersen on 11/21/19.
//

#include "OptimizationProblem.hpp"
#include <iostream>

namespace
{
    std::string log_filename(std::string filename, const char* type)
    {
        if (filename.empty())
            return filename;
        auto n = filename.rfind('.');
        if (n == std::string::npos)
        {
            filename.append(type);
        }
        else
        {
            filename.insert(n, type);
        }
        return filename;
    }
}

cOptimizationProblem::cOptimizationProblem(int pass_number)
	:
	mOutputDebugMsgs(false),
	mpDebugOutput(&std::clog),
	mpProgress(nullptr),
	mPass(pass_number)
{
}

void cOptimizationProblem::setProgressOutput(std::ostream& out)
{
	mpProgress = &out;
}

void cOptimizationProblem::outputDebugMsgs(bool output)
{
    mOutputDebugMsgs = output;
}

void cOptimizationProblem::setDebugOutput(std::ostream& out)
{
    mpDebugOutput = &out;
}

