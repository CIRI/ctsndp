//
//  MinimizeDelaysModel.hpp
//
//  Created by Brett Feddersen on 11/21/19.
//

#pragma once
#ifndef MinimizeDelaysModel_hpp
#define MinimizeDelaysModel_hpp

#include "OptimizationProblem.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include <vector>
#include <map>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 3 on page 1307 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cMinimizeDelaysModel : public cOptimizationProblem
{
public:
    cMinimizeDelaysModel(int pass_number, 
						 const cPartiallyTimeExpandedNetwork& expandedNetwork,
                         const cServiceNetwork& serviceNetwork,
                         const ctsndp::commodity_path_arc_t& p_ka,
                         const ctsndp::dispatch_time_t& dispatchTimes_ki,
                         bool verboseMode, bool printTimingInfo);
    ~cMinimizeDelaysModel();

    ctsndp::arcs_to_shorten_t returnArcsToShorten();

    void BuildModel() override;

protected:
	virtual void ConstructOptimizatonVariables() = 0;
	virtual void BuildOptimizatonProblem() = 0;

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
    virtual void constraint18_EnsureDispatchIsAfterAvailable() = 0;
	virtual void constraint19_EnsureArrivalBeforeMaxLateTime() = 0;
    

    //
    // Inputs
    //
    
    const cPartiallyTimeExpandedNetwork& mPartiallyTimeExpandedNetwork;
    const cServiceNetwork&               mServiceNetwork;
    const ctsndp::commodity_path_arc_t&  mP_ka;
    const ctsndp::dispatch_time_t&       mDispatchTimes_ki;
    
    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    // the actual travel time of a commodity utilizing the arc (i,j)
    std::vector<double> tau_ij;

    // the minimimum arrival time of commodity k at the destination of arc (i,j)
	// where the destination is some point in the delay arc ((d_k, t), (d_k, ldt))
    std::map<ctsndp::sCommodityArc, double, ctsndp::compareCommodityArcByPtrId> a_kij;

    //
    // Variables to hold the output data
    //
	ctsndp::arcs_to_shorten_t mArcsToShorten;

	// the delay penalty rate of commodity k
	std::vector<double> p_k;

	//
    // Debug Variables
    //
    bool mVerboseMode;
    bool mPrintTimingInfo;

public:
	std::string mLogPath;
};

#endif /* MinimizeDelaysModel_hpp */
