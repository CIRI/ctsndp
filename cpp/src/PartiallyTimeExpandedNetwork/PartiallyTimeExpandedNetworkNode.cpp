
#include "PartiallyTimeExpandedNetworkNode.hpp"
#include "PartiallyTimeExpandedNetworkArc.hpp"
#include "../ServiceNetwork/ServiceLocation.hpp"

#include <cassert>
#include <algorithm>

///////////////////////////////////////////////////////////////////////////
// Class cPartiallyTimeExpandedNetworkNode
///////////////////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkNode::cPartiallyTimeExpandedNetworkNode
                (
                 node_id_t id,
                 const cServiceLocation* const pServiceNode,
                 double time
                )
: cPartiallyTimeExpandedNetworkNode(id, pServiceNode, time, nullptr)
{}

cPartiallyTimeExpandedNetworkNode::cPartiallyTimeExpandedNetworkNode
                (
                    node_id_t id,
                    const cServiceLocation* const pServiceNode,
                    double time,
                    const cCommodityBundle* const pCommodity
                 )
:
    mId(id), mTime(time), mpServiceNode(pServiceNode)
{
	assert(mpServiceNode);

	if (pCommodity)
		mCommodities.push_back(pCommodity);
}

const std::string& cPartiallyTimeExpandedNetworkNode::getName() const
{
    return mpServiceNode->getName();
}

double cPartiallyTimeExpandedNetworkNode::cPartiallyTimeExpandedNetworkNode::getTime() const
{
    return mTime;
}

bool cPartiallyTimeExpandedNetworkNode::hasServiceLocation() const
{
    return mpServiceNode == nullptr ? false : true;
}

const cServiceLocation* const cPartiallyTimeExpandedNetworkNode::getServiceLocation() const
{
    return mpServiceNode;
}

bool cPartiallyTimeExpandedNetworkNode::operator==(const cPartiallyTimeExpandedNetworkNode& rhs) const
{
    return mTime == rhs.mTime && mpServiceNode == rhs.mpServiceNode && (mCommodities == rhs.mCommodities);
}

bool cPartiallyTimeExpandedNetworkNode::operator!=(const cPartiallyTimeExpandedNetworkNode& rhs) const
{
    return ! operator==(rhs);
}

bool cPartiallyTimeExpandedNetworkNode::hasCommodities() const
{
    return mCommodities.size() > 0;
}

const std::vector<const cCommodityBundle*>&
cPartiallyTimeExpandedNetworkNode::getCommodities() const
{
    return mCommodities;
}

void cPartiallyTimeExpandedNetworkNode::addCommodity(
                        const cCommodityBundle* const pCommodity)
{
    if (pCommodity)
    {
        if (!std::binary_search(mCommodities.begin(), mCommodities.end(), pCommodity))
        {
            mCommodities.push_back(pCommodity);
			std::sort(mCommodities.begin(), mCommodities.end());
		}
    }
}

bool cPartiallyTimeExpandedNetworkNode::hasCommodity(
                        const cCommodityBundle* const pCommodity) const
{
	if (!pCommodity)
	{
		std::cout << "hasCommodity: commodity is a nullptr!" << std::endl;
		return mCommodities.empty();
	}
    return std::binary_search(mCommodities.begin(), mCommodities.end(), pCommodity);
}

cPartiallyTimeExpandedNetworkArc* cPartiallyTimeExpandedNetworkNode::getStorageInflow() const
{
	cPartiallyTimeExpandedNetworkArc* result = nullptr;
	for (auto arc : mInflows)
	{
		if (arc->isStorageArc())
		{
			assert(!result);
			result = arc;
		}
	}
	return result;
}

cPartiallyTimeExpandedNetworkArc* cPartiallyTimeExpandedNetworkNode::getStorageOutflow() const
{
	cPartiallyTimeExpandedNetworkArc* result = nullptr;
	for (auto arc : mOutflows)
	{
		if (arc->isStorageArc())
		{
			assert(!result);
			result = arc;
		}
	}
	return result;
}


void cPartiallyTimeExpandedNetworkNode::removeFlow(const cPartiallyTimeExpandedNetworkArc* const pArc)
{
	removeInflow(pArc);
	removeOutflow(pArc);
}

void cPartiallyTimeExpandedNetworkNode::removeInflow(const cPartiallyTimeExpandedNetworkArc* const pArc)
{
    auto it = std::remove(mInflows.begin(), mInflows.end(), pArc);
    if (it != mInflows.end())
        mInflows.erase(it);
}

void cPartiallyTimeExpandedNetworkNode::removeOutflow(const cPartiallyTimeExpandedNetworkArc* const pArc)
{
    auto it = std::remove(mOutflows.begin(), mOutflows.end(), pArc);
    if (it != mOutflows.end())
        mOutflows.erase(it);
}

///////////////////////////////////////////////////////////////////////////
// Class cPartiallyTimeExpandedNetworkDelayNode
///////////////////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkDelayNode::cPartiallyTimeExpandedNetworkDelayNode(node_id_t id,
                                                            const cServiceLocation* const pServiceNode,
                                                            double time,
                                                            const cCommodityBundle* const pCommodity)
:
    cPartiallyTimeExpandedNetworkNode(id, pServiceNode, time, pCommodity),
    mHasMaxDelayTime(false),
    mMaxDelayTime(0)
{
    assert(pCommodity);

    mName = pServiceNode->getName() + ":delay";
}

const std::string& cPartiallyTimeExpandedNetworkDelayNode::getName() const
{
    return mName;
}

void cPartiallyTimeExpandedNetworkDelayNode::setMaxDelayTime(double max_delay_time)
{
    mHasMaxDelayTime = max_delay_time > 0;
    mMaxDelayTime = max_delay_time;
}

///////////////////////////////////////////////////////////////////////////////
// Free Functions
///////////////////////////////////////////////////////////////////////////////

std::string to_string(const cPartiallyTimeExpandedNetworkNode& node)
{
    return node.getName() + "(t:" + std::to_string(node.getTime()) + ")";
}

std::ostream& operator<<(std::ostream& out, const cPartiallyTimeExpandedNetworkNode& node)
{
    out << node.getName() << "(t:" << node.getTime();
    if (node.hasCommodities())
    {
		out << ", k = " << node.getCommodities().size();
	}
	out << ")";
	return out;
}
