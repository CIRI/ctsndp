
#include "PartiallyTimeExpandedNetworkArc.hpp"
#include "PartiallyTimeExpandedNetworkNode.hpp"
#include "../ServiceNetwork/ServiceLink.hpp"

#include <cassert>
#include <numeric>
#include <algorithm>


////////////////////////////////////////////////////////////
// cPartiallyTimeExpandedNetworkArc
////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkArc::cPartiallyTimeExpandedNetworkArc(
                arc_id_t id,
                const node_ptr_t pSource,
                const node_ptr_t pDestination,
                unsigned int capacity, 
                unsigned int per_unit_of_flow_capacity, 
                double fixed_cost,
                double per_unit_of_flow_cost, 
                const cServiceLink* pLink)
:
    mpSourceNode(pSource),
    mpDestination(pDestination),
    mId(id),
    mLimitArcCapacity(false),
    mArcCapacity(capacity),
    mArcPerUnitOfFlowCapacity(per_unit_of_flow_capacity),
    mFixedCost(fixed_cost),
    mPerUnitOfFlowCost(per_unit_of_flow_cost),
    mpServiceLink(pLink)
{
    assert(mpSourceNode);
    assert(mpDestination);
    assert(mArcCapacity >= 0);
    assert(per_unit_of_flow_capacity >= 1);
    assert(mFixedCost >= 0);
    assert(mPerUnitOfFlowCost >= 0);

    if (per_unit_of_flow_capacity >= 1)
        mArcPerUnitOfFlowCapacity = per_unit_of_flow_capacity;
}

cPartiallyTimeExpandedNetworkArc::cPartiallyTimeExpandedNetworkArc(
                                                                   arc_id_t id,
                                                                   const node_ptr_t pSource,
                                                                   const node_ptr_t pDestination,
                                                                   unsigned int capacity,
                                                                   double fixed_cost,
																   double per_unit_of_flow_cost)
:
    mpSourceNode(pSource),
    mpDestination(pDestination),
    mId(id),
    mLimitArcCapacity(false),
    mArcCapacity(capacity),
    mArcPerUnitOfFlowCapacity(1),
    mFixedCost(fixed_cost),
    mPerUnitOfFlowCost(per_unit_of_flow_cost),
    mpServiceLink(nullptr)
{
    assert(mpSourceNode);
    assert(mpDestination);
    assert(mArcCapacity >= 0);
    assert(mFixedCost >= 0);
	assert(mPerUnitOfFlowCost >= 0);
}

const node_ptr_t cPartiallyTimeExpandedNetworkArc::getSource() const
{
    return mpSourceNode;
}

bool cPartiallyTimeExpandedNetworkArc::hasSourceServiceLocation() const
{
    if (!mpSourceNode)
        return false;
    
    return mpSourceNode->hasServiceLocation();
}

const cServiceLocation* const cPartiallyTimeExpandedNetworkArc::getSourceServiceLocation() const
{
    if (!mpSourceNode)
        return nullptr;
    
    return mpSourceNode->getServiceLocation();
}

const std::string& cPartiallyTimeExpandedNetworkArc::getSourceServiceName() const
{
    return mpSourceNode->getName();
}

double cPartiallyTimeExpandedNetworkArc::getSourceTime() const
{
    return mpSourceNode->getTime();
}

bool cPartiallyTimeExpandedNetworkArc::isSource(const cPartiallyTimeExpandedNetworkNode* const node) const
{
    return (mpSourceNode->getServiceLocation() == node->getServiceLocation())
                && (mpSourceNode->getTime() == node->getTime());
}

const node_ptr_t cPartiallyTimeExpandedNetworkArc::getDestination() const
{
    return mpDestination;
}

node_ptr_t cPartiallyTimeExpandedNetworkArc::getDestinationNode()
{
    return mpDestination;
}

bool cPartiallyTimeExpandedNetworkArc::hasDestinationServiceLocation() const
{
    if (!mpDestination)
        return false;
    
    return mpDestination->hasServiceLocation();
}

const cServiceLocation* const cPartiallyTimeExpandedNetworkArc::getDestinationServiceLocation() const
{
    if (!mpDestination)
        return nullptr;

    return mpDestination->getServiceLocation();
}

const std::string& cPartiallyTimeExpandedNetworkArc::getDestinationServiceName() const
{
    return mpDestination->getName();
}
    
double cPartiallyTimeExpandedNetworkArc::getDestinationTime() const
{
    return mpDestination->getTime();
}

bool cPartiallyTimeExpandedNetworkArc::isDestination(const cPartiallyTimeExpandedNetworkNode* const node) const
{
    return (mpDestination->getServiceLocation() == node->getServiceLocation())
    && (mpDestination->getTime() == node->getTime());
}

const cServiceLink* const cPartiallyTimeExpandedNetworkArc::getServiceLink() const
{
    return mpServiceLink;
}

double cPartiallyTimeExpandedNetworkArc::getCycleTime() const
{
    if (mpServiceLink)
        return mpServiceLink->getCycleTime();
    
    return 0.0;
}

bool cPartiallyTimeExpandedNetworkArc::connectedToDelayNode() const
{
    return mpDestination->isDelayNode() || mpSourceNode->isDelayNode();
}

void cPartiallyTimeExpandedNetworkArc::setCapacity(unsigned int capacity)
{
	mArcCapacity = capacity;
}

////////////////////////////////////////////////////////////
// cPartiallyTimeExpandedNetworkTravelArc
////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkTravelArc::cPartiallyTimeExpandedNetworkTravelArc(arc_id_t id,
                                 const node_ptr_t pSource,
                                 const node_ptr_t pDestination,
                                 unsigned int capacity,
                                 unsigned int per_unit_of_flow_capacity,
                                 double fixed_cost,
                                 double per_unit_of_flow_cost,
                                 double travel_time,
                                 const cServiceLink* pLink)
:
    cPartiallyTimeExpandedNetworkArc(id, pSource, pDestination, capacity, per_unit_of_flow_capacity,
                                     fixed_cost, per_unit_of_flow_cost, pLink),
    mActualTravelTime(travel_time)
{
    assert(pSource->getServiceLocation() != pDestination->getServiceLocation());
    assert(mActualTravelTime >= 0.0);
}



double cPartiallyTimeExpandedNetworkTravelArc::getActualTravelTime() const
{
    return mActualTravelTime;
}


////////////////////////////////////////////////////////////
// cPartiallyTimeExpandedNetworkHoldoverArc
////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkHoldoverArc::cPartiallyTimeExpandedNetworkHoldoverArc(
        arc_id_t id,
        const node_ptr_t pSource,
        const node_ptr_t pDestination)
:
    cPartiallyTimeExpandedNetworkArc(id, pSource, pDestination,
            std::numeric_limits<unsigned int>::max(), 0.0, 0.0)
{
    assert(pSource->getServiceLocation() == pDestination->getServiceLocation());
}

cPartiallyTimeExpandedNetworkHoldoverArc::cPartiallyTimeExpandedNetworkHoldoverArc(
        arc_id_t id,
        const node_ptr_t pSource,
        const node_ptr_t pDestination,
        unsigned int capacity,
        double cost)
:
    cPartiallyTimeExpandedNetworkArc(id, pSource, pDestination,
                                     capacity, cost, 0.0)
{
    assert(pSource->getServiceLocation() == pDestination->getServiceLocation());
}

double cPartiallyTimeExpandedNetworkHoldoverArc::getActualTravelTime() const
{
    // A holdover arc has ZERO "travel time" because holdover arcs represent storage
    // at a given service location.  Zero travel time means that the commodity can
    // leave at any time after it is received.
    return 0.0;
}

////////////////////////////////////////////////////////////
// cPartiallyTimeExpandedNetworkHoldArc
////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkHoldArc::cPartiallyTimeExpandedNetworkHoldArc(
        arc_id_t id,
        const node_ptr_t pSource,
        const node_ptr_t pDestination,
        double hold_time)
:
	cPartiallyTimeExpandedNetworkArc(id, pSource, pDestination,
		std::numeric_limits<unsigned int>::max(), 0, 0)
{
	assert(pSource->getServiceLocation() == pDestination->getServiceLocation());
    mHoldTime = hold_time;
}

cPartiallyTimeExpandedNetworkHoldArc::cPartiallyTimeExpandedNetworkHoldArc(
        arc_id_t id,
        const node_ptr_t pSource,
        const node_ptr_t pDestination,
        double hold_time,
        unsigned int capacity,
        double cost)
:
	cPartiallyTimeExpandedNetworkArc(id, pSource, pDestination,
                                     capacity, cost, 0)
{
	assert(pSource->getServiceLocation() == pDestination->getServiceLocation());
	mHoldTime = hold_time;
}

double cPartiallyTimeExpandedNetworkHoldArc::getActualTravelTime() const
{
    // In hold arc, we use the "hold time" as the minimum time of storage
    // at a given service location.
    return mHoldTime;
}

////////////////////////////////////////////////////////////
// cPartiallyTimeExpandedNetworkDelayArc
////////////////////////////////////////////////////////////

cPartiallyTimeExpandedNetworkDelayArc::cPartiallyTimeExpandedNetworkDelayArc(arc_id_t id,
																const node_ptr_t pSource,
																const node_ptr_t pDestination)
:
	cPartiallyTimeExpandedNetworkArc(id, pSource, pDestination,
		std::numeric_limits<unsigned int>::max(),
		0.0, 0.0),
	mDelayTime(mpDestination->getTime() - mpSourceNode->getTime())
{
	assert(pSource);
	assert(pDestination);
	assert(pSource->getServiceLocation() == pDestination->getServiceLocation());
	assert(pSource->isDelayNode());
}

double cPartiallyTimeExpandedNetworkDelayArc::getActualTravelTime() const
{
    return mDelayTime;
}


////////////////////////////////////////////////////////////
// Arc algorithms
////////////////////////////////////////////////////////////

std::string to_string(const cPartiallyTimeExpandedNetworkArc& arc)
{
    return to_string(*arc.getSource()) + " -> " + to_string(*arc.getDestination());
}

std::ostream& operator<<(std::ostream& out, const cPartiallyTimeExpandedNetworkArc& arc)
{
    out << arc.getSourceServiceName() << "(t:" << arc.getSourceTime();

	if (arc.isDelayArc())
	{
		out << ", " << static_cast<const cPartiallyTimeExpandedNetworkDelayArc&>(arc).getCommodities().front()->getUniqueName();
	}

	out << ") -> ";
    out << arc.getDestinationServiceName() << "(t:" << arc.getDestinationTime();
    if (arc.getDestination()->hasCommodities())
    {
//        out << ", " << arc.getDestination()->getCommodityName();
		out << ", k:" << arc.getDestination()->getCommodities().size();
	}
    out << ")";
    return out;
}
