
#include "PartiallyTimeExpandedNetwork.hpp"
#include "../Commodities/CommodityBundle.hpp"
#include "../ServiceNetwork/ServiceLocation.hpp"
#include "../ServiceNetwork/ServiceLink.hpp"
#include <algorithm>
#include <cassert>

cPartiallyTimeExpandedNetwork::cPartiallyTimeExpandedNetwork()
{
    mNumNodes = 0;
    mNumArcs = 0;
	mNumHoldoverArcs = 0;

	mNumTavelArcs = 0;
	mNumDelayArcs = 0;
	mNumHoldArcs = 0;
	mNumDelayGroups = 0;

	mHasDelays = false;
}

cPartiallyTimeExpandedNetwork::~cPartiallyTimeExpandedNetwork()
{
    mNumNodes = 0;
    mNumArcs = 0;
	mNumHoldoverArcs = 0;

	mNumTavelArcs = 0;
	mNumDelayArcs = 0;
	mNumHoldArcs = 0;
	mNumDelayGroups = 0;

    for (auto arc : mArcs)
        delete arc;
    mArcs.clear();

    for (auto arc : mHoldoverArcs)
        delete arc;
    mHoldoverArcs.clear();

    for (auto node : mNodes)
        delete node;
    mNodes.clear();

    mCommodities.clear();
}

timepoints_t cPartiallyTimeExpandedNetwork::timepoints(const cServiceLocation* const pServiceNode) const
{
    timepoints_t time_points;
    auto it = mTimePoints.find(pServiceNode);
    if (it != mTimePoints.end())
        time_points.assign(it->second.begin(), it->second.end());
    return time_points;
}


void cPartiallyTimeExpandedNetwork::addShipment(const cCommodityShipment* const pShipment,
                 const node_ptr_t src, const node_ptr_t dst)
{
    auto it = mCommoditySourceNodes.find(pShipment);
    if (it == mCommoditySourceNodes.end())
    {
        mCommoditySourceNodes.insert(std::make_pair(pShipment, src));
    }
    
    it = mCommodityDestinationNodes.find(pShipment);
    if (it == mCommodityDestinationNodes.end())
    {
        mCommodityDestinationNodes.insert(std::make_pair(pShipment, dst));
    }
}

void cPartiallyTimeExpandedNetwork::addCommodity(
                        const cCommodityBundle* const pCommodity)
{
    if (pCommodity)
    {
        if (!std::binary_search(mCommodities.begin(), mCommodities.end(), pCommodity))
        {
            mCommodities.push_back(pCommodity);
            std::sort(mCommodities.begin(), mCommodities.end());
        }
    }
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findSourceNode(
                        const cCommodityBundle* const pCommodity) const
{
	if (pCommodity->getSourceNode())
		return const_cast<const node_ptr_t>(pCommodity->getSourceNode());

	/*
	 * If we had to "offset" the commodity bundle from the shipment,
	 * we will have to look it up the old fashion way.
	 */
    auto it = mCommoditySourceNodes.find(&pCommodity->getShipment());
    if (it != mCommoditySourceNodes.end())
        return it->second;

    return findNode(pCommodity->getSourceLocation(),
                    pCommodity->getEarliestAvailableTime(),
                    pCommodity);
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findDestinationNode(
                        const cCommodityBundle* const pCommodity) const
{
	if (pCommodity->getDestinationNode())
		return const_cast<const node_ptr_t>(pCommodity->getDestinationNode());

	auto it = mCommodityDestinationNodes.find(&pCommodity->getShipment());
    if (it != mCommodityDestinationNodes.end())
        return it->second;
    
	return findNode(pCommodity->getDestinationLocation(),
                    pCommodity->getLatestDeliveryTime(),
                    pCommodity);
}

const node_ptr_t cPartiallyTimeExpandedNetwork::addNode(
                            const cServiceLocation* const pServiceNode,
                            double time)
{
    auto it = mTimePoints.find(pServiceNode);
    if (it == mTimePoints.end())
    {
        timepoints_t time_points;
        time_points.push_back(time);
        mTimePoints.insert(std::make_pair(pServiceNode, time_points));
    }
    else
    {
        timepoints_t& time_points = it->second;
        time_points.push_back(time);
        std::sort(time_points.begin(), time_points.end());
        time_points.erase(std::unique(time_points.begin(), time_points.end()), time_points.end());
    }
    
    auto* node = new cPartiallyTimeExpandedNetworkNode(mNumNodes++, pServiceNode, time, nullptr);
    mNodes.push_back(node);
    
    return node;
}


const node_ptr_t cPartiallyTimeExpandedNetwork::addNode(const cPartiallyTimeExpandedNetworkNode* pNode, double time)
{
	auto it = mTimePoints.find(pNode->getServiceLocation());
	if (it == mTimePoints.end())
	{
		timepoints_t time_points;
		time_points.push_back(time);
		mTimePoints.insert(std::make_pair(pNode->getServiceLocation(), time_points));
	}
	else
	{
		timepoints_t& time_points = it->second;
		time_points.push_back(time);
		std::sort(time_points.begin(), time_points.end());
		time_points.erase(std::unique(time_points.begin(), time_points.end()), time_points.end());
	}

	auto* node = new cPartiallyTimeExpandedNetworkNode(mNumNodes++, pNode->getServiceLocation(), time);
	for (auto& k : pNode->getCommodities())
	{
		node->addCommodity(k);
	}

	mNodes.push_back(node);

	return node;
}

const delay_node_ptr_t cPartiallyTimeExpandedNetwork::addDelayNode(
                            const cServiceLocation* const pServiceNode,
                            double time,
                            const cCommodityBundle* const pCommodity)
{
    mHasDelays = true;
    
    auto it = mTimePoints.find(pServiceNode);
    if (it == mTimePoints.end())
    {
        timepoints_t time_points;
        time_points.push_back(time);
        mTimePoints.insert(std::make_pair(pServiceNode, time_points));
    }
    else
    {
        timepoints_t& time_points = it->second;
        time_points.push_back(time);
        std::sort(time_points.begin(), time_points.end());
        time_points.erase(std::unique(time_points.begin(), time_points.end()), time_points.end());
    }
    
    auto* node = new cPartiallyTimeExpandedNetworkDelayNode(mNumNodes++, pServiceNode, time, pCommodity);
    mNodes.push_back(node);
	mDelayNodes.push_back(node);

    return node;
}

const delay_node_ptr_t cPartiallyTimeExpandedNetwork::addDelayNode(
							const cServiceLocation* const pServiceNode,
							double time,
							const commodities_t& commodities)
{
	assert(!commodities.empty());

	mHasDelays = true;

	auto it = mTimePoints.find(pServiceNode);
	if (it == mTimePoints.end())
	{
		timepoints_t time_points;
		time_points.push_back(time);
		mTimePoints.insert(std::make_pair(pServiceNode, time_points));
	}
	else
	{
		timepoints_t& time_points = it->second;
		time_points.push_back(time);
		std::sort(time_points.begin(), time_points.end());
		time_points.erase(std::unique(time_points.begin(), time_points.end()), time_points.end());
	}

	auto* node = new cPartiallyTimeExpandedNetworkDelayNode(mNumNodes++, pServiceNode, time, commodities.front());
	for (auto k : commodities)
	{
		node->addCommodity(k);
	}
	mNodes.push_back(node);
	mDelayNodes.push_back(node);

	return node;
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findNode(
                                const std::string& location_name,
                                double time) const
{
    for (auto& node : mNodes)
    {
        if ((node->getServiceLocation()->getName() == location_name)
            && (node->getTime() == time))
            return node;
    }
    
    return nullptr;
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findNode(
                                const cServiceLocation* const pServiceNode,
                                double time) const
{
    for (auto& node : mNodes)
    {
        if ((node->getServiceLocation() == pServiceNode)
            && (node->getTime() == time))
                return node;
    }
    
    return nullptr;
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findNode(
							const node_ptr_t pNode, double time) const
{
	if (pNode->getTime() == time)
		return pNode;

	auto node = pNode;
	auto t = node->getTime();
	while (t < time)
	{
		auto arc = node->getStorageOutflow();
		if (!arc)
			return nullptr;
		node = arc->getDestinationNode();
		t = node->getTime();
		if (t == time)
			return node;
		if (t > time)
			return nullptr;
	}

	node = pNode;
	t = node->getTime();
	while (t > time)
	{
		auto arc = node->getStorageInflow();
		if (!arc)
			return nullptr;
		node = arc->getSource();
		t = node->getTime();
		if (t == time)
			return node;
		if (t < time)
			return nullptr;
	}

	return nullptr;
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findNode(
                            const std::string& location_name,
                            double time,
                            const cCommodityBundle* const pCommodity) const
{
    for (auto& node : mNodes)
    {
        if ((node->getServiceLocation()->getName() == location_name)
            && (node->getTime() == time)
            && (node->hasCommodity(pCommodity)))
            return node;
    }
    
    return nullptr;
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findNode(
                            const cServiceLocation* const pServiceNode,
                            double time,
                            const cCommodityBundle* const pCommodity) const
{
    for (auto& node : mNodes)
    {
        if ((node->getServiceLocation() == pServiceNode)
            && (node->getTime() == time))
        {
            if (!pCommodity)
                return node;
            if (node->hasCommodity(pCommodity))
                return node;
        }
    }
    
    return nullptr;
}


bool cPartiallyTimeExpandedNetwork::hasNode(const node_ptr_t node, double time) const
{
	if (node->getTime() == time)
		return true;

	auto n = node;
	auto t = n->getTime();
	while (t < time)
	{
		auto arc = n->getStorageOutflow();
		if (!arc)
			return false;
		n = arc->getDestinationNode();
		t = n->getTime();
		if (t == time)
			return true;
		if (t > time)
			return false;
	}

	n = node;
	t = n->getTime();
	while (t > time)
	{
		auto arc = n->getStorageInflow();
		if (!arc)
			return false;
		n = arc->getSource();
		t = n->getTime();
		if (t == time)
			return true;
		if (t < time)
			return false;
	}

	return false;
}

bool cPartiallyTimeExpandedNetwork::hasNode(const cServiceLocation* const pServiceNode, double time) const
{
	for (auto& node : mNodes)
	{
		if ((node->getServiceLocation() == pServiceNode)
			&& (node->getTime() == time))
			return true;
	}

	return false;
}

bool cPartiallyTimeExpandedNetwork::hasDelayNode(const node_ptr_t node, double time) const
{
	return hasDelayNode(node->getServiceLocation(), time);
}

bool cPartiallyTimeExpandedNetwork::hasDelayNode(const cServiceLocation* const pServiceNode, double time) const
{
	for (auto& node : mDelayNodes)
	{
		if ((node->getServiceLocation() == pServiceNode)
			&& (node->getTime() == time))
			return true;
	}

	return false;
}

std::vector<node_ptr_t> cPartiallyTimeExpandedNetwork::findNodes(
                                        const cServiceLocation* const pServiceNode,
                                        bool include_delay_nodes) const
{
    std::vector<node_ptr_t> matching_nodes;
    for (auto& node : mNodes)
    {
//        if (!include_delay_nodes && node->isDelayNode())
		if (!include_delay_nodes && node->isDelayNode())
				continue;
        if (node->getServiceLocation() == pServiceNode)
            matching_nodes.push_back(node);
    }
    return matching_nodes;
}

const node_ptr_t cPartiallyTimeExpandedNetwork::findDelayNode(
                                        const cServiceLocation* const pServiceNode,
                                        double time) const
{
    for (auto& node : mNodes)
    {
        if (node->isDelayNode())
        {
            if ((node->getServiceLocation() == pServiceNode)
                    && (node->getTime() == time))
                return node;
        }
    }
    
    return nullptr;
}

std::vector<node_ptr_t> cPartiallyTimeExpandedNetwork::findDelayNodes(
                                        const cServiceLocation* const pServiceNode) const
{
    std::vector<node_ptr_t> matching_nodes;
    for (auto& node : mNodes)
    {
        if (node->isDelayNode())
        {
            if (node->getServiceLocation() == pServiceNode)
                matching_nodes.push_back(node);
        }
    }
    return matching_nodes;
}

void cPartiallyTimeExpandedNetwork::removeNode(node_ptr_t pNode)
{
	if (!pNode) return;
	auto inflows = pNode->mInflows;
	for (auto& inflow : inflows)
	{
		if (inflow->isHoldoverArc())
		{
			deleteHoldoverArc(inflow);
		}
		else
			deleteArc(inflow);
	}

	auto outflows = pNode->mOutflows;
	for (auto& outflow : outflows)
	{
		if (outflow->isHoldoverArc())
		{
			deleteHoldoverArc(outflow);
		}
		else
			deleteArc(outflow);
	}

	auto it = std::find(mNodes.begin(), mNodes.end(), pNode);
	if (it != mNodes.end())
	{
		mNodes.erase(it);
	}
}

void cPartiallyTimeExpandedNetwork::deleteNode(node_ptr_t& pNode)
{
	if (!pNode) return;
	removeNode(pNode);

	delete pNode;
	pNode = nullptr;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::addArc(
                const node_ptr_t pSourceNode,
                const node_ptr_t pDestinationNode,
                unsigned int capacity, unsigned int per_unit_of_flow_capacity,
                double fixed_cost, double per_unit_of_flow_cost,
                double travel_time, const cServiceLink* link)
{
    // We can't have an arc whose source is a delay node.  Delay nodes only
    // terminate arcs
    if (pSourceNode->isDelayNode())
        return nullptr;
    
    auto* arc = new cPartiallyTimeExpandedNetworkTravelArc(mNumArcs++,
                                                    pSourceNode,
                                                    pDestinationNode,
                                                    capacity,
                                                    per_unit_of_flow_capacity,
                                                    fixed_cost, per_unit_of_flow_cost,
                                                    travel_time, link);

	++mNumTavelArcs;
	mArcs.push_back(arc);
    std::sort(mArcs.begin(), mArcs.end());
    pSourceNode->mOutflows.push_back(arc);
    pDestinationNode->mInflows.push_back(arc);
    return arc;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::addArc(
                                                      const node_ptr_t pSourceNode,
                                                      const node_ptr_t pDestinationNode,
                                                      const cServiceLink* link)
{
    return addArc(pSourceNode, pDestinationNode,
                   link->getCapacity(), link->getCommodityBundleCapacity(),
                   link->getFixedCost(), link->getCommodityBundleCost(),
                   link->getTravelTime(), link);
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::addArc(
                                                      const node_ptr_t pSourceNode,
                                                      const node_ptr_t pDestinationNode,
                                                      const arc_ptr_t arc)
{
    return addArc(pSourceNode, pDestinationNode,
                   arc->getCapacity(), arc->getPerUnitOfFlowCapacity(),
                   arc->getFixedCost(), arc->getPerUnitOfFlowCost(),
                   arc->getActualTravelTime(), arc->mpServiceLink);
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::findArc(const node_ptr_t pSourceNode,
                        const node_ptr_t pDestinationNode) const
{
	for (auto& arc : pSourceNode->getOutflows())
	{
		if (arc->getDestination() == pDestinationNode)
			return arc;
	}
/*
    for (auto& arc : mArcs)
    {
        if ((arc->getSource() == pSourceNode) && (arc->getDestination() == pDestinationNode))
            return arc;
    }
*/
    return nullptr;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::findArc(
                    const cServiceLocation* const pSourceNode,
                    const cServiceLocation* const pDestinationNode,
                    const cCommodityBundle* const pCommodity) const
{
    arc_ptr_t result = nullptr;
    for (auto& arc : mArcs)
    {
        if ((arc->getSourceServiceLocation() == pSourceNode) && (arc->getDestinationServiceLocation() == pDestinationNode))
        {
            auto src = arc->getSource();
            auto dst = arc->getDestination();
            if (pCommodity)
            {
                if (src->hasCommodities() || dst->hasCommodities())
                {
                    if (src->hasCommodity(pCommodity)
                         || dst->hasCommodity(pCommodity))
                    {
                        return arc;
                    }
                }
                else
                    result = arc;
            }
            else
            {
                if (!src->hasCommodities() && !dst->hasCommodities())
                    return arc;
            }
        }
    }
    return result;
}

std::vector<arc_ptr_t> cPartiallyTimeExpandedNetwork::findArcs(
                            const cServiceLocation* const pSourceNode,
                            const cServiceLocation* const pDestinationNode,
                            const cCommodityBundle* const pCommodity,
                            bool excludeDelayArcs) const
{
    std::vector<arc_ptr_t> results;
    for (auto& arc : mArcs)
    {
        if (excludeDelayArcs && arc->connectedToDelayNode())
            continue;
        
        if ((arc->getSourceServiceLocation() == pSourceNode) && (arc->getDestinationServiceLocation() == pDestinationNode))
        {
            if (pCommodity)
            {
                if (arc->getSource()->hasCommodity(pCommodity))
                {
                    results.push_back(arc);
                }
                else if (arc->getDestination()->hasCommodities()
                    && arc->getDestination()->hasCommodity(pCommodity))
                {
                    results.push_back(arc);
                }
                else
                    results.push_back(arc);
            }
            else
            {
//                if (!arc->getSource()->hasCommodities() &&
//                      !arc->getDestination()->hasCommodities())
                    results.push_back(arc);
            }
        }
    }
    return results;
}

bool cPartiallyTimeExpandedNetwork::hasArc(const arc_ptr_t pArc) const
{
    if (!pArc) return false;
    if (std::binary_search(mArcs.begin(), mArcs.end(), pArc))
        return true;
    return std::binary_search(mHoldoverArcs.begin(), mHoldoverArcs.end(), static_cast<holdover_arc_ptr_t>(pArc));
}

void cPartiallyTimeExpandedNetwork::removeArc(arc_ptr_t pArc)
{
    if (!pArc) return;
    pArc->mpSourceNode->removeOutflow(pArc);
    pArc->mpDestination->removeInflow(pArc);

    auto it = std::find(mArcs.begin(), mArcs.end(), pArc);
    if (it != mArcs.end())
    {
        mArcs.erase(it);
    }
}

void cPartiallyTimeExpandedNetwork::deleteArc(arc_ptr_t& pArc)
{
    if (!pArc) return;
    removeArc(pArc);

    delete pArc;
    pArc = nullptr;
}

const holdover_arc_ptr_t  cPartiallyTimeExpandedNetwork::addHoldoverArc(
                const node_ptr_t pSourceNode,
                const node_ptr_t pDestinationNode)
{
    holdover_arc_ptr_t arc = new cPartiallyTimeExpandedNetworkHoldoverArc(mNumHoldoverArcs++,
                                                                         pSourceNode, pDestinationNode);

    mHoldoverArcs.push_back(arc);
    std::sort(mHoldoverArcs.begin(), mHoldoverArcs.end());

	assert(pSourceNode->getStorageOutflow() == nullptr);
    pSourceNode->mOutflows.insert(pSourceNode->mOutflows.begin(), arc);

	assert(pDestinationNode->getStorageInflow() == nullptr);
	pDestinationNode->mInflows.insert(pDestinationNode->mInflows.begin(), arc);
    
    return arc;
}

const holdover_arc_ptr_t  cPartiallyTimeExpandedNetwork::addHoldoverArc(const node_ptr_t pSourceNode,
                                                   const node_ptr_t pDestinationNode,
                                                   unsigned int capacity,
                                                   double cost)
{
    holdover_arc_ptr_t arc = new cPartiallyTimeExpandedNetworkHoldoverArc(mNumHoldoverArcs++,
                                                                          pSourceNode,
                                                                          pDestinationNode,
                                                                          capacity, cost);

    mHoldoverArcs.push_back(arc);
    std::sort(mHoldoverArcs.begin(), mHoldoverArcs.end());

	assert(pSourceNode->getStorageOutflow() == nullptr);
	pSourceNode->mOutflows.insert(pSourceNode->mOutflows.begin(), arc);

	assert(pDestinationNode->getStorageInflow() == nullptr);
	pDestinationNode->mInflows.insert(pDestinationNode->mInflows.begin(), arc);

    return arc;
}

const holdover_arc_ptr_t cPartiallyTimeExpandedNetwork::findHoldoverArc(const node_ptr_t pSourceNode,
                                                       const node_ptr_t pDestinationNode) const
{
    for (auto& arc : mHoldoverArcs)
    {
        if ((arc->getSource() == pSourceNode) && (arc->getDestination() == pDestinationNode))
            return arc;
    }
    return nullptr;
}

void cPartiallyTimeExpandedNetwork::removeHoldoverArc(const holdover_arc_ptr_t pArc)
{
	if (!pArc) return;

	pArc->mpSourceNode->removeOutflow(pArc);
	pArc->mpDestination->removeInflow(pArc);

	auto it = std::find(mHoldoverArcs.begin(), mHoldoverArcs.end(), pArc);
	if (it != mHoldoverArcs.end())
	{
		mHoldoverArcs.erase(it);
	}
}

void cPartiallyTimeExpandedNetwork::removeHoldoverArc(const arc_ptr_t pArc)
{
    if (!pArc) return;

    pArc->mpSourceNode->removeOutflow(pArc);
    pArc->mpDestination->removeInflow(pArc);

    auto it = std::find(mHoldoverArcs.begin(), mHoldoverArcs.end(), pArc);
    if (it != mHoldoverArcs.end())
    {
        mHoldoverArcs.erase(it);
    }
}

void cPartiallyTimeExpandedNetwork::deleteHoldoverArc(holdover_arc_ptr_t& pArc)
{
	if (!pArc) return;
	removeHoldoverArc(pArc);

	delete pArc;
	pArc = nullptr;
}

void cPartiallyTimeExpandedNetwork::deleteHoldoverArc(arc_ptr_t& pArc)
{
    if (!pArc) return;
    removeHoldoverArc(pArc);

    delete pArc;
    pArc = nullptr;
}


const arc_ptr_t  cPartiallyTimeExpandedNetwork::addHoldArc(
				const node_ptr_t pSourceNode,
                const node_ptr_t pDestinationNode,
                double hold_time)
{
	arc_ptr_t arc = new cPartiallyTimeExpandedNetworkHoldArc(mNumArcs++,
																 pSourceNode, pDestinationNode,
                                                                 hold_time);
	++mNumHoldArcs;
	mArcs.push_back(arc);
	std::sort(mArcs.begin(), mArcs.end());

	assert(pSourceNode->getStorageOutflow() == nullptr);
	pSourceNode->mOutflows.insert(pSourceNode->mOutflows.begin(), arc);

	assert(pDestinationNode->getStorageInflow() == nullptr);
	pDestinationNode->mInflows.insert(pDestinationNode->mInflows.begin(), arc);
    
    return arc;
}

const arc_ptr_t  cPartiallyTimeExpandedNetwork::addHoldArc(const node_ptr_t pSourceNode,
												const node_ptr_t pDestinationNode,
                                                double hold_time,
                                                unsigned int capacity,
                                                double cost)
{
	arc_ptr_t arc = new cPartiallyTimeExpandedNetworkHoldArc(mNumArcs++,
																  pSourceNode,
                                                                  pDestinationNode,
                                                                  hold_time,
                                                                  capacity, cost);

	++mNumHoldArcs;
	mArcs.push_back(arc);
	std::sort(mArcs.begin(), mArcs.end());

	assert(pSourceNode->getStorageOutflow() == nullptr);
	pSourceNode->mOutflows.insert(pSourceNode->mOutflows.begin(), arc);

	assert(pDestinationNode->getStorageInflow() == nullptr);
	pDestinationNode->mInflows.insert(pDestinationNode->mInflows.begin(), arc);

    return arc;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::findHoldArc(const node_ptr_t pSourceNode,
															const node_ptr_t pDestinationNode) const
{
	for (auto& arc : mArcs)
	{
		if ((arc->getSource() == pSourceNode) && (arc->getDestination() == pDestinationNode))
			return arc;
	}
	return nullptr;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::addDelayArc(const node_ptr_t pSourceNode,
														const node_ptr_t pDestinationNode)
{
	mHasDelays = true;

	// The source node must be a delay node.
	if (!pSourceNode->isDelayNode())
		return nullptr;

	auto* delay_arc = new cPartiallyTimeExpandedNetworkDelayArc(mNumArcs++,
		pSourceNode,
		pDestinationNode);

	++mNumDelayArcs;

	mArcs.push_back(delay_arc);
	std::sort(mArcs.begin(), mArcs.end());

	assert(pSourceNode->getStorageOutflow() == nullptr);
	pSourceNode->mOutflows.insert(pSourceNode->mOutflows.begin(), delay_arc);

	// We don't check the destination because we expect multiple incoming
	// storage arcs
	pDestinationNode->mInflows.push_back(delay_arc);

	return delay_arc;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::getAllInflowArcsToNode(
	const node_ptr_t pNode,
	bool include_holdover_arcs,
	bool include_delay_arcs) const
{
	std::vector<arc_ptr_t> inflows;

	auto& arcs = pNode->mInflows;
	for (auto& arc : arcs)
	{
		if (!include_holdover_arcs && arc->isHoldoverArc())
			continue;

		if (!include_delay_arcs && arc->isDelayArc())
			continue;

		inflows.push_back(arc);
	}

	return inflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::getAllInflowArcsToNode(
	const node_ptr_t pNode,
	const cCommodityBundle* const pCommodity,
	bool include_holdover_arcs) const
{
	std::vector<arc_ptr_t> inflows;

	auto& arcs = pNode->mInflows;
	for (auto& arc : arcs)
	{
		if (!include_holdover_arcs && arc->isHoldoverArc())
			continue;

		if (arc->getDestination()->hasCommodity(pCommodity))
			inflows.push_back(arc);
	}

	return inflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::getAllOutflowArcsFromNode(
	const node_ptr_t pNode,
	bool include_holdover_arcs) const
{
	std::vector<arc_ptr_t> outflows;

	auto& arcs = pNode->mOutflows;
	for (auto& arc : arcs)
	{
		if (!include_holdover_arcs && arc->isHoldoverArc())
			continue;

		outflows.push_back(arc);
	}

	return outflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::getAllHoldoverArcsInflowToNode(
	const node_ptr_t pNode) const
{
	std::vector<arc_ptr_t> inflows;

	auto& arcs = pNode->mInflows;
	for (auto& arc : arcs)
	{
		if (arc->isHoldoverArc())
			inflows.push_back(arc);
	}

	return inflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::getAllHoldoverArcsOutflowFromNode(
	const node_ptr_t pNode) const
{
	std::vector<arc_ptr_t> outflows;

	auto& arcs = pNode->mOutflows;
	for (auto& arc : arcs)
	{
		if (arc->isHoldoverArc())
			outflows.push_back(arc);
	}
	return outflows;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::findDelayArc(const node_ptr_t pSourceNode,
									const node_ptr_t pDestinationNode) const
{
	for (auto& arc : mArcs)
	{
		if (!arc->isDelayArc())
			continue;

		if ((arc->getSource() == pSourceNode) && (arc->getDestination() == pDestinationNode))
		{
			return arc;
		}
	}
	return nullptr;
}

const arc_ptr_t cPartiallyTimeExpandedNetwork::findDelayArc(const node_ptr_t pSourceNode,
									const node_ptr_t pDestinationNode,
									const cCommodityBundle* const pCommodity) const
{
    for (auto& arc : mArcs)
    {
        if (!arc->isDelayArc())
            continue;
        
		if ((arc->getSource() == pSourceNode) && (arc->getDestination() == pDestinationNode)
			&& static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc)->hasCommodity(pCommodity))
		{
			return arc;
		}
    }
    return nullptr;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::findAllDelayArcs() const
{
    std::vector<arc_ptr_t> delayArcs;
    for (auto& arc : mArcs)
    {
        if (! arc->isDelayArc() )
            continue;
        
        delayArcs.push_back(arc);
    }

    return delayArcs;
}

void cPartiallyTimeExpandedNetwork::removeDelayArc(delay_arc_ptr_t pArc)
{
	if (!pArc) return;
	pArc->mpSourceNode->removeOutflow(pArc);
	pArc->mpDestination->removeInflow(pArc);

	auto it = std::find(mArcs.begin(), mArcs.end(), pArc);
	if (it != mArcs.end())
	{
		mArcs.erase(it);
	}
}

void cPartiallyTimeExpandedNetwork::deleteDelayArc(delay_arc_ptr_t& pArc)
{
	if (!pArc) return;
	removeDelayArc(pArc);

	delete pArc;
	pArc = nullptr;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::findAllInflowArcsToNode(
                                                        const node_ptr_t pNode,
                                                        bool include_holdover_arcs,
                                                        bool include_delay_arcs) const
{
    std::vector<arc_ptr_t> inflows;

	for (auto& arc : mArcs)
	{
		if (!include_delay_arcs && arc->isDelayArc())
			continue;

		if (arc->getDestination() == pNode)
		{
			inflows.push_back(arc);
		}
	}

	if (include_holdover_arcs)
    {
		for (auto& arc : mHoldoverArcs)
		{
			if (arc->getDestination() == pNode)
			{
				inflows.push_back(arc);
			}
		}
	}
    
   return inflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::findAllOutflowArcsFromNode(
                                                        const node_ptr_t pNode,
                                                        bool include_holdover_arcs) const
{
    std::vector<arc_ptr_t> outflows;

	for (auto& arc : mArcs)
	{
		if (arc->getSource() == pNode)
		{
			outflows.push_back(arc);
		}
	}

	if (include_holdover_arcs)
	{
		for (auto& arc : mHoldoverArcs)
		{
			if (arc->getSource() == pNode)
			{
				outflows.push_back(arc);
			}
		}
	}

	return outflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::findAllHoldoverArcsInflowToNode(
                                                                               const node_ptr_t pNode) const
{
    std::vector<arc_ptr_t> inflows;

	for (auto& arc : mHoldoverArcs)
	{
		if (arc->getDestination() == pNode)
		{
			inflows.push_back(arc);
		}
	}

    return inflows;
}

std::vector<arc_ptr_t>  cPartiallyTimeExpandedNetwork::findAllHoldoverArcsOutflowFromNode(
                                                                                  const node_ptr_t pNode) const
{
    std::vector<arc_ptr_t> outflows;

	for (auto& arc : mHoldoverArcs)
	{
		if (arc->getSource() == pNode)
		{
			outflows.push_back(arc);
		}
	}
	return outflows;
}


void cPartiallyTimeExpandedNetwork::rehash()
{
    mNumNodes = 0;
    for (auto& node : mNodes)
        node->mId = mNumNodes++;
    
    mNumArcs = 0;
	mNumTavelArcs = 0;
	mNumHoldArcs = 0;

	for (auto& arc : mArcs)
	{
		if (arc->isDelayArc())
			continue;
		arc->mId = mNumArcs++;

		if (arc->isStorageArc())
			++mNumHoldArcs;
		else
			++mNumTavelArcs;
	}

	mNumDelayArcs = 0;
	for (auto& arc : mArcs)
	{
		if (!arc->isDelayArc())
			continue;
		arc->mId = mNumArcs++;
		++mNumDelayArcs;
	}

	std::sort(mArcs.begin(), mArcs.end(), [](arc_ptr_t a, arc_ptr_t b) {return a->mId < b->mId;  });


    mNumHoldoverArcs = 0;
	for (auto& arc : mHoldoverArcs)
	{
		arc->mId = mNumHoldoverArcs++;
	}
}

void cPartiallyTimeExpandedNetwork::print(std::ostream& out) const
{
    std::vector<const cServiceLocation*> serviceLocations;
    
    out << "Partially Time Expanded Network:" << "\n";
    out << "Number of Nodes: " << mNodes.size() << "\n";
    out << "Number of Arcs: " << mArcs.size() << "\n";
    out << "Number of Holdover Arcs: " << mHoldoverArcs.size() << "\n";
    out << "Number of Commodities: " << mCommodities.size() << "\n";
    out << "\n";
    out << "Commodity Information: " << "\n";
    for (auto& k : mCommodities)
    {
        out << "\t" << k->getFullyQualifiedName() << ": " << k->getSourceName() << "(eat: " << k->getEarliestAvailableTime();
        out << ") -> " << k->getDestinationName() << "(ldt: " << k->getLatestDeliveryTime();
        if (k->hasDelayCost())
            out << ", delay cost: " << k->getDelayCost();
        if (k->hasMaxDelayTime())
            out << ", max delay time: " << k->getMaxDelayTime();
		if (k->hasDelayPenalty())
			out << ", delay penalty: " << k->getDelayPenaltyRate();
		if (k->hasDelayPenaltyLimit())
			out << ", max delay penalty: " << k->getDelayPenaltyLimit();
		out << ")\n";
    }
    out << "\n";
    out << "Node Information: " << "\n";
    for (auto& node : mNodes)
    {
        if (node->isDelayNode())
            continue;
        const auto serviceLocation = node->getServiceLocation();
	if (!std::binary_search(serviceLocations.begin(), serviceLocations.end(), serviceLocation))
	{
            serviceLocations.push_back(node->getServiceLocation());
	    std::sort(serviceLocations.begin(), serviceLocations.end());
	}
        out << "\tNode" << node->getId() << ": " << *node << "\n";
    }
    out << "\n";
    out << "Delay Node Information: " << "\n";
    for (auto& node : mNodes)
    {
        if (node->isDelayNode())
        {
			const auto serviceLocation = node->getServiceLocation();
			if (!std::binary_search(serviceLocations.begin(), serviceLocations.end(), serviceLocation))
			{
				serviceLocations.push_back(node->getServiceLocation());
				std::sort(serviceLocations.begin(), serviceLocations.end());
			}
            out << "\tDelay Node" << node->getId() << ": " << *node << "\n";
        }
    }
    out << "\n";
    out << "Arc Information: " << "\n";
    for (auto& arc : mArcs)
    {
        if (arc->isDelayArc() || arc->isStorageArc())
            continue;
        out << "\tArc" << arc->getId() << ": " << *arc;
        if (arc->getServiceLink())
            out << " Link: " << arc->getServiceLink()->getName();
        if (arc->arcCapacityIsLimited())
            out << " (cap=" << arc->getCapacity() << ")";
        out << "\n";
    }
    out << "\n";
	out << "Hold Arc Information: " << "\n";
	for (auto& arc : mArcs)
	{
		if (arc->isDelayArc() || !arc->isStorageArc())
			continue;
		out << "\tArc" << arc->getId() << ": " << *arc << "\n";
	}
	out << "\n";
	out << "Holdover Arc Information: " << "\n";
    for (auto& arc : mHoldoverArcs)
    {
        out << "\tArc" << arc->getId() << ": " << *arc << "\n";
    }
    out << "\n";
    out << "Delay Arc Information: " << "\n";
    for (auto& arc : mArcs)
    {
        if (arc->isDelayArc())
        {
            out << "\tArc" << arc->getId() << ": " << *arc << "\n";
        }
    }
    out << "\n";
    out << "Timepoint Information: " << "\n";
    for (auto& loc : serviceLocations)
    {
        auto tp = timepoints(loc);
        if (tp.empty())
            continue;
        
        out << loc->getName() << ": ";
        for (int i = 0; i < tp.size()-1; ++i)
        {
            out << tp[i] << ", ";
        }
        out << tp[tp.size()-1] << std::endl;
    }
}
