
#pragma once

#include "PartiallyTimeExpandedNetworkNode.hpp"
#include "PartiallyTimeExpandedNetworkArc.hpp"

#include <vector>
#include <deque>
#include <map>
#include <iostream>

class cServiceLocation;
class cServiceLink;

typedef std::vector<const cCommodityBundle*> commodities_t;
typedef std::vector<cPartiallyTimeExpandedNetworkNode*> nodes_t;
typedef std::vector<arc_ptr_t> arcs_t;
typedef std::vector<holdover_arc_ptr_t> holdover_arcs_t;
typedef std::vector<double> timepoints_t;

class cPartiallyTimeExpandedNetwork
{
public:
    cPartiallyTimeExpandedNetwork();
    ~cPartiallyTimeExpandedNetwork();

    /**
     * Return all of the commodities within the service
     * network
     **/
    const commodities_t&  commodities() const;

    const nodes_t& nodes() const;
	const nodes_t& delay_nodes() const;
	const arcs_t& arcs() const;
    const holdover_arcs_t& holdover_arcs() const;
    timepoints_t timepoints(const cServiceLocation* const pServiceNode) const;

    unsigned int getNumNodes() const;
    unsigned int getNumArcs() const;
	unsigned int getNumHoldoverArcs() const;

	// Returns the number of the different types of arcs
	unsigned int getNumTravelArcs() const;
	unsigned int getNumDelayArcs() const;
	unsigned int getNumHoldArcs() const;

    bool hasDelays() const;

    void addShipment(const cCommodityShipment* const pShipment,
                     const node_ptr_t src, const node_ptr_t dst);

    void addCommodity(const cCommodityBundle* const pCommodity);

    const node_ptr_t findSourceNode(const cCommodityBundle* const pCommodity) const;
    
    const node_ptr_t findDestinationNode(const cCommodityBundle* const pCommodity) const;
    

    const node_ptr_t addNode(const cServiceLocation* const pServiceNode,
                    double time);

	const node_ptr_t addNode(const cPartiallyTimeExpandedNetworkNode* pNode, double time);

    const delay_node_ptr_t addDelayNode(const cServiceLocation* const pServiceNode,
                                        double time,
                                        const cCommodityBundle* const pCommodity);

	const delay_node_ptr_t addDelayNode(const cServiceLocation* const pServiceNode,
										double time,
										const commodities_t& commodities);

    const node_ptr_t findNode(const std::string& location_name,
                              double time) const;
    
    const node_ptr_t findNode(const cServiceLocation* const pServiceNode, double time) const;

	const node_ptr_t findNode(const node_ptr_t pNode, double time) const;

    const node_ptr_t findNode(const std::string& location_name,
                              double time,
                              const cCommodityBundle* const pCommodity) const;
    
    const node_ptr_t findNode(const cServiceLocation* const pServiceNode,
                              double time,
                              const cCommodityBundle* const pCommodity) const;


	bool hasNode(const node_ptr_t node, double time) const;
	bool hasNode(const cServiceLocation* const pServiceNode, double time) const;

	bool hasDelayNode(const node_ptr_t node, double time) const;
	bool hasDelayNode(const cServiceLocation* const pServiceNode, double time) const;

    std::vector<node_ptr_t> findNodes(const cServiceLocation* const pServiceNode, bool include_delay_nodes = true) const;
    
    const node_ptr_t findDelayNode(const cServiceLocation* const pServiceNode, double time) const;
    std::vector<node_ptr_t> findDelayNodes(const cServiceLocation* const pServiceNode) const;

	void removeNode(node_ptr_t pNode);
	void deleteNode(node_ptr_t& pNode);

    const arc_ptr_t addArc(const node_ptr_t pSourceNode,
                           const node_ptr_t pDestinationNode,
                           unsigned int capacity,
                           unsigned int per_unit_of_flow_capacity,
                           double fixed_cost,
                           double per_unit_of_flow_cost,
                           double travel_time,
                           const cServiceLink* link);

    const arc_ptr_t addArc(const node_ptr_t pSourceNode,
                           const node_ptr_t pDestinationNode,
                           const cServiceLink* link);

    const arc_ptr_t addArc(const node_ptr_t pSourceNode,
                           const node_ptr_t pDestinationNode,
                           const arc_ptr_t arc);

    const arc_ptr_t findArc(const node_ptr_t pSourceNode,
                             const node_ptr_t pDestinationNode) const;

    const arc_ptr_t findArc(const cServiceLocation* const pSourceNode,
                            const cServiceLocation* const pDestinationNode,
                            const cCommodityBundle* const pCommodity) const;

    std::vector<arc_ptr_t> findArcs(
                            const cServiceLocation* const pSourceNode,
                            const cServiceLocation* const pDestinationNode,
                            const cCommodityBundle* const pCommodity,
                            bool excludeDelayArcs = false) const;
    
    bool hasArc(const arc_ptr_t pArc) const;

    void removeArc(arc_ptr_t pArc);
    void deleteArc(arc_ptr_t& pArc);

    const holdover_arc_ptr_t addHoldoverArc(const node_ptr_t pSourceNode,
                                            const node_ptr_t pDestinationNode);

    const holdover_arc_ptr_t addHoldoverArc(const node_ptr_t pSourceNode,
                                            const node_ptr_t pDestinationNode,
                                            unsigned int capacity, double cost);

    const holdover_arc_ptr_t findHoldoverArc(const node_ptr_t pSourceNode,
                                             const node_ptr_t pDestinationNode) const;

    void removeHoldoverArc(const holdover_arc_ptr_t pArc);
	void removeHoldoverArc(const arc_ptr_t pArc);
	void deleteHoldoverArc(holdover_arc_ptr_t& pArc);
	void deleteHoldoverArc(arc_ptr_t& pArc);

	const arc_ptr_t addHoldArc(const node_ptr_t pSourceNode,
									const node_ptr_t pDestinationNode,
                                    double hold_time);

	const arc_ptr_t addHoldArc(const node_ptr_t pSourceNode,
									const node_ptr_t pDestinationNode,
                                    double hold_time, unsigned int capacity,
                                    double cost);

	const arc_ptr_t findHoldArc(const node_ptr_t pSourceNode,
								const node_ptr_t pDestinationNode) const;

	const arc_ptr_t addDelayArc(const node_ptr_t pSourceNode,
								const node_ptr_t pDestinationNode);

	const arc_ptr_t findDelayArc(const node_ptr_t pSourceNode,
								const node_ptr_t pDestinationNode) const;

	const arc_ptr_t findDelayArc(const node_ptr_t pSourceNode,
                                const node_ptr_t pDestinationNode,
								const cCommodityBundle* const pCommodity) const;

	void removeDelayArc(delay_arc_ptr_t pArc);
	void deleteDelayArc(delay_arc_ptr_t& pArc);

    std::vector<arc_ptr_t>  getAllInflowArcsToNode(const node_ptr_t pNode,
                                                    bool include_holdover_arcs=true,
                                                    bool include_delay_arcs=true) const;

    std::vector<arc_ptr_t>  getAllInflowArcsToNode(const node_ptr_t pNode,
                                        const cCommodityBundle* const pCommodity,
                                        bool include_holdover_arcs=true) const;

    std::vector<arc_ptr_t>  getAllOutflowArcsFromNode(const node_ptr_t pNode,
                                        bool include_holdover_arcs=true) const;

    std::vector<arc_ptr_t>  getAllHoldoverArcsInflowToNode(const node_ptr_t pNode) const;
    
    std::vector<arc_ptr_t>  getAllHoldoverArcsOutflowFromNode(const node_ptr_t pNode) const;

	std::vector<arc_ptr_t>  findAllDelayArcs() const;

	std::vector<arc_ptr_t>  findAllInflowArcsToNode(const node_ptr_t pNode,
		bool include_holdover_arcs = true,
		bool include_delay_arcs = true) const;

	std::vector<arc_ptr_t>  findAllOutflowArcsFromNode(const node_ptr_t pNode,
		bool include_holdover_arcs = true) const;

	std::vector<arc_ptr_t>  findAllHoldoverArcsInflowToNode(const node_ptr_t pNode) const;

	std::vector<arc_ptr_t>  findAllHoldoverArcsOutflowFromNode(const node_ptr_t pNode) const;


    void print(std::ostream& out) const;
    
    void rehash();
    
private:
    nodes_t mNodes;
	nodes_t mDelayNodes;

    /* A collection for partially time expanded arcs that have a cost/time associated with them:
     *  Travel Arcs, Delay Arcs, Hold Arcs, ...
     */
    arcs_t mArcs;

    /* A collection for partially time expanded arcs that have NO cost/time associated with them:
     *  Holdover Arcs ...
     */
    holdover_arcs_t mHoldoverArcs;
    commodities_t mCommodities;
    std::map<const cServiceLocation*, timepoints_t> mTimePoints;
    
    unsigned int mNumNodes;
	unsigned int mNumArcs;
	unsigned int mNumTavelArcs;
	unsigned int mNumDelayArcs;
	unsigned int mNumHoldArcs;
	unsigned int mNumHoldoverArcs;

	unsigned int mNumDelayGroups;

    std::unordered_map<const cCommodityShipment*, const node_ptr_t> mCommoditySourceNodes;
    std::unordered_map<const cCommodityShipment*, const node_ptr_t> mCommodityDestinationNodes;

    bool mHasDelays;
};


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const commodities_t&  cPartiallyTimeExpandedNetwork::commodities() const
{
    return mCommodities;
}

inline const nodes_t& cPartiallyTimeExpandedNetwork::nodes() const
{
    return mNodes;
}

inline const nodes_t& cPartiallyTimeExpandedNetwork::delay_nodes() const
{
	return mDelayNodes;
}

inline const arcs_t& cPartiallyTimeExpandedNetwork::arcs() const
{
    return mArcs;
}

inline const holdover_arcs_t& cPartiallyTimeExpandedNetwork::holdover_arcs() const
{
    return mHoldoverArcs;
}

inline unsigned int cPartiallyTimeExpandedNetwork::getNumNodes() const
{
    return mNumNodes;
}

inline unsigned int cPartiallyTimeExpandedNetwork::getNumArcs() const
{
    return mNumArcs;
}

inline unsigned int cPartiallyTimeExpandedNetwork::getNumHoldoverArcs() const
{
	return mNumHoldoverArcs;
}

inline unsigned int cPartiallyTimeExpandedNetwork::getNumTravelArcs() const
{
	return mNumTavelArcs;
}

inline unsigned int cPartiallyTimeExpandedNetwork::getNumDelayArcs() const
{
	return mNumDelayArcs;
}

inline unsigned int cPartiallyTimeExpandedNetwork::getNumHoldArcs() const
{
	return mNumHoldArcs;
}

inline bool cPartiallyTimeExpandedNetwork::hasDelays() const
{
    return mHasDelays;
}

