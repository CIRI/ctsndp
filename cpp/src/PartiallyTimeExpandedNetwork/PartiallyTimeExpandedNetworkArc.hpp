
#pragma once

#include "PartiallyTimeExpandedNetworkNode.hpp"
#include "../Commodities/CommodityBundle.hpp"

#include <string>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

class cPartiallyTimeExpandedNetworkNode;
class cPartiallyTimeExpandedNetworkArc;
class cPartiallyTimeExpandedNetworkDelayArc;
class cPartiallyTimeExpandedNetworkHoldoverArc;
class cPartiallyTimeExpandedNetworkHoldArc;
class cServiceLocation;
class cServiceLink;

typedef unsigned int  arc_id_t;
typedef cPartiallyTimeExpandedNetworkArc* arc_ptr_t;
typedef cPartiallyTimeExpandedNetworkDelayArc* delay_arc_ptr_t;
typedef cPartiallyTimeExpandedNetworkHoldoverArc* holdover_arc_ptr_t;
typedef cPartiallyTimeExpandedNetworkHoldArc* hold_arc_ptr_t;

class cPartiallyTimeExpandedNetworkArc
{
public:
    const arc_id_t getId() const;
    
    const node_ptr_t getSource() const;
    bool hasSourceServiceLocation() const;
    const cServiceLocation* const getSourceServiceLocation() const;
    const std::string& getSourceServiceName() const;
    double getSourceTime() const;
    bool isSource(const cPartiallyTimeExpandedNetworkNode* const node) const;

    const node_ptr_t getDestination() const;
    bool hasDestinationServiceLocation() const;
    const cServiceLocation* const getDestinationServiceLocation() const;
    const std::string& getDestinationServiceName() const;
    double getDestinationTime() const;
    bool isDestination(const cPartiallyTimeExpandedNetworkNode* const node) const;

    const cServiceLink* const getServiceLink() const;

    bool arcCapacityIsLimited() const;
    void limitArcCapacity(bool limit);

    unsigned int getCapacity() const;
    unsigned int getPerUnitOfFlowCapacity() const;
    double getFixedCost() const;
	virtual double getPerUnitOfFlowCost() const;
    virtual double getActualTravelTime() const = 0;
    double getCycleTime() const;
    double getDeltaTime() const;

	bool isStorageArc() const;

    virtual bool isDelayArc() const;
    virtual bool connectedToDelayNode() const;
    const node_ptr_t getDelayNode() const;

    node_ptr_t getDestinationNode();
    
    virtual bool isHoldoverArc() const;
	void setCapacity(unsigned int capacity);

protected:
    cPartiallyTimeExpandedNetworkArc(arc_id_t id,
                                     const node_ptr_t pSource,
                                     const node_ptr_t pDestination,
                                     unsigned int capacity,
                                     unsigned int per_unit_of_flow_capacity,
                                     double fixed_cost,
                                     double per_unit_of_flow_cost,
                                     const cServiceLink* pLink);

    cPartiallyTimeExpandedNetworkArc(arc_id_t id,
                                     const node_ptr_t pSource,
                                     const node_ptr_t pDestination,
                                     unsigned int capacity,
                                     double fixed_cost,
									 double per_unit_of_flow_cost);

    virtual ~cPartiallyTimeExpandedNetworkArc() = default;

    node_ptr_t const mpSourceNode;
    node_ptr_t const mpDestination;

	double mFixedCost;
	double mPerUnitOfFlowCost;

private:
    arc_id_t mId;
    bool mLimitArcCapacity;
    unsigned int mArcCapacity;
    unsigned int mArcPerUnitOfFlowCapacity;
    
    const cServiceLink* mpServiceLink;

    friend class cPartiallyTimeExpandedNetwork;
};

class cPartiallyTimeExpandedNetworkTravelArc : public cPartiallyTimeExpandedNetworkArc
{
public:
    double getActualTravelTime() const override;

protected:
    cPartiallyTimeExpandedNetworkTravelArc(arc_id_t id,
                                     const node_ptr_t pSource,
                                     const node_ptr_t pDestination,
                                     unsigned int capacity,
                                     unsigned int per_unit_of_flow_capacity,
                                     double fixed_cost,
                                     double per_unit_of_flow_cost,
                                     double travel_time,
                                     const cServiceLink* pLink);


    virtual ~cPartiallyTimeExpandedNetworkTravelArc() = default;
    
    friend class cPartiallyTimeExpandedNetwork;
    
private:
    double mActualTravelTime;
};


class cPartiallyTimeExpandedNetworkHoldoverArc : public cPartiallyTimeExpandedNetworkArc
{
public:
    bool connectedToDelayNode() const override;
    double getActualTravelTime() const override;
    bool isHoldoverArc() const override;

protected:
    cPartiallyTimeExpandedNetworkHoldoverArc(arc_id_t id,
                                             const node_ptr_t pSource,
                                             const node_ptr_t pDestination);

    cPartiallyTimeExpandedNetworkHoldoverArc(arc_id_t id,
                                             const node_ptr_t pSource,
                                             const node_ptr_t pDestination,
                                             unsigned int capacity,
                                             double cost);

    virtual ~cPartiallyTimeExpandedNetworkHoldoverArc() = default;
    
    friend class cPartiallyTimeExpandedNetwork;
};

class cPartiallyTimeExpandedNetworkHoldArc : public cPartiallyTimeExpandedNetworkArc
{
public:
    double getActualTravelTime() const override;

protected:
    cPartiallyTimeExpandedNetworkHoldArc(arc_id_t id,
                                         const node_ptr_t pSource,
                                         const node_ptr_t pDestination,
                                         double hold_time);

    cPartiallyTimeExpandedNetworkHoldArc(arc_id_t id,
                                         const node_ptr_t pSource,
                                         const node_ptr_t pDestination,
                                         double hold_time,
                                         unsigned int capacity,
                                         double cost);

    virtual ~cPartiallyTimeExpandedNetworkHoldArc() = default;
    
private:
    double mHoldTime;

    friend class cPartiallyTimeExpandedNetwork;
};

class cPartiallyTimeExpandedNetworkDelayArc : public cPartiallyTimeExpandedNetworkArc
{
public:

	double getActualTravelTime() const override;

    bool isDelayArc() const override;
    bool connectedToDelayNode() const override;

	const std::vector<const cCommodityBundle*>& getCommodities() const;
	bool hasCommodity(const cCommodityBundle* const pCommodity) const;

protected:
	cPartiallyTimeExpandedNetworkDelayArc(arc_id_t id,
		const node_ptr_t pSource,
		const node_ptr_t pDestination);

	virtual ~cPartiallyTimeExpandedNetworkDelayArc() = default;
    
	double mDelayTime;

    friend class cPartiallyTimeExpandedNetwork;
};


std::string to_string(const cPartiallyTimeExpandedNetworkArc& arc);
std::ostream& operator<<(std::ostream& out, const cPartiallyTimeExpandedNetworkArc& arc);


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const arc_id_t cPartiallyTimeExpandedNetworkArc::getId() const
{
    return mId;
}

inline bool cPartiallyTimeExpandedNetworkArc::arcCapacityIsLimited() const
{
    return mLimitArcCapacity;
}

inline void cPartiallyTimeExpandedNetworkArc::limitArcCapacity(bool limit)
{
    mLimitArcCapacity = limit;
}

inline unsigned int cPartiallyTimeExpandedNetworkArc::getCapacity() const
{
    return mArcCapacity;
}

inline unsigned int cPartiallyTimeExpandedNetworkArc::getPerUnitOfFlowCapacity() const
{
    return mArcPerUnitOfFlowCapacity;
}

inline double cPartiallyTimeExpandedNetworkArc::getFixedCost() const
{
    return mFixedCost;
}

inline double cPartiallyTimeExpandedNetworkArc::getPerUnitOfFlowCost() const
{
    return mPerUnitOfFlowCost;
}

inline double cPartiallyTimeExpandedNetworkArc::getDeltaTime() const
{
    return mpDestination->getTime() - mpSourceNode->getTime();
}

inline bool cPartiallyTimeExpandedNetworkArc::isStorageArc() const
{
	return (mpSourceNode->getServiceLocation() == mpDestination->getServiceLocation())
		&& (mpDestination->getTime() > mpSourceNode->getTime());
}

inline bool cPartiallyTimeExpandedNetworkArc::isDelayArc() const
{
    return false;
}

inline bool cPartiallyTimeExpandedNetworkArc::isHoldoverArc() const
{
    return false;
}

inline const node_ptr_t cPartiallyTimeExpandedNetworkArc::getDelayNode() const
{
    if (mpSourceNode->isDelayNode())
        return mpSourceNode;
    
    if (mpDestination->isDelayNode())
        return mpDestination;
    
    return nullptr;
}

/********************   HOLDOVER ARC   ********************/

inline bool cPartiallyTimeExpandedNetworkHoldoverArc::connectedToDelayNode() const
{
    return false;
}

inline bool cPartiallyTimeExpandedNetworkHoldoverArc::isHoldoverArc() const
{
    return true;
}


/********************   DELAY ARC   ********************/

inline bool cPartiallyTimeExpandedNetworkDelayArc::isDelayArc() const
{
    return true;
}

inline bool cPartiallyTimeExpandedNetworkDelayArc::connectedToDelayNode() const
{
    return true;
}

inline const std::vector<const cCommodityBundle*>& cPartiallyTimeExpandedNetworkDelayArc::getCommodities() const
{
	return mpSourceNode->getCommodities();
}

inline bool cPartiallyTimeExpandedNetworkDelayArc::hasCommodity(const cCommodityBundle* const pCommodity) const
{
	return mpSourceNode->hasCommodity(pCommodity);
}


