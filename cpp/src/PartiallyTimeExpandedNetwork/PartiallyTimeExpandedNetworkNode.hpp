
#pragma once

#include "../Commodities/CommodityBundle.hpp"

#include <vector>
#include <string>
#include <iostream>

// Forward declarations
class cServiceLocation;
class cPartiallyTimeExpandedNetworkNode;
class cPartiallyTimeExpandedNetworkDelayNode;
class cPartiallyTimeExpandedNetworkArc;

typedef unsigned int  node_id_t;
typedef cPartiallyTimeExpandedNetworkNode*      node_ptr_t;
typedef cPartiallyTimeExpandedNetworkDelayNode* delay_node_ptr_t;


class cPartiallyTimeExpandedNetworkNode
{
public:
    const node_id_t getId() const;
    virtual const std::string& getName() const;
    double getTime() const;
    
    bool hasServiceLocation() const;
    const cServiceLocation* const getServiceLocation() const;

    bool match(const cServiceLocation* const loc, double time,
               const cCommodityBundle* const commodity) const;

    bool partial_match(const cPartiallyTimeExpandedNetworkNode& rhs) const;

    bool operator==(const cPartiallyTimeExpandedNetworkNode& rhs) const;
    bool operator!=(const cPartiallyTimeExpandedNetworkNode& rhs) const;

    virtual bool isDelayNode() const;

    bool hasCommodities() const;
    const std::vector<const cCommodityBundle*>& getCommodities() const;
    void addCommodity(const cCommodityBundle* const pCommodity);
    bool hasCommodity(const cCommodityBundle* const pCommodity) const;
    
    const std::vector<cPartiallyTimeExpandedNetworkArc*>& getInflows() const;
    const std::vector<cPartiallyTimeExpandedNetworkArc*>& getOutflows() const;
	cPartiallyTimeExpandedNetworkArc* getStorageInflow() const;
	cPartiallyTimeExpandedNetworkArc* getStorageOutflow() const;

	void removeFlow(const cPartiallyTimeExpandedNetworkArc* const arc);

protected:
    cPartiallyTimeExpandedNetworkNode(node_id_t id,
                        const cServiceLocation* const pServiceNode,
                        double time);

    cPartiallyTimeExpandedNetworkNode(node_id_t id,
                        const cServiceLocation* const pServiceNode,
                        double time,
                        const cCommodityBundle* const pCommodity);
    
    virtual ~cPartiallyTimeExpandedNetworkNode() = default;
    
    void removeInflow(const cPartiallyTimeExpandedNetworkArc* const pArc);
    void removeOutflow(const cPartiallyTimeExpandedNetworkArc* const pArc);
    
private:
    node_id_t mId;
    double mTime;
    const cServiceLocation* const mpServiceNode;
    std::vector<const cCommodityBundle*> mCommodities;
    mutable std::vector<cPartiallyTimeExpandedNetworkArc*> mInflows;
    mutable std::vector<cPartiallyTimeExpandedNetworkArc*> mOutflows;

    friend class cPartiallyTimeExpandedNetwork;
};

class cPartiallyTimeExpandedNetworkDelayNode : public cPartiallyTimeExpandedNetworkNode
{
public:
    const std::string& getName() const override;
    bool hasMaxDelayTime() const;
    double getMaxDelayTime() const;

    void setMaxDelayTime(double max_delay_time);

    bool isDelayNode() const override;

protected:
    cPartiallyTimeExpandedNetworkDelayNode(node_id_t id,
                                      const cServiceLocation* const pServiceNode,
                                      double time,
                                      const cCommodityBundle* const pCommodity);
    
    virtual ~cPartiallyTimeExpandedNetworkDelayNode() = default;

private:
    std::string mName;
    bool mHasMaxDelayTime;
    double mMaxDelayTime;

    friend class cPartiallyTimeExpandedNetwork;
};

std::string to_string(const cPartiallyTimeExpandedNetworkNode& node);
std::ostream& operator<<(std::ostream& out, const cPartiallyTimeExpandedNetworkNode& node);


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const node_id_t cPartiallyTimeExpandedNetworkNode::getId() const
{
    return mId;
}

inline bool cPartiallyTimeExpandedNetworkNode::match(const cServiceLocation* const loc, double time, const cCommodityBundle* const commodity) const
{
    return (mpServiceNode == loc)
            && (mTime == time)
            && (hasCommodity(commodity));
}

inline bool cPartiallyTimeExpandedNetworkNode::partial_match(
                        const cPartiallyTimeExpandedNetworkNode& rhs) const
{
    return (mpServiceNode == rhs.mpServiceNode)
                && (mTime == rhs.mTime);
}


inline bool cPartiallyTimeExpandedNetworkNode::isDelayNode() const
{
    return false;
}

inline const std::vector<cPartiallyTimeExpandedNetworkArc*>& cPartiallyTimeExpandedNetworkNode::getInflows() const
{
    return mInflows;
}

inline const std::vector<cPartiallyTimeExpandedNetworkArc*>& cPartiallyTimeExpandedNetworkNode::getOutflows() const
{
    return mOutflows;
}


inline bool cPartiallyTimeExpandedNetworkDelayNode::hasMaxDelayTime() const
{
    return mHasMaxDelayTime;
}

inline double cPartiallyTimeExpandedNetworkDelayNode::getMaxDelayTime() const
{
    return mMaxDelayTime;
}

inline bool cPartiallyTimeExpandedNetworkDelayNode::isDelayNode() const
{
    return true;
}

