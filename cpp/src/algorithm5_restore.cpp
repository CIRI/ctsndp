
#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "ServiceNetwork/ServiceLocation.hpp"

#include <algorithm>
#include <cassert>

namespace
{
	std::ostream& operator<<(std::ostream& out, const timepoints_t& points)
	{
		out << "{";
		if (!points.empty())
		{
			auto last = --(points.end());
			for (auto it = points.begin(); it != last; ++it)
			{
				out << *it << ", ";
			}
			out << *last;
		}
		out << "}";
		return out;
	}
}

/**
 * This is algorithm 5 listed on page 1312
 * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_k < t_new
 **/
std::vector<cPartiallyTimeExpandedNetworkArc*> ctsndp::restore(cPartiallyTimeExpandedNetwork& network,
                     const cPartiallyTimeExpandedNetworkNode& node,
                     double t_k, double t_new,
                     std::ostream* debug)
{
	if (debug)
	{
		*debug << "  Restore: " << node << " t_k=" << t_k;
		*debug << ", t_new=" << t_new << "\n";
	}

    std::vector<cPartiallyTimeExpandedNetworkArc*> arcsToDelete;
    std::vector<cPartiallyTimeExpandedNetworkArc*> arcsToRemove;

    const auto& node_t_k = network.findNode(node.getServiceLocation(), t_k);

	const auto& node_t_new = network.findNode(node.getServiceLocation(), t_new);

	// Line 1: For all ((i, t_k), (j, t)) in 𝒜t do
	auto outflows = network.getAllOutflowArcsFromNode(node_t_k, false);
    for (const auto& outflow : outflows)
    {
        double t = outflow->getDestination()->getTime();
        // Line 2: Set t_prime = arg max{ s in Tj | s <= t_new + tau_ij}
        double t_prime = 0;
		double tau_ij = outflow->getActualTravelTime();
		double t_limit = t_new + tau_ij;
		auto Tj = network.timepoints(outflow->getDestinationServiceLocation());
        for (double s : Tj)
        {
            if ((s <= t_limit) && (s > t_prime))
            {
                t_prime = s;
            }
        }
        
        // Line 3:
        if (t_prime != t)
        {
            // Line 4: Delete arc ((i, t_new), (j, t)) from 𝒜t

            auto dest = network.findNode(outflow->getDestinationServiceLocation(), t);
            auto tmp_arc = network.findArc(node_t_new, dest);
			if (!tmp_arc)
				continue;
			assert(tmp_arc);

            // Line 4: Add arc ((i, t_new), (j, t_prime)) to 𝒜t
            dest = network.findNode(tmp_arc->getDestinationServiceLocation(), t_prime);
            auto arc = network.addArc(node_t_new, dest, outflow);
			if (debug)
			{
				*debug << "    " << t_prime << " = arg max{ s in " << Tj << " | s <= " << t_new << " + " << tau_ij << "}\n";
				*debug << "    Adding arc: " << *arc << "\n";
				*debug << "    Deleting arc: " << *tmp_arc << "\n\n";
			}
			arcsToRemove.push_back(tmp_arc);
        }
    }

    for (auto& arc : arcsToRemove)
    {
        network.removeArc(arc);
        arcsToDelete.push_back(arc);
    }
    
    arcsToRemove.clear();

    // Line 7: For all ((j, t), (i, t_k)) in 𝒜t such that t + tau_ji => t_new do
    auto inflows = network.getAllInflowArcsToNode(node_t_k, false);
    for (auto& inflow : inflows)
    {
		double t = inflow->getSource()->getTime();
		double tau_ij = inflow->getActualTravelTime();
		if (t + tau_ij >= t_new)
        {
            // Line 8: Add arc ((j, t), (i, t_new)) to 𝒜t
//            auto dest = network.findNode(arc->getDestinationServiceLocation(), t_new, commodity);
            auto arc = network.addArc(inflow->getSource(), node_t_new, inflow);
			if (debug)
			{
				*debug << "    " << t << " + " << tau_ij << " >= " << t_new << "\n";
				*debug << "    Adding arc: " << *arc << ", t=" << t << ", t_new=" << t_new << "\n";
			}
			// Line 8: Delete arc ((j, t), (i, t_k)) from 𝒜t
			arcsToRemove.push_back(inflow);
			if (debug)
			{
				*debug << "    Deleting arc: " << *inflow << "\n\n";
			}
		}
    }
    
    for (auto& arc : arcsToRemove)
    {
        network.removeArc(arc);
        arcsToDelete.push_back(arc);
    }
    arcsToRemove.clear();

    if (debug)
        *debug << "  complete!\n\n" << std::endl;

	return arcsToDelete;
}

