//
//  IdentifyArcsToLengthenModel.hpp
//
//  Created by Brett Feddersen on 11/21/19.
//

#pragma once
#ifndef IdentifyArcsToLengthenModel_hpp
#define IdentifyArcsToLengthenModel_hpp

#include "OptimizationProblem.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include <vector>
#include <map>
#include <set>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the "Identify Arcs to Lengthen" algorithm listed
 * in section 4.3 on page 1312 of the Continuous Time Service Network
 * Design Problem paper.
 */
class cIdentifyArcsToLengthenModel : public cOptimizationProblem
{
public:
    cIdentifyArcsToLengthenModel(int pass_number, 
								 const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                 const cServiceNetwork& serviceNetwork,
                                 const ctsndp::commodity_path_node_t& p_ki,
                                 const ctsndp::commodity_path_arc_t& p_ka,
								 const ctsndp::consolidation_groups_t& j_ak,
								 bool verboseMode, bool printTimingInfo);

    ~cIdentifyArcsToLengthenModel();

    void outputIdentifyArcsTimeInfo(bool output);
    void outputCommodityScheduleInfo(bool output);
   
	/**
	 * \brief Builds the "identify arcs to lengthen" optimization problem.
	 * See page 1312.
	 */
	void BuildModel() override;

	/**
	 * \brief Returns all of the arc that are too "short" in the partially time-expanded network
	 *		sigma_kij == 1
	 */
	std::set<ctsndp::arc_ptr_t> returnArcsToLengthen();

	/**
	 * \brief Returns all of the dispatch times for the commodities moving through the service network
	 */
	ctsndp::dispatch_time_t returnDispatchTimes();
    ctsndp::travel_time_t  returnTravelTimes();

protected:
	virtual void ConstructOptimizatonVariables() = 0;
	virtual void BuildOptimizatonProblem() = 0;

    //
    // Constraint definitions used in the Identify Arcs To Lengthen optimization
    //
	virtual void constraint6_CountArcsWithShortenTravelTimes() = 0;
	virtual void constraint7_EnsureAllowableDispatchTimes() = 0;
	virtual void constraint8_EnsureDispatchIsAfterAvailable() = 0;
	virtual void constraint9_EnsureArrivalBeforeDue() = 0;
	virtual void constraint10_EnsureConsolidations() = 0;
    

    //
    // Inputs for the optimization
    //
    const cPartiallyTimeExpandedNetwork& mPartiallyTimeExpandedNetwork;
    
    // the path commodities through the service network by nodes visited
    const ctsndp::commodity_path_node_t& mP_ki;

    // the path commodities through the service network by arc
    const ctsndp::commodity_path_arc_t&  mP_ka;

    // the set of all pairs of commodities dispatched on an arc
//    const ctsndp::consolidation_t&       mJ_ak;
	const ctsndp::consolidation_groups_t&       mJ_ak;

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    std::vector<double> tau_ij;         // the actual travel time of a commodity utilizing the arc (i,j)

    //
    // Decision Variables
    //
    
    struct sDualCommodityNode
    {
        ctsndp::commodity_ptr_t k1;
        ctsndp::commodity_ptr_t k2;
        ctsndp::node_ptr_t      n;
    };
    
    struct compareDualCommodityNodeByPtrId
    {
        bool operator()(const sDualCommodityNode& lhs, const sDualCommodityNode& rhs) const
        {
            if (lhs.n == rhs.n )
            {
                if (lhs.k1 == rhs.k1)
                    return lhs.k2->getId() < rhs.k2->getId();
                return lhs.k1->getId() < rhs.k1->getId();
            }
            return lhs.n < rhs.n;
        }
    };

    struct sDualCommodityArc
    {
        ctsndp::commodity_ptr_t k1;
        ctsndp::commodity_ptr_t k2;
        ctsndp::arc_ptr_t       ij;
    };
    
    struct compareDualCommodityArcByPtrId
    {
        bool operator()(const sDualCommodityArc& lhs, const sDualCommodityArc& rhs) const
        {
            if (lhs.ij == rhs.ij )
            {
                if (lhs.k1 == rhs.k1)
                    return lhs.k2->getId() < rhs.k2->getId();
                return lhs.k1->getId() < rhs.k1->getId();
            }
            return lhs.ij < rhs.ij;
        }
    };
    
    // the travel time in, 𝒟, of a commodity, k, utilizing the arc (i,j)
    std::map<ctsndp::sCommodityArc, double, ctsndp::compareCommodityArcByPtrId>   tau_bar_kij;

    //
    // Variables to hold the output data
    //
    std::set<ctsndp::arc_ptr_t> mArcsToLengthen;

    // the dispatch time of commodity, k, at node i
    ctsndp::dispatch_time_t mDispatchTime_ki;

    // the travel time of arc, (i, j) when taken by commodity, k
    ctsndp::travel_time_t   mTravelTime_kij;

    //
    // Debug Variables
    //
    bool mOutputIdentifyArcsTimeInfo;
    bool mOutputCommodityScheduleInfo;

	bool mVerboseMode;
    bool mPrintTimingInfo;

public:
	std::string mLogPath;
	bool mLogIdentifyArcsTimeInfo;
	bool mLogCommodityScheduleInfo;
};

#endif /* IdentifyArcsToLengthenModel_hpp */
