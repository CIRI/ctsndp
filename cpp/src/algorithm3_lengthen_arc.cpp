
#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"

#include <algorithm>
#include <cassert>

/**
 * This is algorithm 3 listed on page 1312
 * Requires: Arc((i,t), (j,t')) in 𝒜t
 * Modifies: The partially time expanded network 
 **/
void ctsndp::lengthen_arc(cPartiallyTimeExpandedNetwork& network,
                          std::set<cPartiallyTimeExpandedNetworkArc*>&& arcs,
                          std::ostream* debug)
{
    if (debug)
        *debug << "Lengthening arcs:\n";

	std::set<cPartiallyTimeExpandedNetworkArc*> arcs_to_delete;


	while (!arcs.empty())
    {
		auto it = arcs.begin();

		cPartiallyTimeExpandedNetworkArc* arc = *it;
		arcs.erase(it);

        // arc must be an element of 𝒜t
		assert(!arc->isHoldoverArc());
        
        const auto& node = arc->getDestination();
        double t_new = arc->getSource()->getTime() + arc->getActualTravelTime();

        if (debug)
            *debug << *arc << " to " << t_new << "\n";

        if (arc->connectedToDelayNode())
        {
            if (arc->getDestination()->isDelayNode())
            {
				std::cout << "Oops, trying the move delay node!" << std::endl;
//                arc->getDestination()->setTime(t_new);
            }
            continue;
        }
        
        // We need to find the lower bound, t_k, and upper bound, t_k+1
        auto time_points = network.timepoints(node->getServiceLocation());
        assert(!time_points.empty());
        
        // Does the node at t_new already exists? Yes, we
        // can move our arc to that node
        if (!network.findNode(node->getServiceLocation(), t_new))
        {
            
            // Find lower bound such that t_k <= t_new
            double t_k = 0;
            auto lower = std::lower_bound(time_points.begin(), time_points.end(), t_new);
            if (lower == time_points.begin())
                t_k = *(lower);
            else
                t_k = *(--lower);
            
            // Find upper bound such that t_new <= t_k_1
            double t_k_1 = 0;
            auto upper = std::upper_bound(lower, time_points.end(), t_new);
            if (upper == time_points.end())
                t_k_1 = *(--upper);
            else
                t_k_1 = *(upper);
            
			refine(network, *node, t_k, t_new, t_k_1, debug);
			auto tmp = restore(network, *node, t_k, t_new, debug);

			for (auto arc : tmp)
			{
				auto it = arcs.find(arc);
				if (it != arcs.end())
				{
					arcs.erase(it);
				}

				arcs_to_delete.insert(arc);
			}
        }
        else
        {
            const auto& dest = network.findNode(arc->getDestinationServiceLocation(), t_new);
            network.addArc(arc->getSource(), dest, arc);
			arcs_to_delete.insert(arc);
        }
    }
    arcs.clear();

	for (auto arc : arcs_to_delete)
	{
		network.deleteArc(arc);
	}
	arcs_to_delete.clear();

    if (debug)
        *debug << "complete!" << std::endl;
}
