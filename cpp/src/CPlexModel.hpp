//
//  CPlexModel.hpp
//  An abstract CPlex Model class
//
//  Created by Brett Feddersen on 3/23/20.
//

#pragma once
#ifndef CPlexModel_hpp
#define CPlexModel_hpp

#include <ilcplex/ilocplex.h>
#include <exception>

ILOSTLBEGIN


/*
 * An abstract base class to support any CPLEX optimization problem.
 */
class cCPlexModel
{
public:

    void setNumericFocus(int numericFocus);
    void setTimeLimit_sec(double timeLimit_sec);
    void logToConsole(bool log);
	void readTuningParameters(const std::string& filename);

	/*
     * Modifies the Gurobi Model object to create a feasibility relaxation.
     * Note that you need to call optimize on the result to compute the actual relaxed solution.
     * The feasibility relaxation is a model that, when solved, minimizes the amount by which the
     * solution violates the bounds and linear constraints of the original model.
     *
     * \return true if the relaxation could be created
     */
    bool RelaxConstraints();
    
    /*
     * Compute an Irreducible Inconsistent Subsystem (IIS). An IIS is a subset of the constraints and variable bounds with the following properties:
     *      1. the subsystem represented by the IIS is infeasible, and
     *      2. if any of the constraints or bounds of the IIS is removed, the subsystem becomes feasible.
     *
     * \return a string of all the constraints or bounds that could not be satisfied
     */
    std::string ComputeIIS();

	/*!
	 * \brief Converts the value stored in a CPlex variable into the type
	 */
	bool   as_bool(const IloNumVar& var) const;
	int    as_int(const IloNumVar& var) const;
	double as_double(const IloNumVar& var) const;


protected:
    cCPlexModel(const char* model_name);
    ~cCPlexModel();

    void logFilename(const std::string& filename, const char* model_type);

    //
    // The CPLEX environment, model and solver
	IloEnv   mEnv;
	IloModel mModel;
	IloCplex mSolver;

    //
    // Debug Variables
    //
    double mTimeLimit_sec;
};



inline bool cCPlexModel::as_bool(const IloNumVar& var) const
{
	try
	{
		auto b = mSolver.getValue(var);
		return static_cast<bool>((b > 0.001) || (b < -0.001));
	}
	catch (IloException& e)
	{
		throw std::runtime_error(e.getMessage());
	}
}

inline int cCPlexModel::as_int(const IloNumVar& var) const
{
	try
	{
		auto i = mSolver.getValue(var);
		return static_cast<int>(i);
	}
	catch (IloException& e)
	{
		throw std::runtime_error(e.getMessage());
	}
}

inline double cCPlexModel::as_double(const IloNumVar& var) const
{
	try
	{
		auto d = mSolver.getValue(var);
		return static_cast<double>(d);
	}
	catch (IloException& e)
	{
		throw std::runtime_error(e.getMessage());
	}
}

#endif /* CPlexModel_hpp */
