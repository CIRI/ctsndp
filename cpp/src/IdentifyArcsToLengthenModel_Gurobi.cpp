//
//  IdentifyArcsToLengthenModel_Gurobi.cpp
//
//  Created by Brett Feddersen on 03/25/20.
//

#include "IdentifyArcsToLengthenModel_Gurobi.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_utils_priv.hpp"
#include "gurobi_utils_priv.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>
#include <fstream>
#include <cassert>


namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;

#if ADD_DEBUG_NAMES
    std::string strConstraint10(const cCommodityBundle& k1, const cCommodityBundle& k2, const cPartiallyTimeExpandedNetworkArc& arc)
    {
        std::string constrName = "C10_EnsureConsolidations(";
        constrName += k1.getUniqueName();
        constrName += ", ";
        constrName += k2.getUniqueName();
        constrName += ": " + to_string(arc) + ")";
        return constrName;
    }
#endif
}

using namespace ctsndp;


std::unique_ptr<cIdentifyArcsToLengthenModel_Gurobi> 
cIdentifyArcsToLengthenModel_Gurobi::create(int pass_number,
	const cPartiallyTimeExpandedNetwork& expandedNetwork,
	const cServiceNetwork& serviceNetwork,
	const ctsndp::commodity_path_node_t& p_ki,
	const ctsndp::commodity_path_arc_t& p_ka,
	const ctsndp::consolidation_groups_t& j_ak,
	bool verboseMode, bool printTimingInfo)
{
	try
	{
		std::unique_ptr<cIdentifyArcsToLengthenModel_Gurobi> 
			model(new cIdentifyArcsToLengthenModel_Gurobi(pass_number, expandedNetwork,
				serviceNetwork, p_ki, p_ka, j_ak, verboseMode, printTimingInfo));

		return model;
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException(e.getMessage());
	}
}

cIdentifyArcsToLengthenModel_Gurobi::cIdentifyArcsToLengthenModel_Gurobi(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_node_t& p_ki,
                                         const ctsndp::commodity_path_arc_t& p_ka,
										 const ctsndp::consolidation_groups_t& j_ak,
										 bool verboseMode, bool printTimingInfo)
:
    cGurobiModel("IdentifyArcsToLengthen"),
	cIdentifyArcsToLengthenModel(pass_number, expandedNetwork, serviceNetwork, 
								p_ki, p_ka, j_ak, verboseMode, printTimingInfo)
{
    mModel.set(GRB_DoubleParam_FeasibilityTol, 0.001);
}

cIdentifyArcsToLengthenModel_Gurobi::~cIdentifyArcsToLengthenModel_Gurobi()
{
}

void cIdentifyArcsToLengthenModel_Gurobi::setNumericFocus(int numericFocus)
{
	cGurobiModel::setNumericFocus(numericFocus);

}

void cIdentifyArcsToLengthenModel_Gurobi::setTimeLimit_sec(double timeLimit_sec)
{
	cGurobiModel::setTimeLimit_sec(timeLimit_sec);
}

void cIdentifyArcsToLengthenModel_Gurobi::logToConsole(bool log)
{
	cGurobiModel::logToConsole(log);
}

void cIdentifyArcsToLengthenModel_Gurobi::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cIdentifyArcsToLengthenModel_Gurobi::logFilename(const std::string& filename)
{
    cGurobiModel::logFilename(filename, ".identify_arcs");
}

void cIdentifyArcsToLengthenModel_Gurobi::readTuningParameters(const std::string& filename)
{
	cGurobiModel::readTuningParameters(filename);
}

bool cIdentifyArcsToLengthenModel_Gurobi::Optimize()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Identify Arcs to Lengthen Problem\" ";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	int status = 0;

	try
	{
		if (mpProgress)
		{
			*mpProgress << "  Optimizing \"Identify Arcs to Lengthen Problem\"..." << std::flush;
		}

		mModel.optimize();
		status = mModel.get(GRB_IntAttr_Status);

		if (mpProgress)
		{
			*mpProgress << "done" << std::endl;
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("Identify Arcs to Lengthen Problem Optimize: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
        case GRB_OPTIMAL:
        {
			SaveSolution();
            return true;
        }

		case GRB_INFEASIBLE:
		{
			std::string msg = "Model was proven to be infeasible.";

			if (mpProgress)
			{
				*mpProgress << "  " << msg << std::endl;
			}

			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

        case GRB_INF_OR_UNBD:
		{
			std::string msg = "Model was proven to be either infeasible or unbounded. To ";
			msg += "obtain a more definitive conclusion, set the DualReductions parameter ";
			msg += "to 0 and reoptimize.";
			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

		case GRB_UNBOUNDED:
		{
			std::string msg = "Model was proven to be unbounded. Important note: an unbounded ";
			msg += "status indicates the presence of an unbounded ray that allows the objective ";
			msg += "to improve without limit.It says nothing about whether the model has a feasible ";
			msg += "solution.If you require information on feasibility, you should set the objective ";
			msg += "to zero and reoptimize.";
			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

		case GRB_ITERATION_LIMIT:
		{
			std::string msg = "Optimization terminated because the total number of simplex ";
			msg += "iterations performed exceeded the value specified in the IterationLimit ";
			msg += "parameter, or because the total number of barrier iterations exceeded the ";
			msg += "value specified in the BarIterLimit parameter.";
			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

		case GRB_NODE_LIMIT:
		{
			std::string msg = "Optimization terminated because the total number of branch ";
			msg += "-and-cut nodes explored exceeded the value specified in the NodeLimit parameter.";
			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

		case GRB_SOLUTION_LIMIT:
		{
			std::string msg = "Optimization terminated because the number of solutions ";
			msg += "found reached the value specified in the SolutionLimit parameter.";
			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

		case GRB_NUMERIC:
		{
			std::string msg = "Optimization was terminated due to unrecoverable numerical difficulties";
			throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
			break;
		}

		case GRB_TIME_LIMIT:
        {
            std::string msg = "Exceeded time limit (";
            msg += std::to_string(mTimeLimit_sec);
            msg += "):\n";

			try
			{
				auto count = mModel.get(GRB_IntAttr_SolCount);
				if (count == 0)
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    No solution was found!! Terminating" << std::endl;
					}

					msg += "No solution was found!";
				}
				else
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    Feasible solution count = " << count << std::endl;
					}

					SaveSolution();
					return true;
				}
			}
			catch (const GRBException& e)
			{
				msg += e.getMessage();
			}
            
            throw ctsndp::cTimeLimit("IdentifyArcs", msg);
            break;
        }
    }

    return false;
}

bool cIdentifyArcsToLengthenModel_Gurobi::RelaxConstraints()
{
	return cGurobiModel::RelaxConstraints();
}

std::string cIdentifyArcsToLengthenModel_Gurobi::ComputeIIS()
{
	return cGurobiModel::ComputeIIS();
}

void cIdentifyArcsToLengthenModel_Gurobi::FindConstraintViolations(std::ostream& out)
{
    bool printNone = true;
	out << "\nScanning for tau constraints \"tau_ij >= tau_bar_kij\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        const auto& path = mP_ka.at(k);
        if (path.size() <= 1)
            continue;
        auto last = path.end();

        for (auto it = path.begin(); it != last; ++it)
        {
            auto& arc = *it;

            double tau = tau_ij[to_index(arc)];
            double tau_bar = tau_bar_kij[{k, arc}];

			double delta = tau - tau_bar;
			if (arc->connectedToDelayNode())
				delta = tau_bar - tau;

            if (delta < -CONSTRAINT_TOLERANCE)
            {
				out << tau << " < " << tau_bar;
				out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*arc);
				out << " delta = " << delta << "\n";
                printNone = false;
            }
        }
    }

    if (printNone)
		out << "None.\n";
    printNone = true;
   
	out << "\nScanning for constraint 6 \"theta_kij >= tau_ij * (1 - sigma_kij)\" violations:\n";

   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       const auto& path = mP_ka.at(k);
       if (path.size() <= 1)
           continue;
       auto last = path.end();
       
	   out << std::setprecision(10);
       
       for (auto it = path.begin(); it != last; ++it)
       {
           auto& arc = *it;

		   // TERM: tau_ij * (1 - sigma_kij)
           double rhs = tau_ij[to_index(arc)] * (1 - as_int(sigma_kij[{k, arc}]));
           double theta = as_double(theta_kij[{k, arc}]);
		   if (arc->isDelayArc())
		   {
			   double delta = rhs - theta;
			   if (delta < -CONSTRAINT_TOLERANCE)
			   {
				   out << theta << " <= " << tau_ij[to_index(arc)];
				   out << " * (1 - " << as_int(sigma_kij[{k, arc}]) << ") for arc " << to_string(*arc) << " delta = ";
				   out << delta << "\n";
				   printNone = false;
			   }
		   }
		   else
		   {
			   double delta = theta - rhs;
			   if (delta < -CONSTRAINT_TOLERANCE)
			   {
				   out << theta << " >= " << tau_ij[to_index(arc)];
				   out << " * (1 - " << as_int(sigma_kij[{k, arc}]) << ") for arc " << to_string(*arc) << " delta = ";
				   out << delta << "\n";
				   printNone = false;
			   }
		   }
       }
   }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 7 \"gamma_ki + theta_kij <= gamma_ki_1\" violations:\n";
   
   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       const auto& path = mP_ka.at(k);
       if (path.size() <= 1)
           continue;

       auto n = path.size()-1;
	   auto& nodes = mP_ki.at(k);
           
       for (std::size_t i = 0; i < n; ++i)
       {
           auto& arc = path[i];
           auto lhs = as_double(gamma_ki[{k, arc->getSource()}]) + as_double(theta_kij[{k, arc}]);
           auto rhs = as_double(gamma_ki[{k, nodes[i+1]}]);
           double delta = lhs - rhs;
           
           if (delta > CONSTRAINT_TOLERANCE)
           {
               out << as_double(gamma_ki[{k, arc->getSource()}]) << " + " << as_double(theta_kij[{k, arc}]);
               out << " <= " << rhs << " for commodity " << k->getFullyQualifiedName() << ", arc " << to_string(*arc) << " delta = ";
               out << delta << "\n";
               printNone = false;
           }
       }
   }
   
    if (printNone)
        out << "None.\n";
    printNone = true;
   
    out << "\nScanning for constraint 8 \"e_k <= gamma_ko\" violations:\n";
   
   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       auto& nodes = mP_ki.at(k);
       if (nodes.empty())
           continue;
           
       auto& loc = nodes.front();
       auto gamma1 = as_double(gamma_ki[{k, loc}]);
       double delta = k->getEarliestAvailableTime() - gamma1;

       if (delta > CONSTRAINT_TOLERANCE)
       {
           out << k->getEarliestAvailableTime() << " <= " << gamma1;
           out << " for commodity " << k->getFullyQualifiedName() << " delta = ";
           out << delta << "\n";
           printNone = false;
       }
   }
   
    if (printNone)
        out << "None.\n";
    printNone = true;

   out << "\nScanning for constraint 9 \"gamma_ki + theta_kij <= l_k\" violations:\n";
   
   // for all commodities
   for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
   {
       const auto& arcs = mP_ka.at(k);
       if (arcs.empty())
           continue;
           
       auto& arc = arcs.back();
       auto gamma = as_double(gamma_ki[{k, arc->getSource()}]);
       auto theta = as_double(theta_kij[{k, arc}]);
       double ldt = k->getLatestDeliveryTime();
       double delta = (gamma + theta) - ldt;

       if (delta > CONSTRAINT_TOLERANCE)
       {
           out << gamma << " + " << theta << " <= " << ldt;
		   out << " for commodity " << k->getFullyQualifiedName();
		   out << ", " << *arc << " delta = ";
           out << delta << "\n";
           printNone = false;
       }
   }
   
    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 10 \"gamma_k1i = gamma_k2i\" violations:\n";

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					node_ptr_t i = arc->getSource();

					auto gamma1 = as_double(gamma_ki[{k1, i}]);
					auto gamma2 = as_double(gamma_ki[{k2, i}]);
					double delta = std::fabs(gamma1 - gamma2);

					if (delta > CONSTRAINT_TOLERANCE)
					{
						out << gamma1 << " != " << gamma2;
						out << " at node " << to_string(*i);
						out << ", k1 = " << k1->getName();
						out << " and k2 = " << k2->getName();
						out	<< " delta = ";
						out << delta << "\n";
						printNone = false;
					}
				}
			}
		}
	}

    if (printNone)
        out << "None.\n";
}

//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cIdentifyArcsToLengthenModel_Gurobi::SaveSolution()
{
	auto objVal = mModel.get(GRB_DoubleAttr_ObjVal);

	if (mOutputIdentifyArcsTimeInfo)
	{
		DumpIdentifyArcsTimeInfo(*mpDebugOutput);
	}

	if (mOutputCommodityScheduleInfo)
	{
		DumpCommoditySchedules(*mpDebugOutput);
	}

	if (mLogIdentifyArcsTimeInfo)
	{
		std::string filename = mLogPath + "IdentifyArcsTimeInfo_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpIdentifyArcsTimeInfo(file);
		file.close();
	}

	if (mLogCommodityScheduleInfo)
	{
		std::string filename = mLogPath + "IdentifyArcsCommoditySchedule_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpCommoditySchedules(file);
		file.close();
	}

	SaveDispatchAndTravelTimes();

	if (objVal == 0.0)
	{
		if (mOutputDebugMsgs)
		{
			*mpDebugOutput << "none...";
		}

		return;
	}

	FindArcsToLengthen();

	if (mOutputDebugMsgs)
	{
		*mpDebugOutput << mArcsToLengthen.size() << "...";
	}

}

void cIdentifyArcsToLengthenModel_Gurobi::FindArcsToLengthen()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tFinding Arcs to Lengthen";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    mArcsToLengthen.clear();

	try
	{
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& path = mP_ka.at(k);
			for (int i = 0; i < path.size() - 1; ++i)
			{
				auto& arc = path[i];
				if (as_bool(sigma_kij[{k, arc}]))
				{
					mArcsToLengthen.insert(arc);
				}
			}
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("FindArcsToLengthen: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

		if (mArcsToLengthen.size() > 0)
			*mpDebugOutput << ", found " << mArcsToLengthen.size();

        *mpDebugOutput << std::endl;
    }
}

void cIdentifyArcsToLengthenModel_Gurobi::SaveDispatchAndTravelTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tSaving Dispatch and Travel Times";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    mDispatchTime_ki.clear();
    
    // gamma_ki represents the dispatch time of commodity k at node i
    for(auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        for (auto& node : path)
        {
			try
			{
				mDispatchTime_ki[{k, node}] = as_double(gamma_ki[{k, node}]);
			}
			catch (const GRBException& e)
			{
				std::string msg = "SaveDispatchAndTravelTimes (dispatch time): ";
				msg += "[ " + k->getUniqueName() + ", " + node->getName() + "] ";
				throw cOptimizationException(msg + e.getMessage());
			}
        }
    }

    mTravelTime_kij.clear();
    
    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto& path = mP_ka.at(k);
        if (path.size() <= 1)
            continue;
        auto last = path.end();
        
        for (auto it = path.begin(); it != last; ++it)
        {
            auto& arc = *it;
			try
			{
				mTravelTime_kij[{k, arc}] = as_double(theta_kij[{k, arc}]);
			}
			catch (const GRBException& e)
			{
				std::string msg = "SaveDispatchAndTravelTimes (travel time): ";
				msg += "[ " + k->getUniqueName() + ", " + ::to_string(*arc) + "] ";
				throw cOptimizationException(msg + e.getMessage());
			}
		}
    }
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}


//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cIdentifyArcsToLengthenModel_Gurobi::ConstructOptimizatonVariables()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variables";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		// gamma_ki represents the dispatch time of commodity k at node i
		for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
		{
			auto& k = it->first;
			auto& path = it->second;
			for (auto& node : path)
			{
				// Setting the lower bound of gamma to zero enforces
				// constraint 11 list on page 1312.
#if ADD_DEBUG_NAMES
				std::string varName = "gamma_ki{" + k->getUniqueName() + ", "
					+ node->getName() + "}";
				std::replace(varName.begin(), varName.end(), ' ', '_');
				gamma_ki[{k, node}] = mModel.addVar(0, +GRB_INFINITY, 0, GRB_CONTINUOUS, varName.c_str());
#else
				gamma_ki[{k, node}] = mModel.addVar(0, +GRB_INFINITY, 0, GRB_CONTINUOUS);
#endif
			}
		}

		for (auto k_it = mP_ka.begin(); k_it != mP_ka.end(); ++k_it)
		{
			auto& k = k_it->first;
			auto& path = k_it->second;
			auto last = path.end();

			for (auto p_it = path.begin(); p_it != last; ++p_it)
			{
				auto& arc = *p_it;
				assert(!arc->isHoldoverArc());

#if ADD_DEBUG_NAMES
				std::string varName = "theta_kij:" + k->getShipmentName() + "," + to_string(*arc);
				std::replace(varName.begin(), varName.end(), ' ', '_');

				if (arc->isDelayArc())
				{
					theta_kij[{k, arc}] = mModel.addVar(-GRB_INFINITY,
						0.0,
						0.0,
						GRB_CONTINUOUS, varName.c_str());
				}
				else
				{
					theta_kij[{k, arc}] = mModel.addVar(-GRB_INFINITY,
						GRB_INFINITY,
						0.0,
						GRB_CONTINUOUS, varName.c_str());
				}
				varName = "sigma_kij:" + k->getShipmentName() + "," + to_string(*arc);
				std::replace(varName.begin(), varName.end(), ' ', '_');
				sigma_kij[{k, arc}] = mModel.addVar(0, 1, 0, GRB_BINARY, varName.c_str());
#else
				/*
				 *  Delay arc connect a node in the future back to our destination
				 *  node resulting in a negative travel time.  We set the upper bound
				 *  to zero because a travel time of greater than zero means there is
				 *  no need to use the delay arc.
				 */
				if (arc->isDelayArc())
				{
					/*
					 * Tau bar is the "time" difference between the source and destination nodes,
					 * and represents lower bound of the theta, "travel time".  However, delay nodes
					 * are placed far into the future.  In these cases, tau bar represents the
					 * maximum limit of theta.
					 */
					double tau_bar = std::min(tau_bar_kij[{k, arc}], 0.0);

					theta_kij[{k, arc}] = mModel.addVar(-GRB_INFINITY,
						tau_bar,
						0.0,
						GRB_CONTINUOUS);
				}
				else
				{
					//
					// Perform constraint 12 for the algorithm listed on page 1312
					//
					// Tau bar is the "time" difference between the source and destination nodes,
					// and represents lower bound of the theta, "travel time".
					//
					double tau_bar = std::max(tau_bar_kij[{k, arc}], 0.0);

					theta_kij[{k, arc}] = mModel.addVar(tau_bar,
						GRB_INFINITY,
						0.0,
						GRB_CONTINUOUS);
				}

				sigma_kij[{k, arc}] = mModel.addVar(0, 1, 0, GRB_BINARY);
#endif

				//
				// Perform the "speed up" given on page 1313, end of second paragraph
				//
				if (!arc->connectedToDelayNode())
				{
					if (arc->getDeltaTime() == arc->getActualTravelTime())
					{
						sigma_kij[{k, arc}].set(GRB_DoubleAttr_UB, 0);
					}
				}
			}
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("ConstructOptimizatonVariables: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

void cIdentifyArcsToLengthenModel_Gurobi::BuildOptimizatonProblem()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		GRBLinExpr allowArcTooShortVars;

		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& path = mP_ka.at(k);
			if (path.empty())
				continue;

			for (int i = 0; i < path.size() - 1; ++i)
			{
				allowArcTooShortVars += sigma_kij[{k, path[i]}];
			}
		}

		mModel.setObjective(allowArcTooShortVars, GRB_MINIMIZE);
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("BuildOptimizatonProblem: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================

//
// Perform constraint 6 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_Gurobi::constraint6_CountArcsWithShortenTravelTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint6: Count Arcs With Shorten Travel Times";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& path = mP_ka.at(k);
			if (path.size() <= 1)
				continue;

			auto last = path.end();

			for (auto it = path.begin(); it != last; ++it)
			{
				auto& arc = *it;

				// TERM: tau_ij * (1 - sigma_kij)
				auto rhs = GRBLinExpr(tau_ij[to_index(arc)] * (1 - sigma_kij[{k, arc}]));

				if (arc->isDelayArc())
				{
#if ADD_DEBUG_NAMES
					std::string constrName = "C6_CountArcsWithShortenTravelTimes(" + to_string(*arc) + ")";
					mModel.addConstr(theta_kij[{k, arc}], GRB_LESS_EQUAL, rhs, constrName.c_str());
#else
					mModel.addConstr(theta_kij[{k, arc}], GRB_LESS_EQUAL, rhs);
#endif
				}
				else
				{
#if ADD_DEBUG_NAMES
					std::string constrName = "C6_CountArcsWithShortenTravelTimes(" + to_string(*arc) + ")";
					mModel.addConstr(theta_kij[{k, arc}], GRB_GREATER_EQUAL, rhs, constrName.c_str());
#else
					mModel.addConstr(theta_kij[{k, arc}], GRB_GREATER_EQUAL, rhs);
#endif
				}
			}
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint6_CountArcsWithShortenTravelTimes: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 7 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_Gurobi::constraint7_EnsureAllowableDispatchTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint7: Ensure Allowable Dispatch Times";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& path = mP_ka.at(k);
			if (path.size() <= 1)
				continue;

			const auto& nodes = mP_ki.at(k);
			auto n = path.size()-1;
			for (std::size_t i = 0; i < n; ++i)
			{
				auto& arc = path[i];

				auto lhs = GRBLinExpr(gamma_ki[{k, arc->getSource()}] + theta_kij[{k, arc}]);
				const auto& next = nodes[i+1];

#if ADD_DEBUG_NAMES
				std::string constrName = "C7_EnsureAllowableDispatchTimes(";
				constrName += k->getFullyQualifiedName() + ", " + next->getName() + ")";
				mModel.addConstr(lhs, GRB_LESS_EQUAL, gamma_ki[{k, next}], constrName.c_str());
#else
				mModel.addConstr(lhs, GRB_LESS_EQUAL, gamma_ki[{k, next}]);
#endif
			}
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint7_EnsureAllowableDispatchTimes: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 8 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_Gurobi::constraint8_EnsureDispatchIsAfterAvailable()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint8: Ensure Dispatch Is After Available";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& nodes = mP_ki.at(k);
			if (nodes.empty())
				continue;
        
			auto& loc = nodes.front();
			double eat = k->getEarliestAvailableTime();
        
			const auto& path = mP_ka.at(k);
			for (auto& arc : path)
			{
				if (arc->arcCapacityIsLimited())
					eat = arc->getSource()->getTime();
				break;
			}

			gamma_ki[{k, loc}].set(GRB_DoubleAttr_LB, eat);
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint8_EnsureDispatchIsAfterAvailable: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 9 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_Gurobi::constraint9_EnsureArrivalBeforeDue()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint9: Ensure Arrival Before Due";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			const auto& arcs = mP_ka.at(k);
			if (arcs.empty())
				continue;
        
			auto& arc = arcs.back();
			auto lhs = GRBLinExpr(gamma_ki[{k, arc->getSource()}] + theta_kij[{k, arc}]);
			double ldt = k->getLatestDeliveryTime();

#if ADD_DEBUG_NAMES
			std::string constrName = "C9_EnsureArrivalBeforeDue(";
			constrName += k->getFullyQualifiedName() + ", " + arc->getDestinationServiceName() + ")";
			mModel.addConstr(lhs, GRB_LESS_EQUAL, ldt, constrName.c_str());
#else
			mModel.addConstr(lhs, GRB_LESS_EQUAL, ldt);
#endif
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint9_EnsureArrivalBeforeDue: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 10 for the algorithm listed on page 1312
//
void cIdentifyArcsToLengthenModel_Gurobi::constraint10_EnsureConsolidations()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint10: Ensure Consolidations";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			node_ptr_t i = arc->getSource();

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto k_it = commodities.begin();
				auto& k1 = *k_it;

				for (++k_it; k_it != commodities.end(); ++k_it)
				{
					auto& k2 = *k_it;
					try
					{
#if ADD_DEBUG_NAMES
						std::string constrName = strConstraint10(*k1, *k2, *arc);
						mModel.addConstr(gamma_ki[{k1, i}], GRB_EQUAL, gamma_ki[{k2, i}], constrName.c_str());
#else
						mModel.addConstr(gamma_ki[{k1, i}], GRB_EQUAL, gamma_ki[{k2, i}]);
#endif
					}
					catch (const GRBException& e)
					{
						std::string msg(e.getMessage());
						if (gamma_ki.find({ k2, i }) == gamma_ki.end())
						{
							msg += ": gamma_k2i{";
							msg += k2->getUniqueName();
						}
						else if (gamma_ki.find({ k1, i }) == gamma_ki.end())
						{
							msg += ": gamma_k1i{";
							msg += k1->getUniqueName();
						}
						else
						{
							msg += ": unknown{";
							msg += k1->getUniqueName();
							msg += ", ";
							msg += k2->getUniqueName();
						}
						msg += ", " + i->getName() + "}";
						throw std::logic_error(msg);
					}
				}
			}
		}

	    mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint10_EnsureConsolidations: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}


//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

void cIdentifyArcsToLengthenModel_Gurobi::DumpIdentifyArcsTimeInfo(std::ostream& out)
{
    out << std::endl;
    out << "\n*** Times ***\n";
    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        if (path.empty())
            continue;
        
        for (int i = 0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            out << k->getUniqueName() << ", " << *arc;
            out << ": tau_ij= " << tau_ij[to_index(arc)];
            out << ": tau_bar_ij= " << tau_bar_kij[{k, arc}];
            out << ", theta_kij= " << as_double(theta_kij[{k, arc}]);
            out << ", gamma1_ki= " << as_double(gamma_ki[{k, arc->getSource()}]);
            out << ", sigma_kij= " << as_int(sigma_kij[{k, arc}]) << "\n";
        }
        
        out << "\n";
    }
    return;
    
    out << "*** Dispatch times ***\n" << std::endl;
    for(auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        for (auto& node : path)
        {
            out << k->getFullyQualifiedName() << "," << node->getName() << "= " << as_double(gamma_ki[{k, node}]) << std::endl;
        }
    }
    
    out << "\n*** Travel times ***\n" << std::endl;
    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        if (path.empty())
            continue;
        
		auto& nodes = mP_ki.at(k);

        for (int i = 0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            out << k->getFullyQualifiedName() << "," << to_index(arc) << " : " << as_double(theta_kij[{k, arc}]);
            out << " >= " << tau_ij[to_index(arc)] << " and " << tau_bar_kij[{k, arc}] << " @ " << as_int(sigma_kij[{k, arc}]) << std::endl;
            out << k->getFullyQualifiedName() << "," << to_index(arc) << " :  ";
            out << as_double(gamma_ki[{k, arc->getSource()}]) << " + ";
            out << as_double(theta_kij[{k, arc}]) << " <= ";
            out << as_double(gamma_ki[{k, nodes[i+1]}]) << std::endl;
        }
    }
    
    out << "\n*** End times ***\n" << std::endl;
    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;
        auto& path = it->second;
        if (path.empty())
            continue;
        
        auto& arc = path.back();
        
        out << k->getFullyQualifiedName() << "," << to_index(arc) << " :  ";
        out << as_double(gamma_ki[{k, arc->getSource()}]) << " + ";
        out << as_double(theta_kij[{k, arc}]) << " <= ";
        out << k->getLatestDeliveryTime() << " and ";
        out << tau_bar_kij[{k, arc}] << std::endl;
    }
}

void cIdentifyArcsToLengthenModel_Gurobi::DumpCommoditySchedules(std::ostream& out)
{
    out << "\n";
    
    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        DumpCommoditySchedule(out, k);
    }
}

void cIdentifyArcsToLengthenModel_Gurobi::DumpCommoditySchedule(std::ostream& out, const cCommodityBundle* const k)
{
    out << k->getFullyQualifiedName() << " " << k->getSourceName();
    out << " (eat: " << k->getEarliestAvailableTime() << ")";
    out << " -> " << k->getDestinationName();
    out << " (ldt: " << k->getLatestDeliveryTime() << ")\n";
    out << "Schedule:\n";
    const auto& path = mP_ka.at(k);

    if (path.empty())
    {
        out << "\tNONE!\n";
        return;
    }
    
    for(int i = 0; i < path.size()-1; ++i)
    {
        auto& arc = path[i];
        
        double gamma = as_double(gamma_ki[{k, arc->getSource()}]);
        out << "\t" << arc->getSourceServiceName() << ", departs: " << gamma;
		double travel_time = as_double(theta_kij[{k, arc}]);
		
		auto* link = arc->getServiceLink();
		if (link)
		{
			out << " == {" << link->getName() << ", " << travel_time << "} ==> ";
		}
		else
		{
			out << " == {" << arc->getSourceServiceName() << ", " << travel_time << "} ==> ";
		}

		out << arc->getDestinationServiceLocation() << ", arrives: " << gamma + travel_time;
		out << ", tau = " << tau_ij[to_index(arc)];
		out << ", tau bar = " << tau_bar_kij[{k, arc}] << ", sigma = " << as_int(sigma_kij[{k, arc}]);

        if (mJ_ak.count(arc) > 0)
        {
            const auto& commodities = mJ_ak.at(arc);
            if (commodities.empty())
                continue;

            out << ", shared = " << commodities.size();
        }

        out << "\n";
    }
    
    out << "\n";
}
