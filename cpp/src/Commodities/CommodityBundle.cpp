
#include "CommodityBundle.hpp"
#include "CommodityShipment.hpp"
#include <cassert>
#include <algorithm>

namespace
{
    static unsigned int commodityId = 0;
}

commodity_id_t get_num_commodity_bundles()
{
    return commodityId;
}

void reset_num_commodity_bundles()
{
    commodityId = 0;
}

cCommodityBundle::cCommodityBundle(const cCommodityShipment* const pShipment,
                                    const std::string& name)
:
    mId(commodityId++),
    mpShipment(pShipment), mName(name), mQuantity(1),
    mHasDelayCost(false), mDelayCost(0), 
	mHasDelayPenalty(false),
	mDelayPenaltyRate(0), mDelayPenaltyLimit(0),
    mHasMaxDelayTime(false), mMaxDelayTime(0)
{
    assert(mpShipment);
	mpSourceNode = nullptr;
	mpDestinationNode = nullptr;
}

const std::string cCommodityBundle::getUniqueName() const
{
    return mName + "_" + std::to_string(mId);
}

const std::string cCommodityBundle::getFullyQualifiedName() const
{
    return mpShipment->getName() + ":" + mName + "_" + std::to_string(mId);
}

const cCommodityShipment& cCommodityBundle::getShipment() const
{
    return *mpShipment;
}

const std::string& cCommodityBundle::getShipmentName() const
{
    return mpShipment->getName();
}

const std::string& cCommodityBundle::getSourceName() const
{
    return mpShipment->getSourceName();
}

const cServiceLocation* cCommodityBundle::getSourceLocation() const
{
    return mpShipment->getSourceLocation();
}

const cPartiallyTimeExpandedNetworkNode* cCommodityBundle::getSourceNode() const
{
	return mpSourceNode;
}

const std::string& cCommodityBundle::getDestinationName() const
{
    return mpShipment->getDestinationName();
}

const cServiceLocation* cCommodityBundle::getDestinationLocation() const
{
    return mpShipment->getDestinationLocation();
}

const cPartiallyTimeExpandedNetworkNode* cCommodityBundle::getDestinationNode() const
{
	return mpDestinationNode;
}

double cCommodityBundle::getEarliestAvailableTime() const
{
    return mpShipment->getEarliestAvailableTime();
}

double cCommodityBundle::getLatestDeliveryTime() const
{
    return mpShipment->getLatestDeliveryTime();
}

const std::vector<std::string>&  cCommodityBundle::getCategories() const
{
    return mCategories;
}

void cCommodityBundle::addCategory(const std::string& category)
{
    mCategories.push_back(category);
}

bool cCommodityBundle::hasRequiredLocations() const
{
    return (mRequiredLocations.size() > 0)
		|| mpShipment->hasRequiredLocations();
}

bool cCommodityBundle::isRequiredLocation(const std::string& loc_name) const
{
    if (mRequiredLocations.empty()) return false;
    
    auto it = std::find(mRequiredLocations.begin(), mRequiredLocations.end(), loc_name);
    return it != mRequiredLocations.end();
}

const std::vector<std::string>& cCommodityBundle::getRequiredLocations() const
{
    return mRequiredLocations;
}

void cCommodityBundle::addRequiredLocation(const std::string& loc_name)
{
    if (std::find(mRequiredLocations.begin(), mRequiredLocations.end(), loc_name)
        == mRequiredLocations.end())
    {
        mRequiredLocations.push_back(loc_name);
    }
}

void cCommodityBundle::setSourceNode(const cPartiallyTimeExpandedNetworkNode* pNode)
{
	mpSourceNode = pNode;
}

void cCommodityBundle::setDestinationNode(const cPartiallyTimeExpandedNetworkNode* pNode)
{
	mpDestinationNode = pNode;
}


////////////////////////////////////////////////////////////
// Commodity bundle algorithms
////////////////////////////////////////////////////////////

std::string to_string(const cCommodityBundle& k)
{
	return k.getUniqueName();
}

std::ostream& operator<<(std::ostream& out, const cCommodityBundle& k)
{
	out << k.getUniqueName();
	return out;
}
