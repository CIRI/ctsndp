
#include "CommodityShipment.hpp"
#include <cassert>
#include <algorithm>

cCommodityShipments::cCommodityShipments()
:
	mShortestPathRatioThreshold(0.75)
{
    reset_num_commodity_bundles();
}

cCommodityShipments::~cCommodityShipments()
{
    for (auto shipment : mShipments)
        delete shipment;
    mShipments.clear();
}

const unsigned int cCommodityShipments::getNumOfCommodityBundles() const
{
    return get_num_commodity_bundles();
}

const std::set<cCommodityShipment*>&  cCommodityShipments::getShipments() const
{
    return mShipments;
}

void cCommodityShipments::addShipment(std::unique_ptr<cCommodityShipment> shipment)
{
    mShipments.insert(shipment.release());
}

cCommodityShipment* cCommodityShipments::addShipment(const std::string& name,
                                                     const std::string& src_name,
                                                     const std::string& dest_name)
{
    cCommodityShipment* shipment = new cCommodityShipment(name, src_name, dest_name);
    mShipments.insert(shipment);
    return shipment;
}

cCommodityBundle* const cCommodityShipments::find(const std::string& bundle_name)
{
    auto sep_pos = bundle_name.find(":");
    std::string shipment_name = bundle_name.substr(0, sep_pos);
    
    for (auto& shipment : mShipments)
    {
        if (shipment->getName() == shipment_name)
        {
            auto& commodities = shipment->getCommodityBundles();
            for (auto& commodity : commodities)
            {
                if (commodity->getFullyQualifiedName() == bundle_name)
                    return commodity;
            }
        }
    }
    
    return nullptr;
}

double cCommodityShipments::getShortestPathRatioThreshold() const
{
	return mShortestPathRatioThreshold;
}

void cCommodityShipments::setShortestPathRatioThreshold_pct(double ratio_pct)
{
	mShortestPathRatioThreshold = ratio_pct / 100.0;
}


/***********************************************************************
 *                 C o m m o d i t y   S h i p m e n t
 **********************************************************************/

cCommodityShipment::cCommodityShipment(const std::string& name, 
    const std::string& src_name, const std::string& dest_name)
:
    mName(name),
    mSourceName(src_name),
    mpSourceLocation(nullptr),
    mDestinationName(dest_name),
    mpDestinationLocation(nullptr)
{
    mDelayCost = 0.0;
	mDelayPenaltyRate = 0.0;
	mEarliestAvailableTime = 0.0;
    mLatestDeliveryTime = 0.0;
	mShortestPathTime = -1.0;
}

cCommodityShipment::~cCommodityShipment()
{
    for(auto k : mCommodities)
        delete k;

    mCommodities.clear();
} 

const std::string& cCommodityShipment::getName() const
{
    return mName;
}

const std::string& cCommodityShipment::getSourceName() const
{
    return mSourceName;
}

const std::string& cCommodityShipment::getDestinationName() const
{
    return mDestinationName;
}

void cCommodityShipment::setSourceName(const std::string& new_name)
{
    mSourceName = new_name;
}

void cCommodityShipment::setSourceLocation(const cServiceLocation* loc)
{
    mpSourceLocation = loc;
}

void cCommodityShipment::setDestinationName(const std::string& new_name)
{
    mDestinationName = new_name;
}

void cCommodityShipment::setDestinationLocation(const cServiceLocation* loc)
{
    mpDestinationLocation = loc;
}

void cCommodityShipment::addCommodityBundle(std::unique_ptr<cCommodityBundle> bundle)
{
    mCommodities.push_back(bundle.release());
}

cCommodityBundle* cCommodityShipment::addCommodityBundle(const std::string& type)
{
    cCommodityBundle* bundle = new cCommodityBundle(this, type);
    mCommodities.push_back(bundle);
    return bundle;
}

std::size_t cCommodityShipment::numCommodityBundles() const
{
    return mCommodities.size();
}

cCommodityBundle* cCommodityShipment::getCommodityBundle(std::size_t index) const
{
    return mCommodities.at(index);
}

bool cCommodityShipment::hasRequiredLocations() const
{
    return mRequiredLocations.size() > 0;
}

bool cCommodityShipment::isRequiredLocation(const std::string& loc_name) const
{
    if (mRequiredLocations.empty()) return false;
    
    auto it = std::find(mRequiredLocations.begin(), mRequiredLocations.end(), loc_name);
    return it != mRequiredLocations.end();
}

const std::vector<std::string>& cCommodityShipment::getRequiredLocations() const
{
    return mRequiredLocations;
}

void cCommodityShipment::addRequiredLocation(const std::string& loc_name)
{
    if (std::find(mRequiredLocations.begin(), mRequiredLocations.end(), loc_name)
        == mRequiredLocations.end())
    {
        mRequiredLocations.push_back(loc_name);
    }
}

double cCommodityShipment::getShortestPathTime() const
{
	return mShortestPathTime;
}

void cCommodityShipment::setShortestPathTime(double time)
{
	mShortestPathTime = time;
}
