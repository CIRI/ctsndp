
#pragma once

#include "CommodityBundle.hpp"
#include "../Properties/Properties.hpp"

#include <vector>
#include <memory>
#include <string>
#include <set>

class cCommodityShipment;
class cServiceLocation;

class cCommodityShipments
{
public:
    cCommodityShipments();
    ~cCommodityShipments();

    /**
     * Return the total number of commodity bundles
     **/
    const unsigned int  getNumOfCommodityBundles() const;

    /**
     * Return all of the commodities within the service
     * network
     **/
    const std::set<cCommodityShipment*>&  getShipments() const;

    /**
     * Add a shipement (set of commodities) to the network
     **/
    void addShipment(std::unique_ptr<cCommodityShipment> shipment);
    cCommodityShipment* addShipment(const std::string& name,
                                    const std::string& src_name,
                                    const std::string& dest_name);
    
    cCommodityBundle* const find(const std::string& bundle_name);

	double getShortestPathRatioThreshold() const;
	void setShortestPathRatioThreshold_pct(double ratio_pct);

private:
    std::set<cCommodityShipment*> mShipments;

	double mShortestPathRatioThreshold;
};

/**
 * We define a shipment as:
 *
 * Cargo transported under the terms of a single bill of lading or air waybill,
 * irrespective of the quantity or number of containers, packages, or pieces.
 *
 * Read more: http://www.businessdictionary.com/definition/shipment.html
 *
 * In our case that means a shipment has a single source, destination, EAT and LDT.
 */
class cCommodityShipment : public cProperties
{
public:
    cCommodityShipment(const std::string& name, 
                        const std::string& src_name, 
                        const std::string& dest_name);
    ~cCommodityShipment();

    const std::string& getName() const;
    const std::string& getSourceName() const;
    const cServiceLocation* getSourceLocation() const;
    const std::string& getDestinationName() const;
    const cServiceLocation* getDestinationLocation() const;

	double getDelayCost() const;
	double getDelayPenaltyRate() const;

    double getEarliestAvailableTime() const;
    double getLatestDeliveryTime() const;

    void setDelayCost(double cost, double penalty_rate);

    void setEarliestAvailableTime(double time);
    void setLatestDeliveryTime(double time);

    std::size_t numCommodityBundles() const;
    cCommodityBundle* getCommodityBundle(std::size_t index) const;
    const std::vector<cCommodityBundle*>& getCommodityBundles() const;
    void addCommodityBundle(std::unique_ptr<cCommodityBundle> bundle);
    cCommodityBundle* addCommodityBundle(const std::string& type);

    void setSourceName(const std::string& new_name);
    void setDestinationName(const std::string& new_name);

    void setSourceLocation(const cServiceLocation* loc);
    void setDestinationLocation(const cServiceLocation* loc);

    bool hasRequiredLocations() const;
    bool isRequiredLocation(const std::string& loc_name) const;
    const std::vector<std::string>& getRequiredLocations() const;
    void addRequiredLocation(const std::string& loc_name);

	double getShortestPathTime() const;
	void setShortestPathTime(double time);

private:
    std::string mName;
    std::string mSourceName;
    const cServiceLocation* mpSourceLocation;
    std::string mDestinationName;
    const cServiceLocation* mpDestinationLocation;
    std::vector<std::string> mRequiredLocations;

	double mDelayCost;
	double mDelayPenaltyRate;
	double mEarliestAvailableTime;
    double mLatestDeliveryTime;
	double mShortestPathTime;

    std::vector<cCommodityBundle*> mCommodities;
    
    friend class cCommodityShipments;
	friend class cCommodityBundle;
};


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const cServiceLocation* cCommodityShipment::getSourceLocation() const
{
    return mpSourceLocation;
}

inline const cServiceLocation* cCommodityShipment::getDestinationLocation() const
{
    return mpDestinationLocation;
}

inline double cCommodityShipment::getDelayCost() const
{
    return mDelayCost;
}

inline double cCommodityShipment::getDelayPenaltyRate() const
{
	return mDelayPenaltyRate;
}

inline double cCommodityShipment::getEarliestAvailableTime() const
{
    return mEarliestAvailableTime;
}

inline double cCommodityShipment::getLatestDeliveryTime() const
{
    return mLatestDeliveryTime;
}

inline void cCommodityShipment::setDelayCost(double cost, double rate)
{
	mDelayCost = cost >= 0 ? cost : 0.0;
	mDelayPenaltyRate = rate >= 0 ? rate : 0.0;
}

inline void cCommodityShipment::setEarliestAvailableTime(double time)
{
    mEarliestAvailableTime = time;
}

inline void cCommodityShipment::setLatestDeliveryTime(double time)
{
    mLatestDeliveryTime = time;
}

inline const std::vector<cCommodityBundle*>& cCommodityShipment::getCommodityBundles() const
{
    return mCommodities;
}

