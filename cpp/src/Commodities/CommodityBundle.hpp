
#ifndef COMMODITY_BUNDLE
#define COMMODITY_BUNDLE

#include "../Properties/Properties.hpp"

#include <string>
#include <vector>

class cCommodityShipment;
class cCommodityBundle;
class cServiceLocation;
class cPartiallyTimeExpandedNetworkNode;

typedef const unsigned int  commodity_id_t;

/**
 * A "commodity bundle" represents the atomic unit of cargo in a shipment.
 *
 * For example, if I am talking about move commodities around a shipping port,
 * then a commodity bundle would represent a shipping container (TEU).
 *
 * If I am moving pallets around a warehouse, then a commodity bundle represents
 * a pallet of goods.
 */
class cCommodityBundle : public cProperties
{
public:
    cCommodityBundle(const cCommodityShipment* const pShipment, 
                        const std::string& name);
    ~cCommodityBundle() = default;

    commodity_id_t getId() const;
    const std::string& getName() const;
    const std::string getUniqueName() const;
    const std::string getFullyQualifiedName() const;

    unsigned int getQuantity() const;

    const cCommodityShipment& getShipment() const;

    const std::string& getShipmentName() const;
    const std::string& getSourceName() const;
    const cServiceLocation* getSourceLocation() const;
    const std::string& getDestinationName() const;
    const cServiceLocation* getDestinationLocation() const;

	const cPartiallyTimeExpandedNetworkNode* getSourceNode() const;
	const cPartiallyTimeExpandedNetworkNode* getDestinationNode() const;

    double getEarliestAvailableTime() const;
    double getLatestDeliveryTime() const;

    bool hasDelayCost() const;
    double getDelayCost() const;
	bool hasDelayPenalty() const;
	double getDelayPenaltyRate() const;
	bool hasDelayPenaltyLimit() const;
	double getDelayPenaltyLimit() const;

    bool hasMaxDelayTime() const;
    double getMaxDelayTime() const;

    void setQuantity(unsigned int quantity);
    void setDelayCost(double cost);
	void setDelayPenalties(double rate, double limit);
	void setMaxDelayTime(double max_delay_time);
    
    const std::vector<std::string>&  getCategories() const;
    void addCategory(const std::string& category);

    bool hasRequiredLocations() const;
    bool isRequiredLocation(const std::string& loc_name) const;
    const std::vector<std::string>& getRequiredLocations() const;
    void addRequiredLocation(const std::string& loc_name);

    bool operator<(const cCommodityBundle& rhs) const;
    bool operator<(const cCommodityBundle* rhs) const;

	void setSourceNode(const cPartiallyTimeExpandedNetworkNode* pNode);
	void setDestinationNode(const cPartiallyTimeExpandedNetworkNode* pNode);

protected:

private:
    commodity_id_t mId;
    const cCommodityShipment* const mpShipment;
    std::string mName;
    unsigned int mQuantity;
    bool mHasDelayCost;
    double mDelayCost;
	bool mHasDelayPenalty;
	double mDelayPenaltyRate;
	double mDelayPenaltyLimit;
	bool mHasMaxDelayTime;
    double mMaxDelayTime;

	const cPartiallyTimeExpandedNetworkNode* mpSourceNode;
	const cPartiallyTimeExpandedNetworkNode* mpDestinationNode;

    std::vector<std::string> mCategories;
    std::vector<std::string> mRequiredLocations;

    friend class cCommodityShipment;
};

commodity_id_t get_num_commodity_bundles();
void reset_num_commodity_bundles();

std::ostream& operator<<(std::ostream& out, const cCommodityBundle& k);

///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline commodity_id_t cCommodityBundle::getId() const
{
    return mId;
}

inline const std::string& cCommodityBundle::getName() const
{
    return mName;
}

inline unsigned int cCommodityBundle::getQuantity() const
{
    return mQuantity;
}

inline bool cCommodityBundle::hasDelayCost() const
{
    return mHasDelayCost;
}

inline double cCommodityBundle::getDelayCost() const
{
    return mDelayCost;
}

inline bool cCommodityBundle::hasDelayPenalty() const
{
	return mHasDelayPenalty;
}

inline double cCommodityBundle::getDelayPenaltyRate() const
{
	return mDelayPenaltyRate;
}

inline bool cCommodityBundle::hasDelayPenaltyLimit() const
{
	return mDelayPenaltyLimit > 0.0;
}

inline double cCommodityBundle::getDelayPenaltyLimit() const
{
	return mDelayPenaltyLimit;
}

inline bool cCommodityBundle::hasMaxDelayTime() const
{
    return mHasMaxDelayTime;
}

inline double cCommodityBundle::getMaxDelayTime() const
{
    return mMaxDelayTime;
}


inline void cCommodityBundle::setQuantity(unsigned int quantity)
{
    mQuantity = quantity;
}

inline void cCommodityBundle::setDelayCost(double cost)
{
    mHasDelayCost = (cost >= 0.0);
    mDelayCost = cost >= 0.0 ? cost : 0.0;
}

inline void cCommodityBundle::setDelayPenalties(double rate, double limit)
{
	mHasDelayPenalty = (rate >= 0.0);
	mDelayPenaltyRate = rate >= 0.0 ? rate : 0.0;
	mDelayPenaltyLimit = limit >= 0.0 ? limit : 0.0;
}

inline void cCommodityBundle::setMaxDelayTime(double max_delay_time)
{
    mHasMaxDelayTime = max_delay_time > 0.0;
    mMaxDelayTime = max_delay_time;
}

inline bool cCommodityBundle::operator<(const cCommodityBundle& rhs) const
{
    return mId < rhs.mId;
}

inline bool cCommodityBundle::operator<(const cCommodityBundle* rhs) const
{
    return mId < rhs->mId;
}

#endif
