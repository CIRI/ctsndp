
#pragma once

#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "routes.hpp"
#include <nlohmann/json.hpp>
#include <string>
#include <memory>

class cProperties;


/*!
 * \brief Create an instance of cServiceNetwork from a JSON file.
 *
 * @param the filename of the JSON based service network file.
 *
 * @return a unique pointer to the class representing the service network
 */
std::unique_ptr<cServiceNetwork> create_service_network_from_json_file(const std::string& jsonFileName);

/*!
 * \brief Create an instance of cCommodityShipments from a JSON file.
 *
 * @param the filename of the JSON based commodity shipments file.
 *
 * @return a unique pointer to the class representing the shipments
 */
std::unique_ptr<cCommodityShipments> create_shipments_from_json_file(const std::string& jsonFileName, 
	bool no_delays, bool group);

/*!
 * \brief Serializes a JSON document to a file.
 *
 * @param the filename of the output JSON file.
 * @param a reference to the JSON document to output.
 */
void write_json_file(const std::string& jsonFileName, const nlohmann::json& jsonDoc);

std::any to_any(const nlohmann::json& json_value);
nlohmann::json from_any(const std::any& value);
nlohmann::json from_any(const std::string& name, const std::any& value);
void add_properties(nlohmann::json& json_obj, const cProperties& properties);


template<typename T>
T get_required(nlohmann::json& json, const char* name, const std::string& jsonFileName)
{
	try
	{
		if (!json.contains(name))
		{
			std::string msg = "In the file \"";
			msg += jsonFileName;
			msg += "\", the required entry \"";
			msg += name;
			msg += "\" is missing.\nHere is the json block:\n";
			msg += json.dump(4);
			throw std::runtime_error(msg.c_str());
		}

		T result = json[name].get<T>();
		return result;
	}
	catch (const std::runtime_error& e)
	{
		throw;
	}
	catch (const std::exception& e)
	{
		std::string msg = "In the file \"";
		msg += jsonFileName;
		msg += "\", the required entry \"";
		msg += name;
		msg += "\" generated the following error: ";
		msg += e.what();
		msg += "\nHere is the json block:\n";
		msg += json.dump(4);
		throw std::runtime_error(msg.c_str());
	}
}

template<typename T>
T get_optional(nlohmann::json& json, const char* name, T _default, const std::string& jsonFileName)
{
	try
	{
		T result = _default;
		if (json.contains(name) && !json[name].empty())
			result = json[name].get<T>();
		return result;
	}
	catch (const std::exception& e)
	{
		std::string msg = "In the file \"";
		msg += jsonFileName;
		msg += "\", the optional entry \"";
		msg += name;
		msg += "\" generated the following error: ";
		msg += e.what();
		msg += "\nHere is the json block:\n";
		msg += json.dump(4);
		throw std::runtime_error(msg.c_str());
	}
}
