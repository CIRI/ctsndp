//
//  algorithm6_ensure_capacities.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 2/26/20.
//

#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "ServiceNetwork/ServiceLink.hpp"
#include "ctsndp_utils.hpp"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <iterator>

double find_t_new(cPartiallyTimeExpandedNetwork& network, node_ptr_t reference_node, double t_initial)
{
	auto node = network.findNode(reference_node, t_initial);

	// Are already at t_initial?  Yes, walk the storage arc until we find
	// the last node
	if (!node)
	{
		// We need to find the lower bound, t_k, and upper bound, t_k+1
		auto time_points = network.timepoints(reference_node->getServiceLocation());
		assert(!time_points.empty());

		// Find lower bound such that t_k <= t_new
		double t_k = 0;
		auto lower = std::lower_bound(time_points.begin(), time_points.end(), t_initial);
		if (lower == time_points.begin())
			t_k = *(lower);
		else
			t_k = *(--lower);

		node = network.findNode(reference_node->getServiceLocation(), t_k);
	}

	auto arc = node->getStorageOutflow();
	while (arc && (arc->getActualTravelTime() > 0))
	{
		node = arc->getDestination();
		arc = node->getStorageOutflow();
	}

	if (node->getTime() > t_initial)
		return node->getTime();

	return t_initial;
}

void ctsndp::ensure_capacities(cPartiallyTimeExpandedNetwork& network,
                  ctsndp::arcs_over_capacity_t&& over_capacity_arcs,
				  double consolidation_time,
                  std::ostream* debug)
{
	if (debug)
	{
		*debug << "Ensuring arc capacities in partially time expanded network:\n";
		for (auto& info : over_capacity_arcs)
		{
			*debug << "   " << *(info.arc) << ", dispatch = " << info.dispatch_time;
			*debug << ", utilization = " << info.utilization << "\n";
		}

		*debug << "\n\n";
	}

	std::set<cPartiallyTimeExpandedNetworkArc*> arcs_to_delete;


	for (auto it = over_capacity_arcs.begin(); it != over_capacity_arcs.end(); ++it)
	{
		auto& info = *it;

		if (info.arc == nullptr)
			continue;

		// Determine the capacity we are assigning to the arc based on
		// the link and the consolidation of arcs
		unsigned int u_ij = info.utilization;
		auto link = info.arc->getServiceLink();
		u_ij = static_cast<unsigned int>(std::ceil(static_cast<double>(u_ij) / link->getCapacity()));

		auto cycle_time = link->getCycleTime();
		double capacity_boost = std::max(consolidation_time / cycle_time, 1.0);
		u_ij = static_cast<unsigned int>(std::ceil(u_ij / capacity_boost));
		unsigned int capacity = capacity_boost * link->getCapacity();

		double step_time = std::max(consolidation_time, cycle_time);

		auto node = info.arc->getSource();

		double t_k = 0;
		double t_k_1 = 0;

		double t_new = find_t_new(network, node, info.dispatch_time);

		// If the node already exists at the correct time, we don't
		// have to do anything here
		auto tmp_node = network.findNode(node, t_new);
		if (! tmp_node)
		{
			// We need to find the lower bound, t_k, and upper bound, t_k+1
			auto time_points = network.timepoints(node->getServiceLocation());
			assert(!time_points.empty());

			// Find lower bound such that t_k <= t_new
			auto lower = std::lower_bound(time_points.begin(), time_points.end(), t_new);
			if (lower == time_points.begin())
				t_k = *(lower);
			else
				t_k = *(--lower);

			// Find upper bound such that t_new <= t_k_1
			auto upper = std::upper_bound(lower, time_points.end(), t_new);
			if (upper == time_points.end())
				t_k_1 = *(--upper);
			else
				t_k_1 = *(upper);

			auto n = node;
			auto arcs = refine(network, n, t_k, t_new, t_k_1, step_time, capacity, link, debug);
			for (auto arc : arcs)
			{
				auto itt = it;
				for (++itt; itt != over_capacity_arcs.end(); ++itt)
				{
					if (arc == itt->arc)
						itt->arc = nullptr;
				}

				arcs_to_delete.insert(arc);
			}

			auto tmp = restore(network, *node, t_k, t_new, debug);

			for (auto arc : tmp)
			{
				arcs_to_delete.insert(arc);
			}

			node = network.findNode(node, t_new);
		}
		else
		{
			node = tmp_node;
		}

		arc_ptr_t arc = nullptr;

		for (auto a : node->getOutflows())
		{
			if (a->getServiceLink() == info.arc->getServiceLink())
			{
				a->limitArcCapacity(true);
				arc = a;
				break;
			}
		}

		assert(arc);
		assert(node == arc->getSource());

        auto src = node;
        auto dst = arc->getDestinationNode();

		auto loc = info.arc->getSourceServiceLocation();

        if (debug)
            *debug << "Arc " << to_string(*arc) << " is being expanded " << u_ij << " times.\n";
            
        for (unsigned int i = 1; i < u_ij; ++i)
        {
			double t_k = t_new;
			t_new = src->getTime() + step_time;

			auto arcs = refine(network, src, t_k, t_new, t_k_1, step_time, capacity, link, debug);
			for (auto arc : arcs)
			{
				auto itt = it;
				for (++itt; itt != over_capacity_arcs.end(); ++itt)
				{
					if (arc == itt->arc)
						itt->arc = nullptr;
				}

				arcs_to_delete.insert(arc);
			}

			arcs = restore(network, *node, t_k, t_new, debug);
			for (auto arc : arcs)
			{
				auto itt = it;
				for (++itt; itt != over_capacity_arcs.end(); ++itt)
				{
					if (arc == itt->arc)
						itt->arc = nullptr;
				}

				arcs_to_delete.insert(arc);
			}
		}

		// If the original arc is unlimited, we need to shutdown that
		// path otherwise it will still be used and are capacity limits
		// will be ignored.
		if (!info.arc->arcCapacityIsLimited())
		{
			info.arc->setCapacity(0);
			info.arc->limitArcCapacity(true);

//			arcs_to_delete.insert(info.arc);
		}
    }
	over_capacity_arcs.clear();

	for (auto arc : arcs_to_delete)
	{
		network.deleteArc(arc);
	}
	arcs_to_delete.clear();

    if (debug)
        *debug << "complete!" << std::endl;
}
