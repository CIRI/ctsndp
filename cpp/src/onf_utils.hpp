
#pragma once

#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "routes.hpp"
#include <nlohmann/json.hpp>
#include <string>
#include <memory>

namespace onf
{
    /*!
     * \brief Test to see if the JSON document represents a service network formatted
     * in the ONF scheme.
     *
     * @param the JSON document to test.
     *
     * @return True if the file is a valid ONF service network,
     * otherwise false
     */
    bool is_valid_service_network_json(nlohmann::json& jsonDoc, const std::string& fileName);
    
    /*!
     * \brief Test to see if the JSON document represents a shipment data formatted
     * in the ONF scheme.
     *
     * @param the JSON document to test.
     *
     * @return True if the file is a valid ONF shipment file,
     * otherwise false
     */
    bool is_valid_shipment_json(nlohmann::json& jsonDoc, const std::string& fileName);

    /*!
     * \brief Create an instance of cServiceNetwork from a JSON document.
     *
     * @param a JSON document formatted in the ONF scheme representing
     * a service network
     *
     * @return a unique pointer to the class representing the service network
     */
    std::unique_ptr<cServiceNetwork> create_service_network_from_json_doc(nlohmann::json& jsonDoc);
    
    /*!
     * \brief Create an instance of cServiceNetwork from a ONF JSON file.
     *
     * @param the filename of the JSON based service network file.
     *
     * @return a unique pointer to the class representing the service network
     */
    std::unique_ptr<cServiceNetwork> create_service_network_from_json_file(const std::string& jsonFileName);

    /*!
     * \brief Create an instance of cCommodityShipments from a JSON document.
     *
     * @param a JSON document formatted in the ONF scheme representing
     * the commodities being shipped arcoss the service network
     *
     * @return a unique pointer to the class representing the commodity shipments
     */
    std::unique_ptr<cCommodityShipments> create_shipments_from_json_doc(nlohmann::json& jsonDoc, 
		bool no_delays, bool group);
    
    /*!
     * \brief Create an instance of cCommodityShipments from a ONF JSON file.
     *
     * @param the filename of the JSON based file.
     *
     * @return a unique pointer to the class representing the commodity shipments
     */
    std::unique_ptr<cCommodityShipments> create_shipments_from_json_file(const std::string& jsonFileName, 
		bool no_delays, bool group);

    /**
     *
     */
    nlohmann::json convert_service_network_to_json_format(const cServiceNetwork& network);

    /**
     *
     */
    nlohmann::json convert_shipments_to_json_format(const cCommodityShipments& shipments);

    /*!
     * \brief Converts the resulting Routes data to an ONF based JSON document.
     *
     * @param a reference to routes.
     *
     * @return a JSON document representing the resulting commodity routes
     */
    nlohmann::json convert_routes_to_json_format(const cRoutes& routes, std::ostream* debug);
}

