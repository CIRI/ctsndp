//
//  cplex_utils_priv.hpp
//  ctsndp
//
//  Created by Brett Feddersen on 03/24/20.
//

#pragma once
#ifndef cplex_utils_priv_hpp
#define cplex_utils_priv_hpp

#include <ilcplex/ilocplex.h>

namespace ctsndp
{
    /*!
     * \brief Converts the value stored in a CPlex variable into the type
     */
    bool   as_bool(IloCplex& solver, const IloNumVar& var);
    int    as_int(IloCplex& solver, const IloNumVar& var);
    double as_double(IloCplex& solver, const IloNumVar& var);
}


inline bool ctsndp::as_bool(IloCplex& solver, const IloNumVar& var)
{
	auto b = solver.getValue(var);
//    return static_cast<bool>(var);
	return false;
}

inline int ctsndp::as_int(IloCplex& solver, const IloNumVar& var)
{
	auto i = solver.getValue(var);
    return static_cast<int>(i);
}

inline double ctsndp::as_double(IloCplex& solver, const IloNumVar& var)
{
	auto d = solver.getValue(var);
	return static_cast<double>(d);
}

#endif /* cplex_utils_priv_hpp */
