
#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"

#include <algorithm>
#include <exception>
#include <cassert>

/**
 * This algorithm is similar to algorithm 4 listed on page 1312
 * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_new < t_k_1
 * Modifies: The partially time expanded network 
 **/
void ctsndp::refine_delays(cPartiallyTimeExpandedNetwork& network,
	const cPartiallyTimeExpandedNetworkDelayNode& node,
	double t_k, double t_new, double t_k_1,
	std::ostream* debug)
{
	assert(t_new < t_k_1);

	if (debug)
	{
		*debug << "  Refine: " << node << " t_k=" << t_k;
		*debug << ", t_new=" << t_new << ", t_k_1=" << t_k_1 << "\n";
	}

    std::vector<arc_ptr_t>	delayArcsToDelete;
	const auto& commodities = node.getCommodities();

    // Add delay node (i, t_new) to 𝒩t
    auto* node_t_new = network.addDelayNode(node.getServiceLocation(), t_new, commodities);

	if (debug)
	{
		*debug << "    Adding delay node: " << *node_t_new << "\n";
	}

    const auto node_t_k_1 = network.findNode(node.getServiceLocation(), t_k_1);
	assert(node_t_k_1);

    // The new node should be between two nodes, we will need to remove the 
	// delay arcs between (i, t_k_1) and (i, t_k). Then, we will reconnect
	// by adding delay arcs ((i, t_k_1), (i, t_new)) and ((i, t_new), (i, t_k)) to 𝒜t
	if ((t_k < t_new) && (t_new < t_k_1))
	{
		// Delete delay arcs ((i, t_k_1), (i, t_k)) from 𝒜t
		const auto node_t_k = network.findNode(node.getServiceLocation(), t_k);

		auto& outflows = node_t_k_1->getOutflows();
		for (auto& outflow : outflows)
		{
			if (outflow->getDestination() == node_t_k)
			{
				if (outflow->isDelayArc())
					delayArcsToDelete.push_back(outflow);
			}
		}

		// Add delay arcs ((i, t_k_1), (i, t_new)) and ((i, t_new), (i, t_k)) to 𝒜t
		auto arc = network.addDelayArc(node_t_k_1, node_t_new);
		if (debug)
		{
			*debug << "    Adding delay arc: " << *arc << "\n";
		}

		arc = network.addDelayArc(node_t_new, node_t_k);
		if (debug)
		{
			*debug << "    Adding delay arc: " << *arc << "\n";
		}
	}

	// For all ((j, t), (i, t_k_1)) in 𝒜t
	// Copy all of the inflows from the node (i, t_k_1) to the new
	// node (i, t_new)
	for (auto inflow : node_t_k_1->getInflows())
	{
		if (inflow->isDelayArc() || inflow->isStorageArc())
			continue;
		network.addArc(inflow->getSource(), node_t_new, inflow);
	}
        
    for (cPartiallyTimeExpandedNetworkArc*& arc : delayArcsToDelete)
    {
		if (debug)
		{
			*debug << "    Deleting delay arc: " << *arc << "\n";
		}
		network.deleteArc(arc);
    }
	delayArcsToDelete.clear();
    
    if (debug)
        *debug << "  complete!\n" << std::endl;
}
