
#pragma once
#ifndef ctsndp_exceptions_hpp
#define ctsndp_exceptions_hpp

#include <stdexcept>
#include <string>

namespace ctsndp
{
	class cOptimizationException : public std::runtime_error
	{
	public:
		cOptimizationException(const std::string& what_args);
	};
	
	class cInfeasibleSolution : public std::runtime_error
    {
    public:
        cInfeasibleSolution(const std::string& modelName, const std::string& what_args);
        virtual const char* model() const noexcept;
    private:
        std::string mModelName;
    };

    class cTimeLimit : public std::runtime_error
    {
    public:
        cTimeLimit(const std::string& modelName, const std::string& what_args);
        virtual const char* model() const noexcept;
    private:
        std::string mModelName;
    };
}

#endif /* ctsndp_exceptions_h */
