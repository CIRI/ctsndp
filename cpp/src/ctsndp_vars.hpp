
#pragma once

#include <string>

namespace ctsndp
{
    // Special string constants used for implicit nodes

    static const std::string HOLD = "::HOLD";
    static const std::string IN = "::IN";
    static const std::string OUT = "::OUT";

    // Special string constants used for implicit arcs

    static const std::string HOLDING = "::HOLDING";
    static const std::string PROCESSING = "::PROCESSING";

	double constexpr MINIMUM_COST = 0.0001;
	double constexpr MINIMUM_VARIABLE_COST = 0.0001;
}