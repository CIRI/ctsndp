
#include "Properties.hpp"

#include <vector>
#include <iostream>


namespace
{
    // Compute Levenshtein Distance
    // Martin Ettl, 2012-10-05
 
    size_t levenshtein_distance(const std::string &s1, const std::string &s2)
    {
        const size_t m(s1.size());
        const size_t n(s2.size());
 
        if( m==0 ) return n;
        if( n==0 ) return m;
 
        std::vector<size_t> costs(n + 1);
 
        for( size_t k=0; k<=n; k++ ) 
            costs[k] = k;
 
        size_t i = 0;
        for ( std::string::const_iterator it1 = s1.begin(); it1 != s1.end(); ++it1, ++i )
        {
            costs[0] = i+1;
            size_t corner = i;
 
            size_t j = 0;
            for ( std::string::const_iterator it2 = s2.begin(); it2 != s2.end(); ++it2, ++j )
            {
                size_t upper = costs[j+1];
                if( *it1 == *it2 )
                {
		            costs[j+1] = corner;
	            }
                else
	            {
		            size_t t(upper < corner ? upper : corner);
                    costs[j+1] = (costs[j]<t?costs[j]:t)+1;
	            }
 
                corner = upper;
            }
        }
 
        size_t result = costs[n];
 
        return result;
    }
}
 

std::size_t cProperties::numProperties() const
{
    return mProperties.size();
}

bool cProperties::hasProperty(const std::string& property_name) const
{
    return (mProperties.find(property_name) != mProperties.end());
}

const std::any& cProperties::getProperty(const std::string& property_name) const
{
    static std::any not_found;
    auto it = mProperties.find(property_name);
    if (it == mProperties.end())
    {
        if (mProperties.empty())
        {
            std::cout << "No properties!" << std::endl;
        }
        else
        {
            std::cout << "The property \"" << property_name << "\" was not found." << std::endl;
            auto it = mProperties.begin();
            std::string closest_match(it->first);
            size_t closest_distance = levenshtein_distance(it->first, property_name);

            for(++it; it != mProperties.end(); ++it)
            {
                size_t distance = levenshtein_distance(it->first, property_name);
                if (distance < closest_distance)
                {
                    closest_distance = distance;
                    closest_match = it->first;
                }
            }
            std::cout << "Did you mean: " << closest_match << std::endl;
        }
        return not_found;
    }
    return it->second;
}

bool cProperties::addProperty(const std::string& property_name, 
                     const std::any& property_value)
{
    if (!property_value.has_value())
        return false;

    if (mProperties.find(property_name) == mProperties.end())
    {
        mProperties.insert(std::make_pair(property_name, property_value));
        return true;
    }
    
    return false;
}
