
#pragma once

#include <string>
#include <unordered_map>
#include <any>

class cProperties
{
public:
    cProperties() = default;

    std::size_t numProperties() const;

    bool hasProperty(const std::string& property_name) const;
    const std::any& getProperty(const std::string& property_name) const;

    template <typename T>
    T getProperty(const std::string& property_name) const;

    bool addProperty(const std::string& property_name, 
                     const std::any& property_value);
    
    std::unordered_map<std::string, std::any>::const_iterator cbegin() const;
    std::unordered_map<std::string, std::any>::const_iterator cend() const;

private:
    template <typename T>
    T to_value(const std::any& property) const;

    std::unordered_map<std::string, std::any> mProperties;
};


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

template <typename T>
inline T cProperties::getProperty(const std::string& property_name) const
{
    const std::any& property = getProperty(property_name);
    if ( std::is_arithmetic<T>::value )
        return to_value<T>(property);
    return std::any_cast<T>(property);
}

template <typename T>
inline T cProperties::to_value(const std::any& property) const
{
    auto& type = property.type();
    if (type == typeid(T))
        return std::any_cast<T>(property);
    if (type == typeid(bool))
        return static_cast<T>(std::any_cast<bool>(property));
    if (type == typeid(char))
        return static_cast<T>(std::any_cast<char>(property));
    if (type == typeid(std::int8_t))
        return static_cast<T>(std::any_cast<std::int8_t>(property));
    if (type == typeid(std::uint8_t))
        return static_cast<T>(std::any_cast<std::uint8_t>(property));
    if (type == typeid(std::int16_t))
        return static_cast<T>(std::any_cast<std::int16_t>(property));
    if (type == typeid(std::uint16_t))
        return static_cast<T>(std::any_cast<std::uint16_t>(property));
    if (type == typeid(std::int32_t))
        return static_cast<T>(std::any_cast<std::int32_t>(property));
    if (type == typeid(std::uint32_t))
        return static_cast<T>(std::any_cast<std::uint32_t>(property));
    if (type == typeid(std::int64_t))
        return static_cast<T>(std::any_cast<std::int64_t>(property));
    if (type == typeid(std::uint64_t))
        return static_cast<T>(std::any_cast<std::uint64_t>(property));
    if (type == typeid(float))
        return static_cast<T>(std::any_cast<float>(property));
    if (type == typeid(double))
        return static_cast<T>(std::any_cast<double>(property));

    return std::any_cast<T>(property);
}

inline std::unordered_map<std::string, std::any>::const_iterator cProperties::cbegin() const
{
    return mProperties.cbegin();
}

inline std::unordered_map<std::string, std::any>::const_iterator cProperties::cend() const
{
    return mProperties.cend();
}
