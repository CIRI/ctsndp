//
//  ctsndp_utils_priv.hpp
//  ctsndp
//
//  Created by Brett Feddersen on 8/7/19.
//

#pragma once
#ifndef ctsndp_utils_priv_hpp
#define ctsndp_utils_priv_hpp

#include "ctsndp_defs.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"

#include <string>
#include <iostream>
#include <chrono>

namespace ctsndp
{
	double constexpr COST_THREASHOLD = 0.009;

	arc_id_t to_index(cPartiallyTimeExpandedNetworkArc* const& arc);
    arc_id_t to_index(cPartiallyTimeExpandedNetworkHoldoverArc* const& arc, unsigned int offset);
    commodity_id_t to_index(const cCommodityBundle* const& bundle);

    /*!
     * \brief Inserts model type into the log filename
     */
    std::string log_filename(std::string filename, const char * type);

    void print_end_elapse_times(std::ostream& out,
                                const std::chrono::system_clock::time_point& start);

	double lower_limit(double value, double threashold, double lb);
	double lower_limit(double value, double lb);
}


 
inline arc_id_t ctsndp::to_index(cPartiallyTimeExpandedNetworkArc* const& arc)
{
    if (!arc)
    {
        return -1;
    }
     
    return arc->getId();
}

inline arc_id_t ctsndp::to_index(cPartiallyTimeExpandedNetworkHoldoverArc* const& arc, unsigned int offset)
{
    return arc->getId() + offset;
}


inline commodity_id_t ctsndp::to_index(const cCommodityBundle* const& bundle)
{
    return bundle->getId();
}
 
inline std::string ctsndp::log_filename(std::string filename, const char * type)
{
    if (filename.empty())
        return filename;
    auto n = filename.rfind('.');
    if (n == std::string::npos)
    {
        filename.append(type);
    }
    else
    {
        filename.insert(n, type);
    }
    return filename;
}

inline void ctsndp::print_end_elapse_times(std::ostream& out,
                                           const std::chrono::system_clock::time_point& start)
{
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

    auto end_time = std::chrono::system_clock::to_time_t(end);
    out << ", ended at " << ctsndp::to_string(end_time);
    out << ", elapsed time: " << elapsed_seconds.count() << "s";
}

inline double ctsndp::lower_limit(double value, double threashold, double lb)
{
	return value < threashold ? lb : value;
}

inline double ctsndp::lower_limit(double value, double lb)
{
	return lower_limit(value, COST_THREASHOLD, lb);
}

#endif /* ctsndp_utils_priv_hpp */
