//
//  IdentifyArcsToLengthenModel.cpp
//
//  Created by Brett Feddersen on 11/21/19.
//

#include "IdentifyArcsToLengthenModel.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_utils_priv.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>
#include <cassert>


using namespace ctsndp;

cIdentifyArcsToLengthenModel::cIdentifyArcsToLengthenModel(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_node_t& p_ki,
                                         const ctsndp::commodity_path_arc_t& p_ka,
										 const ctsndp::consolidation_groups_t& j_ak,
										 bool verboseMode, bool printTimingInfo)
:
	cOptimizationProblem(pass_number),
	mPartiallyTimeExpandedNetwork(expandedNetwork),
    mP_ki(p_ki),
    mP_ka(p_ka),
    mJ_ak(j_ak),
    mOutputIdentifyArcsTimeInfo(false),
    mOutputCommodityScheduleInfo(false),
    mVerboseMode(verboseMode),
    mPrintTimingInfo(printTimingInfo)
{
	mLogIdentifyArcsTimeInfo = false;
	mLogCommodityScheduleInfo = false;
		
	auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    
    auto start = std::chrono::system_clock::now();

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\n\tCreating optimization variables";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }
*/

	auto numArcs = arcs.size();
	tau_ij.resize(numArcs);
	for (auto& arc : arcs)
    {
        auto ij = to_index(arc);
        tau_ij[ij] = arc->getActualTravelTime();
    }
    
    for(auto k_it = mP_ka.begin(); k_it != mP_ka.end(); ++k_it)
    {
        auto& k = k_it->first;
        auto& path = k_it->second;
        auto last = path.end();
        for (auto p_it = path.begin(); p_it != last; ++p_it)
        {
            auto& arc = *p_it;
			assert(!arc->isHoldoverArc());

			tau_bar_kij[{k, arc}] = arc->getDeltaTime();
        }
    }

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
*/
}

cIdentifyArcsToLengthenModel::~cIdentifyArcsToLengthenModel()
{
}

void cIdentifyArcsToLengthenModel::outputIdentifyArcsTimeInfo(bool output)
{
    mOutputIdentifyArcsTimeInfo = output;
}

void cIdentifyArcsToLengthenModel::outputCommodityScheduleInfo(bool output)
{
    mOutputCommodityScheduleInfo = output;
}

std::set<ctsndp::arc_ptr_t> cIdentifyArcsToLengthenModel::returnArcsToLengthen()
{
    return mArcsToLengthen;
}

ctsndp::dispatch_time_t cIdentifyArcsToLengthenModel::returnDispatchTimes()
{
    return mDispatchTime_ki;
}

ctsndp::travel_time_t  cIdentifyArcsToLengthenModel::returnTravelTimes()
{
    return mTravelTime_kij;
}

void cIdentifyArcsToLengthenModel::BuildModel()
{
	if (mpProgress)
	{
		*mpProgress << "  ConstructOptimizatonVariables..." << std::flush;
	}

	ConstructOptimizatonVariables();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tAdding constraints";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "...\n" << std::flush;
    }

    // Add constraints

	if (mpProgress)
	{
		*mpProgress << "  constraint6_CountArcsWithShortenTravelTimes..." << std::flush;
	}
	
	constraint6_CountArcsWithShortenTravelTimes();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint7_EnsureAllowableDispatchTimes..." << std::flush;
	}

	constraint7_EnsureAllowableDispatchTimes();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint8_EnsureDispatchIsAfterAvailable..." << std::flush;
	}

	constraint8_EnsureDispatchIsAfterAvailable();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint9_EnsureArrivalBeforeDue..." << std::flush;
	}

	constraint9_EnsureArrivalBeforeDue();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint10_EnsureConsolidations..." << std::flush;
	}

	constraint10_EnsureConsolidations();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tdone";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}

	if (mpProgress)
	{
		*mpProgress << "  BuildOptimizatonProblem..." << std::flush;
	}

	BuildOptimizatonProblem();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}
}

