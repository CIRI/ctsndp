
#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"

#include <algorithm>
#include <exception>
#include <cassert>

/**
 * This is algorithm 4 listed on page 1312
 * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_k < t_new
 * Modifies: The partially time expanded network 
 **/
void ctsndp::refine(cPartiallyTimeExpandedNetwork& network,
	const cPartiallyTimeExpandedNetworkNode& node,
	double t_k, double t_new, double t_k_1,
	std::ostream* debug)
{
	if (debug)
	{
		*debug << "  Refine: " << node << " t_k=" << t_k;
		*debug << ", t_new=" << t_new << ", t_k_1=" << t_k_1 << "\n";
	}

    std::vector<cPartiallyTimeExpandedNetworkHoldoverArc*>	holdoverArcsToDelete;
	std::vector<cPartiallyTimeExpandedNetworkArc*>		arcsToDelete;
    
    // Line 1: Add node (i, t_new) to 𝒩t
	const cPartiallyTimeExpandedNetworkNode* pNode = &node;
    auto* node_t_new = network.addNode(pNode, t_new);
	if (debug)
	{
		*debug << "    Adding node: " << *node_t_new << "\n";
	}

    const auto node_t_k = network.findNode(node.getServiceLocation(), t_k);

    // If the new node is between two nodes, we will need to remove the 
	// holdover arc between (i, t_k) and (i, t_k_1). Then, we will reconnect
	// the holdover arcs by adding ((i, t_k), (i, t_new)) and 
	// ((i, t_new), (i, t_k_1)) to ℋt
    if ((t_k < t_new) && (t_new < t_k_1))
    {
        // Line 2: Delete arc ((i, t_k), (i, t_k_1)) from ℋt
        const auto node_t_k_1 = network.findNode(node.getServiceLocation(), t_k_1);
        
        if (node_t_k_1)
        {
            auto old_arc = network.findHoldoverArc(node_t_k, node_t_k_1);
			if (old_arc)
			{
				network.removeHoldoverArc(old_arc);
				holdoverArcsToDelete.push_back(old_arc);

				// Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
				// The first add is outside of the if statement
				auto arc = network.addHoldoverArc(node_t_new, node_t_k_1);
				if (debug)
				{
					*debug << "    Adding holdover arc: " << *arc << "\n";
				}
			}
			else
			{
				auto old_arc = network.findHoldArc(node_t_k, node_t_k_1);
				if (old_arc)
				{
					network.removeArc(old_arc);
					arcsToDelete.push_back(old_arc);

					// Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
					// The first add is outside of the if statement
					auto arc = network.addHoldArc(node_t_new, node_t_k_1,
						node_t_k_1->getTime() - node_t_new->getTime());
					if (debug)
					{
						*debug << "    Adding hold arc: " << *arc << "\n";
					}
				}
				else
				{
					if (debug)
					{
						auto& arcs = node_t_k->getOutflows();
						for (auto arc : arcs)
							*debug << *arc << "\n";
					}
				}
			}
		}
    }
    // Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
    auto arc = network.addHoldoverArc(node_t_k, node_t_new);
	if (debug)
	{
		*debug << "    Adding holdover arc: " << *arc << "\n";
	}


    // Line 3: for all ((i, t_k), (j, t)) in 𝒜t
	// Copy all of the outflows from the node (i, t_k) to the new
	// node (i, t_new)
    auto outFlows = network.getAllOutflowArcsFromNode(node_t_k, false);
    for (auto& outflow : outFlows)
    {
		if (outflow->isStorageArc())
			continue;

        // Line 4: Add arc ((i, t_new), (j, t)) to 𝒜t
        auto arc = network.addArc(node_t_new, outflow->getDestination(), outflow);
		if (debug)
		{
			*debug << "    Adding arc: " << *arc << "\n";
		}

		// We want the arcs connected to delay nodes to always be
		// at the maximum time!
		if (outflow->connectedToDelayNode())
		{
			arcsToDelete.push_back(outflow);
		}

	}

	for (cPartiallyTimeExpandedNetworkArc*& arc : arcsToDelete)
	{
		if (debug)
		{
			if (arc->isStorageArc())
				*debug << "    Deleting hold arc: " << *arc << "\n";
			else if (arc->connectedToDelayNode())
				*debug << "    Deleting arc to delay node: " << *arc << "\n";
		}
		network.deleteArc(arc);
	}
	arcsToDelete.clear();

    for (cPartiallyTimeExpandedNetworkHoldoverArc*& arc : holdoverArcsToDelete)
    {
		if (debug)
		{
			*debug << "    Deleting holdover arc: " << *arc << "\n";
		}
		network.deleteHoldoverArc(arc);
    }
    holdoverArcsToDelete.clear();
    
    if (debug)
        *debug << "  complete!\n" << std::endl;
}
