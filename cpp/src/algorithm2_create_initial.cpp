
#include "ctsndp_algorithms.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"

#include <algorithm>
#include <cassert>

const double MAX_ALLOWED_DELAY = 1000000;

namespace
{
	// We are defining some helper types and function to
	// make sure we only add one node at (i,t) and one 
	// holdover arc ((i,t), (i,t'))  
	typedef std::map<double, node_ptr_t> Nt_t;
	typedef std::map<cServiceLocation*, Nt_t> Lt_t;
}


/**
 * This is algorithm 2 listed on page 1311
 * Requires: Directed network 𝒟 = (𝒩, 𝒜), commodity set 𝒦
 * Returns: A partially time expanded network 𝒟t = (𝒩t, 𝒜t U ℋt), commodity set 𝒦
 **/
std::unique_ptr<cPartiallyTimeExpandedNetwork> ctsndp::create_initial(
                                    const cServiceNetwork& service_network,
                                    const cCommodityShipments& shipments,
                                    std::ostream* debug)
{
    if (debug)
        *debug << "Create partially time expanded network:\n";

    std::unique_ptr<cPartiallyTimeExpandedNetwork> pNetwork(new cPartiallyTimeExpandedNetwork);

    if (debug)
        *debug << "  Scanning all commodities...\n";

	Lt_t locations;
    // Line 1: for all commodities in 𝒦
    int numCommodities = 0;
    for (auto& shipment : shipments.getShipments())
    {
        auto source_location = shipment->getSourceLocation();
        assert(source_location);

        auto destination_location = shipment->getDestinationLocation();
        assert(destination_location);

        // Line 2: Add node to 𝒩t
        auto source_node = pNetwork->findNode(source_location, shipment->getEarliestAvailableTime());
        if (!source_node)
        {
			if (debug)
			{
				*debug << "    Adding node(" << source_location->getName()
					<< ", " << shipment->getEarliestAvailableTime() << ")\n";
			}
            source_node = pNetwork->addNode(source_location, shipment->getEarliestAvailableTime());
        }

        // Line 3:
        auto destination_node = pNetwork->findNode(destination_location, shipment->getLatestDeliveryTime());
        if (!destination_node)
        {
			if (debug)
			{
				*debug << "    Adding node(" << destination_location->getName()
					<< ", " << shipment->getLatestDeliveryTime() << ")\n";
			}
			destination_node = pNetwork->addNode(destination_location, shipment->getLatestDeliveryTime());
        }
        
        pNetwork->addShipment(shipment, source_node, destination_node);

        for (auto& commodity : shipment->getCommodityBundles())
        {
            pNetwork->addCommodity(commodity);

            ++numCommodities;
            
			commodity->setSourceNode(source_node);
			commodity->setDestinationNode(destination_node);

            source_node->addCommodity(commodity);
            destination_node->addCommodity(commodity);
        }
    }

    if (debug)
        *debug << "  done.\n  Creating t=0 node for each service location...\n";

    // line 5
    for (auto& location : service_network.serviceLocations())
    {
        // Line 6
		auto node = pNetwork->findNode(location, 0);
		if (!node)
		{
			if (debug)
			{
				*debug << "    Adding node(" << location->getName() << ", 0)\n";
			}
			pNetwork->addNode(location, 0);
		}
    }

    if (debug)
        *debug << "  done.\n  Creating arcs in partially time expanded network...\n";

    // Line 8
    for (auto& source_node : pNetwork->nodes())
    {
        double t = source_node->getTime();
        // Line 9
        auto edges = service_network.findLinksWithSource(*(source_node->getServiceLocation()));
        for (auto& edge : edges)
        {
            double t_prime = -1;
            cPartiallyTimeExpandedNetworkNode* dst_node = nullptr;
            double t_limit = t + edge->getTravelTime();
            // Line 10 - 
            auto nodes = pNetwork->findNodes(edge->getDestination());
            for (auto& node : nodes)
            {
				if ((node->getTime() <= t_limit)
					&& (node->getTime() > t_prime))
				{
					t_prime = node->getTime();
					dst_node = node;
				}
            }

            assert(dst_node);

			if (debug)
			{
				*debug << "    " << t_prime << " <= " << t << " + "
					<< edge->getTravelTime() << " :: ";
				*debug << "Adding arc " << *source_node << " -> "
					<< *dst_node << "\n";
			}
			
			pNetwork->addArc(source_node, dst_node, edge);
        }

        // Line 12: Find smallest t_prime such that (i, t_time) is in 𝒩t and t_prime > t
        auto service_nodes = pNetwork->findNodes(source_node->getServiceLocation(), false);
		double t_prime = std::numeric_limits<double>::max();
		cPartiallyTimeExpandedNetworkNode* dst_node = nullptr;
		for (auto node : service_nodes)
		{
			auto time = node->getTime();
			if ((time > t) && (time < t_prime))
			{
				t_prime = time;
				dst_node = node;
			}
		}
		if (dst_node)
		{
			auto holdover_arc = source_node->getStorageOutflow();
			if (holdover_arc)
			{
				*debug << "    Error: " << *source_node << " already has a outflow holdover arc "
					<< *holdover_arc << "!\n";
			}
			holdover_arc = dst_node->getStorageInflow();
			if (holdover_arc)
			{
				*debug << "    Error: " << *dst_node << " already has a inflow holdover arc "
					<< *holdover_arc << "!\n";
			}

			if (debug)
			{
				*debug << "    Adding holdover_arc " << *source_node << " -> "
					<< *dst_node << "\n";
			}
			pNetwork->addHoldoverArc(source_node, dst_node);
		}
    }

    if (debug)
        *debug << "  done.\n  Creating delay nodes/arcs in partially time expanded network...\n";


    // We are extending the algorithm by adding "delay" nodes into
    // the graph.  The delay nodes hold the idea that it might be
    // cheaper to have some commodity arrive late than adding capacity
    // to arcs.
	double threshold = shipments.getShortestPathRatioThreshold();

    for (auto& shipment : shipments.getShipments())
    {
		double dt = shipment->getLatestDeliveryTime() - shipment->getEarliestAvailableTime();
		double st = shipment->getShortestPathTime();

		if ((st >= 0.0) && ((st / dt) < threshold))
			continue;

        for (auto& commodity : shipment->getCommodityBundles())
        {
            if (commodity->hasDelayCost() || commodity->hasDelayPenalty())
            {
                auto dest_loc = service_network.findLocation(commodity->getDestinationName());
                assert(dest_loc);

                double ldt = commodity->getLatestDeliveryTime();
                auto terminal_node = pNetwork->findNode(dest_loc,
                                                ldt, commodity);

                // We are going to set the delay node as far into
                // the future as possible with the restriction we
                // don't go pass the max delay time if that exist
                double t = ldt;
                if (commodity->hasMaxDelayTime())
                {
                    t += commodity->getMaxDelayTime();
                }
                else
                {
                    t += MAX_ALLOWED_DELAY;
                }
                
				if (debug)
				{
					*debug << "    Adding delay node (" << *dest_loc << ", "
						<< t << ", " << commodity->getName() << ")\n";
				}

				auto delay_node = pNetwork->findDelayNode(dest_loc, t);

				if (delay_node)
				{
					delay_node->addCommodity(commodity);
				}
				else
				{
					delay_node = pNetwork->addDelayNode(dest_loc, t, commodity);

					// Create copies the arcs that goto (o_k, l) to the delay node
					auto links = service_network.findLinksWithDestination(*dest_loc);

					for (auto link : links)
					{
						auto nodes = pNetwork->findNodes(link->getSource(), false);
						auto src = *(nodes.begin());

						for (auto node : nodes)
						{
							if (node->getTime() > src->getTime())
							{
								src = node;
							}
						}

						if (debug)
						{
							*debug << "    Adding arc " << *src << " -> "
								<< *delay_node << "\n";
						}
						pNetwork->addArc(src, delay_node, link);
					}
				}

				// We only need one delay arc per delay node because we will use the
				// delay node to hold the list of commodities that can use the arc
				auto delay_arc = pNetwork->findDelayArc(delay_node, terminal_node);

				if (!delay_arc)
				{
					if (debug)
					{
						*debug << "    Adding delay arc " << *delay_node << " -> "
							<< *terminal_node << "\n";
					}

					pNetwork->addDelayArc(delay_node, terminal_node);
				}
            }
        }
    }

    if (debug)
        *debug << "  done.\ncomplete!" << std::endl;

   return pNetwork;
}
