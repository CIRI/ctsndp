//
//  ConstructFeasibleSolutionModel_Gurobi.hpp
//
//  Created by Brett Feddersen on 03/25/20.
//

#pragma once
#ifndef ConstructFeasibleSolutionModel_Gurobi_hpp
#define ConstructFeasibleSolutionModel_Gurobi_hpp

#include "ConstructFeasibleSolutionModel.hpp"
#include "GurobiModel.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"

#include <map>
#include <string>
#include <iosfwd>
#include <memory>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 4.4 on page 1313 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cConstructFeasibleSolutionModel_Gurobi final : public cConstructFeasibleSolutionModel,
													 private cGurobiModel
{
public:
	static std::unique_ptr<cConstructFeasibleSolutionModel_Gurobi>
	create(int pass_number,
            const cPartiallyTimeExpandedNetwork& expandedNetwork,
            const cServiceNetwork& serviceNetwork,
            const ctsndp::commodity_path_node_t& p_ki,
            const ctsndp::commodity_path_arc_t& p_ka,
			const ctsndp::consolidation_groups_t& j_ak,
			bool verboseMode, bool printTimingInfo);
    
    ~cConstructFeasibleSolutionModel_Gurobi();

	void setNumericFocus(int numericFocus) override;
	void setTimeLimit_sec(double timeLimit_sec) override;
	void logToConsole(bool log) override;
	void outputDebugMsgs(bool msgs) override;
	void logFilename(const std::string& filename) override;
	void readTuningParameters(const std::string& filename) override;


    /**
     * \brief Calls optimize to try and solve the "construct feasible solution" optimization problem.
     */
    bool Optimize() override;

	bool RelaxConstraints() override;
	void FindConstraintViolations(std::ostream& out) override;
	std::string ComputeIIS() override;

    /**
     * \brief Returns all of the dispatch times for the commodities moving through the service network
     */
    ctsndp::dispatch_time_t     returnDispatchTimes();

    /**
     * \brief Returns the mismatch of dispatch times between two commodities from the same location
     */
    ctsndp::dispatch_mismatch_times_t returnMismatchTimes();

private:
	cConstructFeasibleSolutionModel_Gurobi(int pass_number,
		const cPartiallyTimeExpandedNetwork& expandedNetwork,
		const cServiceNetwork& serviceNetwork,
		const ctsndp::commodity_path_node_t& p_ki,
		const ctsndp::commodity_path_arc_t& p_ka,
		const ctsndp::consolidation_groups_t& j_ak,
		bool verboseMode, bool printTimingInfo);

	void ConstructOptimizatonVariables() override;
	void BuildOptimizatonProblem() override;

	void SaveSolution();
	void FindArcsToLengthen();
	void SaveDispatchAndMismatchTimes();

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
    void constraint13_EnsureAllowableDispatchTimes() override;
    void constraint14_EnsureDispatchIsAfterAvailable() override;
    void constraint15_EnsureArrivalBeforeDue() override;
    void constraint16and17_DispatchDelta_k1_k2() override;
    
    //
    // Debug Methods
    //
    void DumpFeasibleSolutionInfo(std::ostream& out);
	void DumpGamma_ki(std::ostream& out) const;
	void DumpDelta_kk_ij(std::ostream& out) const;

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    std::map<ctsndp::sCommodityNode, GRBVar, ctsndp::compareCommodityNodeByPtrId>       gamma_ki;
    std::map<ctsndp::sDualCommodityArc, GRBVar, ctsndp::compareDualCommodityArcByPtrId> delta_kk_ij;
};

#endif /* ConstructFeasibleSolutionModel_Gurobi_hpp */
