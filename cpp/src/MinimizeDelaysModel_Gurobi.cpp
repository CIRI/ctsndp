//
//  MinimizeDelaysModel.cpp
//
//  Created by Brett Feddersen on 11/22/19.
//

#include "MinimizeDelaysModel_Gurobi.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include "gurobi_utils_priv.hpp"
#include <iostream>
#include <chrono>
#include <cassert>

namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;
}

using namespace ctsndp;


std::unique_ptr<cMinimizeDelaysModel_Gurobi> cMinimizeDelaysModel_Gurobi::create(int pass_number,
	const cPartiallyTimeExpandedNetwork& expandedNetwork,
	const cServiceNetwork& serviceNetwork,
	const ctsndp::commodity_path_arc_t& p_ka,
	const ctsndp::dispatch_time_t& dispatchTimes_ki,
	bool verboseMode, bool printTimingInfo)
{
	try
	{
		std::unique_ptr<cMinimizeDelaysModel_Gurobi> 
			model(new cMinimizeDelaysModel_Gurobi(pass_number, expandedNetwork, serviceNetwork,
				p_ka, dispatchTimes_ki, verboseMode, printTimingInfo));

		return model;
	}
	catch (GRBException& e)
	{
		throw cOptimizationException(e.getMessage());
	}
}

cMinimizeDelaysModel_Gurobi::cMinimizeDelaysModel_Gurobi(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_arc_t& p_ka,
                                         const ctsndp::dispatch_time_t& dispatchTimes_ki,
                                         bool verboseMode, bool printTimingInfo)
:
    cGurobiModel("MinimizeDelays"),
	cMinimizeDelaysModel(pass_number, expandedNetwork, serviceNetwork, p_ka,
						 dispatchTimes_ki, verboseMode, printTimingInfo)
{
}

cMinimizeDelaysModel_Gurobi::~cMinimizeDelaysModel_Gurobi()
{
}

void cMinimizeDelaysModel_Gurobi::setNumericFocus(int numericFocus)
{
	cGurobiModel::setNumericFocus(numericFocus);

}

void cMinimizeDelaysModel_Gurobi::setTimeLimit_sec(double timeLimit_sec)
{
	cGurobiModel::setTimeLimit_sec(timeLimit_sec);
}

void cMinimizeDelaysModel_Gurobi::logToConsole(bool log)
{
	cGurobiModel::logToConsole(log);
}

void cMinimizeDelaysModel_Gurobi::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cMinimizeDelaysModel_Gurobi::logFilename(const std::string& filename)
{
    cGurobiModel::logFilename(filename, ".min_delay");
}

void cMinimizeDelaysModel_Gurobi::readTuningParameters(const std::string& filename)
{
	cGurobiModel::readTuningParameters(filename);
}

bool cMinimizeDelaysModel_Gurobi::Optimize()
{
    if (a_kij.empty())
        return true;
    
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Minimize Delay Problem\" ";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	int status = 0;

	try
	{
		if (mpProgress)
		{
			*mpProgress << "  Optimizing \"Minimize Delay Problem\"..." << std::flush;
		}

		mModel.optimize();
		status = mModel.get(GRB_IntAttr_Status);

		if (mpProgress)
		{
			*mpProgress << "done" << std::endl;
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("Minimize Delay Problem Optimize: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
        case GRB_OPTIMAL:

			SaveSolution();
            return true;

        case GRB_INF_OR_UNBD:
        case GRB_INFEASIBLE:
        {
            std::string msg = "Minimize Delays model is infeasible!";
            throw ctsndp::cInfeasibleSolution("MinimizeDelays", msg);
            break;
        }
                   
        case GRB_TIME_LIMIT:
        {
            std::string msg = "Exceeded time limit (";
            msg += std::to_string(mTimeLimit_sec);
            msg += "):\n";

			try
			{
				auto count = mModel.get(GRB_IntAttr_SolCount);
				if (count == 0)
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    No solution was found!! Terminating" << std::endl;
					}

					msg += "No solution was found!";
				}
				else
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    Feasible solution count = " << count << std::endl;
					}

					SaveSolution();
					return true;
				}
			}
			catch (const GRBException& e)
			{
				msg += e.getMessage();
			}
                       
            throw ctsndp::cTimeLimit("MinimizeDelays", msg);
            break;
        }
    }

    return false;
}

bool cMinimizeDelaysModel_Gurobi::RelaxConstraints()
{
	return cGurobiModel::RelaxConstraints();
}

std::string cMinimizeDelaysModel_Gurobi::ComputeIIS()
{
	return cGurobiModel::ComputeIIS();
}

void cMinimizeDelaysModel_Gurobi::FindConstraintViolations(std::ostream& out)
{
    bool printNone = true;
    out << "\nScanning for constraint 18 \"a_kij <= gamma_ki\" violations:\n";

    for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
    {
        auto& k = it->first.k;
        const auto& loc = it->first.ij->getDestination();
        double arrival_time = it->second;

        double delta = as_double(gamma_ki[{k, loc}]) - arrival_time;

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << as_double(gamma_ki[{k, loc}]) << " < " << arrival_time;
            out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*(it->first.ij));
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 19, arrival before max delay time violations:\n";

    // for all commodities
    for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
    {
        auto& k = it->first.k;

        if (!k->hasMaxDelayTime())
            continue;

        const auto& arc = it->first.ij;
        
        double arrival_time = as_double(gamma_ki[{k, arc->getDestination()}]) + tau_ij[to_index(arc)];
        double ldt = k->getLatestDeliveryTime() + k->getMaxDelayTime();
        double delta = ldt - arrival_time;

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << ldt << " < " << arrival_time;
            out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*arc);
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;
}

//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cMinimizeDelaysModel_Gurobi::SaveSolution()
{
	FindArcsToShorten();
}

void cMinimizeDelaysModel_Gurobi::FindArcsToShorten()
{
	mArcsToShorten.clear();

	if (a_kij.empty())
		return;

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Arcs to Shorten";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
	{
		auto& k = it->first.k;
		const auto& arc = it->first.ij;
		assert(arc->getDestinationNode()->isDelayNode());

		const auto& node = arc->getDestination();
		ctsndp::delay_arc_ptr_t delay_arc = nullptr;

		for (auto outflow : node->getOutflows())
		{
			if (!outflow->isDelayArc())
				continue;
			auto arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(outflow);
			if (arc->hasCommodity(k))
			{
				delay_arc = arc;
				break;
			}
		}

		double t_min = arc->getSourceTime() + arc->getActualTravelTime();
		double t_new = 0.0;
		try
		{
			t_new = std::max(as_double(gamma_ki[{k, node}]), t_min);
		}
		catch (const GRBException& e)
		{
			std::string msg = "FindArcsToShorten: ";
			msg += "[ " + k->getUniqueName() + ", " + node->getName() + "] ";
			throw cOptimizationException(msg + e.getMessage());
		}

		double t_old = mDispatchTimes_ki.at({ k, node });
		double tau_new = t_new - k->getLatestDeliveryTime();
		double tau_old = -delay_arc->getActualTravelTime();
		if ((tau_new < tau_old) && (t_new != node->getTime()))
		{
			if (!mPartiallyTimeExpandedNetwork.hasDelayNode(arc->getDestinationServiceLocation(), t_new))
			{
				mArcsToShorten.insert({ arc, t_new, k, delay_arc });
			}
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		if (mArcsToShorten.size() > 0)
			*mpDebugOutput << ", found " << mArcsToShorten.size();

		*mpDebugOutput << std::endl;
	}
}


//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cMinimizeDelaysModel_Gurobi::ConstructOptimizatonVariables()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variables";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		for (auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
		{
			auto& k = it->first;

			// Make sure this commodity has a delay cost.
			// If yes, then the path contain a "delay" node that can be shortened
			if (!k->hasDelayCost())
				continue;

			auto& path = it->second;
			if (path.empty())
				continue;

			// We can only minimize delays on paths that pass through a delay node
			if (!path.back()->connectedToDelayNode())
				continue;

			for (auto it = path.rbegin(); it != path.rend(); ++it)
			{
				auto arc = *it;
				if (arc->getSource()->isDelayNode())
					continue;

				const auto node = arc->getDestination();
				gamma_ki[{k, node}] = mModel.addVar(0, +GRB_INFINITY, 0, GRB_CONTINUOUS);
				break;
			}
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("ConstructOptimizatonVariables: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

void cMinimizeDelaysModel_Gurobi::BuildOptimizatonProblem()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		GRBLinExpr shortenDispatchVars;

		for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
		{
			auto& k = it->first.k;
			auto& ij = it->first.ij;
			const auto& loc = ij->getDestination();

			shortenDispatchVars = (1.0 + p_k[k->getId()]) * (gamma_ki[{k, loc}] - it->second);
		}

		mModel.setObjective(shortenDispatchVars, GRB_MINIMIZE);
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("BuildOptimizatonProblem: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}


//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================

//
// Perform constraint 8 for the algorithm listed on page 1312
//
void cMinimizeDelaysModel_Gurobi::constraint18_EnsureDispatchIsAfterAvailable()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint18: Ensure Dispatch Is After Available";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
		{
			auto& k = it->first.k;
			const auto& node = it->first.ij->getDestination();
			double t = it->second;

#if ADD_DEBUG_NAMES
			std::string constrName = "EnsureDispatchIsAfterAvailable(";
			constrName += k->getFullyQualifiedName() + ", " + loc->getName() + ")";
			mModel.addConstr(gamma_ki[{k, node}], GRB_GREATER_EQUAL, t, constrName.c_str());
#else
			mModel.addConstr(gamma_ki[{k, node}], GRB_GREATER_EQUAL, t);
#endif
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint18_EnsureDispatchIsAfterAvailable: " + e.getMessage());
	}
    
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

void cMinimizeDelaysModel_Gurobi::constraint19_EnsureArrivalBeforeMaxLateTime()
{
    return;
    
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint19: Ensure Arrival Before Max Late Time";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto it = a_kij.begin(); it != a_kij.end(); ++it)
		{
			auto& k = it->first.k;

			if (!k->hasMaxDelayTime())
				continue;

			const auto& arc = it->first.ij;
        
			auto lhs = GRBLinExpr(gamma_ki[{k, arc->getDestination()}] + tau_ij[to_index(arc)]);
			double ldt = k->getLatestDeliveryTime() + k->getMaxDelayTime();

#if ADD_DEBUG_NAMES
			std::string constrName = "EnsureArrivalBeforeMaxLateTime(";
			constrName += k->getFullyQualifiedName() + ", " + to_string(*arc) /*->getDestinationServiceName()*/ + ")";
			mModel.addConstr(lhs, GRB_LESS_EQUAL, ldt, constrName.c_str());
#else
	        mModel.addConstr(lhs, GRB_LESS_EQUAL, ldt);
#endif
	    }
    
		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint19_EnsureArrivalBeforeMaxLateTime: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

