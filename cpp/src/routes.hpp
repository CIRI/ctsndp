
#pragma once
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"

#include <memory>
#include <string>
#include <system_error>
#include <vector>
#include <map>
#include <chrono>


struct sLeg
{
    double fixedCost;
    double variableCost;
    double departureTime;
    int quantity;
    double utilization;
    const cServiceLink* pLink;
    
    sLeg();
};

struct sCommodityPath
{
    double fixedCost;
    double variableCost;
    double delayCost;
	double variableDelayCost;
	std::vector<sLeg> path;
    const cCommodityBundle* const pCommodity;
    
    sCommodityPath(const cCommodityBundle* const commodity);
};

struct sDispatch
{
    const cCommodityBundle* const pCommodity;
    double departureTime;
    
    sDispatch(const cCommodityBundle* const k, double time);
};

struct sMismatchedDispatch
{
    const cServiceLink* pLink;
    std::vector<sDispatch> dispatches;
    
    sMismatchedDispatch(const cServiceLink* link);
    
    void addDispatch(const cCommodityBundle* const k, double time);
};

class cRoutes
{
public:
    cRoutes();
    
    std::unique_ptr<cServiceNetwork> mpServiceNetwork;
    std::unique_ptr<cCommodityShipments> mpShipments;
	std::unique_ptr<cPartiallyTimeExpandedNetwork> mpPartiallyTimeExpandedNetwork;
	bool mSuccess;
    std::string mErrorMessage;
	std::chrono::duration<double> mSolutionTime_s;
    
    double mFixedCost;
    double mVariableCost;
    double mDelayCost;
	double mVariableDelayCost;
	double mTotalCost;

    double mIdealFixedCost;
    double mIdealVariableCost;
    double mIdealDelayCost;
	double mIdealVariableDelayCost;
	double mIdealTotalCost;
    
    std::vector<sCommodityPath> mPaths;
    std::vector<sMismatchedDispatch> mMismatches;
    
    std::map<std::string, int> mLinkUtilizations;

    friend class cCTSNDP;
};

void print_result(std::ostream& out, const cRoutes& routes);
void write_full_report(std::ostream& out, const cRoutes& routes);
