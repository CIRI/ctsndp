
#pragma once
#include <algorithm>

// Forward declarations
template<typename T>
class matrix_row_span;

// Matrix class
template<typename T>
class matrix
{
public:
    typedef T                                       value_type;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef T*                                      iterator;
    typedef const iterator                          const_iterator;
    typedef std::size_t                             size_type;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;
    
    matrix();
    matrix(size_type numRows, size_type numCols, const T& value = T());
    matrix(const matrix&) = delete;
    matrix(const matrix&& other);
    ~matrix();

    size_type numRows() const;
    size_type numCols() const;
    
    iterator begin() const;
    iterator end() const;

    T& operator()(size_type row, size_type col);
    const T& operator()(size_type row, size_type col) const;
    matrix_row_span<T> operator[](size_type pos);
    const matrix_row_span<T> operator[](size_type pos) const;

    // Modifiers
    void clear();
    void resize(size_type numRows, size_type numCols);
    void swap(matrix& other);
    
private:
    T* mpData;
    size_type mNumRows;
    size_type mNumCols;
};

template<typename T>
class matrix_row_span
{
public:
    typedef T           value_type;
    typedef std::size_t size_type;
    
    T& operator[](size_type pos);
    const T& operator[](size_type pos) const;
    
private:
    matrix_row_span(T* start, T* end);
    
    T* mpStart;
    T* mpEnd;
    
    friend matrix<T>;
};


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

template<typename T>
matrix<T>::matrix() : mpData(nullptr), mNumRows(0), mNumCols(0)
{
}

template<typename T>
matrix<T>::matrix(size_type numRows, size_type numCols, const T& value)
    : mpData(nullptr), mNumRows(numRows), mNumCols(numCols)
{
    mpData = new T[mNumRows * mNumCols];
    std::fill_n(mpData, mNumRows * mNumCols, value);
}

template<typename T>
matrix<T>::matrix(const matrix&& other)
{
    swap(other);
}

template<typename T>
matrix<T>::~matrix()
{
    clear();
}

template<typename T>
typename matrix<T>::size_type matrix<T>::numRows() const
{
    return mNumRows;
}

template<typename T>
typename matrix<T>::size_type matrix<T>::numCols() const
{
    return mNumCols;
}

template<typename T>
typename matrix<T>::iterator matrix<T>::begin() const
{
    return mpData;
}

template<typename T>
typename matrix<T>::iterator matrix<T>::end() const
{
    if (mpData)
        return &mpData[mNumCols*mNumRows];
    return mpData;
}

template<typename T>
T& matrix<T>::operator()(size_type row, size_type col)
{
    return *(mpData + row*mNumCols + col);
}

template<typename T>
const T& matrix<T>::operator()(size_type row, size_type col) const
{
    return *(mpData + row*mNumCols + col);
}

template<typename T>
matrix_row_span<T> matrix<T>::operator[](size_type pos)
{
    return matrix_row_span(mpData + pos*mNumCols, mpData + (pos+1)*mNumCols);
}

template<typename T>
const matrix_row_span<T> matrix<T>::operator[](size_type pos) const
{
    return matrix_row_span(mpData + pos*mNumCols, mpData + (pos+1)*mNumCols);
}

template<typename T>
void matrix<T>::clear()
{
    delete [] mpData;
    mpData = nullptr;
    mNumRows = 0;
    mNumCols = 0;
}

template<typename T>
void matrix<T>::resize(size_type numRows, size_type numCols)
{
    if (!mpData)
    {
        mNumRows = numRows;
        mNumCols = numCols;
        mpData = new T[mNumRows * mNumCols];
        return;
    }
    
    matrix<T> m(numRows, numCols);
    swap(m);
}

template<typename T>
void matrix<T>::swap(matrix<T>& other)
{
    std::swap(mNumCols, other.mNumCols);
    std::swap(mNumRows, other.mNumRows);
    std::swap(mpData, other.mpData);
}

template<typename T>
matrix_row_span<T>::matrix_row_span(T* start, T* end) : mpStart(start), mpEnd(end)
{}

template<typename T>
T& matrix_row_span<T>::operator[](size_type pos)
{
    return *(mpStart + pos);
}

template<typename T>
const T& matrix_row_span<T>::operator[](size_type pos) const
{
    return *(mpStart + pos);
}
