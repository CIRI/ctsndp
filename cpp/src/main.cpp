
#include "json_utils.hpp"
#include "ctsndp.hpp"
#include "ctsndp_utils.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "onf_utils.hpp"
#include "heartland_utils.hpp"

#include <clara.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include <filesystem>

int main(int argc, char** argv)
{
    // Command line parameters
    using namespace clara;

	// Set defaults
    std::string shipmentsFileName;
    std::string serviceNetworkFileName;
    std::string scheduleFileName;
    std::string routeFileName;
    std::string resultsFileName;
    std::string logFileName;
    std::string debugFileName;
    std::string outputFormat;
	std::string mcParameterFileName;
	std::string iatlParameterFileName;
	std::string cfsParameterFileName;
	std::string mdParameterFileName;
	bool print_network = false;
    bool quite_mode = false;
    bool verbose_mode = false;
    int debug_level = -1;
    bool log_to_console = false;
    bool showHelp = false;
    bool enable_timing = false;
    bool no_capacity_limits = false;
	bool ignore_required_locations = true;  // should be false!!
	bool no_delays = false;
	bool group = false;
	double delay_threshold_pct = 75.0;
    double consolidation_time = -1.0;
	bool allowInvalidExpansion = false;
	double time_limit_sec = -1.0;
	int numeric_focus = 0;
	enum {
		ONF = 0,
		DES,
		BOTH
	} routeFormat = ONF;

	// Consolidation options
	bool no_consolidations = false;
	bool limit_consolidations_by_time = false;
	bool limit_consolidations_by_capacity = false;


	// Logging options
	std::string logPath = std::filesystem::current_path().generic_string();
	bool logAll = false;
	bool logResults = false;
	bool logPTEN = false;
	bool logCreateInitial = false;
	bool logProperties = false;
	bool logMinimizeCostConstraintViolations = false;
	bool logCommodityPaths = false;
	bool logUtilization = false;
	bool logConsolidations = false;
	bool logIdentifyArcsConstraintViolations = false;
	bool logArcToLengthen = false;
	bool logIdentifyArcsTimeInfo = false;
	bool logCommoditySchedules = false;
	bool logConstructSolutionConstraintViolations = false;
	bool logMinimizeDelayConstraintViolations = false;
	bool logArcToShorten = false;
	bool logLinkUtilization = false;
	bool logLinkOvercapacity = false;

    auto cli = 
        Opt( log_to_console )
        ["-c"]["--log_console"]
        ("Log optimization data to console.") |
        Opt( debug_level, "Debug Level" )
        ["-d"]["--debug"]
        ("Debug level [0 to 5]. Five is the most information.") |
        Opt( outputFormat, "Output Format" )
        ["-f"]["--format"]
        ("The format of the route data, default is ONF.\nCurrent formats: DES, ONF, or BOTH") |
		Opt(group)
		["-g"]["--group"]
		("Group commodities") |
		Opt( logFileName, "Log Filename" )
        ["-l"]["--log"]
        ("Log optimization data to file.") |
        Opt( numeric_focus, "Numeric Focus Level" )
        ["-m"]["--numeric"]
        ("The numeric focus level of the solvers [0 to 3].") |
        Opt( serviceNetworkFileName, "Service Network Filename" )
        ["-n"]["--service_network"]
        ("The filename defining the service network.") |
        Opt( routeFileName, "Output Filename" )
        ["-o"]["--output"]
        ("The output filename for the routes.") |
        Opt( print_network )
        ["-p"]["--print"]
        ("Print partially time expanded network.") |
        Opt( quite_mode )
        ["-q"]["--quite"]
        ("Quite mode.") |
        Opt( resultsFileName, "Results Filename" )
        ["-r"]["--result"]
        ("The output filename for the results (same as ONF routes).") |
        Opt( shipmentsFileName, "Shipments Filename" )
        ["-s"]["--shipments"]
        ("The filename defining the shipments occuring within the service network.") |
        Opt(consolidation_time, "Consolidation Time" )
        ["-t"]["--time"]
        ("Group capacity limited arcs.") |
        Opt( debugFileName, "Debug Filename" )
        ["-w"]["--write"]
        ("Write debug information to file.") |
        Opt( scheduleFileName, "Schedule Filename" )
        ["-u"]["--schedule"]
        ("The filename defining the system schedule, includes service network and shipments.") |
        Opt( verbose_mode )
        ["-v"]["--verbose"]
        ("Verbose mode.") |
		Opt(ignore_required_locations)
		["--ignore_required_locations"] 
		("Ignore all required locations in the shipment files.") |
		Opt(no_delays)
		["--no_delays"]
		("Disable the ability of commoditities to arrive late.") |
		Opt(delay_threshold_pct, "Delay Threshold (%)")
		["--delay_threshold"]
		("Sets the threshold at which delay nodes are added to the network: 0 - 100") |
		Opt( enable_timing )
        ["--timing"]
        ("Show timing information") |
		Opt(time_limit_sec, "Time Limit (sec)")
		["--time_limit"]
		("Limits optimization time (in seconds).") |
		Opt(mcParameterFileName, "Tuning Parameters Filename")
		["--mcp"]
		("Tuning parameters for the Minimize Cost Model") |
		Opt(iatlParameterFileName, "Tuning Parameters Filename")
		["--iatlp"]
		("Tuning parameters for the Identify Arcs To Lengthen Model") |
		Opt(cfsParameterFileName, "Tuning Parameters Filename")
		["--cfsp"]
		("Tuning parameters for the Construct Feasible Solution Model") |
		Opt(mdParameterFileName, "Tuning Parameters Filename")
		["--mdp"]
		("Tuning parameters for the Minimize Delays Model") |
		Opt(logPath, "Log Path")
		["--log_path"]
		("The location to write the log files") |
		Opt(logResults)
		["--log_results"]
		("Log the results to file \"results.txt\"") |
		Opt(logCreateInitial)
		["--log_create"]
		("Log the results to file \"create_initial.txt\"") |
		Opt(logPTEN)
		["--log_pten"]
		("Log the partially time expanded network to file \"pten_#.txt\"") |
		Opt(logProperties)
		["--log_properties"]
		("Log the property [1 thru 4] checks of the partially time expanded network to file \"properties_#.txt\"") |
		Opt(logMinimizeCostConstraintViolations)
		["--log_min_cost_violations"]
		("Log the minimize cost optimization constraint violations to file \"\"") |
		Opt(logCommodityPaths)
		["--log_commodity_paths"]
		("Log the commodity paths to file \"\"") |
		Opt(logUtilization)
		["--log_utilization"]
		("Log the arc utilization to file \"\"") |
		Opt(logConsolidations)
		["--log_consolidations"]
		("Log the commodity consolidations to file \"\"") |
		Opt(logIdentifyArcsConstraintViolations)
		["--log_identify_arcs_violations"]
		("Log the identify arcs to lengthen optimization constraint violations to file \"\"") |
		Opt(logArcToLengthen)
		["--log_arcs_to_lengthen"]
		("Log the arcs to lengthen to file \"\"") |
		Opt(logIdentifyArcsTimeInfo)
		["--log_identify_arcs_time_info"]
		("Log the time info for the arcs to lengthen to file \"\"") |
		Opt(logCommoditySchedules)
		["--logCommoditySchedules"]
		("Log the commodity schedule to file \"\"") |
		Opt(logConstructSolutionConstraintViolations)
		["--log_construct_solution_violations"]
		("Log the construct solution optimization constraint violations to file \"\"") |
		Opt(logMinimizeDelayConstraintViolations)
		["--logMinimizeDelayConstraintViolations"]
		("Log the minimize delay optimization constraint violations to file \"\"") |
		Opt(logArcToShorten)
		["--logArcToShorten"]
		("Log the arcs to shorten to file \"\"") |
		Opt(logLinkUtilization)
		["--log_link_utilization"]
		("Log the service link utilization info file \"\"") |
		Opt(logLinkOvercapacity)
		["--log_link_overcapacity"]
		("Log the links in the service network that are over capacity to file \"\"") |
		Opt(logAll)
		["--log_all"]
		("Turn on all logging.  Be prepared for A LOT of files!") |
		Opt(no_consolidations)
		["--no_consolidations"]
		("Do not consolidate any of the commodities") |
		Opt(limit_consolidations_by_time)
		["--consolidate_by_time"]
		("Consolidate commodities based an ideal arrival time") |
		Opt(limit_consolidations_by_capacity)
		["--consolidate_by_capacity"]
		("Consolidate commodities only if the arc has limited capacity") |
		Opt(no_capacity_limits)
		["--no_capacity_limits"]
		("Do not limit the capacity of an arc") |
		Opt(allowInvalidExpansion)
		["--allow_invalid_expansion"]
		("Allow an invalid expansion of the partially time-expanded network on capacity limits.") |
		Help(showHelp);

    auto result = cli.parse( Args( argc, argv ) );
    if( !result ) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    if (showHelp || ((serviceNetworkFileName.empty()
                      || shipmentsFileName.empty()) && scheduleFileName.empty()))
    {
        cli.writeToStream(std::cout);
        exit(0);
    }

	if (!logPath.empty())
	{
		if ((logPath.back() != '\\') && (logPath.back() != '/'))
			logPath += "/";
	}

	std::ostream* out = &std::clog;

    std::ofstream debugFile;
	if (!debugFileName.empty())
	{
		debugFile.open(debugFileName);
		out = &debugFile;
	}
    
    if (!outputFormat.empty())
    {
        std::transform(outputFormat.begin(), outputFormat.end(), outputFormat.begin(), ::tolower);
        if (outputFormat.compare("des") == 0)
            routeFormat = DES;
		if (outputFormat.compare("both") == 0)
			routeFormat = BOTH;
	}
    
    std::unique_ptr<cServiceNetwork> network;
    std::unique_ptr<cCommodityShipments> shipments;

    if (!scheduleFileName.empty())
    {
        if (!serviceNetworkFileName.empty())
            *out << "Warning: the -n option will be ignored as the service network is defined in the schedule file.";

        if (!shipmentsFileName.empty())
			*out << "Warning: the -s option will be ignored as the shipment is defined in the schedule file.";

		heartland::sModelInputs_t inputs;
		try
		{
			inputs = heartland::create_inputs_from_schedule_file(scheduleFileName, no_delays, group);
		}
		catch (const std::exception& e)
		{
			*out << e.what() << std::endl;
			exit(2);
		}
        if (!inputs.network || !inputs.shipments)
        {
			*out << "Something when wrong in loading the data files!  Aborting..." << std::endl;
            exit(2);
        }
        network.reset(inputs.network.release());
        shipments.reset(inputs.shipments.release());
        
    }
    else
    {
        auto start = std::chrono::system_clock::now();
        
        if (verbose_mode || (debug_level >= 0) || enable_timing)
        {
			*out << "Creating service network from ";
			*out << serviceNetworkFileName;
            
            if (enable_timing)
            {
                auto start_time = std::chrono::system_clock::to_time_t(start);
				*out << " started at " << ctsndp::to_string(start_time) << "... ";
            }
        }
        network = create_service_network_from_json_file(serviceNetworkFileName);
        if (!network)
        {
			*out << "Something when wrong in loading the network file!  Aborting..." << std::endl;
            exit(2);
        }
        else if (enable_timing)
        {
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end-start;
            
            auto end_time = std::chrono::system_clock::to_time_t(end);
			*out << "Completed at " << ctsndp::to_string(end_time);
			*out << ", elapsed time: " << elapsed_seconds.count() << "s";
        }

        if (verbose_mode || (debug_level >= 0) || enable_timing)
			*out << std::endl;

        start = std::chrono::system_clock::now();

        if (verbose_mode || (debug_level >= 0) || enable_timing)
        {
			*out << "Creating shipments from ";
			*out << shipmentsFileName;
            
            if (enable_timing)
            {
                auto start_time = std::chrono::system_clock::to_time_t(start);
				*out << " started at " << ctsndp::to_string(start_time) << "... ";
            }
        }

        shipments = create_shipments_from_json_file(shipmentsFileName, no_delays, group);
        if (!shipments)
        {
			*out << "Something when wrong in loading the network file!  Aborting..." << std::endl;
            exit(2);
        }
        else if (enable_timing)
        {
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end-start;
            
            auto end_time = std::chrono::system_clock::to_time_t(end);
			*out << "Completed at " << ctsndp::to_string(end_time);
			*out << ", elapsed time: " << elapsed_seconds.count() << "s";
        }

        if (verbose_mode || (debug_level >= 0) || enable_timing)
			*out << std::endl;
    }

    try
    {
        auto start = std::chrono::system_clock::now();

        if (verbose_mode || (debug_level >= 0) || enable_timing)
        {
			*out << "Validating inputs and applying fixups";
            
            if (enable_timing)
            {
                auto start_time = std::chrono::system_clock::to_time_t(start);
				*out << " started at " << ctsndp::to_string(start_time) << "... ";
            }
            else
            {
				*out << "... ";
            }
        }

        if (!ctsndp::validate_inputs(*network.get(), *shipments.get(), debug_level > 0))
            return 1;
        
        ctsndp::apply_implicit_node_fixups(*network.get(),
                                       *shipments.get(),
                                       debug_level >= 0);

        ctsndp::apply_shortest_path_fixups(*network.get(),
                                       *shipments.get());

		if (verbose_mode || (debug_level >= 0) || enable_timing)
		{
			if (enable_timing)
			{
				auto end = std::chrono::system_clock::now();
				std::chrono::duration<double> elapsed_seconds = end - start;

				auto end_time = std::chrono::system_clock::to_time_t(end);
				*out << "Completed at " << ctsndp::to_string(end_time);
				*out << ", elapsed time: " << elapsed_seconds.count() << "s";
			}
			else
			{
				*out << "done!";
			}
			*out << std::endl;
		}
    }
    catch (const std::exception& e)
    {
		*out << e.what() << std::endl;
        return 1;
    }

	if ((delay_threshold_pct >= 
		0.0) && (delay_threshold_pct < 100.0))
		shipments->setShortestPathRatioThreshold_pct(delay_threshold_pct);

    try
    {
        cCTSNDP ctsndp;
        ctsndp.debugLevel(debug_level);
        ctsndp.logToConsole(log_to_console);
        ctsndp.logFilename(logFileName);
        ctsndp.numericFocus(numeric_focus);
        ctsndp.printPartiallyTimeExpandedNetwork(print_network);
        ctsndp.quiteMode(quite_mode);
        ctsndp.setTimeLimit_sec(time_limit_sec);
        ctsndp.verboseMode(verbose_mode);
        ctsndp.printTimingInfo(enable_timing);
		ctsndp.ignoreRequiredLocations(ignore_required_locations);
		if (no_consolidations)
            ctsndp.allowConsolidations(false);
		ctsndp.limitConsolidationByTime(limit_consolidations_by_time);
		ctsndp.limitConsolidationByCapacity(limit_consolidations_by_capacity);
		if (no_capacity_limits)
            ctsndp.allowCapacityConstraints(false);
        if (debugFile.is_open())
            ctsndp.setDebugOutput(debugFile);
		if (!mcParameterFileName.empty())
			ctsndp.setMinCostParameters(mcParameterFileName);
		if (!iatlParameterFileName.empty())
			ctsndp.setIdentArcsToLenParameters(iatlParameterFileName);
		if (!cfsParameterFileName.empty())
			ctsndp.setConstructFeasSolParameters(cfsParameterFileName);
		if (!mdParameterFileName.empty())
			ctsndp.setMinDelaysParameters(mdParameterFileName);

		ctsndp.setConsolidationTime(consolidation_time);
		ctsndp.mAllowInvalidExpansion = allowInvalidExpansion;

		ctsndp.mLogPath = logPath;
		ctsndp.mLogPTEN = logPTEN;
		ctsndp.mLogCreateInitial = logCreateInitial;
		ctsndp.mLogProperties = logProperties;
		ctsndp.mLogMinimizeCostConstraintViolations = logMinimizeCostConstraintViolations;
		ctsndp.mLogCommodityPaths = logCommodityPaths;
		ctsndp.mLogUtilization = logUtilization;
		ctsndp.mLogConsolidations = logConsolidations;
		ctsndp.mLogIdentifyArcsConstraintViolations = logIdentifyArcsConstraintViolations;
		ctsndp.mLogArcToLengthen = logArcToLengthen;
		ctsndp.mLogIdentifyArcsTimeInfo = logIdentifyArcsTimeInfo;
		ctsndp.mLogCommoditySchedules = logCommoditySchedules;
		ctsndp.mLogConstructSolutionConstraintViolations = logConstructSolutionConstraintViolations;
		ctsndp.mLogMinimizeDelayConstraintViolations = logMinimizeDelayConstraintViolations;
		ctsndp.mLogArcToShorten = logArcToShorten;
		ctsndp.mLogLinkUtilization = logLinkUtilization;
		ctsndp.mLogLinkOvercapacity = logLinkOvercapacity;
		ctsndp.mLogResults = logResults;

		if (logAll)
		{
			ctsndp.mLogPTEN = true;
			ctsndp.mLogCreateInitial = true;
			ctsndp.mLogProperties = true;
			ctsndp.mLogMinimizeCostConstraintViolations = true;
			ctsndp.mLogCommodityPaths = true;
			ctsndp.mLogUtilization = true;
			ctsndp.mLogConsolidations = true;
			ctsndp.mLogIdentifyArcsConstraintViolations = true;
			ctsndp.mLogArcToLengthen = true;
			ctsndp.mLogIdentifyArcsTimeInfo = true;
			ctsndp.mLogCommoditySchedules = true;
			ctsndp.mLogConstructSolutionConstraintViolations = true;
			ctsndp.mLogMismatches = true;
			ctsndp.mLogMinimizeDelayConstraintViolations = true;
			ctsndp.mLogArcToShorten = true;
			ctsndp.mLogLinkUtilization = true;
			ctsndp.mLogLinkOvercapacity = true;
			ctsndp.mLogResults = true;
			ctsndp.mLogProgress = true;
		}

        auto results = ctsndp.solve(std::move(network), std::move(shipments));

        if (verbose_mode || (debug_level >= 0))
        {
            if (debugFile.is_open())
                print_result(debugFile, *results.get());
        }
        
        if (results->mSuccess)
        {
            if (!quite_mode)
            {
				std::ostream* out = nullptr;
				if (verbose_mode || (debug_level >= 0) || enable_timing)
				{
					if (debugFile.is_open())
						out = &debugFile;
					else
						out = &std::clog;
				}


				nlohmann::json jsonDoc;
				if ((routeFormat == BOTH) && !routeFileName.empty())
				{
					// Write the DES routes file
					{
						std::string filename = routeFileName + ".des";
						jsonDoc = heartland::convert_routes_to_json_format(*results, out);
						write_json_file(filename, jsonDoc);
					}

					// Write the ONF routes file
					{
						std::string filename = routeFileName + ".onf";
						jsonDoc = onf::convert_routes_to_json_format(*results, out);
						write_json_file(filename, jsonDoc);
					}
				}
				else
				{
					switch (routeFormat)
					{
					case DES:
						jsonDoc = heartland::convert_routes_to_json_format(*results, out);
						break;
					case ONF:
					default:
						jsonDoc = onf::convert_routes_to_json_format(*results, out);
					}

					if (routeFileName.empty())
					{
//						*out << jsonDoc.dump(4) << std::flush;

					}
					else
					{
						write_json_file(routeFileName, jsonDoc);
					}
				}

                if (!resultsFileName.empty())
                {
					std::ofstream file(resultsFileName);
					write_full_report(file, *results.get());
					file.close();
                }
            }
        }
        else
        {
            if (!quite_mode)
            {
				*out << results->mErrorMessage << std::endl;
            }
            return 1;
        }
    }
    catch (const std::exception& e)
    {
		*out << e.what() << std::endl;
        return 1;
    }
    
    return 0;
}
