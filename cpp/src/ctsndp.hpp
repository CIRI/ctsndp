
#pragma once
#ifndef INCLUDE_CTSNDP_HPP
#define INCLUDE_CTSNDP_HPP

#include "ctsndp_defs.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include "routes.hpp"
#include <memory>
#include <vector>
#include <map>
#include <set>
#include <fstream>


// Forward Declarations
class cServiceNetwork;
class cServiceLocation;
class cCommodityShipments;
class cMinimizeCostsModel;


class cCTSNDP
{
public:

    cCTSNDP();
    ~cCTSNDP();

    void quiteMode(bool quite);
    void verboseMode(bool verbose);
    void debugLevel(int level);
    void printPartiallyTimeExpandedNetwork(bool print);

	void ignoreRequiredLocations(bool ignore);

    void allowConsolidations(bool allow);
	void limitConsolidationByTime(bool limit);
	void limitConsolidationByCapacity(bool limit);

    void allowCapacityConstraints(bool allow);
    void dumpPartiallyTimeExpandedNetwork(std::ostream& out);
    void logToConsole(bool log);
    void logFilename(const std::string& filename);
    void printTimingInfo(bool print);
    void setTimeLimit_sec(double time_sec);
    void numericFocus(int level);
    void setDebugOutput(std::ostream& out);
	void setConsolidationTime(double time);
	void setMinCostParameters(const std::string& filename);
	void setIdentArcsToLenParameters(const std::string& filename);
	void setConstructFeasSolParameters(const std::string& filename);
	void setMinDelaysParameters(const std::string& filename);


    /**
     * This is algorithm 1 listed on page 1310
     * Requires: Flat network 𝒟 = (𝒩, 𝒜), commodity set 𝒦
    **/
    std::unique_ptr<cRoutes> solve(
                std::unique_ptr<cServiceNetwork> pServiceNetwork,
                std::unique_ptr<cCommodityShipments> pShipments);
    
private:
   
    /*
     * Implementation of the algorithm list in section 3 on page 1307
     */
    bool MinimizeCosts();
    
    /**
     * Implementation of the algorithm list in section 4.3 on page 1312
     */
    std::set<arc_ptr_t> IdentifyArcsToLengthen();
    
    /**
     * Implementation of the algorithm list in section 4.4 on page 1313
     */
#ifdef CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP
	std::set<arc_ptr_t> ConstructFeasibleSolution();
#else
	bool ConstructFeasibleSolution();
#endif

    /**
     * Minimize the delays in the partial time expanded network by
     * moving delay node closer to their corresponding ldt.
     * Returns true if any delay nodes where moved!
     */
	ctsndp::arcs_to_shorten_t MinimizeDelays();

	/**
	 * For all commodities k in 𝒦, the nodes (o_k, e_k) and (d_k, l_k)
	 * are in 𝒩_t.
	 */
	bool TestProperty1(std::ostream& out) const;

	/**
	 * Every arc ((i, t), (j,t_bar)) in 𝒜_t has t_bar <= t + tau_ij.
	 */
	bool TestProperty2(std::ostream& out) const;

	/**
	 * For every arc a = (i, j) in 𝒜 in the flat network, 𝒟, and for
	 * every node (i, t) in the partially time-expanded network,
	 * 𝒟_t = (𝒩_t, 𝒜_t union H_t), there is a timed copy of a in 𝒜_t
	 * starting at (i, t).
	 */
	bool TestProperty3(std::ostream& out) const;

	/**
	 * if arc ((i, t), (j, t')) in 𝒜_t, then there does not exist a 
	 * node (j, t") in 𝒩_t with t' < t" <= t + tau_ij
	 */
	bool TestProperty4(std::ostream& out) const;

    /**
     * Find all arcs in the partially time expanded network that are over
     * the arc's capacity limit
     */
	ctsndp::arcs_over_capacity_t IdentifyArcsOverCapacity();

	/**
	 * Validate the incoming service network and shipments
	 */
	bool validate();

	/**
	 * Reset internal containers for next pass
	 */
	void reset();

	/*!
	 * \brief Apply any capacity constraints fixups to the shipments
	 * by adjusting the EATs of various commodity bundles
	 */
	void ensureSourceArcCapacityIsWithinLimits();

	void BuildRoutes();
	
    void FindAllSharedArcs();

	/**
	 * Dumps various debugging information to out.
	 */
	void DumpCommodityPaths(std::ostream& out);
    void DumpCommodityPath(std::ostream& out, const cCommodityBundle* const k);
    void DumpArcsToLengthen(std::ostream& out, const std::set<arc_ptr_t>& arcs) const;
	void DumpArcsToShorten(std::ostream& out, const ctsndp::arcs_to_shorten_t& arcs) const;
	void DumpConsolidationCount(std::ostream& out) const;
    void DumpConsolidations(std::ostream& out) const;

	typedef std::set<arc_ptr_t> arc_ptrs_t;
	struct utilization_t
	{
		unsigned int N;						// The TOTAL number of times the link is needed
		std::map<double, arc_ptrs_t> arcs_by_dispatch_time;
	};

	typedef std::map<ctsndp::link_ptr_t, utilization_t> link_utilization_t;

	void DumpLinkUtilization(std::ostream& out, const link_utilization_t& u_l) const;
	void DumpLinkOvercapacity(std::ostream& out, const ctsndp::arcs_over_capacity_t& arcs) const;

    std::unique_ptr<cServiceNetwork> mpServiceNetwork;
    std::unique_ptr<cCommodityShipments> mpShipments;
    std::unique_ptr<cPartiallyTimeExpandedNetwork> mpPartiallyTimeExpandedNetwork;

	unsigned int mPass;
    double mMinimalCost;
    double mMinimalFixedCost;
    double mMinimalVariableCost;
    double mMinimalDelayCost;
	double mMinimalVariableDelayCost;

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    // Results from "minimize costs" optimization
    ctsndp::commodity_path_node_t mP_ki;        // the path commodities through the service network by nodes visited
    ctsndp::commodity_path_arc_t  mP_ka;        // the path commodities through the service network by arc
    ctsndp::utilization_t mUtilization_ij;      // the utilization of arc (i,j).  Note: this is the saved version of y_ij

	/**
	 * In the paper, J_ak is the set of the pair of commodities that
	 * are consolidated on an arc.  We expanded this to a group of 
	 * commodities that all share the arc for efficiency
	 */
	ctsndp::consolidation_groups_t mJ_ak;		// the set of all commodities dispatched on an arc at the same time
	std::vector<arc_ptr_t> mJ_a;                // the set of all arc on which more than one commodity is dispatched
    std::map<arc_ptr_t, unsigned int> mN_a;     // the number of commodities carried on a given arc
    std::map<arc_ptr_t, unsigned int> mQ_a;     // the number of commodity bundles carried on a given arc

    // Results from "identify arcs to lengthen" and "construct feasible solution" optimizations
    ctsndp::dispatch_time_t  mDispatchTime_ki;  // the dispatch time of commodity, k, at node i
    ctsndp::travel_time_t    mTravelTime_kij;   // the travel time of arc, (i, j) when taken by commodity, k

    // the mismatch in the dispatch times of commodity, k1 and k2, at node i
    ctsndp::dispatch_mismatch_times_t  mMismatchTimes_ki;

    std::unique_ptr<cRoutes> mRoutes;
    
    // Various options
    bool mQuiteMode;
    bool mVerboseMode;
    bool mDebugLevel1;
    bool mDebugLevel2;
    bool mDebugLevel3;
    bool mDebugLevel4;
    bool mDebugLevel5;
    bool mPrintPartiallyTimeExpandedNetwork;
	bool mIgnoreRequireLocations;
	bool mAllowConsolidation;
	bool mLimitConsolidationByTime;
	bool mLimitConsolidationByCapacity;
	bool mAllowArcCapacityLimits;
    bool mPrintTimingInfo;
    bool mLogToConsole;
    std::string mLogFilename;
    double mTimeLimit_sec;
    int mNumericFocus;
    std::ostream* mpDebugOutput;
	double mConsolidationTime;
	std::string mMinCostParameterFileName;
	std::string mIdentArcsToLenParameterFileName;
	std::string mConstructFeasSolParameterFileName;
	std::string mMinDelaysParameterFileName;
	std::ofstream mProgressOutput;

public:
	std::string mLogPath;
	bool mLogProgress;
	bool mLogResults;

	// Logging options for "Partially Time Expanded Network"
	bool mLogPTEN;
	bool mLogCreateInitial;
	bool mLogProperties;

	// Logging options for "Minimize Cost"
	bool mLogMinimizeCostConstraintViolations;
	bool mLogMinimizeCostConstraints;
	bool mLogCommodityPaths;
	bool mLogUtilization;
	bool mLogConsolidations;

	// Logging options for "Idenity Arcs To Lengthen"
	bool mLogIdentifyArcsConstraintViolations;
	bool mLogArcToLengthen;
	bool mLogIdentifyArcsTimeInfo;
	bool mLogCommoditySchedules;

	// Logging options for "Construct Feasible Solution"
	bool mLogConstructSolutionConstraintViolations;
	bool mLogMismatches;

	// Logging options for "Minimize Delays"
	bool mLogMinimizeDelayConstraintViolations;
	bool mLogArcToShorten;

	// Logging options for "Capacity Constraints"
	bool mLogLinkUtilization;
	bool mLogLinkOvercapacity;
	bool mLogEnsureCapacity;

	bool mAllowInvalidExpansion;
};

#endif