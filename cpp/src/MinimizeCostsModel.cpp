//
//  MinimizeCostsModel.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 11/21/19.
//

#include "MinimizeCostsModel.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_defs.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include <iostream>
#include <chrono>
#include <cmath>
#include <fstream>
#include <cassert>


namespace
{
    arc_id_t to_index(cPartiallyTimeExpandedNetworkArc* const& arc, unsigned int offset)
    {
        return arc->getId() + offset;
    }
}

using namespace ctsndp;

cMinimizeCostsModel::cMinimizeCostsModel(int pass_number,
										 const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         bool verboseMode, bool printTimingInfo)
:
	cOptimizationProblem(pass_number),
	mPartiallyTimeExpandedNetwork(expandedNetwork),
    mServiceNetwork(serviceNetwork),
    mMinimalCost(0),
    mVerboseMode(verboseMode),
    mPrintTimingInfo(printTimingInfo)
{
	mOutputX_kij = false;
	mOutputY_ij = false;

	mLogX_kij = false;
	mLogY_ij = false;
	mLogConstraints = false;

	mIgnoreRequireLocations = false;

    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    auto& holdover_arcs = mPartiallyTimeExpandedNetwork.holdover_arcs();
    
    auto numArcs = arcs.size();

	mSizeOfKij = 0;
	for (auto arc : arcs)
	{
		if (arc->isDelayArc())
		{
			for (auto k : static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc)->getCommodities())
			{
				mKij_Indexes[{k, arc}] = mSizeOfKij++;
			}
		}
		else
			++mSizeOfKij;
	}
    auto totalNumArcs = numArcs + holdover_arcs.size();

    w_kij.resize(mSizeOfKij);
	u_kij.resize(mSizeOfKij);
	f_kij.resize(mSizeOfKij);
    c_kij.resize(mSizeOfKij);

	tau_ij.resize(totalNumArcs);
    
    auto start = std::chrono::system_clock::now();

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\n\tCreating optimization variables";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }
*/

    for (auto& arc : arcs)
    {
        auto ij = to_index(arc);

		if (arc->isDelayArc())
		{
			auto delay_arc = static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc);
			for (auto k : delay_arc->getCommodities())
			{
				auto kij = to_kij_index(arc, k);

				double delay_cost = ctsndp::MINIMUM_COST;
				if (delay_arc->getDestinationNode() == k->getDestinationNode()
					&& k->hasDelayCost())
				{
					delay_cost = k->getDelayCost();
				}

				f_kij[kij] = std::max(delay_cost, ctsndp::MINIMUM_COST);

				u_kij[kij] = arc->getPerUnitOfFlowCapacity();

				// Delay arc are never limited in capacity!
				w_kij[kij] = std::numeric_limits<unsigned int>::max();

				double delay_penalty = k->getDelayPenaltyRate() * std::fabs(arc->getActualTravelTime());
				if (k->hasDelayPenaltyLimit())
					delay_penalty = std::min(delay_penalty, k->getDelayPenaltyLimit());

				c_kij[kij] = std::max(delay_penalty, ctsndp::MINIMUM_VARIABLE_COST);
			}
		}
		else
		{
			f_kij[ij] = std::max(arc->getFixedCost(), ctsndp::MINIMUM_COST);
			u_kij[ij] = arc->getPerUnitOfFlowCapacity();

			if (arc->arcCapacityIsLimited())
				w_kij[ij] = arc->getCapacity();
			else
				w_kij[ij] = std::numeric_limits<unsigned int>::max();

			c_kij[ij] = std::max(arc->getPerUnitOfFlowCost(), ctsndp::MINIMUM_VARIABLE_COST);
		}

        tau_ij[ij] = arc->getActualTravelTime();

		if (arc->isDelayArc())
		{
			assert(tau_ij[ij] <= 0.0);
		}
    }
    
    for (auto& arc : holdover_arcs)
    {
        auto ij = to_index(arc, numArcs);
        tau_ij[ij] = arc->getActualTravelTime();
    }
    
    
    auto& commodities = mPartiallyTimeExpandedNetwork.commodities();
    q_k.resize(commodities.size());
    
    for (auto commodity : commodities)
    {
        auto k = to_index(commodity);
        q_k[k] = commodity->getQuantity();
    }

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
*/
}

cMinimizeCostsModel::~cMinimizeCostsModel()
{
}

void cMinimizeCostsModel::ignoreRequiredLocations(bool ignore)
{
	mIgnoreRequireLocations = ignore;
}

void cMinimizeCostsModel::printX_kij(bool print)
{
	mOutputX_kij = print;
}

double cMinimizeCostsModel::getMinimalCost() const
{
    return mMinimalCost;
}

const ctsndp::utilization_t& cMinimizeCostsModel::getArcUtilization() const
{
    return mUtilization_ij;
}

ctsndp::commodity_path_node_t cMinimizeCostsModel::returnCommodityPathsByNode() const
{
    return mP_ki;
}

ctsndp::commodity_path_arc_t  cMinimizeCostsModel::returnCommodityPathsByArc() const
{
    return mP_ka;
}

void cMinimizeCostsModel::BuildModel()
{
	if (mpProgress)
	{
		*mpProgress << "  ConstructOptimizatonVariables..." << std::flush;
	}

	ConstructOptimizatonVariables();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tAdding constraints";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "...\n" << std::flush;
    }

	std::ostream* debug = nullptr;
	std::ofstream file;
	if (mLogConstraints)
	{
		std::string filename = mLogPath + "MinimizeCostsConstraints_";
		filename += std::to_string(mPass) + ".txt";
		file.open(filename);
		debug = &file;
	}

    // Add constraints
	if (mpProgress)
	{
		*mpProgress << "  constraint1_ConservationOfFlow..." << std::flush;
	}

	constraint1_ConservationOfFlow(debug);

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint2_EnsureSufficientCapacityPerFlow..." << std::flush;
	}
	
	constraint2_EnsureSufficientCapacityPerFlow(debug);
    
	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint2a_EnsureSufficientCapacityPerArc..." << std::flush;
	}
	
	constraint2a_EnsureSufficientCapacityPerArc(debug);

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint5_TravelTime..." << std::flush;
	}
	
	constraint5_TravelTime(debug);

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint5a_RestrictionOfFlow..." << std::flush;
	}
	
	constraint5a_RestrictionOfFlow(debug);

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint5b_RequiredFlows..." << std::flush;
	}
	
	constraint5b_RequiredFlows(debug);

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	if (mLogConstraints)
	{
		file.close();
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tdone";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}

	if (mpProgress)
	{
		*mpProgress << "  BuildOptimizatonProblem..." << std::flush;
	}

	BuildOptimizatonProblem();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}
}


//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

arc_id_t cMinimizeCostsModel::to_kij_index(cPartiallyTimeExpandedNetworkArc* const& arc, ctsndp::commodity_ptr_t commodity) const
{
	if (arc->isDelayArc())
	{
		auto it = mKij_Indexes.find({ commodity, arc });
		assert(it != mKij_Indexes.end());
		return it->second;
	}

	return arc->getId();
}

bool cMinimizeCostsModel::RestrictCommodityFromNode(ctsndp::commodity_ptr_t commodity,
													const ctsndp::node_ptr_t node) const
{
	if (node->isDelayNode() &&
		!(node->hasCommodity(commodity)))
	{
		return true;
	}

	/**
	 * Future work: return true to stop a commodity from using this node in its possible
	 * path.
	 */

	return false;
}

bool cMinimizeCostsModel::RestrictCommodityInflowToNode(ctsndp::commodity_ptr_t commodity,
	const ctsndp::arc_ptr_t arc) const
{
	if (arc->connectedToDelayNode() && !arc->getDelayNode()->hasCommodity(commodity))
	{
		return true;
	}

	/**
	 * Future work: return true to stop a commodity from using the inflow arc
	 * to a node in its possible path.
	 */

	return false;
}

bool cMinimizeCostsModel::RestrictCommodityOutflowFromNode(ctsndp::commodity_ptr_t commodity,
	const ctsndp::arc_ptr_t arc) const
{
	if (arc->isDelayArc())
	{
		return !static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc)->hasCommodity(commodity);
	}

	if (arc->connectedToDelayNode() && !arc->getDelayNode()->hasCommodity(commodity))
	{
		return true;
	}

	/**
	 * Future work: return true to stop a commodity from using the outflow arc
	 * to a node in its possible path.
	 */

	return false;
}

bool cMinimizeCostsModel::RestrictCommodityFromArc(ctsndp::commodity_ptr_t commodity,
													const ctsndp::arc_ptr_t arc) const
{
	if (arc->isDelayArc())
	{
		return !static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc)->hasCommodity(commodity);
	}

	if (arc->connectedToDelayNode() && !arc->getDelayNode()->hasCommodity(commodity))
	{
		return true;
	}

	/**
	 * Future work: return true to stop a commodity from using the arc in its possible path.
	 */

	return false;
}

bool cMinimizeCostsModel::RestrictCommodityFromArc(ctsndp::commodity_ptr_t commodity,
	const ctsndp::holdover_arc_ptr_t arc) const
{
	/**
	 * Future work: return true to stop a commodity from using the holdover arc in
	 * its possible path.
	 */

	return false;
}

//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

