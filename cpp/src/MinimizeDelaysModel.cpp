//
//  MinimizeDelaysModel.cpp
//
//  Created by Brett Feddersen on 11/22/19.
//

#include "MinimizeDelaysModel.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include <iostream>
#include <chrono>
#include <cassert>


using namespace ctsndp;


cMinimizeDelaysModel::cMinimizeDelaysModel(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_arc_t& p_ka,
                                         const ctsndp::dispatch_time_t& dispatchTimes_ki,
                                         bool verboseMode, bool printTimingInfo)
:
	cOptimizationProblem(pass_number),
    mPartiallyTimeExpandedNetwork(expandedNetwork),
    mServiceNetwork(serviceNetwork),
    mP_ka(p_ka),
    mDispatchTimes_ki(dispatchTimes_ki),
    mVerboseMode(verboseMode),
    mPrintTimingInfo(printTimingInfo)
{
    auto& arcs = mPartiallyTimeExpandedNetwork.arcs();
    auto& holdover_arcs = mPartiallyTimeExpandedNetwork.holdover_arcs();
   
    auto numArcs = arcs.size();
    auto totalNumArcs = numArcs + holdover_arcs.size();
    tau_ij.resize(totalNumArcs);

	p_k.resize(p_ka.size(), 0.0);

    auto start = std::chrono::system_clock::now();

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\n\tCreating optimization variables";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }
*/

    for (auto& arc : arcs)
    {
        auto ij = to_index(arc);
        tau_ij[ij] = arc->getActualTravelTime();
    }
    
    for (auto& arc : holdover_arcs)
    {
        auto ij = to_index(arc, numArcs);
        tau_ij[ij] = arc->getActualTravelTime();
    }

    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        auto& k = it->first;

        // Make sure this commodity has a delay cost.
        // If yes, then the path contain a "delay" node that can be shortened
        if (! (k->hasDelayCost() || k->hasDelayPenalty()))
            continue;

		if (k->hasDelayPenalty())
		{
			double penalty = k->getDelayPenaltyRate();
			assert(penalty >= 0.0);
			p_k[k->getId()] = penalty;
		}

        auto& path = it->second;
        if (path.empty())
            continue;

        // We can only minimize delays on paths that pass through a delay node
        if (! path.back()->connectedToDelayNode())
            continue;

		for (auto it = path.rbegin(); it != path.rend(); ++it)
		{
			auto arc = *it;
			if (arc->getSource()->isDelayNode())
				continue;

			const auto node = arc->getDestination();
			a_kij[{k, arc}] = mDispatchTimes_ki.at({ k, arc->getSource() }) + tau_ij[to_index(arc)];
			break;
		}
    }

/*
    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
       
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
*/
}

cMinimizeDelaysModel::~cMinimizeDelaysModel()
{
}

ctsndp::arcs_to_shorten_t cMinimizeDelaysModel::returnArcsToShorten()
{
	return mArcsToShorten;
}

void cMinimizeDelaysModel::BuildModel()
{
    if (a_kij.empty())
        return;

	if (mpProgress)
	{
		*mpProgress << "  ConstructOptimizatonVariables..." << std::flush;
	}

	ConstructOptimizatonVariables();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tAdding constraints";
         
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
             
            *mpDebugOutput << " started at " << to_string(start_time);
        }
         
        *mpDebugOutput << "...\n" << std::flush;
    }

    // Add constraints
	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint18_EnsureDispatchIsAfterAvailable..." << std::flush;
	}

	constraint18_EnsureDispatchIsAfterAvailable();
	
	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
		*mpProgress << "  constraint19_EnsureArrivalBeforeMaxLateTime..." << std::flush;
	}

	constraint19_EnsureArrivalBeforeMaxLateTime();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tdone";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}

	if (mpProgress)
	{
		*mpProgress << "  BuildOptimizatonProblem..." << std::flush;
	}

	BuildOptimizatonProblem();

	if (mpProgress)
	{
		*mpProgress << "done" << std::endl;
	}
}


//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

