
#include "ctsndp.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_utils_priv.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_algorithms.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityShipment.hpp"

#ifdef USE_CPLEX
	#include "MinimizeCostsModel_CPlex.hpp"
	#include "IdentifyArcsToLengthenModel_CPlex.hpp"
	#include "ConstructFeasibleSolutionModel_CPlex.hpp"
	#include "MinimizeDelaysModel_CPlex.hpp"
#else
	#include "MinimizeCostsModel_Gurobi.hpp"
	#include "IdentifyArcsToLengthenModel_Gurobi.hpp"
	#include "ConstructFeasibleSolutionModel_Gurobi.hpp"
	#include "MinimizeDelaysModel_Gurobi.hpp"
#endif

#include <exception>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <fstream>
#include <filesystem>
#include <cassert>


using namespace ctsndp;


cCTSNDP::cCTSNDP()
	:
	mPass(0),
    mMinimalCost(0),
    mMinimalFixedCost(0),
    mMinimalVariableCost(0),
    mpDebugOutput(&std::clog)
{
    mQuiteMode = true;
    mVerboseMode = false;
    mDebugLevel1 = mDebugLevel2 = mDebugLevel3 = mDebugLevel4 = mDebugLevel5 = false;
    mPrintPartiallyTimeExpandedNetwork = false;
	mIgnoreRequireLocations = false;
    mAllowConsolidation = true;
	mLimitConsolidationByTime = false;
	mLimitConsolidationByCapacity = false;
    mLogToConsole = false;
    mTimeLimit_sec = -1.0;
    mNumericFocus = 0;
    mPrintTimingInfo = false;
    mAllowArcCapacityLimits = true;
	mConsolidationTime = 0;

	// General logging options
	mLogProgress = false;
	mLogResults = false;

	// Logging options for "Partially Time Expanded Network"
	mLogPTEN = false;
	mLogCreateInitial = false;
	mLogProperties = false;

	// Logging options for "Minimize Cost"
	mLogMinimizeCostConstraintViolations = false;
	mLogMinimizeCostConstraints = false;
	mLogCommodityPaths = false;
	mLogUtilization = false;
	mLogConsolidations = false;

	// Logging options for "Idenity Arcs To Lengthen"
	mLogIdentifyArcsConstraintViolations = false;
	mLogArcToLengthen = false;
	mLogIdentifyArcsTimeInfo = false;
	mLogCommoditySchedules = false;

	// Logging options for "Construct Feasible Solution"
	mLogConstructSolutionConstraintViolations = false;
	mLogMismatches = false;

	// Logging options for "Minimize Delays"
	mLogMinimizeDelayConstraintViolations = false;
	mLogArcToShorten = false;

	// Logging options for "Capacity Constraints"
	mLogLinkUtilization = false;
	mLogLinkOvercapacity = false;
	mLogEnsureCapacity = false;

	mAllowInvalidExpansion = false;
}

cCTSNDP::~cCTSNDP()
{
}

void cCTSNDP::quiteMode(bool quite)
{
    mQuiteMode = quite;
    if (quite)
        mVerboseMode = false;
}

void cCTSNDP::verboseMode(bool verbose)
{
    mVerboseMode = verbose;
    if (verbose)
        mQuiteMode = false;
}

void cCTSNDP::debugLevel(int level)
{
    mVerboseMode |= level >= 0;
    mDebugLevel1 = level >= 1;
    mDebugLevel2 = level >= 2;
    mDebugLevel3 = level >= 3;
    mDebugLevel4 = level >= 4;
    mDebugLevel5 = level >= 5;
}

void cCTSNDP::printPartiallyTimeExpandedNetwork(bool print)
{
    mPrintPartiallyTimeExpandedNetwork = print;
}

void cCTSNDP::ignoreRequiredLocations(bool ignore)
{
	mIgnoreRequireLocations = ignore;
}

void cCTSNDP::allowConsolidations(bool allow)
{
    mAllowConsolidation = allow;
}

void cCTSNDP::limitConsolidationByTime(bool limit)
{
	mLimitConsolidationByTime = limit;
}

void cCTSNDP::limitConsolidationByCapacity(bool limit)
{
	mLimitConsolidationByCapacity = limit;
}

void cCTSNDP::allowCapacityConstraints(bool allow)
{
    mAllowArcCapacityLimits = allow;
}

void cCTSNDP::dumpPartiallyTimeExpandedNetwork(std::ostream& out)
{
    if (mpPartiallyTimeExpandedNetwork)
        mpPartiallyTimeExpandedNetwork->print(out);
}

void cCTSNDP::logToConsole(bool log)
{
    mLogToConsole = log;
}

void cCTSNDP::logFilename(const std::string& filename)
{
    mLogFilename = filename;
}

void cCTSNDP::setTimeLimit_sec(double time_sec)
{
    mTimeLimit_sec = time_sec;
}

void cCTSNDP::printTimingInfo(bool print)
{
    mPrintTimingInfo = print;
}

void cCTSNDP::numericFocus(int level)
{
    mNumericFocus = std::clamp(level, 0, 3);
}

void cCTSNDP::setDebugOutput(std::ostream& out)
{
    mpDebugOutput = &out;
}

void cCTSNDP::setConsolidationTime(double time)
{
	mConsolidationTime = time;
}

void cCTSNDP::setMinCostParameters(const std::string& filename)
{
	mMinCostParameterFileName = filename;
}

void cCTSNDP::setIdentArcsToLenParameters(const std::string& filename)
{
	mIdentArcsToLenParameterFileName = filename;
}

void cCTSNDP::setConstructFeasSolParameters(const std::string& filename)
{
	mConstructFeasSolParameterFileName = filename;
}

void cCTSNDP::setMinDelaysParameters(const std::string& filename)
{
	mMinDelaysParameterFileName = filename;
}


std::unique_ptr<cRoutes> cCTSNDP::solve(
                    std::unique_ptr<cServiceNetwork> pServiceNetwork,
                    std::unique_ptr<cCommodityShipments> pShipments)
{
    auto start = std::chrono::system_clock::now();

    if (mPrintTimingInfo)
    {
        auto start_time = std::chrono::system_clock::to_time_t(start);
        *mpDebugOutput << "Main solver started at " << ctsndp::to_string(start_time) << "...\n";
    }
  
	if (mLogProgress)
	{
		mProgressOutput.open(mLogPath + "ctsndp_progress.txt");
	}

    mRoutes.reset(new cRoutes());

    mpServiceNetwork = std::move(pServiceNetwork);
    mpShipments = std::move(pShipments);
    if (!validate())
        return std::move(mRoutes);

	if (mLogCreateInitial)
	{
		std::string filename = mLogPath + "create_initial.txt";
		std::ofstream file(filename);
		auto network = ctsndp::create_initial(*mpServiceNetwork, *mpShipments, &file);
		file.close();
		mpPartiallyTimeExpandedNetwork.reset(network.release());
	}
	else
	{
		auto network = ctsndp::create_initial(*mpServiceNetwork, *mpShipments, mDebugLevel3 ? mpDebugOutput : nullptr);
		mpPartiallyTimeExpandedNetwork.reset(network.release());
	}

	if (mAllowArcCapacityLimits && (mConsolidationTime > 0))
	{
		ensureSourceArcCapacityIsWithinLimits();
	}

	mpPartiallyTimeExpandedNetwork->rehash();

    bool solved = false;
	int shorteningPass = 0;
	mPass = 0;
    
    try
    {
        while (! solved)
        {
			++mPass;
			mProgressOutput << "Partially Time Expanded Network Stats (pass " << mPass << "):\n";
			mProgressOutput << "\tNumber of commodity bundles: " << mpPartiallyTimeExpandedNetwork->commodities().size() << "\n";
			mProgressOutput << "\tNumber of nodes: " << mpPartiallyTimeExpandedNetwork->nodes().size();
			if (mpPartiallyTimeExpandedNetwork->delay_nodes().size() > 0)
			{
				mProgressOutput << " of which " << mpPartiallyTimeExpandedNetwork->delay_nodes().size() << " are delay nodes";
			}
			mProgressOutput << "\n";
			mProgressOutput << "\tNumber of arcs: " << mpPartiallyTimeExpandedNetwork->arcs().size() << "\n";
			mProgressOutput << "\tNumber of storage arcs: " << mpPartiallyTimeExpandedNetwork->holdover_arcs().size() << std::endl;

			if (mLogPTEN)
			{
				std::string filename = mLogPath + "pten_";
				filename += std::to_string(mPass) + ".txt";
				std::ofstream pten(filename);
				mpPartiallyTimeExpandedNetwork->print(pten);
				pten.close();
			}

			mProgressOutput << "Testing properties..." << std::flush;

			if (mLogProperties)
			{
				std::string filename = mLogPath + "properties_";
				filename += std::to_string(mPass) + ".txt";
				std::ofstream file(filename);

				file << "Checking properties 1, 2, 3, and 4 for any voilations...\n";

				bool pass = true;
				pass &= TestProperty1(file);
				pass &= TestProperty2(file);
				pass &= TestProperty3(file);
				pass &= TestProperty4(file);

				if (pass)
					file << "  None.";
				file.close();

				if (!pass)
				{
					mRoutes->mErrorMessage = "A property violation of the partially time-expanded ";
					mRoutes->mErrorMessage += "network was detected! Check the log file.";
					mRoutes->mpServiceNetwork.reset(mpServiceNetwork.release());
					mRoutes->mpShipments.reset(mpShipments.release());
					mRoutes->mpPartiallyTimeExpandedNetwork.reset(mpPartiallyTimeExpandedNetwork.release());
					return std::move(mRoutes);
				}
			}
			else
			{
				assert(TestProperty1(*mpDebugOutput));
				assert(TestProperty2(*mpDebugOutput));
				assert(TestProperty3(*mpDebugOutput));
				assert(TestProperty4(*mpDebugOutput));
			}

			mProgressOutput << " done" << std::endl;

            if (mPrintPartiallyTimeExpandedNetwork)
                mpPartiallyTimeExpandedNetwork->print(*mpDebugOutput);
            else if (mVerboseMode)
            {
				*mpDebugOutput << "\n";
                *mpDebugOutput << "Partially Time Expanded Network Stats (pass " << mPass << "):\n";
                *mpDebugOutput << "\tNumber of commodity bundles: " << mpPartiallyTimeExpandedNetwork->commodities().size() << "\n";
				*mpDebugOutput << "\tNumber of nodes: " << mpPartiallyTimeExpandedNetwork->nodes().size();
				if (mpPartiallyTimeExpandedNetwork->delay_nodes().size() > 0)
				{
					*mpDebugOutput << " of which " << mpPartiallyTimeExpandedNetwork->delay_nodes().size() << " are delay nodes";
				}
				*mpDebugOutput << "\n";
				int num_delay_arcs = 0;
				auto& arcs = mpPartiallyTimeExpandedNetwork->arcs();
				std::for_each(arcs.begin(), arcs.end(),
					[&num_delay_arcs](arc_ptr_t arc) 
					{
						if (arc->isDelayArc())
							++num_delay_arcs;
					}
				);
				*mpDebugOutput << "\tNumber of arcs: " << mpPartiallyTimeExpandedNetwork->arcs().size();
				if (num_delay_arcs > 0)
				{
					*mpDebugOutput << " of which " << num_delay_arcs << " are delay arcs";
				}
				*mpDebugOutput << "\n";
				*mpDebugOutput << "\tNumber of storage arcs: " << mpPartiallyTimeExpandedNetwork->holdover_arcs().size() << std::endl;
            }

            reset();

			mProgressOutput << "Minimize Costs:" << std::endl;
            // Line 3: Solve SND(𝒟t)
            if (!MinimizeCosts())
	            break;

			mProgressOutput << "done" << std::endl;

			// Line 4: Determine whether the solution to SND(𝒟t) can be converted to feasible solution
            //         to CTSNDP with the same cost
			mProgressOutput << "Identify Arcs To Lengthen:" << std::endl;
#ifdef CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP
			std::set<arc_ptr_t>  arcs_to_lengthen;
			try {
					arcs_to_lengthen = IdentifyArcsToLengthen();
			}
			catch (const ctsndp::cInfeasibleSolution&)
			{
				mProgressOutput << "failed!" << std::endl;
				mProgressOutput << "Construct Feasible Solution:" << std::endl;
				arcs_to_lengthen = ConstructFeasibleSolution();
				mProgressOutput << "done" << std::endl;
			}
#else
			std::set<arc_ptr_t>  arcs_to_lengthen = IdentifyArcsToLengthen();
#endif
			mProgressOutput << "done" << std::endl;

			if (mDebugLevel2 && !mDebugLevel3)
			{
				mProgressOutput << "Dumping Arcs To Lengthen..." << std::flush;
				DumpArcsToLengthen(*mpDebugOutput, arcs_to_lengthen);
				mProgressOutput << " done" << std::endl;
			}

            // Line 5: If there are no arcs that are "too short", convert SND(𝒟t) to feasible solution
            if (arcs_to_lengthen.empty())
            {
                // Line 6: Stop. The converted solution is optimal for CTSNDP
#ifdef CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP
				bool feasible_solution = true;
#else
				mProgressOutput << "Construct Feasible Solution..." << std::flush;
				bool feasible_solution = ConstructFeasibleSolution();
				mProgressOutput << " done" << std::endl;
#endif

				if (feasible_solution)
				{
					mProgressOutput << "Identify Arcs Over Capacity..." << std::flush;
					auto  arcs_to_increase_cap = IdentifyArcsOverCapacity();
					mProgressOutput << " found " << arcs_to_increase_cap.size() << " done" << std::endl;
					
					if (arcs_to_increase_cap.size() > 0)
                    {
						std::ostream* out = mDebugLevel3 ? mpDebugOutput : nullptr;
						std::ofstream file;
						if (mLogEnsureCapacity)
						{
							std::string filename = mLogPath + "ensure_capacities_";
							filename += std::to_string(mPass) + ".txt";
							file.open(filename);
							DumpArcsToLengthen(file, arcs_to_lengthen);
							out = &file;
						}
						
						mProgressOutput << "Calling ensure_capacities..." << std::flush;
						ctsndp::ensure_capacities(*mpPartiallyTimeExpandedNetwork,
							std::move(arcs_to_increase_cap),
							mConsolidationTime, out);
						mProgressOutput << " done" << std::endl;

						file.close();

						mpPartiallyTimeExpandedNetwork->rehash();
						shorteningPass = 0;
					}
                    else
					{
						++shorteningPass;
						// Test to see if any delay nodes are too far in the future.  
						// If yes (return true), move the corresponding nodes closer
						// to LDT.  If no (returning false), then we are done!
						mProgressOutput << "Minimize Delays:" << std::endl;
						ctsndp::arcs_to_shorten_t arcs_to_shorten = MinimizeDelays();

						if ((shorteningPass > 2) || arcs_to_shorten.empty())
						{
							if (mLogArcToShorten)
							{
								std::string filename = mLogPath + "arcs_to_shorten_";
								filename += std::to_string(mPass) + ".txt";
								std::ofstream file(filename);
								DumpArcsToShorten(file, arcs_to_shorten);
								file.close();
							}

							solved = true;
						}
						else
						{
							std::ostream* out = mDebugLevel3 ? mpDebugOutput : nullptr;
							std::ofstream file;
							if (mLogArcToShorten)
							{
								std::string filename = mLogPath + "arcs_to_shorten_";
								filename += std::to_string(mPass) + ".txt";
								file.open(filename);
								DumpArcsToShorten(file, arcs_to_shorten);
								out = &file;
							}

							mProgressOutput << "Calling shorten_arc (algorithm 8)..." << std::flush;

							ctsndp::shorten_arc(*mpPartiallyTimeExpandedNetwork, std::move(arcs_to_shorten), out);

							mProgressOutput << " done" << std::endl;

							file.close();

							mpPartiallyTimeExpandedNetwork->rehash();
						}

						mProgressOutput << "done" << std::endl;
					}
                }
            }
            else
            {
                // Line 8: The solution to SND(𝒟t) must use at least one arc that is "too short."
                // Refine the partially time-expanded network 𝒟t by correcting the length of at
                // least one such arc, in the process adding at least one new time point to 𝒯i
                // for some i in 𝒩

				std::ostream* out = mDebugLevel3 ? mpDebugOutput : nullptr;
				std::ofstream file;
				if (mLogArcToLengthen)
				{
					std::string filename = mLogPath + "arcs_to_lengthen_";
					filename += std::to_string(mPass) + ".txt";
					file.open(filename);
					DumpArcsToLengthen(file, arcs_to_lengthen);
					out = &file;
				}

				mProgressOutput << "Found " << arcs_to_lengthen.size() << " arcs to lengthen." << std::endl;
				mProgressOutput << "Calling lengthen_arc (algorithm 3)..." << std::flush;
				ctsndp::lengthen_arc(*mpPartiallyTimeExpandedNetwork, std::move(arcs_to_lengthen), out);
				mProgressOutput << " done" << std::endl;

				file.close();

				mpPartiallyTimeExpandedNetwork->rehash();

				shorteningPass = 0;
            }
			mProgressOutput << "\n" << std::endl;
		}
    
		if (solved)
		{
			mRoutes->mSuccess = true;

			mProgressOutput << "Building Routes..." << std::flush;
			BuildRoutes();
			mProgressOutput << " done" << std::endl;
		}
    }
    catch (const ctsndp::cInfeasibleSolution& e)
    {
        mRoutes->mErrorMessage = "The ";
        mRoutes->mErrorMessage += e.model();
        mRoutes->mErrorMessage += " model is infeasible!\n";
        mRoutes->mErrorMessage += e.what();
    }
	catch (const ctsndp::cTimeLimit& e)
	{
		mRoutes->mErrorMessage = "The ";
		mRoutes->mErrorMessage += e.model();
		mRoutes->mErrorMessage += " model exceeded its time limit!\n";
		mRoutes->mErrorMessage += e.what();
	}
    mRoutes->mpServiceNetwork.reset(mpServiceNetwork.release());
    mRoutes->mpShipments.reset(mpShipments.release());
    
	if (mLogResults)
	{
		std::string filename = mLogPath + "results.txt";
		std::ofstream file(filename);
		print_result(file, *mRoutes.get());
		file.close();
	}

	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;

	auto end_time = std::chrono::system_clock::to_time_t(end);

    if (mPrintTimingInfo)
    {
        *mpDebugOutput << "Completed at " << ctsndp::to_string(end_time);
        *mpDebugOutput << ", elapsed time: " << elapsed_seconds.count() << "s\n";
    }

	mProgressOutput.close();
    
	mRoutes->mSolutionTime_s = elapsed_seconds;
	mRoutes->mpPartiallyTimeExpandedNetwork.reset(std::move(mpPartiallyTimeExpandedNetwork.release()));
    return std::move(mRoutes);
}

///////////////////////////////////////////////////////////
// Private Methods
///////////////////////////////////////////////////////////

void cCTSNDP::reset()
{
    mMinimalCost = 0;
    mMinimalFixedCost = 0;
    mMinimalVariableCost = 0;
	mMinimalDelayCost = 0;
	mMinimalVariableDelayCost = 0;

    // Remove all data stored
	for (auto& ki : mP_ki)
	{
		ki.second.clear();
	}
    mP_ki.clear();

	for (auto& ka : mP_ka)
	{
		ka.second.clear();
	}
	mP_ka.clear();

    mUtilization_ij.clear();

	for (auto& ak : mJ_ak)
	{
		ak.second.clear();
	}
	mJ_ak.clear();

    mJ_a.clear();
	mN_a.clear();
	mQ_a.clear();

	mDispatchTime_ki.clear();
    mTravelTime_kij.clear();
    mMismatchTimes_ki.clear();
}

bool cCTSNDP::validate()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "Validating input data";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "...";
    }
    
    if (!mpServiceNetwork)
    {
        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "no service network (nullptr)";

            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << "\n";
        }
        
        mRoutes->mErrorMessage = "Validation Failed: No Service Network";
        return false;
    }
    
    if (mpServiceNetwork->serviceLocations().empty()
        || mpServiceNetwork->serviceLinks().empty())
    {
        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "no service locations or links";
            
            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << "\n";
        }
        
        mRoutes->mErrorMessage = "Validation Failed: Invalid Service Network";
        return false;
    }
    
    if (!mpShipments)
    {
        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "no shipments (nullptr)";
      
            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << "\n";
        }

        mRoutes->mErrorMessage = "Validation Failed: No Shipment Information";
        return false;
    }
    
    if (mpShipments->getNumOfCommodityBundles() == 0)
    {
        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "no commodities to move";
            
            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << "\n";
        }
        
        mRoutes->mErrorMessage = "Validation Failed: Empty Shipment Information";
        return false;
    }
    
    for (auto& shipment : mpShipments->getShipments())
    {
        std::string location_name = shipment->getSourceName();
        auto location = mpServiceNetwork->findLocation(location_name);

        if (!location)
        {
            if (mVerboseMode || mPrintTimingInfo)
            {
                *mpDebugOutput << "unknown source location";

                if (mPrintTimingInfo)
                {
                    print_end_elapse_times(*mpDebugOutput, start);
                }

                *mpDebugOutput << "\n";
            }
            
            mRoutes->mErrorMessage = "Validation Failed: Unknown source location \"";
            mRoutes->mErrorMessage += location_name + "\" for shipment " + shipment->getName();
            return false;
        }
        
        shipment->setSourceLocation(location);
        
        location_name = shipment->getDestinationName();
        location = mpServiceNetwork->findLocation(location_name);
            
        if (!location)
        {
            if (mVerboseMode || mPrintTimingInfo)
            {
                *mpDebugOutput << "unknown destination location";

                if (mPrintTimingInfo)
                {
                    print_end_elapse_times(*mpDebugOutput, start);
                }

                *mpDebugOutput << "\n";
            }
            
            mRoutes->mErrorMessage = "Validation Failed: Unknown destination location \"";
            mRoutes->mErrorMessage += location_name + "\" for shipment " + shipment->getName();
            return false;
        }
        
        shipment->setDestinationLocation(location);
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << " done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << "\n";
    }
    
    return true;
}

void cCTSNDP::ensureSourceArcCapacityIsWithinLimits()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "Ensure source arcs are within capacity limits ";

		if (mPrintTimingInfo)
		{
			std::time_t start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "...";
	}

	/**
	 * Determine which links are over capacity
	 */
	struct link_info_t
	{
		double total_quantity;
		std::map<double, commodities_t> commodities;
	};

	std::map<cServiceLink*, link_info_t> link_capacities;

	for (auto shipment : mpShipments->getShipments())
	{
		for (auto commodity : shipment->getCommodityBundles())
		{
			auto loc = commodity->getSourceLocation();

			auto links = mpServiceNetwork->findLinksWithSource(*loc);

			commodity->getSourceNode()->getOutflows();

			// We can only adjust EATs if there is a single link
			// at the source location!
			if (links.size() != 1)
				continue;

			commodity->getSourceNode()->getOutflows();

			auto& link = links.front();

			if (link->getCycleTime() > 0)
			{
				double eat = commodity->getEarliestAvailableTime();
				auto it = link_capacities.find(link);
				if (it == link_capacities.end())
				{
					link_info_t info;
					info.total_quantity = commodity->getQuantity();
					commodities_t commodities;
					commodities.push_back(commodity);
					info.commodities.insert(std::make_pair(eat, commodities));
					link_capacities.insert(std::make_pair(link, info));
				}
				else
				{
					link_info_t& info = it->second;
					info.total_quantity += commodity->getQuantity();
					auto eat_it = info.commodities.find(eat);
					if (eat_it == info.commodities.end())
					{
						commodities_t commodities;
						commodities.push_back(commodity);
						info.commodities.insert(std::make_pair(eat, commodities));
					}
					else
					{
						(eat_it->second).push_back(commodity);
					}
				}
			}
		}
	}

	// This is a safety check and should never really happen.
	if (link_capacities.empty())
	{
		if (mVerboseMode || mPrintTimingInfo)
		{
			*mpDebugOutput << " done";

			if (mPrintTimingInfo)
			{
				print_end_elapse_times(*mpDebugOutput, start);
			}

			*mpDebugOutput << ", found 0\n";
		}
		return;
	}

	ctsndp::arcs_over_capacity_t arcs_over_capacity;

	for (auto[link, info] : link_capacities)
	{
		double link_max_capacity = link->getCapacity() * link->getCommodityBundleCapacity();

		if (info.total_quantity > link_max_capacity)
		{
			for (auto[eat, commodities] : info.commodities)
			{
				ctsndp::over_capacity_t over_capacity;
				over_capacity.dispatch_time = 0.0;
				over_capacity.utilization = 0.0;
				over_capacity.arc = nullptr;

				over_capacity.dispatch_time = eat;
				auto node = commodities.front()->getSourceNode();
				for (auto arc : node->getOutflows())
				{
					if (arc->getServiceLink() == link)
					{
						over_capacity.arc = arc;
						break;
					}
				}

				for (auto k : commodities)
				{
					over_capacity.utilization += k->getQuantity();
				}

				arcs_over_capacity.push_back(over_capacity);
			}
		}
	}

	// If we don't have any links over-capacity, WE ARE DONE.
	if (arcs_over_capacity.empty())
	{
		if (mVerboseMode || mPrintTimingInfo)
		{
			*mpDebugOutput << " done";

			if (mPrintTimingInfo)
			{
				print_end_elapse_times(*mpDebugOutput, start);
			}

			*mpDebugOutput << ", found 0\n";
		}
		return;
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << ", Found " << arcs_over_capacity.size() << " ";
	}

	ctsndp::ensure_capacities(*mpPartiallyTimeExpandedNetwork,
		std::move(arcs_over_capacity),
		mConsolidationTime, nullptr);

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << " done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << "\n";
	}
}

void cCTSNDP::BuildRoutes()
{
    mRoutes->mIdealTotalCost = mMinimalCost;
    mRoutes->mIdealFixedCost = mMinimalFixedCost;
    mRoutes->mIdealVariableCost = mMinimalVariableCost;
    mRoutes->mIdealDelayCost = mMinimalDelayCost;
	mRoutes->mIdealVariableDelayCost = mMinimalVariableDelayCost;

    // Add all link utilization information
    for (auto& arc : mpPartiallyTimeExpandedNetwork->arcs())
    {
		auto ij = to_index(arc);
		if (mUtilization_ij.find(ij) == mUtilization_ij.end())
			continue;

        auto utilization = mUtilization_ij[ij];
        if (utilization == 0)
            continue;
        
        if (arc->isDelayArc())
        {
            if (mRoutes->mLinkUtilizations.find(::to_string(*arc)) == mRoutes->mLinkUtilizations.end())
            {
                mRoutes->mLinkUtilizations[::to_string(*arc)] = utilization;
            }
            continue;
        }
        
        auto link = arc->getServiceLink();
		if (link)
		{
			if (mRoutes->mLinkUtilizations.find(link->getName()) == mRoutes->mLinkUtilizations.end())
			{
				mRoutes->mLinkUtilizations[link->getName()] = utilization;
			}
			else
			{
				mRoutes->mLinkUtilizations[link->getName()] += utilization;
			}
		}
    }

    // Determine the total quantity of commodities that flow over a given link.
    std::map<link_ptr_t, unsigned int> q_l;

    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        commodity_ptr_t k = it->first;
        auto& path = it->second;
        if (path.empty())
        {
            continue;
        }

        for (auto& arc : path)
        {
            auto link = arc->getServiceLink();
            if (link)
            {
                if (q_l.find(link) == q_l.end())
                    q_l[link] = k->getQuantity();
                else
                    q_l[link] += k->getQuantity();
            }
        }
    }

    // Add all path information
	mRoutes->mFixedCost = 0.0;
	mRoutes->mDelayCost = 0.0;
	mRoutes->mVariableCost = 0.0;
	mRoutes->mVariableDelayCost = 0.0;

    for(auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
    {
        commodity_ptr_t k = it->first;
        auto& path = it->second;
        if (path.empty())
        {
            continue;
        }

        double q_k = k->getQuantity();
        sCommodityPath tmp_path(k);
		tmp_path.fixedCost = 0.0;
		tmp_path.delayCost = 0.0;
		tmp_path.variableCost = 0.0;
		tmp_path.variableDelayCost = 0.0;
        
        cPartiallyTimeExpandedNetworkArc* arc;
        for (auto it = path.begin(); it != path.end(); ++it)
        {
            arc = *it;
            
            // We don't care about the storage (holdover or hold) arcs in our path
			if (arc->isStorageArc())
				continue;
            
            double arc_cost = 0.0;
			double variable_cost = 0.0;
			double utilization = 0.0;

            if (arc->isDelayArc())
            {
                if (!static_cast<cPartiallyTimeExpandedNetworkDelayArc*>(arc)->hasCommodity(k))
                    std::cout << "Oops wrong commodity!" << std::endl;
                utilization = mUtilization_ij[to_index(arc)];

				// We only add the fixed delay cost if the arc ends at our destination!!
				if (arc->getDestinationNode() == k->getDestinationNode())
				{
					double f_ij = lower_limit(k->getDelayCost(), 0.0);
					arc_cost = f_ij;
					tmp_path.delayCost += arc_cost;
				}

				double delay_penalty = k->getDelayPenaltyRate() * std::fabs(arc->getActualTravelTime());
				double c_ij = lower_limit(delay_penalty, 0.0);

				variable_cost = c_ij * q_k;
				tmp_path.variableDelayCost += variable_cost;
				if (k->hasDelayPenaltyLimit())
					tmp_path.variableDelayCost = std::min(tmp_path.variableDelayCost, k->getDelayPenaltyLimit());
			}
            else
            {
				auto link = arc->getServiceLink();
				if (link)
				{
					utilization = mRoutes->mLinkUtilizations[link->getName()];

					if ((utilization >= 1) && (q_l[link] > 1))
					{
						utilization *= q_k / q_l[link];
					}
				}

				double f_ij = lower_limit(arc->getFixedCost(), 0.0);
				arc_cost = f_ij * utilization;
                tmp_path.fixedCost += arc_cost;
			
				double c_ij = lower_limit(arc->getPerUnitOfFlowCost(), 0.0);
				variable_cost = c_ij * q_k;
				tmp_path.variableCost += variable_cost;
			}

            sLeg leg;
            leg.fixedCost = arc_cost;
            leg.variableCost = variable_cost;
            leg.departureTime = mDispatchTime_ki[{k, arc->getSource()}];
            leg.quantity = static_cast<int>(q_k);
			leg.utilization = utilization;
            leg.pLink = arc->getServiceLink();
            
            tmp_path.path.push_back(leg);
        }

        mRoutes->mFixedCost += tmp_path.fixedCost;
        mRoutes->mDelayCost += tmp_path.delayCost;
        mRoutes->mVariableCost += tmp_path.variableCost;
		mRoutes->mVariableDelayCost += tmp_path.variableDelayCost;
		mRoutes->mPaths.push_back(tmp_path);
    }

    mRoutes->mTotalCost = mRoutes->mFixedCost + mRoutes->mVariableCost
								+ mRoutes->mDelayCost + mRoutes->mVariableDelayCost;

    // Report all mismatches in the commodity dispatch times...
	for (auto& arc : mJ_a)
	{
		const auto& groups = mJ_ak[arc];
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;

					if (std::fabs(mMismatchTimes_ki[{k1, k2, arc}]) > 0.1)
					{
						auto link = arc->getServiceLink();
						if (!link)
							continue;

						auto it = std::find_if(mRoutes->mMismatches.begin(), mRoutes->mMismatches.end(),
							[&](sMismatchedDispatch& mismatch) {return mismatch.pLink == link; });
						if (it == mRoutes->mMismatches.end())
						{
							sMismatchedDispatch mismatch(link);
							mismatch.addDispatch(k1, mDispatchTime_ki[{k1, arc->getSource()}]);
							mismatch.addDispatch(k2, mDispatchTime_ki[{k2, arc->getSource()}]);
							mRoutes->mMismatches.push_back(mismatch);
						}
						else
						{
							it->addDispatch(k1, mDispatchTime_ki[{k1, arc->getSource()}]);
							it->addDispatch(k2, mDispatchTime_ki[{k2, arc->getSource()}]);
						}

					}
				}
			}
		}
	}

    // Sanity check to make sure none of the routes are empty
    for (auto& route : mRoutes->mPaths)
    {
        if (route.path.empty())
        {
            mRoutes->mSuccess = false;
            mRoutes->mErrorMessage += "Route for ";
            mRoutes->mErrorMessage += route.pCommodity->getFullyQualifiedName();
            mRoutes->mErrorMessage += " is empty!\n";
        }
    }
}

bool cCTSNDP::MinimizeCosts()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
		*mpDebugOutput << "\n\n";
		*mpDebugOutput << "Minimizing Costs";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }
    
    std::unique_ptr<cMinimizeCostsModel> pModel;
    try
    {
#ifdef USE_CPLEX
		pModel.reset(new cMinimizeCostsModel_CPlex(mPass,
										*mpPartiallyTimeExpandedNetwork,
										*mpServiceNetwork,
										mVerboseMode, mPrintTimingInfo));
#else
		pModel = cMinimizeCostsModel_Gurobi::create(mPass,
				*mpPartiallyTimeExpandedNetwork,
				*mpServiceNetwork,
				mVerboseMode, mPrintTimingInfo);
#endif
        
        pModel->setNumericFocus(mNumericFocus);
        pModel->logToConsole(mLogToConsole);
        pModel->logFilename(mLogFilename);
        pModel->setTimeLimit_sec(mTimeLimit_sec);
        pModel->outputDebugMsgs(mDebugLevel5);
        pModel->setDebugOutput(*mpDebugOutput);
		pModel->mLogPath = mLogPath;
		pModel->mLogX_kij = mLogCommodityPaths;
		pModel->mLogY_ij = mLogUtilization;
		pModel->mLogConstraints = mLogMinimizeCostConstraints;
		pModel->ignoreRequiredLocations(mIgnoreRequireLocations);
		pModel->readTuningParameters(mMinCostParameterFileName);

		if (mLogProgress)
		{
			pModel->setProgressOutput(mProgressOutput);
		}


        pModel->BuildModel();
    }
    catch(const cOptimizationException& e)
    {
        if (mVerboseMode)
            *mpDebugOutput << "Optimization Exception: " << e.what() << std::endl;
		mRoutes->mErrorMessage = "In \"MinimizeCostsModel\", ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    try
    {
        bool result = false;
        try {
            result = pModel->Optimize();
            if (result)
            {
                mMinimalCost = pModel->getMinimalCost();
                std::tie(mMinimalFixedCost, mMinimalDelayCost) = pModel->getMinimalFixedCost();
                std::tie(mMinimalVariableCost, mMinimalVariableDelayCost) = pModel->getMinimalVariableCost();
                
                mP_ki.clear();
                mP_ki = pModel->returnCommodityPathsByNode();
                
                mP_ka.clear();
                mP_ka = pModel->returnCommodityPathsByArc();
                
                mUtilization_ij.clear();
                mUtilization_ij = pModel->getArcUtilization();

				if (mLogCommodityPaths)
				{
					std::string filename = mLogPath + "CommodityPaths_";
					filename += std::to_string(mPass) + ".txt";
					std::ofstream file(filename);
					DumpCommodityPaths(file);
					file.close();
				}

                if (mDebugLevel2)
                {
                    DumpCommodityPaths(*mpDebugOutput);
                }
                
                FindAllSharedArcs();
            }
        }
        catch (const ctsndp::cInfeasibleSolution&)
        {
            if (mQuiteMode)
                throw;
            
            if (mVerboseMode)
                *mpDebugOutput << "model is infeasible.  Relaxing constraints...";
            
            pModel->RelaxConstraints();

            try {
                result = pModel->Optimize();

                if (mVerboseMode)
                {
                    *mpDebugOutput << "Finding the \"Minimize Costs\" constraint violations...";
                }

				if (mLogMinimizeCostConstraintViolations)
				{
					std::string filename = mLogPath + "MinimizeCostsConstraintViolations_";
					filename += std::to_string(mPass) + ".txt";
					std::ofstream file(filename);
					pModel->FindConstraintViolations(file);
					file.close();
				}

				if (mLogCommodityPaths)
				{

					mP_ki.clear();
					mP_ki = pModel->returnCommodityPathsByNode();

					mP_ka.clear();
					mP_ka = pModel->returnCommodityPathsByArc();

					mUtilization_ij.clear();
					mUtilization_ij = pModel->getArcUtilization();

					std::string filename = mLogPath + "CommodityPaths_";
					filename += std::to_string(mPass) + ".txt";
					std::ofstream file(filename);
					DumpCommodityPaths(file);
					file.close();
				}

				pModel->FindConstraintViolations(*mpDebugOutput);
			}
            catch (const ctsndp::cInfeasibleSolution&)
            {
                if (mVerboseMode)
                {
                    *mpDebugOutput << "model is infeasible.  Computing IIS...";
                }
                
                std::string msg = pModel->ComputeIIS();
                
                if (!mQuiteMode)
                {
                    *mpDebugOutput << msg << std::endl;
                }

                if (mVerboseMode)
                {
                    *mpDebugOutput << "infeasible\n";
                }

                if (mDebugLevel1)
                {
                    *mpDebugOutput << msg << "\n";
                }
                
				throw ctsndp::cInfeasibleSolution("MinimizeCosts", msg);
			}

			throw;
		}
        
        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "done";
            
            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << std::endl;
        }
        
        return result;
    }
    catch(const cOptimizationException& e)
    {
        if (mVerboseMode)
            *mpDebugOutput << "GRBException: " << e.what() << std::endl;

		mRoutes->mErrorMessage = "MinimizeCosts: ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "failed";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << "\n";
    }
    
    return false;
}

/**
 * Implementation of the algorithm list in section 4.3 on page 1312
 */
std::set<arc_ptr_t> cCTSNDP::IdentifyArcsToLengthen()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
		*mpDebugOutput << "\n\n";
		*mpDebugOutput << "Identifying Arcs to Lengthen";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... ";
    }
 
	assert(validate_paths(mP_ki, mP_ka));

    std::unique_ptr<cIdentifyArcsToLengthenModel> pModel;
    try
    {
#ifdef USE_CPLEX
		pModel.reset(new cIdentifyArcsToLengthenModel_CPlex(mPass,
			*mpPartiallyTimeExpandedNetwork,
			*mpServiceNetwork, mP_ki, mP_ka, mJ_ak,
			mVerboseMode, mPrintTimingInfo));
#else
		pModel = cIdentifyArcsToLengthenModel_Gurobi::create(mPass,
											*mpPartiallyTimeExpandedNetwork,
											*mpServiceNetwork, mP_ki, mP_ka, mJ_ak,
											mVerboseMode, mPrintTimingInfo);
#endif
        
        pModel->setNumericFocus(mNumericFocus);
        pModel->logToConsole(mLogToConsole);
        pModel->logFilename(mLogFilename);
        pModel->setTimeLimit_sec(mTimeLimit_sec);
        pModel->outputDebugMsgs(mDebugLevel1);
        pModel->outputIdentifyArcsTimeInfo(mDebugLevel2);
        pModel->outputCommodityScheduleInfo(mDebugLevel3);
        pModel->setDebugOutput(*mpDebugOutput);
		pModel->mLogPath = mLogPath;
		pModel->mLogCommodityScheduleInfo = mLogCommoditySchedules;
		pModel->mLogIdentifyArcsTimeInfo = mLogIdentifyArcsTimeInfo;
		pModel->readTuningParameters(mIdentArcsToLenParameterFileName);

		if (mLogProgress)
		{
			pModel->setProgressOutput(mProgressOutput);
		}

		pModel->BuildModel();
	}
    catch(const cOptimizationException& e)
    {
		mRoutes->mErrorMessage = "In \"IdentifyArcsToLengthenModel\", ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    try
    {
        std::set<arc_ptr_t> arc_to_lengthen;
        try {
            if (pModel->Optimize())
            {
                arc_to_lengthen  = pModel->returnArcsToLengthen();
                mDispatchTime_ki = pModel->returnDispatchTimes();
                mTravelTime_kij  = pModel->returnTravelTimes();
            }
        }
        catch (const ctsndp::cInfeasibleSolution& e)
        {
#ifdef CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP
			if (mVerboseMode || mPrintTimingInfo)
			{
				*mpDebugOutput << "\n" << e.what() << "\n";
				*mpDebugOutput << "The \"Identify Arcs To Lengthen\" model is infeasible.";
			}
			throw;
#else
            if (mQuiteMode)
                throw;

			*mpDebugOutput << "\n" << e.what() << "\n";
            *mpDebugOutput << "model is infeasible.  Relaxing constraints...";
            pModel->RelaxConstraints();

            try
            {
                pModel->Optimize();
                
                if (mVerboseMode)
                {
                    *mpDebugOutput << "Finding the \"Identify Arcs To Lengthen\" constraint violations...";
                }
                
                if (mVerboseMode)
                {
                    *mpDebugOutput << "done\n";
                }

				if (mLogIdentifyArcsConstraintViolations)
				{
					std::string filename = mLogPath + "IdentifyArcsConstraintViolations_";
					filename += std::to_string(mPass) + ".txt";
					std::ofstream file(filename);
					pModel->FindConstraintViolations(file);
					file.close();
				}

				pModel->FindConstraintViolations(*mpDebugOutput);
            }
            catch (const ctsndp::cInfeasibleSolution&)
            {
                if (mVerboseMode)
                {
                    *mpDebugOutput << "model is infeasible.  Computing IIS...";
                }
                
                std::string msg = pModel->ComputeIIS();
                
                if (mVerboseMode)
                {
                    *mpDebugOutput << "done\n";
                }

                throw ctsndp::cInfeasibleSolution("IdentifyArcs", msg);
            }

			throw ctsndp::cInfeasibleSolution("IdentifyArcs", "Constraint Violations!");
#endif
		}
 
        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "done";
            
            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << "\n";
        }
        
        return arc_to_lengthen;
    }
    catch(const cOptimizationException& e)
    {
		mRoutes->mErrorMessage = "IdentifyArcs: ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }
}

/**
 * Implementation of the algorithm list in section 4.4 on page 1313
 */
#ifdef CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP
std::set<arc_ptr_t> cCTSNDP::ConstructFeasibleSolution()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\n";
		*mpDebugOutput << "Construct Feasible Solution";

		if (mPrintTimingInfo)
		{
			std::time_t start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... ";
	}

	assert(validate_paths(mP_ki, mP_ka));

	std::unique_ptr<cConstructFeasibleSolutionModel> pModel;
	try
	{
#ifdef USE_CPLEX
		pModel.reset(new cConstructFeasibleSolutionModel_CPlex(mPass,
			*mpPartiallyTimeExpandedNetwork,
			*mpServiceNetwork, mP_ki, mP_ka, mJ_ak,
			mVerboseMode, mPrintTimingInfo));
#else
		pModel = cConstructFeasibleSolutionModel_Gurobi::create(mPass,
			*mpPartiallyTimeExpandedNetwork,
			*mpServiceNetwork, mP_ki, mP_ka, mJ_ak,
			mVerboseMode, mPrintTimingInfo);
#endif

		pModel->setNumericFocus(mNumericFocus);
		pModel->logToConsole(mLogToConsole);
		pModel->logFilename(mLogFilename);
		pModel->setTimeLimit_sec(mTimeLimit_sec);
		pModel->outputDebugMsgs(mDebugLevel5);
		pModel->setDebugOutput(*mpDebugOutput);
		pModel->mLogPath = mLogPath;
		pModel->mLogGamma_ki = mLogCommoditySchedules;
		pModel->mLogDelta_kk_ij = mLogMismatches;
		pModel->readTuningParameters(mConstructFeasSolParameterFileName);

		if (mLogProgress)
		{
			pModel->setProgressOutput(mProgressOutput);
		}

		pModel->BuildModel();
	}
	catch (const cOptimizationException& e)
	{
		mRoutes->mErrorMessage = "In \"ConstructFeasibleSolutionModel\", ";
		mRoutes->mErrorMessage += e.what();
		throw std::runtime_error(mRoutes->mErrorMessage);
	}

	try
	{
		std::set<arc_ptr_t> arc_to_lengthen;

		if (pModel->Optimize())
		{
			arc_to_lengthen = pModel->returnArcsToLengthen();

			mDispatchTime_ki.clear();
			mDispatchTime_ki = pModel->returnDispatchTimes();

			mMismatchTimes_ki.clear();
			mMismatchTimes_ki = pModel->returnMismatchTimes();

			if (mVerboseMode || mPrintTimingInfo)
			{
				*mpDebugOutput << "done";

				if (mPrintTimingInfo)
				{
					print_end_elapse_times(*mpDebugOutput, start);
				}

				*mpDebugOutput << "\n";
			}

			return arc_to_lengthen;
		}
	}
	catch (const ctsndp::cInfeasibleSolution&)
	{
		if (mQuiteMode)
			throw;

		*mpDebugOutput << "model is infeasible.  Relaxing constraints...";
		pModel->RelaxConstraints();

		try
		{
			pModel->Optimize();

			if (mVerboseMode)
			{
				*mpDebugOutput << "Finding the \"Construct Feasible Solution\" constraint violations...";
			}

			if (mLogConstructSolutionConstraintViolations)
			{
				std::string filename = mLogPath + "ConstructSolutionConstraintViolations_";
				filename += std::to_string(mPass) + ".txt";
				std::ofstream file(filename);
				pModel->FindConstraintViolations(file);
				file.close();
			}

			pModel->FindConstraintViolations(*mpDebugOutput);
		}
		catch (const ctsndp::cInfeasibleSolution&)
		{
			if (mVerboseMode)
			{
				*mpDebugOutput << "model is infeasible.  Computing IIS...";
			}

			std::string msg = pModel->ComputeIIS();

			if (mVerboseMode)
			{
				*mpDebugOutput << "done\n";
			}

			throw ctsndp::cInfeasibleSolution("ConstructSolution", msg);
		}
	}

	catch (const cOptimizationException& e)
	{
		mRoutes->mErrorMessage = "ConstructFeasibleSolution: ";
		mRoutes->mErrorMessage += e.what();
		throw std::runtime_error(mRoutes->mErrorMessage);
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "failed";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << "\n";
	}

	throw ctsndp::cInfeasibleSolution("ConstructSolution", "See constraint violations...");
}
#else
bool  cCTSNDP::ConstructFeasibleSolution()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
		*mpDebugOutput << "\n\n";
		*mpDebugOutput << "Construct Feasible Solution";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... ";
    }
    
	assert(validate_paths(mP_ki, mP_ka));

	std::unique_ptr<cConstructFeasibleSolutionModel> pModel;
    try
    {
#ifdef USE_CPLEX
		pModel.reset(new cConstructFeasibleSolutionModel_CPlex(mPass,
										*mpPartiallyTimeExpandedNetwork,
										*mpServiceNetwork, mP_ki, mP_ka, mJ_ak,
										mVerboseMode, mPrintTimingInfo));
#else
		pModel.reset( new cConstructFeasibleSolutionModel_Gurobi(mPass,
										*mpPartiallyTimeExpandedNetwork,
                                        *mpServiceNetwork, mP_ki, mP_ka, mJ_ak,
                                        mVerboseMode, mPrintTimingInfo) );
#endif
        
        pModel->setNumericFocus(mNumericFocus);
        pModel->logToConsole(mLogToConsole);
        pModel->logFilename(mLogFilename);
        pModel->setTimeLimit_sec(mTimeLimit_sec);
        pModel->outputDebugMsgs(mDebugLevel5);
        pModel->setDebugOutput(*mpDebugOutput);
		pModel->mLogPath = mLogPath;
		pModel->mLogGamma_ki = mLogCommoditySchedules;
		pModel->mLogDelta_kk_ij = mLogMismatches;
		pModel->readTuningParameters(mConstructFeasSolParameterFileName);

		if (mLogProgress)
		{
			pModel->setProgressOutput(mProgressOutput);
		}

        pModel->BuildModel();
    }
    catch(const cOptimizationException& e)
    {
		mRoutes->mErrorMessage = "ConstructSolution: ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    try
    {
        if (pModel->Optimize())
        {
            mDispatchTime_ki.clear();
            mDispatchTime_ki = pModel->returnDispatchTimes();
            
            mMismatchTimes_ki.clear();
            mMismatchTimes_ki = pModel->returnMismatchTimes();

            if (mVerboseMode || mPrintTimingInfo)
            {
                *mpDebugOutput << "done";
                   
                if (mPrintTimingInfo)
                {
                    print_end_elapse_times(*mpDebugOutput, start);
                }

                *mpDebugOutput << "\n";
            }
            
            return true;
        }
    }
    catch (const ctsndp::cInfeasibleSolution&)
    {
        if (mQuiteMode)
            throw;

        *mpDebugOutput << "model is infeasible.  Relaxing constraints...";
        pModel->RelaxConstraints();

        try
        {
            pModel->Optimize();

            if (mVerboseMode)
            {
                *mpDebugOutput << "Finding the \"Construct Feasible Solution\" constraint violations...";
            }

			if (mLogConstructSolutionConstraintViolations)
			{
				std::string filename = mLogPath + "ConstructSolutionConstraintViolations_";
				filename += std::to_string(mPass) + ".txt";
				std::ofstream file(filename);
				pModel->FindConstraintViolations(file);
				file.close();
			}

			pModel->FindConstraintViolations(*mpDebugOutput);
        }
        catch (const ctsndp::cInfeasibleSolution&)
        {
            if (mVerboseMode)
            {
                *mpDebugOutput << "model is infeasible.  Computing IIS...";
            }

            std::string msg = pModel->ComputeIIS();

            if (mVerboseMode)
            {
                *mpDebugOutput << "done\n";
            }

            throw ctsndp::cInfeasibleSolution("ConstructSolution", msg);
        }
    }
        
    catch(const cOptimizationException& e)
    {
		mRoutes->mErrorMessage = "ConstructFeasibleSolution: ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "failed";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << "\n";
    }
    
    return false;
}
#endif


ctsndp::arcs_to_shorten_t cCTSNDP::MinimizeDelays()
{
    if (!mpPartiallyTimeExpandedNetwork->hasDelays())
        return ctsndp::arcs_to_shorten_t();

    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
		*mpDebugOutput << "\n\n";
		*mpDebugOutput << "Minimize Delays";
        
        if (mPrintTimingInfo)
        {
             std::time_t start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... ";
    }
    
    std::unique_ptr<cMinimizeDelaysModel> pModel;
    try
    {
#ifdef USE_CPLEX
		pModel.reset(new cMinimizeDelaysModel_CPlex(mPass,
										*mpPartiallyTimeExpandedNetwork,
										*mpServiceNetwork, mP_ka, mDispatchTime_ki,
										mVerboseMode, mPrintTimingInfo));
#else
		pModel = cMinimizeDelaysModel_Gurobi::create(mPass,
										*mpPartiallyTimeExpandedNetwork,
                                        *mpServiceNetwork, mP_ka, mDispatchTime_ki,
                                        mVerboseMode, mPrintTimingInfo);
#endif
        
        pModel->setNumericFocus(mNumericFocus);
        pModel->logToConsole(mLogToConsole);
        pModel->logFilename(mLogFilename);
        pModel->setTimeLimit_sec(mTimeLimit_sec);
        pModel->outputDebugMsgs(mDebugLevel5);
        pModel->setDebugOutput(*mpDebugOutput);
		pModel->readTuningParameters(mMinDelaysParameterFileName);

		if (mLogProgress)
		{
			pModel->setProgressOutput(mProgressOutput);
		}

        pModel->BuildModel();
    }
    catch(const cOptimizationException& e)
    {
		mRoutes->mErrorMessage = "In \"MinimizeDelaysModel\", ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    try
    {
		ctsndp::arcs_to_shorten_t arc_to_shorten;
		try
        {
            if (pModel->Optimize())
            {
                arc_to_shorten  = pModel->returnArcsToShorten();
            }
        }
        catch (const ctsndp::cInfeasibleSolution&)
        {
            if (mQuiteMode)
                throw;

            *mpDebugOutput << "model is infeasible.  Relaxing constraints...";
            pModel->RelaxConstraints();

            try
            {
                pModel->Optimize();

                if (mVerboseMode)
                {
                    *mpDebugOutput << "Finding the \"Minimize Delays\" constraint violations...";
                }

				if (mLogMinimizeDelayConstraintViolations)
				{
					std::string filename = mLogPath + "MinimizeDelaysConstraintViolations_";
					filename += std::to_string(mPass) + ".txt";
					std::ofstream file(filename);
					pModel->FindConstraintViolations(file);
					file.close();
				}

				pModel->FindConstraintViolations(*mpDebugOutput);
            }
            catch (const ctsndp::cInfeasibleSolution&)
            {
                if (mVerboseMode)
                {
                    *mpDebugOutput << "model is infeasible.  Computing IIS...";
                }

                std::string msg = pModel->ComputeIIS();

                if (mVerboseMode)
                {
                    *mpDebugOutput << "done\n";
                }

                throw ctsndp::cInfeasibleSolution("MinimizeDelays", msg);
            }
        }

        if (mVerboseMode || mPrintTimingInfo)
        {
            *mpDebugOutput << "done";

            if (mPrintTimingInfo)
            {
                print_end_elapse_times(*mpDebugOutput, start);
            }

            *mpDebugOutput << "\n";
        }

        return arc_to_shorten;
    }
    catch(const cOptimizationException& e)
    {
		mRoutes->mErrorMessage = "MinimizeDelays: ";
		mRoutes->mErrorMessage += e.what();
        throw std::runtime_error(mRoutes->mErrorMessage);
    }

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "failed";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << "\n";
    }
    
    return ctsndp::arcs_to_shorten_t();
}


/**
 * Find the arcs that are used to transport more than on commodity
 */
void cCTSNDP::FindAllSharedArcs()
{
    auto& commodities = mpPartiallyTimeExpandedNetwork->commodities();
    if (commodities.empty())
        return;
 
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tFinding all shared arcs";
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

    // We are looking for all arcs carrying more than one commodity: Ja page 1312
    for (auto& k : commodities)
    {
        for (auto& arc : mP_ka[k])
        {
            if (mN_a.find(arc) == mN_a.end())
            {
                mN_a[arc] = 1;
                mQ_a[arc] = k->getQuantity();
            }
            else
            {
                mN_a[arc] = mN_a[arc] + 1;
                mQ_a[arc] += k->getQuantity();
            }
        }
    }

    if (mDebugLevel3)
        DumpConsolidationCount(*mpDebugOutput);
    
    // Count only arc with count greater than one
    for (auto& [arc, count] : mN_a)
    {
		// Sharing any sort of storage arc does not make
		// sense as there is not cost savings
		if (arc->isStorageArc())
			continue;

		// Can't share any arc that passes through a delay node as they
		// are commodity specific
		if ( arc->connectedToDelayNode())
            continue;

        if (count > 1)
            mJ_a.push_back(arc);
    }


	ctsndp::dispatch_time_t dispatchTime_ki;
	for (auto& k : commodities)
	{
		auto& path = mP_ka[k];
		if (path.empty())
			continue;

		double travel_time = 0.0;
		double t = k->getEarliestAvailableTime();
		for (auto& arc : path)
		{
			dispatchTime_ki[{k, arc->getSource()}] = t;
			t += arc->getActualTravelTime();
			travel_time += arc->getActualTravelTime();
		}

		if (t > k->getLatestDeliveryTime())
		{
			double tt = k->getLatestDeliveryTime() - k->getEarliestAvailableTime();
			std::cout << "Invalid path for commodity: " << *k << ", ldt-eat =" << tt << ", travel time = " << travel_time << "\n";
		}
	}


    if (!mAllowConsolidation)
    {
        if (mDebugLevel2 || mPrintTimingInfo)
        {
            *mpDebugOutput << " *** Consolidations have been turned off! ***";
            if (!mPrintTimingInfo)
                *mpDebugOutput << "\n";
        }
    
        if (mVerboseMode || mPrintTimingInfo)
        {
			*mpDebugOutput << " *** Consolidations have been turned off! ***";
            if (mPrintTimingInfo)
                print_end_elapse_times(*mpDebugOutput, start);
            *mpDebugOutput << std::endl;
        }
        
        return;
    }


	std::map<commodity_ptr_t, std::vector<link_ptr_t>> exclude_links;
	for (auto& k : commodities)
	{
		if (k->hasRequiredLocations())
		{
			std::vector<link_ptr_t> restricted_links;
			auto required_locations = k->getRequiredLocations();
			for (auto& required_location : required_locations)
			{
				for (auto& arc : mJ_a)
				{
					// Test to see if a "PROCESSING" arc has already been added.
					if (arc->getServiceLink()->getName() == required_location + ctsndp::PROCESSING)
						restricted_links.push_back(arc->getServiceLink());
				}
			}

			required_locations = k->getShipment().getRequiredLocations();
			for (auto& required_location : required_locations)
			{
				for (auto& arc : mJ_a)
				{
					if (!arc->getServiceLink())
						continue;

					// Test to see if a "PROCESSING" arc has already been added.
					if (arc->getServiceLink()->getName() == required_location + ctsndp::PROCESSING)
						restricted_links.push_back(arc->getServiceLink());
				}
			}

			exclude_links.insert(std::make_pair(k, restricted_links));
		}
	}

	ctsndp::commodities_t ks;

	// We are looking for commodity pairs sharing an arc: Ja(k1,k2) page 1312
	for (auto& arc : mJ_a)
	{
		if (mLimitConsolidationByCapacity)
		{
			if (!arc->arcCapacityIsLimited())
				continue;
		}

		ks.clear();
		for (auto& k : commodities)
		{
/*BAF
			if (k->hasRequiredLocations())
			{
				const auto& link = arc->getServiceLink();

				auto it = exclude_links.find(k);
				if (it != exclude_links.end())
				{
					auto& restricted_links = it->second;
					if (std::find(restricted_links.begin(), restricted_links.end(), link) != restricted_links.end())
					{
						// Nope, move on to the next commodity.
						continue;
					}
				}
			}
*/

			auto& path = mP_ka[k];
			// Does this commodity use this arc...
			if (std::find(path.begin(), path.end(), arc) == path.end())
			{
				// Nope, move on to the next commodity.
				continue;
			}

			// Yes, add it to our list
			ks.push_back(k);
		}

		if (ks.size() > 1)
		{
			std::vector<ctsndp::commodities_t> groups;
			ctsndp::commodities_t k_group;

			while (ks.size() > 1)
			{
				auto it = ks.begin();
				k_group.push_back(*it);
				double eat = (*it)->getEarliestAvailableTime();
				double ldt = (*it)->getLatestDeliveryTime();
				double lower_cutoff = eat + (ldt - eat) * 0.25;
				double upper_cutoff = ldt - (ldt - eat) * 0.25;
				for (++it; it != ks.end(); ++it)
				{
					double e = (*it)->getEarliestAvailableTime();
					double l = (*it)->getLatestDeliveryTime();
//					if (((eat <= e) && (e < lower_cutoff)) ||
//						((upper_cutoff < l) && (l <= ldt)))
					if ((eat == e) && (l == ldt))
					{
						k_group.push_back(*it);
					}
				}

				for (auto k : k_group)
				{
					auto it = std::find(ks.begin(), ks.end(), k);
					ks.erase(it);
				}


				if (k_group.size() > 1)
					groups.push_back(std::move(k_group));

				k_group.clear();
			}


			if (!groups.empty())
				mJ_ak.insert(std::make_pair(arc, groups));
        }
    }

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << " done";
		if (mPrintTimingInfo)
			print_end_elapse_times(*mpDebugOutput, start);
		*mpDebugOutput << std::endl;
	}

	if (mLogConsolidations)
	{
		std::string filename = mLogPath + "consolidations_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpConsolidations(file);
		file.close();
	}

    if (mDebugLevel3)
        DumpConsolidations(*mpDebugOutput);
}

ctsndp::arcs_over_capacity_t cCTSNDP::IdentifyArcsOverCapacity()
{
	ctsndp::arcs_over_capacity_t results;

	if (!mAllowArcCapacityLimits || (mConsolidationTime <= 0.0))
	{
		if (mDebugLevel2)
			*mpDebugOutput << "Capacity checking have been turned off!\n";
		return results;
	}

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\nIdentifying arcs which are over capacity";
		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);
			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	// Determine the total utilization of a given link, which means
	// we have to scan over all of the paths to consolidate the link
	// usage from the arc usage.
	link_utilization_t u_l;

	for (auto it = mP_ka.begin(); it != mP_ka.end(); ++it)
	{
		auto& k = it->first;
		auto& path = it->second;

		if (path.empty())
		{
			continue;
		}

		for (auto& arc : path)
		{
			if (arc->arcCapacityIsLimited())
				continue;

			if (arc->isStorageArc())
				continue;

			if (arc->connectedToDelayNode())
				continue;

			auto link = arc->getServiceLink();
			if (!link)
				continue;

			if (u_l.find(link) == u_l.end())
			{
				u_l[link].N = mUtilization_ij[to_index(arc)];
				arc_ptrs_t arcs;
				arcs.insert(arc);
				double t = mDispatchTime_ki[{k, arc->getSource()}];
				u_l[link].arcs_by_dispatch_time.insert(std::make_pair(t, arcs));

			}
			else
			{
				utilization_t& u = u_l[link];
				double t = mDispatchTime_ki[{k, arc->getSource()}];
				auto it = u.arcs_by_dispatch_time.find(t);
				if (it == u.arcs_by_dispatch_time.end())
				{
					arc_ptrs_t arcs;
					arcs.insert(arc);
					auto ret = u_l[link].arcs_by_dispatch_time.insert(std::make_pair(t, arcs));
					if (ret.second)
						u.N += mUtilization_ij[to_index(arc)];
				}
				else
				{
					auto ret = (it->second).insert(arc);
					if (ret.second)
						u.N += mUtilization_ij[to_index(arc)];
				}
			}
		}
	}

	if (mDebugLevel4)
	{
		DumpLinkUtilization(*mpDebugOutput, u_l);
	}

	if (mLogLinkUtilization)
	{
		// Useful for debugging the links overcapacity algorithm!
		std::string filename = mLogPath + "link_utilization_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpLinkUtilization(file, u_l);
		file.close();
	}

	bool invalidExpansion = false;

	// Now that we have to scan all of the links to check
	// if any are over capacity...
	for (auto it = u_l.begin(); it != u_l.end(); ++it)
	{
		auto& link = it->first;

		// As cycle_time gets smaller, the number of
		// times a link can be used in a given period
		// goes up.  In the limit that cycle time is
		// zero, I am assuming no capacity constraint!
		double cycle_time = link->getCycleTime();
		if (cycle_time <= 0.0)
			continue;

		auto& utilization = it->second;
		double capacity = static_cast<double>(link->getCapacity());
		auto& arc = *((utilization.arcs_by_dispatch_time.begin()->second).begin());
		double boost = std::max(arc->getCapacity() / capacity, 1.0);
		capacity *= boost;
		if (capacity < utilization.N)
		{
			// We are, considering all of the arcs independent of time,
			// over capacity.  Now we regroup the arcs by time intervals
			// to see if the link is over capacity during the time it
			// takes the link to become available again (cycle_time).
			auto start = utilization.arcs_by_dispatch_time.begin();
			for (; start != utilization.arcs_by_dispatch_time.end();)
			{
				auto last = start;
				double dispatch_time = start->first;
				unsigned int n = 0;
				for (; last != utilization.arcs_by_dispatch_time.end(); ++last)
				{
					if (last->first < (dispatch_time + cycle_time))
					{
						auto& arcs = last->second;

						for (auto& arc : arcs)
						{
							n += mUtilization_ij[to_index(arc)];
						}
					}
					else
						break;
				}

				if (n > capacity)
				{
					over_capacity_t data;
					data.utilization = n;
					data.dispatch_time = dispatch_time;
					data.arc = nullptr;

					for (auto it = start; it != last; ++it)
					{
						for (auto arc : it->second)
						{
							if (data.arc == nullptr)
							{
								if (arc->getSourceTime() > dispatch_time)
								{
									auto node = arc->getSource();
									while (node->getTime() > dispatch_time)
									{
										node = node->getStorageInflow()->getSource();
									}

									auto& arcs = node->getOutflows();
									for (auto a : arcs)
									{
										if (a->getDestinationServiceLocation() == arc->getDestinationServiceLocation())
										{
											arc = a;
											break;
										}
									}
								}
								assert(arc);
								data.arc = arc;
							}
							else if ((arc->getSourceTime() <= dispatch_time)
								&& (arc->getSourceTime() > data.arc->getSourceTime()))
								data.arc = arc;
						}
					}

					results.push_back(data);

					// Determine this the expansion of the partially expanded network
					// will cross over the next higher (in time) node
					double u_ij = static_cast<unsigned int>(std::ceil(data.utilization / boost));
					double step_time = std::max(mConsolidationTime, cycle_time);
					double end_time = data.arc->getSourceTime() + step_time * u_ij;
					auto dest_arc = data.arc->getSource()->getStorageOutflow();
					if (dest_arc)
					{
						invalidExpansion |= dest_arc->getDestinationTime() <= end_time;
					}

				}

				start = last;
			}
		}
	}

	if (mDebugLevel4)
	{
		DumpLinkOvercapacity(*mpDebugOutput, results);
	}

	if (mLogLinkOvercapacity)
	{
		// Useful for debugging the links overcapacity algorithm!
		std::string filename = mLogPath + "link_overcapacity_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpLinkOvercapacity(file, results);
		file.close();
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << " done";
        if (mPrintTimingInfo)
            print_end_elapse_times(*mpDebugOutput, start);
        *mpDebugOutput << ", found " << results.size() << std::endl;
    }

	if (invalidExpansion)
	{
		if (mAllowInvalidExpansion)
		{
			*mpDebugOutput << "An invalid expansion was detected\n";
		}
		else
		{
			*mpDebugOutput << "Capacity checking are being discarded!\n";
			results.clear();
		}
	}

    return results;
}


/**
 * For all commodities k in 𝒦, the nodes (o_k, e_k) and (d_k, l_k)
 * are in 𝒩_t.
 */
bool cCTSNDP::TestProperty1(std::ostream& out) const
{
	auto& commodities = mpPartiallyTimeExpandedNetwork->commodities();

	for (auto& k : commodities)
	{
		node_ptr_t node = mpPartiallyTimeExpandedNetwork->findNode(k->getSourceLocation(), k->getEarliestAvailableTime(), k);
		if (!node)
		{
			out << "Property 1 Violation:\n";
			out << "Commodity, " << k->getName() << ", does not have a source node in N_t!" << std::endl;
			return false;
		}

		node = mpPartiallyTimeExpandedNetwork->findNode(k->getDestinationLocation(), k->getLatestDeliveryTime(), k);
		if (!node)
		{
			out << "Property 1 Violation:\n";
			out << "Commodity, " << k->getName() << ", does not have a destination node in N_t!" << std::endl;
			return false;
		}
	}

	return true;
}

/**
 * Every arc ((i, t), (j,t_bar)) in 𝒜_t has t_bar <= t + tau_ij.
 */
bool cCTSNDP::TestProperty2(std::ostream& out) const
{
	auto& arcs = mpPartiallyTimeExpandedNetwork->arcs();
	for (auto& arc : arcs)
	{
		if (arc->connectedToDelayNode())
			continue;

		if (!(arc->getDestinationTime() <= arc->getSourceTime() + arc->getActualTravelTime()))
		{
			out << "Property 2 Violation:\n";
			out << "Arc: " << *arc << "\n";
			out << arc->getDestinationTime() << " <= " << arc->getSourceTime();
			out << " + " << arc->getActualTravelTime() << std::endl;
			return false;
		}
	}
	return true;
}

/**
 * For every arc a = (i, j) in 𝒜 in the flat network, 𝒟, and for
 * every node (i, t) in the partially time-expanded network,
 * 𝒟_t = (𝒩_t, 𝒜_t union H_t), there is a timed copy of a in 𝒜_t
 * starting at (i, t).
 */
bool cCTSNDP::TestProperty3(std::ostream& out) const
{
	auto& links = mpServiceNetwork->serviceLinks();
	for (auto& link : links)
	{
		auto nodes = mpPartiallyTimeExpandedNetwork->findNodes(link->getSource(), false);
		if (nodes.empty())
		{
			out << "Property 3 Violation:\n";
			out << "Node( " << link->getSource() << ", t) does not exists in N_t";
			out << std::endl;
			return false;
		}

		for (auto& node : nodes)
		{
			auto arcs = mpPartiallyTimeExpandedNetwork->findAllOutflowArcsFromNode(node, false);
			if (arcs.empty())
			{
				out << "Property 3 Violation:\n";
				out << "The node, " << *node << ", does not any outflowing arcs!";
				out << std::endl;
				return false;
			}
		}
	}
	return true;
}

/**
 * if arc ((i, t), (j, t')) in 𝒜_t, then there does not exist a
 * node (j, t") in 𝒩_t with t' < t" <= t + tau_ij
 */
bool cCTSNDP::TestProperty4(std::ostream& out) const
{
	auto& arcs = mpPartiallyTimeExpandedNetwork->arcs();
	for (auto& arc : arcs)
	{
		if (arc->connectedToDelayNode())
			continue;

		auto nodes = mpPartiallyTimeExpandedNetwork->findNodes(arc->getDestinationServiceLocation());

		if (nodes.empty())
		{
			out << "Property 4 Violation:\n";
			out << "Arc " << *arc << "\n";
			out << "Node (" << *(arc->getDestinationServiceLocation()) << ", t) does not exists!";
			out << std::endl;
			return false;
		}

		double t_lower_bound = arc->getDestinationTime();
		double t_upper_bound = arc->getSourceTime() + arc->getActualTravelTime();

		for (auto& node : nodes)
		{
			double t = node->getTime();
			if ((t_lower_bound < t) && (t <= t_upper_bound))
			{
				out << "Property 4 Violation:\n";
				out << "Node: " << *node << "\n";
				out << "Arc: " << *arc << "\n";
				out << t_lower_bound << " < " << t << " <= "
					<< arc->getSourceTime() << " + " << arc->getActualTravelTime();
				out << std::endl;
				return false;
			}
		}
	}
	return true;
}


void cCTSNDP::DumpCommodityPaths(std::ostream& out)
{
    out << "\n";

    // for all commodities
    for (auto& k : mpPartiallyTimeExpandedNetwork->commodities())
    {
        DumpCommodityPath(out, k);
    }
}

void cCTSNDP::DumpCommodityPath(std::ostream& out, const cCommodityBundle* const k)
{
    out << k->getFullyQualifiedName() << " (id:" << k->getId() << ") " << k->getSourceName();
    out << " (eat: " << k->getEarliestAvailableTime() << ")";
    out << " -> " << k->getDestinationName();
    out << " (ldt: " << k->getLatestDeliveryTime() << ")\n";
    out << "Path:\n";
        
    const auto& path = mP_ka[k];
    if (path.empty())
    {
        out << "\tNONE!\n";
        return;
    }
        
    for(int i = 0; i < path.size(); ++i)
    {
        out << "\t" << *path[i] << " id = " << path[i]->getId() << "\n";
    }
        
    out << "\n";
}

void cCTSNDP::DumpArcsToLengthen(std::ostream& out, const std::set<arc_ptr_t>& arcs) const
{
    if (arcs.empty())
        return;
    
    out << "Lengthening arcs:\n";
    for (auto& arc : arcs)
    {
        out << "  " << *arc << "\n";
    }
	out << "\n";

	for (auto& arc : arcs)
	{
		out << "Arcs Information:\n\n";
		out << *arc << "\n";
		out << "  Inflows:\n";
		auto inflows = arc->getDestinationNode()->getInflows();
		for (auto inflow : inflows)
		{
			out << "    " << *inflow << "\n";
		}
		out << "\n  Outflows:\n";
		auto outflows = arc->getDestinationNode()->getOutflows();
		for (auto outflow : outflows)
		{
			out << "    " << *outflow << "\n";
		}
		out << "\n\n";
	}
	out << "\n\n";
}


void cCTSNDP::DumpArcsToShorten(std::ostream& out, const ctsndp::arcs_to_shorten_t& arcs) const
{
	if (arcs.empty())
		return;

	out << "Shortening arcs:\n";
	for (auto& data : arcs)
	{
		out << "  " << *data.ij << " to " << data.t_new << "\n";
	}
	out << "\n\n";
}

void cCTSNDP::DumpConsolidationCount(std::ostream& out) const
{
    if (mN_a.empty())
    {
        out << "Arc commodity sharing info is empty!" << std::endl;
        return;
    }

    out << "Arc commodity sharing info:\n";
    for (auto& [arc, count] : mN_a)
    {
        out << *arc << ": " << count << "\n";
    }
    
}

void cCTSNDP::DumpConsolidations(std::ostream& out) const
{
    if (!mJ_a.empty() && !mJ_ak.empty())
    {
        out << "Arcs carrying more than one commodity:\n";
        for (const auto& arc : mJ_a)
        {
            if (arc->isHoldoverArc())
                continue;
            
            auto count = mN_a.find(arc)->second;
            out << *arc << ": " << count << "\n";
            
            auto it = mJ_ak.find(arc);

			int n = 1;
			if (it != mJ_ak.end())
			{
				auto& groups = it->second;

				for (auto& commodities : groups)
				{
					out << "  Group " << n++ << ":\n";
					for (auto k : commodities)
					{
						out << "    " << k->getUniqueName();
						out << ", eat = " << k->getEarliestAvailableTime();
						out << ", ldt = " << k->getLatestDeliveryTime() << "\n";
					}
					out << "\n";
				}
			}
		}
    }
}

void cCTSNDP::DumpLinkUtilization(std::ostream& out, const link_utilization_t& u_l) const
{
	out << "\nLink Utilization:\n";

	// Now that we have to scan all of the links to check
	// if any are over capacity...
	for (auto it = u_l.begin(); it != u_l.end(); ++it)
	{
		auto& link = it->first;
		auto& utilization = it->second;
		out << "   " << link->getName() << " used " << utilization.N <<" times by\n";
		for (auto& data : utilization.arcs_by_dispatch_time)
		{
			out << "      Dispatch Time = " << data.first << "\n";
			for (auto& arc : data.second)
			{
				out << "         " << *arc << " use " << mUtilization_ij.at(to_index(arc)) << " times\n";
			}
		}
		out << "\n";
	}
	out << "\n";
}

void cCTSNDP::DumpLinkOvercapacity(std::ostream& out, const ctsndp::arcs_over_capacity_t& arcs) const
{
	out << "\nLink Overcapacity:\n";

	for (auto& data : arcs)
	{
		const auto& link = data.arc->getServiceLink();
		out << "   " << link->getName() << " used " << data.utilization << " times by\n";
		out << "      Dispatch Time = " << data.dispatch_time << "\n";
		out << "         " << *(data.arc) << " use " << data.utilization << " times\n";
		out << "\n";

	}
	out << "\n";
}
