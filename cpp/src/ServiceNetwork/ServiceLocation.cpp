
#include "ServiceLocation.hpp"
#include <iostream>

cServiceLocation::cServiceLocation(const unsigned int id, const std::string& name)
:
    mId(id),
    mName(name)
{}

void cServiceLocation::rename(const std::string& new_name)
{
    mName = new_name;
}

bool cServiceLocation::operator==(const cServiceLocation& rhs) const
{
    return mName == rhs.mName;
}

bool cServiceLocation::operator==(const cServiceLocation* const rhs) const
{
    return mName == rhs->mName;
}

////////////////////////////////////////////////////////////
// Location algorithms
////////////////////////////////////////////////////////////

std::string to_string(const cServiceLocation& loc)
{
    return loc.getName();
}

std::ostream& operator<<(std::ostream& out, const cServiceLocation& loc)
{
    out << loc.getName();
    return out;
}

std::ostream& operator<<(std::ostream& out, const cServiceLocation* const loc)
{
    if (loc)
        out << loc->getName();
    return out;
}
