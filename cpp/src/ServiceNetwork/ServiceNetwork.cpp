
#include "ServiceNetwork.hpp"
#include "../Commodities/CommodityShipment.hpp"
#include <set>
#include <iostream>
#include <cassert>

cServiceNetwork::cServiceNetwork()
{

}

cServiceNetwork::~cServiceNetwork()
{
    for (auto link : mLinks)
        delete link;
    mLinks.clear();

    for (auto location : mLocations)
        delete location;
    mLocations.clear();

    mCommodities.clear();
    mShipments.clear();
}

cServiceNetwork::operator bool() const
{
    return !mLocations.empty() && !mLinks.empty();
}

cServiceLocation* cServiceNetwork::addLocation(const std::string& name)
{
    static unsigned int id = 0;
    cServiceLocation* loc = new cServiceLocation(id++, name);
    mLocations.push_back(loc);
    return loc;
}


void cServiceNetwork::renameLocation(const std::string& loc_name, const std::string& new_name)
{
    for (auto& location : mLocations)
    {
        if (location->getName() == loc_name)
        {
            location->rename(new_name);
            return;
        }
    }
}

cServiceLink* cServiceNetwork::addLink(const std::string& name,
                                       const cServiceLocation* const src,
                                       const cServiceLocation* const dest)
{
    cServiceLink* link = new cServiceLink(name, src, dest);
    mLinks.push_back(link);
    return link;
}

void cServiceNetwork::addLink(std::unique_ptr<cServiceLink> link)
{
    mLinks.push_back(link.release());
}

void cServiceNetwork::addShipment(std::unique_ptr<cCommodityShipment> shipment)
{
    for(auto& k : shipment->getCommodityBundles())
        mCommodities.insert(k);

    mShipments.insert(shipment.release());
}

bool cServiceNetwork::hasLocation(const std::string& name) const
{
    for (auto loc : mLocations)
        if (loc->getName() == name)
            return true;
    return false;
}

const cServiceLocation* const cServiceNetwork::findLocation(const std::string& name) const
{
    for (auto loc : mLocations)
        if (loc->getName() == name)
            return loc;
    return nullptr;
}

cServiceLink* cServiceNetwork::findLink(const std::string& name) const
{
    for (auto link : mLinks)
    {
        if (name == link->getName())
            return link;
    }
    return nullptr;
}

std::vector<cServiceLink*> cServiceNetwork::findLinks(const cServiceLocation& src, const cServiceLocation& dst) const
{
    std::vector<cServiceLink*> links;
    for (auto link : mLinks)
    {
        if ((src == link->getSource()) && (dst == link->getDestination()))
            links.push_back(link);
    }
    return links;
}

std::vector<cServiceLink*> cServiceNetwork::findLinksWithSource(
                                        const cServiceLocation& loc) const
{
    std::vector<cServiceLink*> links;
    for (auto link : mLinks)
    {
        if (loc == link->getSource())
            links.push_back(link);
    }

    return links;
}

std::vector<cServiceLink*> cServiceNetwork::findLinksWithDestination(
                                        const cServiceLocation& loc) const
{
    std::vector<cServiceLink*> links;
    for (auto link : mLinks)
    {
        if (loc == link->getDestination())
            links.push_back(link);
    }
    
    return links;
}

// Program to find Dijkstra's shortest path using STL set
std::vector<double> cServiceNetwork::shortestTimes(const cServiceLocation* src) const
{
    using namespace std;
    
    if (!src)
        return std::vector<double>();
    
    const auto n = mLocations.size();
    
    // Create a set to store vertices that are being prerocessed
    vector<bool> sptSet(n, false);

    // Create a vector for times and initialize all
    // times as infinite (INF)
    vector<double> time(n, std::numeric_limits<double>::max());

    // Initialize source time as 0.
    time[src->getId()] = 0.0;
    
    // Find shortest path for all vertices
    for (int c = 0; c < n-1; ++c )
    {
        // Pick the minimum time vertex from the set of vertices not
        // yet processed.  u is alway equal to src in the first iteration.
        int u = minTime(time, sptSet);
        
        // Mark the picked vertex as processed
        sptSet[u] = true;
        
        // Update time value of the adjacent vertices of the pick vertex.
        for (int v = 0; v < n; ++v)
        {
            auto links = findLinks(*mLocations[u], *mLocations[v]);
            if (links.empty())
                continue;
            
            double minTravelTime = links[0]->getTravelTime();
            for (int i = 1; i < links.size(); ++i)
            {
                if (links[i]->getTravelTime() < minTravelTime)
                    minTravelTime = links[i]->getTravelTime();
            }
            
            if (!sptSet[v] && (time[u] != std::numeric_limits<double>::max())
                && (time[u] + minTravelTime < time[v]))
            {
                time[v] = time[u] + minTravelTime;
            }
        }
    }

    return time;
}

// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
int cServiceNetwork::minTime(std::vector<double> time, std::vector<bool>& sptSet) const
{
    double min_time = std::numeric_limits<double>::max();
    int min_index = -1;
    
    for (int i = 0; i < time.size(); ++i)
    {
        if (!sptSet[i] && (time[i] <= min_time))
        {
            min_time = time[i];
            min_index = i;
        }
    }
    
    return min_index;
}

void cServiceNetwork::moveIncomingLinksTo(const cServiceLocation* old_dst, const cServiceLocation* new_dst)
{
    assert(old_dst);
    assert(new_dst);
    for (auto link : mLinks)
    {
        if (old_dst == link->getDestination())
            link->setDestination(new_dst);
    }
}

void cServiceNetwork::moveOutgoingLinksFrom(const cServiceLocation* old_src, const cServiceLocation* new_src)
{
    assert(old_src);
    assert(new_src);
    for (auto link : mLinks)
    {
        if (old_src == link->getSource())
            link->setSource(new_src);
    }
}
