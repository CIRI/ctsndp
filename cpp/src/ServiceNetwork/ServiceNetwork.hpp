
#pragma once

#include "ServiceLocation.hpp"
#include "ServiceLink.hpp"
#include <vector>
#include <memory>
#include <deque>
#include <set>

class cCommodityBundle;
class cCommodityShipment;

class cServiceNetwork
{
public:
    cServiceNetwork();
    ~cServiceNetwork();
    
    /**
     * Returns true if the service network has nodes and links
     **/
    explicit operator bool() const;

    /**
     * Add service location (node) to the network
     **/
    cServiceLocation* addLocation(const std::string& name);
    
    /**
     * Renames the service location (node) given by "loc_name" to "new_name"
     **/
    void renameLocation(const std::string& loc_name, const std::string& new_name);

    /**
     * Add service link (arc) to the network
     **/
    cServiceLink* addLink(const std::string& name,
                          const cServiceLocation* const src,
                          const cServiceLocation* const dest);
    
    void addLink(std::unique_ptr<cServiceLink> link);

    /**
     * Add a shipement (set of commodities) to the network
     **/
    void addShipment(std::unique_ptr<cCommodityShipment> shipment);
    
    /**
     * Returns true, if the service location (node) with the given name exists
     * in the service network
     **/
    bool hasLocation(const std::string& name) const;

    /**
     * Find the service location (node) with the given name
     * Note: nullptr is returned if the location is not found
     **/
    const cServiceLocation* const findLocation(const std::string& name) const;

    /**
     * Find the service link (arc) with the given name.
     * Note: nullptr is returned if the link is not found
     **/
    cServiceLink* findLink(const std::string& name) const;

    /**
     * Find the all service links (arc) with the source location given
     * by src and a destination location given by dst.
     **/
    std::vector<cServiceLink*> findLinks(const cServiceLocation& src, const cServiceLocation& dst) const;

    /**
     * Find all of the service links (arcs) with the source
     * location given by loc
     **/
    std::vector<cServiceLink*> findLinksWithSource(
                                    const cServiceLocation& loc) const;

    /**
     * Find all of the service links (arcs) with the destination
     * location given by loc
     **/
    std::vector<cServiceLink*> findLinksWithDestination(
                                    const cServiceLocation& loc) const;

    /**
     * Return all of the service locations (nodes) within the service
     * network
     **/
    const std::vector<cServiceLocation*>&  serviceLocations() const;

    /**
     * Return all of the service links (arcs) within the service
     * network
     **/
    const std::vector<cServiceLink*>&  serviceLinks() const;

    /**
     * Return all of the commodities within the service
     * network
     **/
    const std::set<cCommodityBundle*>&  commodities() const;
    
    std::vector<double> shortestTimes(const cServiceLocation* src) const;

    /**
     * Finds all of the links those destination was "old_dst" and
     * sets the destination to "new_dst"
     **/
    void moveIncomingLinksTo(const cServiceLocation* old_dst, const cServiceLocation* new_dst);

    /**
     * Finds all of the links those source was "old_src" and sets the source to "new_src"
     **/
    void moveOutgoingLinksFrom(const cServiceLocation* old_src, const cServiceLocation* new_src);
    
private:
    
    // A utility function to find the vertex with minimum time value, from
    // the set of vertices not yet included in shortest path tree
    int minTime(std::vector<double> time, std::vector<bool>& sptSet) const;

    std::vector<cServiceLocation*> mLocations;
    std::vector<cServiceLink*> mLinks;
    std::set<cCommodityShipment*> mShipments;
    std::set<cCommodityBundle*> mCommodities;
};


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const std::vector<cServiceLocation*>&  cServiceNetwork::serviceLocations() const
{
    return mLocations;
}

inline const std::vector<cServiceLink*>&  cServiceNetwork::serviceLinks() const
{
    return mLinks;
}

inline const std::set<cCommodityBundle*>&  cServiceNetwork::commodities() const
{
    return mCommodities;
}
