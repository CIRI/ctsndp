
#pragma once

#include "../Properties/Properties.hpp"
#include <string>


class cServiceLocation : public cProperties
{
public:
    ~cServiceLocation() = default;

    bool operator==(const cServiceLocation& rhs) const;
    bool operator==(const cServiceLocation* const rhs) const;

    const unsigned int getId() const;
    const std::string& getName() const;

protected:
    cServiceLocation(const unsigned int id, const std::string& name);
    void rename(const std::string& new_name);

private:
    const unsigned int mId;
    std::string mName;
    
    friend class cServiceNetwork;
};


std::string to_string(const cServiceLocation& loc);
std::ostream& operator<<(std::ostream& out, const cServiceLocation& loc);
std::ostream& operator<<(std::ostream& out, const cServiceLocation* const loc);

std::string get_name(const cServiceLocation& node);


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const unsigned int cServiceLocation::getId() const
{
    return mId;
}

inline const std::string& cServiceLocation::getName() const
{
    return mName;
}

inline std::string get_name(const cServiceLocation& node)
{
    if (node.hasProperty("name"))
    {
        return std::any_cast<std::string>(node.getProperty("name"));
    }
    
    return node.getName();
}

