
#include "ServiceLink.hpp"
#include "ServiceLocation.hpp"
#include <iostream>
#include <cassert>
#include <limits>

cServiceLink::cServiceLink(const std::string& name, 
                            const cServiceLocation* const src, 
                            const cServiceLocation* const dest)
:
    mName(name), mpSource(src), mpDestination(dest)
{
    mCapacity = std::numeric_limits<unsigned int>::max();
    mPerUnitBundleCapacity = 1;
    mFixedCost = 0;
    mPerUnitBundleCost = 0;
    mTravelTime = 0;
	// A cycle_time of zero means the link has infinite capacity
	// because the link can be reloaded in zero time
    mCycleTime = 0;
}

void cServiceLink::setSource(const cServiceLocation* const pSrc)
{
    assert(pSrc);
    mpSource = pSrc;
}

void cServiceLink::setDestination(const cServiceLocation* const pDst)
{
    assert(pDst);
    mpDestination = pDst;
}

////////////////////////////////////////////////////////////
// Link algorithms
////////////////////////////////////////////////////////////

std::string to_string(const cServiceLink& link)
{
    return to_string(*link.getSource()) + " -> " + to_string(*link.getDestination());
}

std::ostream& operator<<(std::ostream& out, const cServiceLink& link)
{
    out << link.getSource() << " -> " << link.getDestination();
    return out;
}
