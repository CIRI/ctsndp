
#pragma once

#include "../Properties/Properties.hpp"

#include <string>

// Foward Declarations
class cServiceLocation;

/*!
 * \brief A cServiceLink represents a link (road, railway, seaway, etc.)
 * between two locations in the service network.
 *
 *
 */
class cServiceLink : public cProperties
{
public:
    cServiceLink(const std::string& name,
                 const cServiceLocation* const src, const cServiceLocation* const dest);
    ~cServiceLink() = default;

    const std::string& getName() const;
    const cServiceLocation* const getSource() const;
    const cServiceLocation* const getDestination() const;

    unsigned int getCapacity() const;
    unsigned int getCommodityBundleCapacity() const;
    float getFixedCost() const;
    float getCommodityBundleCost() const;
    double getTravelTime() const;
    double getCycleTime() const;

    void setCapacity(unsigned int capacity);
    void setCommodityBundleCapacity(unsigned int capacity);
    void setCommodityBundleCost(float cost);
    void setFixedCost(float cost);
    void setTravelTime(double travel_time);
    void setCycleTime(double cycle_time);

protected:
    void setSource(const cServiceLocation* const pSrc);
    void setDestination(const cServiceLocation* const pDst);
    
private:
    std::string mName;
    const cServiceLocation* mpSource;
    const cServiceLocation* mpDestination;

    /*!
     * Capacity: the number of times the link can be used
     * by a "unit of transportation" at the same time.
     * A seaway might only be able to hold one ship at a
     * time (mCapacity=1).
     *
     * \remark If capacity is negative, it means the link
     * has no limit to its capacity.
     */
    unsigned int mCapacity;

    /*!
     * PerUnitBundleCapacity represent the maximum number of
     * commodity bundles each "unit of transportation" can
     * hold.  For example, if our commodity bundle is a TEU,
     * a truck could transport two TEUs (mPerUnitBundleCapacity=2),
     * while a shipe count transport 1400 TEUs
     * (mPerUnitBundleCapacity=1400)
     */
    unsigned int mPerUnitBundleCapacity;

    /*!
     * FixedCost: the cost of operating one "unit of transportation"
     * across the link.  If the "transportation unit" is a truck,
     * the fixed cost can include driver, fuel, insurance, etc.
     */
    float mFixedCost;

    /*!
     * PerUnitBundleCost: the variable cost associated with
     * operating the "unit of transporation" across the link.
     */
    float mPerUnitBundleCost;

    /*!
     * TravelTime: the time it take for the "unit of transportation"
     * to travel from source to destination.
     */
    double mTravelTime;
    
    /*!
     * CycleTime: the time it take for the link to accept a
     * new "unit of transportation" after its capacity has
     * been reached.
     */
    double mCycleTime;

    friend class cServiceNetwork;
};

std::string to_string(const cServiceLink& link);
std::ostream& operator<<(std::ostream& out, const cServiceLink& link);

std::string get_name(const cServiceLink& link);

///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

inline const std::string& cServiceLink::getName() const
{
    return mName;
}

inline const cServiceLocation* const cServiceLink::getSource() const
{
    return mpSource;
}

inline const cServiceLocation* const cServiceLink::getDestination() const
{
    return mpDestination;
}

inline unsigned int cServiceLink::getCapacity() const
{
    return mCapacity;
}

inline unsigned int cServiceLink::getCommodityBundleCapacity() const
{
    return mPerUnitBundleCapacity;
}

inline float cServiceLink::getFixedCost() const
{
    return mFixedCost;
}

inline float cServiceLink::getCommodityBundleCost() const
{
    return mPerUnitBundleCost;
}

inline double cServiceLink::getTravelTime() const
{
    return mTravelTime;
}

inline double cServiceLink::getCycleTime() const
{
    return mCycleTime;
}

inline void cServiceLink::setCapacity(unsigned int capacity)
{
    mCapacity = capacity;
}

inline void cServiceLink::setCommodityBundleCapacity(unsigned int capacity)
{
    mPerUnitBundleCapacity = capacity;
}

inline void cServiceLink::setFixedCost(float cost)
{
    mFixedCost = cost;
}

inline void cServiceLink::setCommodityBundleCost(float cost)
{
    mPerUnitBundleCost = cost;
}

inline void cServiceLink::setTravelTime(double travel_time)
{
    mTravelTime = travel_time;
}

inline void cServiceLink::setCycleTime(double cycle_time)
{
    mCycleTime = cycle_time;
}

inline std::string get_name(const cServiceLink& link)
{
    return link.getName();
}

