
#include "json_utils.hpp"
#include "onf_utils.hpp"
#include "heartland_utils.hpp"
#include "ServiceNetwork/ServiceLocation.hpp"
#include "ServiceNetwork/ServiceLink.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "Commodities/CommodityBundle.hpp"
#include <fstream>
#include <memory>
#include <stdexcept>
#include <any>
#include <iostream>

#ifdef WIN32
	#include <direct.h>
#else
	#include <unistd.h>
#endif

void print_cwd()
{
#ifdef WIN32
	std::cerr << "The current working directory is: " << _getcwd(nullptr, 0) << std::endl;
#else
	std::cerr << "The current working directory is: " << getcwd(nullptr, 0) << std::endl;
#endif
}

template<typename T>
std::vector<T> to_array(const nlohmann::json& json_value)
{
    std::vector<T> array;
    for (auto& item : json_value.items())
    {
        array.push_back(item.value());
    }
    return array;
}

std::any to_any(const nlohmann::json& json_value)
{
    if (json_value.is_boolean())
        return std::any(json_value.get<bool>());
    if (json_value.is_number_unsigned())
        return std::any(json_value.get<unsigned int>());
    if (json_value.is_number_integer())
        return std::any(json_value.get<int>());
    if (json_value.is_number_float())
        return std::any(json_value.get<float>());
    if (json_value.is_string())
        return std::any(json_value.get<std::string>());
    if (json_value.is_array())
    {
        auto front = json_value.front();
        if (front.is_string())
            return std::any(to_array<std::string>(json_value));
        if (front.is_number_unsigned())
            return std::any(to_array<unsigned int>(json_value));
        if (front.is_number_integer())
            return std::any(to_array<int>(json_value));
        if (front.is_number_float())
            return std::any(to_array<float>(json_value));
   }

    return std::any();
}
    
nlohmann::json from_any(const std::any& value)
{
    nlohmann::json json_value;
    if (!value.has_value())
        return json_value;

    auto& value_type = value.type();
    if (value_type == typeid(bool))
        json_value = std::any_cast<bool>(value);
    else if (value_type == typeid(unsigned int))
        json_value = std::any_cast<unsigned int>(value);
    else if (value_type == typeid(int))
        json_value = std::any_cast<int>(value);
    else if (value_type == typeid(float))
        json_value = std::any_cast<float>(value);
    else if (value_type == typeid(double))
        json_value = std::any_cast<double>(value);
    else if (value_type == typeid(std::string))
        json_value = std::any_cast<std::string>(value);
    else if (value_type == typeid(std::vector<std::string>))
        json_value = std::any_cast<std::vector<std::string>>(value);
    else if (value_type == typeid(std::vector<unsigned int>))
        json_value = std::any_cast<std::vector<unsigned int>>(value);
    else if (value_type == typeid(std::vector<int>))
        json_value = std::any_cast<std::vector<int>>(value);
    else if (value_type == typeid(std::vector<float>))
        json_value = std::any_cast<std::vector<float>>(value);
    else if (value_type == typeid(std::vector<double>))
        json_value = std::any_cast<std::vector<double>>(value);

    return json_value;
}

nlohmann::json from_any(const std::string& name, const std::any& value)
{
    nlohmann::json json_obj;
    
    json_obj.emplace(name, from_any(value));
    return json_obj;
}
    
void add_properties(nlohmann::json& json_obj, const cProperties& properties)
{
    for (auto it = properties.cbegin(); it != properties.cend(); ++it)
    {
        json_obj.emplace(it->first, from_any(it->second));
    }

}
    

std::unique_ptr<cServiceNetwork> create_service_network_from_json_file(const std::string& jsonFileName)
{
    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;

    if (onf::is_valid_service_network_json(jsonDoc, jsonFileName))
    {
        return onf::create_service_network_from_json_doc(jsonDoc);
    }
    
    if (heartland::is_valid_service_network_json(jsonDoc, jsonFileName))
    {
        return heartland::create_service_network_from_json_doc(jsonDoc);
    }

    return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
}

std::unique_ptr<cServiceNetwork> create_service_network_from_schedule_file(const std::string& jsonFileName)
{
    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;
    std::string networkFileName = jsonDoc["network"];
    jsonDoc.clear();
    in.close();
    
    in.open(networkFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << networkFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cServiceNetwork>(new cServiceNetwork());
    }
    in >> jsonDoc;
    return heartland::create_service_network_from_json_doc(jsonDoc);
}

std::unique_ptr<cCommodityShipments> create_shipments_from_json_file(const std::string& jsonFileName, 
	bool no_delays, bool group)
{
    std::ifstream in(jsonFileName);
    if (!in.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for reading!" << std::endl;
		print_cwd();
        return std::unique_ptr<cCommodityShipments>(new cCommodityShipments());
    }
    
    nlohmann::json jsonDoc;
    in >> jsonDoc;

    if (onf::is_valid_shipment_json(jsonDoc, jsonFileName))
    {
        return onf::create_shipments_from_json_doc(jsonDoc, no_delays, group);
    }
    
    if (heartland::is_valid_shipment_json(jsonDoc, jsonFileName))
    {
        return heartland::create_shipments_from_json_doc(jsonDoc, no_delays, group);
    }

    return std::unique_ptr<cCommodityShipments>(new cCommodityShipments());
}

void write_json_file(const std::string& jsonFileName, const nlohmann::json& jsonDoc)
{
    std::ofstream out(jsonFileName);
    if (!out.is_open())
    {
        std::cerr << "Could not open " << jsonFileName << " for writing!" << std::endl;
		print_cwd();
        return;
    }

    out << jsonDoc.dump(4) << std::flush;
    out.close();
}

