//
//  ConstructFeasibleSolutionModel_Gurobi.cpp
//
//  Implements the "Construct Feasible Solution" problem
//  using the Gurobi optimizer.
//
//  Created by Brett Feddersen on 03/25/20.
//

#include "ConstructFeasibleSolutionModel_Gurobi.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetworkArc.hpp"
#include "ServiceNetwork/ServiceNetwork.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "Commodities/CommodityShipment.hpp"
#include "ctsndp_utils.hpp"
#include "ctsndp_vars.hpp"
#include "ctsndp_exceptions.hpp"
#include "ctsndp_utils_priv.hpp"
#include "gurobi_utils_priv.hpp"

#include <string>
#include <iostream>
#include <chrono>
#include <fstream>


namespace
{
    const double CONSTRAINT_TOLERANCE = 0.00001;
	const double DELTA_TOLERANCE = 1.0;
}

using namespace ctsndp;
using namespace std::string_literals;


std::unique_ptr<cConstructFeasibleSolutionModel_Gurobi> 
cConstructFeasibleSolutionModel_Gurobi::create(int pass_number,
	const cPartiallyTimeExpandedNetwork& expandedNetwork,
	const cServiceNetwork& serviceNetwork,
	const ctsndp::commodity_path_node_t& p_ki,
	const ctsndp::commodity_path_arc_t& p_ka,
	const ctsndp::consolidation_groups_t& j_ak,
	bool verboseMode, bool printTimingInfo)
{
	try
	{
		std::unique_ptr<cConstructFeasibleSolutionModel_Gurobi> 
			model(new cConstructFeasibleSolutionModel_Gurobi(pass_number, expandedNetwork,
				serviceNetwork, p_ki, p_ka, j_ak, verboseMode, printTimingInfo));

		return model;
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException(e.getMessage());
	}
}

cConstructFeasibleSolutionModel_Gurobi::cConstructFeasibleSolutionModel_Gurobi(int pass_number,
                                         const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                         const cServiceNetwork& serviceNetwork,
                                         const ctsndp::commodity_path_node_t& p_ki,
                                         const ctsndp::commodity_path_arc_t& p_ka,
										 const ctsndp::consolidation_groups_t& j_ak,
										 bool verboseMode, bool printTimingInfo)
:
    cGurobiModel("ConstructFeasibleSolution"),
	cConstructFeasibleSolutionModel(pass_number, expandedNetwork, serviceNetwork,
									p_ki, p_ka, j_ak, verboseMode,
									printTimingInfo)
{
}

cConstructFeasibleSolutionModel_Gurobi::~cConstructFeasibleSolutionModel_Gurobi()
{
}

void cConstructFeasibleSolutionModel_Gurobi::setNumericFocus(int numericFocus)
{
	cGurobiModel::setNumericFocus(numericFocus);
}

void cConstructFeasibleSolutionModel_Gurobi::setTimeLimit_sec(double timeLimit_sec)
{
	cGurobiModel::setTimeLimit_sec(timeLimit_sec);
}

void cConstructFeasibleSolutionModel_Gurobi::logToConsole(bool log)
{
	cGurobiModel::logToConsole(log);
}

void cConstructFeasibleSolutionModel_Gurobi::outputDebugMsgs(bool msgs)
{
	cOptimizationProblem::outputDebugMsgs(msgs);
}

void cConstructFeasibleSolutionModel_Gurobi::logFilename(const std::string& filename)
{
    cGurobiModel::logFilename(filename, ".construct_solution");
}

void cConstructFeasibleSolutionModel_Gurobi::readTuningParameters(const std::string& filename)
{
	cGurobiModel::readTuningParameters(filename);
}

bool cConstructFeasibleSolutionModel_Gurobi::Optimize()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\tOptimizing \"Construct Feasible Solution\" ";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	int status = 0;

	try
	{
		if (mpProgress)
		{
			*mpProgress << "  Optimizing \"Construct Feasible Solution\"..." << std::flush;
		}

		mModel.optimize();
		status = mModel.get(GRB_IntAttr_Status);

		if (mpProgress)
		{
			*mpProgress << "done" << std::endl;
		}
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("Construct Feasible Solution Optimize: " + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }

    switch (status)
    {
        case GRB_OPTIMAL:
		{
			SaveSolution();
			return true;
		}
        case GRB_INF_OR_UNBD:
        case GRB_INFEASIBLE:
        {
            std::string msg = "Construct Feasible Solution model is infeasible!";
            throw ctsndp::cInfeasibleSolution("ConstructFeasibleSolution", msg);
            break;
        }
                   
        case GRB_TIME_LIMIT:
        {
            std::string msg = "Exceeded time limit (";
            msg += std::to_string(mTimeLimit_sec);
            msg += "):\n";
			try
			{
				auto count = mModel.get(GRB_IntAttr_SolCount);
				if (count == 0)
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    No solution was found!! Terminating" << std::endl;
					}

					msg += "No solution was found!";
				}
				else
				{
					if (mpProgress)
					{
						*mpProgress << "  " << msg;
						*mpProgress << "    Feasible solution count = " << count << std::endl;
					}

					SaveSolution();
					return true;
				}
			}
			catch (const GRBException& e)
			{
				msg += e.getMessage();
			}
                      
            throw ctsndp::cTimeLimit("ConstructFeasibleSolution", msg);
            break;
        }
    }

    return false;
}

bool cConstructFeasibleSolutionModel_Gurobi::RelaxConstraints()
{
	return cGurobiModel::RelaxConstraints();
}

std::string cConstructFeasibleSolutionModel_Gurobi::ComputeIIS()
{
	return cGurobiModel::ComputeIIS();
}

void cConstructFeasibleSolutionModel_Gurobi::FindConstraintViolations(std::ostream& out)
{
    bool printNone = true;
	out << "\nScanning for constraint 13 \"gamma_ki + tau_ij <= gamma_kj\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto& path = mP_ka.at(k);
        if (path.empty())
            continue;

        for (int i=0; i < path.size()-1; ++i)
        {
            auto& arc = path[i];
            const auto& loc = arc->getDestination();
            auto arrival_time = as_double(gamma_ki[{k, arc->getSource()}]) + tau_ij[to_index(arc)];
            double delta = as_double(gamma_ki[{k, loc}]) - arrival_time;

            if (delta < -CONSTRAINT_TOLERANCE)
            {
				out << arrival_time << " > " << as_double(gamma_ki[{k, loc}]) ;
                out << " for commodity " << k->getFullyQualifiedName() << ", arc "<< to_string(*arc);
                out << " delta = " << delta << "\n";
                printNone = false;
            }
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 14 \"eat <= gamma_ko\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        auto& loc = mP_ki.at(k).front();
        double delta = as_double(gamma_ki[{k, loc}]) - k->getEarliestAvailableTime();

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << k->getEarliestAvailableTime() << " > " << as_double(gamma_ki[{k, loc}]) ;
            out << " for commodity " << k->getFullyQualifiedName() << ", node "<< k->getSourceName();
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }
        
    if (printNone)
        out << "None.\n";
    printNone = true;
     
    out << "\nScanning for constraint 15 \"gamma_ki + tau_ij <= ldt\" violations:\n";

    // for all commodities
    for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
    {
        // This constraint is enforcing the fact that we want the dispatch time, gamma,
        // to be early enough that the commodity can be delivered in time.  This is true
        // if we only have the idea of holdover arcs, but with the idea of delay arcs,
        // we must include that last arc segment to the destination node.
        auto& arc = mP_ka.at(k).back();
        double arrival_time = as_double(gamma_ki[{k, arc->getSource()}]) + tau_ij[to_index(arc)];
        auto ldt = k->getLatestDeliveryTime();
        double delta = ldt - arrival_time;

        if (delta < -CONSTRAINT_TOLERANCE)
        {
            out << arrival_time << " > " << ldt;
            out << " for commodity " << k->getFullyQualifiedName() << ", node "<< k->getDestinationName();
            out << " delta = " << delta << "\n";
            printNone = false;
        }
    }

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 16 \"delta_kk_ij >= gamma_k1i - gamma_k2i\" violations:\n";

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					auto rhs = as_double(gamma_ki[{k1, arc->getSource()}])
						- as_double(gamma_ki[{k2, arc->getSource()}]);
					auto lhs = as_double(delta_kk_ij[{k1, k2, arc}]);
					double delta = lhs - rhs;

					if (delta < -CONSTRAINT_TOLERANCE)
					{
						out << lhs << " < " << rhs;
						out << " for commodities " << k1->getFullyQualifiedName() << " and " << k2->getFullyQualifiedName();
						out << ", node " << arc->getSourceServiceName();
						out << " delta = " << delta << "\n";
						printNone = false;
					}
				}
			}
		}
	}

    if (printNone)
        out << "None.\n";
    printNone = true;

    out << "\nScanning for constraint 17 \"delta_kk_ij >= gamma_k2i - gamma_k1i\" violations:\n";

    for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
    {
        const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					auto rhs = as_double(gamma_ki[{k2, arc->getSource()}])
						- as_double(gamma_ki[{k1, arc->getSource()}]);
					auto lhs = as_double(delta_kk_ij[{k1, k2, arc}]);
					double delta = lhs - rhs;

					if (delta < -CONSTRAINT_TOLERANCE)
					{
						out << lhs << " < " << rhs;
						out << " for commodities " << k1->getFullyQualifiedName() << " and " << k2->getFullyQualifiedName();
						out << ", node " << arc->getSourceServiceName();
						out << " delta = " << delta << "\n";
						printNone = false;
					}
				}
			}
		}
	}

    if (printNone)
        out << "None.\n";
    printNone = true;
}

//=======================================================================================
// H E L P E R   M E T H O D S
//=======================================================================================

void cConstructFeasibleSolutionModel_Gurobi::SaveSolution()
{
	auto objVal = mModel.get(GRB_DoubleAttr_ObjVal);

	SaveDispatchAndMismatchTimes();

	if (mOutputDebugMsgs)
	{
		DumpFeasibleSolutionInfo(*mpDebugOutput);
	}

	if (mLogGamma_ki)
	{
		std::string filename = mLogPath + "ConstructFeasibleSolution_Gamma_ki_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpGamma_ki(file);
		file.close();
	}

	if (mLogDelta_kk_ij)
	{
		std::string filename = mLogPath + "ConstructFeasibleSolution_Delta_kk_ij_";
		filename += std::to_string(mPass) + ".txt";
		std::ofstream file(filename);
		DumpDelta_kk_ij(file);
		file.close();
	}

	if (objVal == 0.0)
	{
		if (mOutputDebugMsgs)
		{
			*mpDebugOutput << "none...";
		}

		return;
	}

	FindArcsToLengthen();

	if (mOutputDebugMsgs)
	{
		*mpDebugOutput << mArcsToLengthen.size() << "...";
	}
}

void cConstructFeasibleSolutionModel_Gurobi::FindArcsToLengthen()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Arcs to Lengthen";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	mArcsToLengthen.clear();

	// delta_kk_ij represents the mismatch in dispatch time between commodity k1 and k2 at node i
	for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
	{
		const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					double delta = as_double(delta_kk_ij[{k1, k2, arc}]);
					if (delta > DELTA_TOLERANCE)
						mArcsToLengthen.insert(arc);
				}
			}
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		if (mArcsToLengthen.size() > 0)
			*mpDebugOutput << ", found " << mArcsToLengthen.size();

		*mpDebugOutput << std::endl;
	}
}

void cConstructFeasibleSolutionModel_Gurobi::SaveDispatchAndMismatchTimes()
{
	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Dispatch Times";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	// gamma_ki represents the dispatch time of commodity k at node i
	for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
	{
		auto& k = it->first;
		auto& path = it->second;
		for (auto& node : path)
		{
			try
			{
				mDispatchTime_ki[{k, node}] = as_double(gamma_ki[{k, node}]);
			}
			catch (const GRBException& e)
			{
				std::string msg = "SaveDispatchAndMismatchTimes (dispatch time): ";
				msg += "[ " + k->getUniqueName() + ", " + node->getName() + "] ";
				throw cOptimizationException(msg + e.getMessage());
			}
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}

	start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tFinding Mismatch Times";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	// delta_kk_ij represents the mismatch in dispatch time between commodity k1 and k2 at node i
	for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
	{
		const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					try
					{
						double delta = as_double(delta_kk_ij[{k1, k2, arc}]);
						if (delta > DELTA_TOLERANCE)
							mMismatches_ki[{k1, k2, arc}] = delta;
					}
					catch (const GRBException& e)
					{
						std::string msg = "SaveDispatchAndMismatchTimes (dispatch time): ";
						msg += "[ " + k1->getUniqueName() + ", " + k2->getUniqueName() + "] ";
						throw cOptimizationException(msg + e.getMessage());
					}
				}
			}
		}
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// M O D E L   B U I L D I N G   M E T H O D S
//=======================================================================================

void cConstructFeasibleSolutionModel_Gurobi::ConstructOptimizatonVariables()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\n\tCreating optimization variables";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
		{
			auto k = it->first;
			auto& path = it->second;
			for (auto& node : path)
			{
#if ADD_DEBUG_NAMES
				std::string varName = "gamma_ki:" + k->getShipmentName() + "," + node->getName();
				std::replace(varName.begin(), varName.end(), ' ', '_');
				// The lower bound of zero enforces constraint 18 for the algorithm list on page 1313
				gamma_ki[{k, node}] = mModel.addVar(0, +GRB_INFINITY, 0, GRB_CONTINUOUS, varName.c_str());
#else
				// Setting the lower bound to zero satifies constraint 18.
				gamma_ki[{k, node}] = mModel.addVar(0, +GRB_INFINITY, 0, GRB_CONTINUOUS);
#endif
			}
		}

		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;

						/*
						 * According to page 1313, delta_kk_ij >= 0
						 */
#if ADD_DEBUG_NAMES
						std::string varName = "delta_kk_ij: (" + k1->getShipmentName()
							+ ", " + k2->getShipmentName() + ") " + to_string(*arc);
						std::replace(varName.begin(), varName.end(), ' ', '_');

						delta_kk_ij[{k1, k2, arc}] = mModel.addVar(0.0, +GRB_INFINITY, 0.0,
							GRB_CONTINUOUS, varName.c_str());
#else
						delta_kk_ij[{k1, k2, arc}] = mModel.addVar(0.0, +GRB_INFINITY, 0.0,
							GRB_CONTINUOUS);
#endif
					}
				}
			}
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("ConstructOptimizatonVariables: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}


void cConstructFeasibleSolutionModel_Gurobi::BuildOptimizatonProblem()
{

	auto start = std::chrono::system_clock::now();

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "\tCreating optimization function";

		if (mPrintTimingInfo)
		{
			auto start_time = std::chrono::system_clock::to_time_t(start);

			*mpDebugOutput << " started at " << ctsndp::to_string(start_time);
		}

		*mpDebugOutput << "... " << std::flush;
	}

	try
	{
		GRBLinExpr dispatchMismatchVars;
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;
						dispatchMismatchVars += delta_kk_ij[{k1, k2, arc}];
					}
				}
			}
		}

		mModel.setObjective(dispatchMismatchVars, GRB_MINIMIZE);
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("BuildOptimizatonProblem: " + e.getMessage());
	}

	if (mVerboseMode || mPrintTimingInfo)
	{
		*mpDebugOutput << "done";

		if (mPrintTimingInfo)
		{
			print_end_elapse_times(*mpDebugOutput, start);
		}

		*mpDebugOutput << std::endl;
	}
}

//=======================================================================================
// C O N S T R A I N T   M E T H O D S
//=======================================================================================

//
// Perform constraint 13 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_Gurobi::constraint13_EnsureAllowableDispatchTimes()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint13: Ensure Allowable Dispatch Times";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			auto& path = mP_ka.at(k);

			if (path.empty())
				continue;

			const auto& nodes = mP_ki.at(k);
			auto n = path.size() - 1;
			for (std::size_t i = 0; i < n; ++i)
			{
				auto& arc = path[i];
				auto node = arc->getSource();

				auto lhs = GRBLinExpr(gamma_ki[{k, node}] + tau_ij[to_index(arc)]);

				const auto& next = nodes[i + 1];

#if ADD_DEBUG_NAMES
				const auto& destination = arc->getDestinationServiceLocation();
				std::string constrName = "C13_EnsureAllowableDispatchTimes(";
				constrName += k->getFullyQualifiedName() + ", " + destination->getName() + ")";
				mModel.addConstr(lhs, GRB_LESS_EQUAL, gamma_ki[{k, node}], constrName.c_str());
#else
				mModel.addConstr(lhs, GRB_LESS_EQUAL, gamma_ki[{k, next}]);
#endif
			}
		}

		mModel.update();
	}
	catch (GRBException& e)
	{
		throw cOptimizationException("constraint13_EnsureAllowableDispatchTimes: "s + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 14 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_Gurobi::constraint14_EnsureDispatchIsAfterAvailable()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint14: Ensure Dispatch Is After Available";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			auto& loc = mP_ki.at(k).front();
			double eat = k->getEarliestAvailableTime();

			gamma_ki[{k, loc}].set(GRB_DoubleAttr_LB, eat);
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint14_EnsureDispatchIsAfterAvailable: "s + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 15 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_Gurobi::constraint15_EnsureArrivalBeforeDue()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint15: Ensure Arrival Before Due";
        
        if (mPrintTimingInfo)
        {
             auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		// for all commodities
		for (auto& k : mPartiallyTimeExpandedNetwork.commodities())
		{
			// This constraint is enforcing the fact that we want the dispatch time, gamma,
			// to be early enough that the commodity can be delivered in time.  This is true
			// if we only have the idea of holdover arcs, but with the idea of delay arcs,
			// we must include that last arc segment to the destination node.
			auto& path = mP_ka.at(k);
			auto it = path.rbegin();

			auto& arc = *it;
			auto lhs = GRBLinExpr(gamma_ki[{k, arc->getSource()}] + tau_ij[to_index(arc)]);
			auto ldt = k->getLatestDeliveryTime();

#if ADD_DEBUG_NAMES
			std::string constrName = "C15_EnsureArrivalBeforeDue(";
			constrName += k->getFullyQualifiedName() + ", " + to_string(*arc) /*->getDestinationServiceName()*/ + ")";
			mModel.addConstr(lhs, GRB_LESS_EQUAL, ldt, constrName.c_str());
#else
			mModel.addConstr(lhs, GRB_LESS_EQUAL, ldt);
#endif
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint15_EnsureArrivalBeforeDue: "s + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}

//
// Perform constraint 16 and 17 for the algorithm listed on page 1313
//
void cConstructFeasibleSolutionModel_Gurobi::constraint16and17_DispatchDelta_k1_k2()
{
    auto start = std::chrono::system_clock::now();

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "\t\tConstraint16 and 17: Dispatch Delta between k1 and k2";
        
        if (mPrintTimingInfo)
        {
            auto start_time = std::chrono::system_clock::to_time_t(start);
            
            *mpDebugOutput << " started at " << ctsndp::to_string(start_time);
        }
        
        *mpDebugOutput << "... " << std::flush;
    }

	try
	{
		for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
		{
			const auto& arc = arc_it->first;

			const auto& groups = arc_it->second;
			if (groups.empty())
				continue;

			for (const auto& commodities : groups)
			{
				if (commodities.empty())
					continue;

				auto last = --commodities.end();
				for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
				{
					auto& k1 = *k1_it;

					auto& gamma_k1 = gamma_ki[{k1, arc->getSource()}];

					auto k2_it = k1_it;
					for (++k2_it; k2_it != commodities.end(); ++k2_it)
					{
						auto& k2 = *k2_it;
						auto& gamma_k2 = gamma_ki[{k2, arc->getSource()}];

						auto rhs16 = GRBLinExpr(gamma_k1 - gamma_k2);
						auto rhs17 = GRBLinExpr(gamma_k2 - gamma_k1);

#if ADD_DEBUG_NAMES
						std::string constrName = "C16_DispatchDelta_{";
						constrName += k1->getFullyQualifiedName() + " - ";
						constrName += k2->getFullyQualifiedName() + "} at ";
						constrName += arc->getSource()->getName();
						mModel.addConstr(delta_kk_ij[{k1, k2, arc}], GRB_GREATER_EQUAL, rhs16, constrName.c_str());

						constrName = "C17_DispatchDelta_{";
						constrName += k2->getFullyQualifiedName() + " - ";
						constrName += k1->getFullyQualifiedName() + "} at ";
						constrName += arc->getSource()->getName();
						mModel.addConstr(delta_kk_ij[{k1, k2, arc}], GRB_GREATER_EQUAL, rhs17, constrName.c_str());
#else
						auto& delta = delta_kk_ij[{k1, k2, arc}];
						mModel.addConstr(delta, GRB_GREATER_EQUAL, rhs16);
						mModel.addConstr(delta, GRB_GREATER_EQUAL, rhs17);
#endif
					}
				}
			}
		}

		mModel.update();
	}
	catch (const GRBException& e)
	{
		throw cOptimizationException("constraint16and17_DispatchDelta_k1_k2: "s + e.getMessage());
	}

    if (mVerboseMode || mPrintTimingInfo)
    {
        *mpDebugOutput << "done";
        
        if (mPrintTimingInfo)
        {
            print_end_elapse_times(*mpDebugOutput, start);
        }

        *mpDebugOutput << std::endl;
    }
}


//=======================================================================================
// D E B U G   M E T H O D S
//=======================================================================================

void cConstructFeasibleSolutionModel_Gurobi::DumpFeasibleSolutionInfo(std::ostream& out)
{
    out << "\n";

	DumpGamma_ki(out);

	out << "\n";

	DumpDelta_kk_ij(out);
}

void cConstructFeasibleSolutionModel_Gurobi::DumpGamma_ki(std::ostream& out) const
{
	out << "*** Dispatch times ***\n";
	for (auto it = mP_ki.begin(); it != mP_ki.end(); ++it)
	{
		auto& k = it->first;
		auto& path = it->second;
		for (auto& node : path)
		{
			out << k->getFullyQualifiedName() << "," << node->getName() << "= " << as_double(gamma_ki.at({k, node})) << "\n";
		}

		out << "\n";
	}
}

void cConstructFeasibleSolutionModel_Gurobi::DumpDelta_kk_ij(std::ostream& out) const
{
	if (mJ_ak.empty())
	{
		out << "No shared arcs!\n" << std::endl;
		return;
	}

	out << "*** Difference in Dispatch times ***\n";
	for (auto arc_it = mJ_ak.begin(); arc_it != mJ_ak.end(); ++arc_it)
	{
		const auto& arc = arc_it->first;

		const auto& groups = arc_it->second;
		if (groups.empty())
			continue;

		for (const auto& commodities : groups)
		{
			if (commodities.empty())
				continue;

			auto last = --commodities.end();
			for (auto k1_it = commodities.begin(); k1_it != last; ++k1_it)
			{
				auto& k1 = *k1_it;

				auto k2_it = k1_it;
				for (++k2_it; k2_it != commodities.end(); ++k2_it)
				{
					auto& k2 = *k2_it;
					out << arc->getSource()->getName() << " {";
					out << k1->getFullyQualifiedName() << ", " << k2->getFullyQualifiedName() << "} = ";
					out << as_double(delta_kk_ij.at({k1, k2, arc})) << "\n";
				}
			}
		}
	}
}
