
#pragma once
#ifndef GUROBI_MATRIX_HPP
#define GUROBI_MATRIX_HPP

#include <gurobi_c++.h>
#include <algorithm>


// Forward declarations
template<char VTYPE>
class grb_matrix_row_span;

// Gurobi Matrix class
template<char VTYPE>
class GRBMatrix
{
public:
    typedef GRBVar                                  value_type;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef GRBVar*                                 iterator;
    typedef const iterator                          const_iterator;
    typedef std::size_t                             size_type;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;
    
	GRBMatrix();
	GRBMatrix(GRBModel& model, size_type numRows, size_type numCols);
	GRBMatrix(const GRBMatrix&) = delete;
	GRBMatrix(const GRBMatrix&& other) = delete;
    ~GRBMatrix();

    size_type numRows() const;
    size_type numCols() const;
    
    iterator begin() const;
    iterator end() const;

	GRBVar& operator()(size_type row, size_type col);
    const GRBVar& operator()(size_type row, size_type col) const;
	grb_matrix_row_span<VTYPE> operator[](size_type pos);
    const grb_matrix_row_span<VTYPE> operator[](size_type pos) const;

    // Modifiers
    void clear();
	void allocate(GRBModel& model, size_type numRows, size_type numCols);

	void set_lower_bound(const double lb);
	void set_upper_bound(const double ub);
	void set_objective(const double obj);

	void set_lower_bound(size_type row, size_type col, const double lb);
	void set_upper_bound(size_type row, size_type col, const double ub);
	void set_objective(size_type row, size_type col, const double obj);
	void set_name(size_type row, size_type col, const std::string& name);
    
private:
	GRBVar* mpData;
    size_type mNumRows;
    size_type mNumCols;
};

template<char VTYPE>
class grb_matrix_row_span
{
public:
    typedef GRBVar      value_type;
    typedef std::size_t size_type;
    
	GRBVar& operator[](size_type pos);
    const GRBVar& operator[](size_type pos) const;
    
private:
	grb_matrix_row_span(GRBVar* start, GRBVar* end);
    
	GRBVar* mpStart;
	GRBVar* mpEnd;
    
    friend GRBMatrix<VTYPE>;
};


///////////////////////////////////////////////////////////////////////////////
//
//  Implementation Details
//
///////////////////////////////////////////////////////////////////////////////

template<char VTYPE>
GRBMatrix<VTYPE>::GRBMatrix() : mpData(nullptr), mNumRows(0), mNumCols(0)
{
}

template<char VTYPE>
GRBMatrix<VTYPE>::GRBMatrix(GRBModel& model, size_type numRows, size_type numCols)
    : mpData(nullptr), mNumRows(numRows), mNumCols(numCols)
{
    mpData = model.addVars(mNumRows * mNumCols, VTYPE);
}

template<char VTYPE>
GRBMatrix<VTYPE>::~GRBMatrix()
{
    clear();
}

template<char VTYPE>
typename GRBMatrix<VTYPE>::size_type GRBMatrix<VTYPE>::numRows() const
{
    return mNumRows;
}

template<char VTYPE>
typename GRBMatrix<VTYPE>::size_type GRBMatrix<VTYPE>::numCols() const
{
    return mNumCols;
}

template<char VTYPE>
typename GRBMatrix<VTYPE>::iterator GRBMatrix<VTYPE>::begin() const
{
    return mpData;
}

template<char VTYPE>
typename GRBMatrix<VTYPE>::iterator GRBMatrix<VTYPE>::end() const
{
    if (mpData)
        return &mpData[mNumCols*mNumRows];
    return mpData;
}

template<char VTYPE>
GRBVar& GRBMatrix<VTYPE>::operator()(size_type row, size_type col)
{
    return *(mpData + row*mNumCols + col);
}

template<char VTYPE>
const GRBVar& GRBMatrix<VTYPE>::operator()(size_type row, size_type col) const
{
    return *(mpData + row*mNumCols + col);
}

template<char VTYPE>
grb_matrix_row_span<VTYPE> GRBMatrix<VTYPE>::operator[](size_type pos)
{
    return grb_matrix_row_span<VTYPE>(mpData + pos*mNumCols, mpData + (pos+1)*mNumCols);
}

template<char VTYPE>
const grb_matrix_row_span<VTYPE> GRBMatrix<VTYPE>::operator[](size_type pos) const
{
    return grb_matrix_row_span<VTYPE>(mpData + pos*mNumCols, mpData + (pos+1)*mNumCols);
}

template<char VTYPE>
void GRBMatrix<VTYPE>::clear()
{
	delete[] mpData;
    mpData = nullptr;
    mNumRows = 0;
    mNumCols = 0;
}

template<char VTYPE>
void GRBMatrix<VTYPE>::allocate(GRBModel& model, size_type numRows, size_type numCols)
{
	if (mpData)
	{
		throw std::logic_error("GRBArray already allocated!");
	}

	mpData = model.addVars(numRows * numCols, VTYPE);
	mNumRows = numRows;
	mNumCols = numCols;
	model.update();
	return;
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_lower_bound(const double lb)
{
	std::for_each_n(mpData, numRows * numCols,
		[lb](GRBVar& var) {var.set(GRB_DoubleAttr_LB, lb); });
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_upper_bound(const double ub)
{
	std::for_each_n(mpData, numRows * numCols,
		[ub](GRBVar& var) {var.set(GRB_DoubleAttr_UB, ub); });
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_objective(const double obj)
{
	std::for_each_n(mpData, numRows * numCols,
		[obj](GRBVar& var) {var.set(GRB_DoubleAttr_Obj, obj); });
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_lower_bound(size_type row, size_type col, const double lb)
{
	(mpData + row * mNumCols + col)->set(GRB_DoubleAttr_LB, lb);
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_upper_bound(size_type row, size_type col, const double ub)
{
	(mpData + row * mNumCols + col)->set(GRB_DoubleAttr_UB, ub);
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_objective(size_type row, size_type col, const double obj)
{
	(mpData + row * mNumCols + col)->set(GRB_DoubleAttr_Obj, obj);
}

template<char VTYPE>
void GRBMatrix<VTYPE>::set_name(size_type row, size_type col, const std::string& name)
{
	(mpData + row * mNumCols + col)->set(GRB_StringAttr_VarName, name.c_str());
}


template<char VTYPE>
grb_matrix_row_span<VTYPE>::grb_matrix_row_span(GRBVar* start, GRBVar* end)
: mpStart(start), mpEnd(end)
{}

template<char VTYPE>
GRBVar& grb_matrix_row_span<VTYPE>::operator[](size_type pos)
{
    return *(mpStart + pos);
}

template<char VTYPE>
const GRBVar& grb_matrix_row_span<VTYPE>::operator[](size_type pos) const
{
    return *(mpStart + pos);
}

#endif