﻿//
//  algorithm7_consolidate_arcs.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 4/8/20.
//

#include "ctsndp_algorithms.hpp"
#include "PartiallyTimeExpandedNetwork/PartiallyTimeExpandedNetwork.hpp"
#include "ServiceNetwork/ServiceLink.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>


/**
 * This is algorithm 7 listed on page 1312
 * Requires: Node i in 𝒩; time point t_new in 𝒯i with t_k < t_new
 * Modifies: The partially time expanded network
 **/
std::vector<cPartiallyTimeExpandedNetworkArc*> ctsndp::refine(cPartiallyTimeExpandedNetwork& network,
	cPartiallyTimeExpandedNetworkNode*& node,
	double t_k, double t_new, double& t_k_1, double step_time, unsigned int capacity,
	const cServiceLink* const link,
	std::ostream* debug)
{
	if (debug)
	{
		*debug << "  Refine: " << *node << " t_k=" << t_k;
		*debug << ", t_new=" << t_new << ", t_k_1=" << t_k_1;
		*debug << ", step_time=" << step_time << "\n";
	}

	std::vector<cPartiallyTimeExpandedNetworkArc*> arcsToDelete;
	std::vector<cPartiallyTimeExpandedNetworkHoldoverArc*>	holdoverArcsToDelete;
	std::vector<cPartiallyTimeExpandedNetworkArc*>		holdArcsToDelete;

	// It is possible that t_new > t_k_1.  In this case, we will need to do
	// two steps:
	//  1) Delete the outflow arc that is using the link of interest as
	//		that flow must join to queue waiting for the link to have 
	//		capacity
	//	2) Find new values for t_k and t_k_1
	if (t_k_1 > 0 && t_k_1 < t_new)
	{
		if ((t_new - t_k_1) < step_time)
		{
			const auto node_t_k_1 = network.findNode(node->getServiceLocation(), t_k_1);
			for (auto arc : node_t_k_1->getOutflows())
			{
				if (arc->getServiceLink() == link)
				{
					arcsToDelete.push_back(arc);
					break;
				}
			}
		}

		// We need to find the lower bound, t_k, and upper bound, t_k+1
		auto time_points = network.timepoints(node->getServiceLocation());
		assert(!time_points.empty());

		// Find lower bound such that t_k <= t_new
		auto lower = std::lower_bound(time_points.begin(), time_points.end(), t_new);
		if (lower == time_points.begin())
			t_k = *(lower);
		else
			t_k = *(--lower);

		// Find upper bound such that t_new <= t_k_1
		auto upper = std::upper_bound(lower, time_points.end(), t_new);
		if (upper == time_points.end())
			t_k_1 = *(--upper);
		else
			t_k_1 = *(upper);
	}

	// Line 1: Add node (i, t_new) to 𝒩t
	auto* node_t_new = network.addNode(node, t_new);
	if (debug)
	{
		*debug << "    Adding node: " << *node_t_new << "\n";
	}

	const auto node_t_k = network.findNode(node->getServiceLocation(), t_k);

	// If the new node is between two nodes, we will need to remove the 
	// storage arc between (i, t_k) and (i, t_k_1). Then, we will reconnect
	// the storage arcs by adding ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
	// Note: if t_new - t_k <= step_time, we will add a hold arc else a holdover arc
	if ((t_k < t_new) && (t_new < t_k_1))
	{
		// Line 2: Delete arc ((i, t_k), (i, t_k_1)) from ℋt
		const auto node_t_k_1 = network.findNode(node->getServiceLocation(), t_k_1);

		if (node_t_k_1)
		{
			auto old_arc = network.findHoldoverArc(node_t_k, node_t_k_1);
			if (old_arc)
			{
				network.removeHoldoverArc(old_arc);
				holdoverArcsToDelete.push_back(old_arc);

				// Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
				// The first add is outside of the if statement
				auto arc = network.addHoldoverArc(node_t_new, node_t_k_1);
				if (debug)
				{
					*debug << "    Adding holdover arc: " << *arc << "\n";
				}
			}
			else
			{
				auto old_arc = network.findHoldArc(node_t_k, node_t_k_1);
				if (old_arc)
				{
					network.removeArc(old_arc);
					holdArcsToDelete.push_back(old_arc);

					// Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
					// The first add is outside of the if statement
					auto arc = network.addHoldArc(node_t_new, node_t_k_1, t_k_1 - t_new);
					if (debug)
					{
						*debug << "    Adding hold arc: " << *arc << "\n";
					}
				}
				else
				{
					if (debug)
					{
						auto& arcs = node_t_k->getOutflows();
						for (auto arc : arcs)
							*debug << *arc << "\n";
					}
				}
			}
		}
	}

	// Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to ℋt
	if ((t_new - t_k) <= step_time)
	{
		auto arc = network.addHoldArc(node_t_k, node_t_new, (t_new - t_k));
		if (debug)
		{
			*debug << "    Adding hold arc: " << *arc << "\n";
		}
	}
	else
	{
		auto arc = network.addHoldoverArc(node_t_k, node_t_new);
		if (debug)
		{
			*debug << "    Adding holdover arc: " << *arc << "\n";
		}
	}

	// Line 3: for all ((i, t_k), (j, t)) in 𝒜t
	// Copy all of the outflows from the node (i, t_k) to the new
	// node (i, t_new)
	auto outFlows = network.getAllOutflowArcsFromNode(node_t_k, false);
	for (auto& outflow : outFlows)
	{
		if (outflow->isStorageArc())
			continue;

		if (outflow->connectedToDelayNode())
			continue;

		// Line 4: Add arc ((i, t_new), (j, t)) to 𝒜t
		auto arc = network.addArc(node_t_new, outflow->getDestination(), outflow);
		if (debug)
		{
			*debug << "    Adding arc: " << *arc << "\n";
		}
		
		if (arc->getServiceLink() == link)
		{
			arc->setCapacity(capacity);
			arc->limitArcCapacity(true);
		}
	}

	for (cPartiallyTimeExpandedNetworkHoldoverArc*& arc : holdoverArcsToDelete)
	{
		if (debug)
		{
			*debug << "    Deleting holdover arc: " << *arc << "\n";
		}
		network.deleteHoldoverArc(arc);
	}
	holdoverArcsToDelete.clear();

	for (cPartiallyTimeExpandedNetworkArc*& arc : holdArcsToDelete)
	{
		if (debug)
		{
			*debug << "    Deleting hold arc: " << *arc << "\n";
		}
		network.deleteArc(arc);
	}
	holdoverArcsToDelete.clear();

	if (debug)
		*debug << "  complete!\n" << std::endl;

	t_k_1 = std::max(t_new, t_k_1);
	node = node_t_new;
	return arcsToDelete;
}



#if 0
void ctsndp::consolidate_arcs(cPartiallyTimeExpandedNetwork& network,
				std::vector<cPartiallyTimeExpandedNetworkArc*>& arcs,
				double dispatch_time,
                std::ostream* debug)
{
	assert(arcs.size() > 1);

    if (debug)
        *debug << "Consolidating arcs in partially time expanded network:\n";

	// Order the arcs by the time given to the source node
	std::sort(arcs.begin(), arcs.end(),
		[](cPartiallyTimeExpandedNetworkArc* a, cPartiallyTimeExpandedNetworkArc* b)
		{ return a->getSourceTime() < b->getSourceTime(); }
	);

	// We need to create a node in the partial time expanded network
	// at the dispatch time.  We will check to see if it already exists...
	auto it = std::find_if(arcs.begin(), arcs.end(),
		[dispatch_time](cPartiallyTimeExpandedNetworkArc* a)
		{ 
			return a->getSourceTime() == dispatch_time; 
		}
	);

	node_ptr_t main_node = nullptr;
	if (it != arcs.end())
	{
		main_node = (*it)->getSource();
	}
	else
	{
		auto loc = arcs.front()->getSourceServiceLocation();
		main_node = network.addNode(loc, dispatch_time);
	}

	// We now find the node below (in time) our grouping of arcs as we need
	// to create a holdover arc from that base node to the one we just created 
	node_ptr_t base_node = arcs.front()->getSource();

	if (base_node->getTime() > 0)
	{
		auto& inflows = base_node->getInflows();
		for (auto& inflow : inflows)
		{
			if (inflow->isHoldoverArc())
			{
				// Shift to the base node and then remove
				// the holdover arc from the new base to
				// the old base
				base_node = inflow->getSource();
				base_node->removeFlow(inflow);
				break;
			}
		}
	}

	// Create the holdover arc: base -> node
	if (base_node != main_node)
	{
		network.addHoldoverArc(base_node, main_node);
	}

	// Create the holdover arc: node -> end
	node_ptr_t end_node = arcs.back()->getSource();
	auto& outflows = end_node->getOutflows();
	for (auto& outflow : outflows)
	{
		if (outflow->isHoldoverArc())
		{
			// Remove the holdover arc from the end node
			// to the later node and then create a new
			// holdover arc to the later node
			end_node->removeFlow(outflow);
			end_node = outflow->getSource();
			network.addHoldoverArc(main_node, end_node);
			break;
		}
	}


	// Determine the destination node.  
	std::set<node_ptr_t> dest_nodes;
	for (auto& arc : arcs)
	{
		node_ptr_t node = arc->getDestinationNode();
		dest_nodes.insert(node);
	}

	node_ptr_t dest_node = *dest_nodes.begin();
	if (dest_nodes.size() > 1)
	{
		auto it = dest_nodes.begin();
		for (++it; it != dest_nodes.end(); ++it)
		{
			double t = (*it)->getTime();
			if ((t > dest_node->getTime())
				&& (t <= dispatch_time))
			{
				dest_node = *it;
			}
		}
	}

	// We need to do a fixup of the partially time expanded
	// network:
	//		1) All arcs going to the source nodes of the
	//		   arc we are consolidating we need to recreate
	//		   to our "main" node except any storage arcs.
	//		2) Reproduce all outflow arcs from the source
	//		   nodes to other destination nodes
	//		3) We will also delete the source nodes since
	//		   our "main" node has taken over that job.
	for (auto& arc : arcs)
	{
		node_ptr_t node = arc->getSource();
		if (node == main_node)
			continue;

		// We can't mess with the 
		if (node->getTime() == 0)
			continue;

		auto inflows = node->getInflows();
		for (auto& inflow : inflows)
		{
			if (!inflow->isHoldoverArc())
			{
				network.addArc(inflow->getSource(), main_node, inflow);
			}

			network.deleteArc(inflow);
		}

		auto outflows = node->getInflows();
		for (auto& outflow : outflows)
		{
			node_ptr_t dst = arc->getDestination();

			if (!outflow->isHoldoverArc() && (dst != main_node))
			{
				network.addArc(main_node, dst, outflow);
			}

			network.deleteArc(outflow);
		}

		network.deleteNode(node);
	}

	// Finally connect our node at dispatch_time to its destination
	arc_ptr_t main_arc = network.addArc(main_node, dest_node, arcs.front());

	// Finally, we have consolidated all of the nodes/arcs and we
	// replaced it with our "main" arc
	arcs.clear();
	arcs.push_back(main_arc);

    if (debug)
        *debug << "complete!" << std::endl;
}
#endif