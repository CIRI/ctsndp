//
//  ConstructFeasibleSolutionModel.hpp
//
//  Created by Brett Feddersen on 11/22/19.
//

#pragma once
#ifndef ConstructFeasibleSolutionModel_hpp
#define ConstructFeasibleSolutionModel_hpp

#include "OptimizationProblem.hpp"
#include "ctsndp_defs.hpp"
#include "Commodities/CommodityBundle.hpp"
#include "matrix.hpp"
#include <vector>
#include <map>

// Forward Declarations
class cPartiallyTimeExpandedNetwork;
class cServiceNetwork;


/*
 * Implementation of the algorithm list in section 4.4 on page 1313 of the
 * Continuous Time Service Network Design Problem paper.
 */
class cConstructFeasibleSolutionModel : public cOptimizationProblem
{
public:
    cConstructFeasibleSolutionModel(int pass_number,
									const cPartiallyTimeExpandedNetwork& expandedNetwork,
                                    const cServiceNetwork& serviceNetwork,
                                    const ctsndp::commodity_path_node_t& p_ki,
                                    const ctsndp::commodity_path_arc_t& p_ka,
									const ctsndp::consolidation_groups_t& j_ak,
									bool verboseMode, bool printTimingInfo);
    
    ~cConstructFeasibleSolutionModel();

    /**
     * \brief Builds the "construct feasible solution" optimization problem.
	 * See page 1313.
     */
    void BuildModel() override;

	/**
	 * \brief Returns all of the arc that are too "short" in the partially time-expanded network
	 *		delta_k1k2ij > 0
	 */
	std::set<ctsndp::arc_ptr_t> returnArcsToLengthen();

    /**
     * \brief Returns all of the dispatch times for the commodities moving through the service network
     */
    ctsndp::dispatch_time_t     returnDispatchTimes();

    /**
     * \brief Returns the mismatch of dispatch times between two commodities from the same location
     */
    ctsndp::dispatch_mismatch_times_t returnMismatchTimes();

protected:
	virtual void ConstructOptimizatonVariables() = 0;
	virtual void BuildOptimizatonProblem() = 0;

    //
    // Constraint definitions used in the Minimize Cost optimization
    //
	virtual void constraint13_EnsureAllowableDispatchTimes() = 0;
	virtual void constraint14_EnsureDispatchIsAfterAvailable() = 0;
	virtual void constraint15_EnsureArrivalBeforeDue() = 0;
	virtual void constraint16and17_DispatchDelta_k1_k2() = 0;
    

	//
	// Inputs
	//
    const cPartiallyTimeExpandedNetwork& mPartiallyTimeExpandedNetwork;
    const cServiceNetwork&               mServiceNetwork;
    
    // the path commodities through the service network by nodes visited
    const ctsndp::commodity_path_node_t& mP_ki;

    // the path commodities through the service network by arc
    const ctsndp::commodity_path_arc_t&  mP_ka;

    // the set of all pairs of commodities dispatched on an arc
//	const ctsndp::consolidation_t&       mJ_ak;
	const ctsndp::consolidation_groups_t&       mJ_ak;

    //
    // Note: "i" refers to the source node of a arc, while "j" refers to the destination
    //       "k" refers to a single commodity from the set of commodities
    //

    std::vector<double> tau_ij;         // the actual travel time of a commodity utilizing the arc (i,j)

    //
    // Variables to hold the output data
    //
	std::set<ctsndp::arc_ptr_t>			mArcsToLengthen;
	ctsndp::dispatch_time_t				mDispatchTime_ki;
	ctsndp::dispatch_mismatch_times_t	mMismatches_ki;

    //
    // Debug Variables
    //
	bool mVerboseMode;
    bool mPrintTimingInfo;

public:
	std::string mLogPath;
	bool mLogGamma_ki;
	bool mLogDelta_kk_ij;
};

#endif /* ConstructFeasibleSolutionModel_hpp */
