//
//  test_service_networks.hpp
//  commodities_shared
//
//  Created by Brett Feddersen on 4/22/19.
//

#ifndef test_service_networks_hpp
#define test_service_networks_hpp

#include "../src/ServiceNetwork/ServiceNetwork.hpp"
#include <memory>

/**
 *  Creates a unique smart pointer containing the nullptr.
 */
std::unique_ptr<cServiceNetwork> create_null_network();

/**
 *  Creates an empty (no nodes/arcs) service network
 */
std::unique_ptr<cServiceNetwork> create_empty_network();

/**
 *  Creates a service network with a single source node, "src" and a single
 *  destination node, "dst".  The service network will have numNodes between
 *  the source node and the destination node.
 *
 *  src --> n1 --> n2 --> ... --> n_numNodes --> dst
 */
std::unique_ptr<cServiceNetwork> create_linear_network(unsigned int numNodes,
                                                       double* travel_time = nullptr);

/**
 *  Creates a service nework with a multiple source nodes, "src#" and multiple
 *  destination nodes, "dst#".  The service network will have numNodes between
 *  the source nodes and the destination nodes.  Note: there must be at least
 *  one intermediate node (numNodes >= 1)
 *
 *  Prerequisites:
 *      numSrc > 0 and numDst > 0
 *
 *  src1 -+                                     +-> dst1
 *  src2 -+                                     +-> dst2
 *  src3 -+-> n1 --> n2 --> ... --> n_numNodes -+-> dst3
 *  src4 -+                                     +-> dst4
 *  srcN -+                                     +-> dstM
 */
std::unique_ptr<cServiceNetwork> create_multiple_src_dst_linear_network(unsigned int numSrc,
                                                                        unsigned int numNodes,
                                                                        unsigned int numDst,
                                                                        double* travel_time = nullptr);


#endif /* test_service_networks_hpp */
