//
//  test_shipments.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 4/22/19.
//

#include "test_shipments.hpp"

std::unique_ptr<cCommodityShipments> create_null_shipment()
{
    std::unique_ptr<cCommodityShipments> shipments;
    return shipments;
}

std::unique_ptr<cCommodityShipments> create_empty_shipment()
{
    std::unique_ptr<cCommodityShipments> shipments(new cCommodityShipments);
    return shipments;
}

std::unique_ptr<cCommodityShipments> create_empty_commodities()
{
    std::unique_ptr<cCommodityShipments> shipments(new cCommodityShipments);
    auto shipment = shipments->addShipment("Shipment1", "src", "dst");
    shipment->setEarliestAvailableTime(0);
    shipment->setLatestDeliveryTime(1);
    return shipments;
}

std::unique_ptr<cCommodityShipments> create_shipments(const info_t& info)
{
    std::unique_ptr<cCommodityShipments> shipments(new cCommodityShipments);
    for (int i = 0; i < info.size(); ++i)
    {
        auto shipment = shipments->addShipment("Shipment" + std::to_string(i+1),
                                               info[i].sourceName, info[i].destinationName);
        cCommodityBundle* bundle = shipment->addCommodityBundle("Basic");
        shipment->setEarliestAvailableTime(info[i].earliestAvailableTime);
        shipment->setLatestDeliveryTime(info[i].latestDeliveryTime);
        
		if (info[i].delayCost > 0)
			bundle->setDelayCost(info[i].delayCost);

		if (info[i].delayPenaltyRate > 0)
			bundle->setDelayPenalties(info[i].delayPenaltyRate, info[i].delayPenaltyLimit);

		if (info[i].maxDelayTime > 0)
            bundle->setMaxDelayTime(info[i].maxDelayTime);
    }
    return shipments;

}


