//
//  test_service_networks.cpp
//  commodities_shared
//
//  Created by Brett Feddersen on 4/22/19.
//

#include "test_service_networks.hpp"
#include <cassert>

std::unique_ptr<cServiceNetwork> create_null_network()
{
    std::unique_ptr<cServiceNetwork> network;
    return network;
}

std::unique_ptr<cServiceNetwork> create_empty_network()
{
    std::unique_ptr<cServiceNetwork> network(new cServiceNetwork);
    return network;
}

std::unique_ptr<cServiceNetwork> create_linear_network(unsigned int numNodes, double* travel_time)
{
    std::unique_ptr<cServiceNetwork> network(new cServiceNetwork);
    auto* src = network->addLocation("src");
    auto* dst = network->addLocation("dst");
    unsigned int numArcs = 1;
    for (int i = 0; i < numNodes; ++i)
    {
        auto* node = network->addLocation("n" + std::to_string(i+1));
        auto* link = network->addLink("link" + std::to_string(numArcs++), src, node);
        link->setCapacity(1);
        link->setCommodityBundleCapacity(10);
        link->setFixedCost(1);
        link->setCommodityBundleCost(1);
        link->setTravelTime(5);
        src = node;
    }
    auto* link = network->addLink("link" + std::to_string(numArcs++), src, dst);
    link->setCapacity(1);
    link->setCommodityBundleCapacity(10);
    link->setFixedCost(1);
    link->setCommodityBundleCost(1);
    link->setTravelTime(5);
    if (travel_time)
    {
        *travel_time = 5 * (numNodes + 1);
    }
    return network;
}

std::unique_ptr<cServiceNetwork> create_multiple_src_dst_linear_network(unsigned int numSrc,
                                                                        unsigned int numNodes,
                                                                        unsigned int numDst,
                                                                        double* travel_time)
{
    assert(numSrc > 0);
    assert(numDst > 0);
    numNodes = numNodes == 0 ? 1 : numNodes;
    std::unique_ptr<cServiceNetwork> network(new cServiceNetwork);
    auto* node = network->addLocation("n0");
    unsigned int numArcs = 1;
    for (int i = 0; i < numSrc; ++i)
    {
        auto* src = network->addLocation("src" + std::to_string(i+1));
        auto* link = network->addLink("link" + std::to_string(numArcs++), src, node);
        link->setCapacity(1);
        link->setCommodityBundleCapacity(10);
        link->setFixedCost(1);
        link->setCommodityBundleCost(1);
        link->setTravelTime(5);
    }
    
    for (int i = 1; i < numNodes; ++i)
    {
        auto* tmp = network->addLocation("n" + std::to_string(i));
        auto* link = network->addLink("link" + std::to_string(numArcs++), node, tmp);
        link->setCapacity(1);
        link->setCommodityBundleCapacity(10);
        link->setFixedCost(1);
        link->setCommodityBundleCost(1);
        link->setTravelTime(5);
        node = tmp;
    }
    
    for (int i = 0; i < numDst; ++i)
    {
        auto* dst = network->addLocation("dst" + std::to_string(i+1));
        auto* link = network->addLink("link" + std::to_string(numArcs++), node, dst);
        link->setCapacity(1);
        link->setCommodityBundleCapacity(10);
        link->setFixedCost(1);
        link->setCommodityBundleCost(1);
        link->setTravelTime(5);
    }
    
    if (travel_time)
    {
        *travel_time = 5 * (numNodes + 1);
    }

    return network;
}
