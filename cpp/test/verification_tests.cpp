
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch2/catch.hpp"
#include "../src/ctsndp.hpp"
#include "test_shipments.hpp"
#include "test_service_networks.hpp"

#include <fstream>
#include <memory>
#ifdef WIN32
#else
	#include <unistd.h>
#endif

#define PRINT_RESULTS false

const char* log_path = "K:\\CIRI\\test_logs\\";

void PRINT_RESULT(const cRoutes& result)
{
    if (PRINT_RESULTS) return;
    if (!result.mSuccess)
        print_result(std::cout, result);

}


TEST_CASE( "Simple verification of model inputs", "[verificaton]" )
{
    cCTSNDP ctsndp;

    SECTION( "Testing input validation..." )
    {
        auto result = ctsndp.solve(create_null_network(), create_null_shipment());
        REQUIRE(!result->mSuccess);
        
        result = ctsndp.solve(create_empty_network(), create_null_shipment());
        REQUIRE(!result->mSuccess);
        
        result = ctsndp.solve(create_linear_network(0), create_null_shipment());
        REQUIRE(!result->mSuccess);
        
        result = ctsndp.solve(create_linear_network(0), create_empty_shipment());
        REQUIRE(!result->mSuccess);
        
        result = ctsndp.solve(create_linear_network(0), create_empty_commodities());
        REQUIRE(!result->mSuccess);
    }
}

TEST_CASE( "Testing of two location single road networks", "[trivial networks]" )
{
    cCTSNDP ctsndp;
    ctsndp.printPartiallyTimeExpandedNetwork(false);

    double travel_time = 0;
    auto network = create_linear_network(0, &travel_time);
    auto loc = network->serviceLocations();

    info_t info;
    info.emplace_back("src", "dst", 1, 5+travel_time);
    
    auto shipments = create_shipments(info);
    
    SECTION( "Testing valid delivery time..." )
    {
        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.verboseMode(false);
        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
		REQUIRE(result->mIdealFixedCost == result->mFixedCost);
		REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
        
        // Reclaim the network and shipment.
        network.reset(result->mpServiceNetwork.release());
        shipments.reset(result->mpShipments.release());
    }

    SECTION( "Testing invalid delivery time..." )
    {
        // We will make it so the shipment can't be delivered in time!
        auto& shipment = *(shipments->getShipments().begin());
        shipment->setLatestDeliveryTime(travel_time-1);

        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.verboseMode(false);
        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        REQUIRE(!result->mSuccess);
        
        // Reclaim the network and shipment.
        network.reset(result->mpServiceNetwork.release());
        shipments.reset(result->mpShipments.release());
    }
    
    SECTION( "Testing delayed delivery time..." )
    {
        // We will make it so the shipment can't be delivered in time,
        // but there is now a delay cost!
		// The cost should be:
		//	Fixed = 1, Variable = 1 and delay = 1
        auto& shipment = *(shipments->getShipments().begin());
        shipment->getCommodityBundle(0)->setDelayCost(1);
		shipment->setLatestDeliveryTime(travel_time);

		ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.verboseMode(false);
		ctsndp.quiteMode(false);

/*
		ctsndp.mLogPath = log_path;
		ctsndp.mLogArcToLengthen = true;
		ctsndp.mLogPTEN = true;
		ctsndp.mLogCommoditySchedules = true;
		ctsndp.mLogConsolidations = true;
		ctsndp.mLogCommodityPaths = true;
		ctsndp.mLogUtilization = true;
		ctsndp.mLogResults = true;
		ctsndp.mLogLinkOvercapacity = true;
		ctsndp.mLogProperties = true;
		ctsndp.mLogLinkUtilization = true;
		ctsndp.mLogMinimizeCostConstraints = true;
		ctsndp.mLogMinimizeCostConstraintViolations = true;
		ctsndp.mLogIdentifyArcsConstraintViolations = true;
		ctsndp.mLogConstructSolutionConstraintViolations = true;
		ctsndp.mLogMinimizeDelayConstraintViolations = true;
*/

        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);
        if (!result->mSuccess) print_result(std::cout, *result);
        REQUIRE(result->mSuccess);
		REQUIRE(result->mIdealDelayCost == result->mDelayCost);
		REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
		REQUIRE(result->mIdealVariableCost == result->mVariableCost);
		REQUIRE(result->mIdealVariableDelayCost == result->mVariableDelayCost);
		REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
	}
}

TEST_CASE( "Testing of sharing of single road networks", "[sharing arc]" )
{
    cCTSNDP ctsndp;
    ctsndp.printPartiallyTimeExpandedNetwork(false);

    SECTION( "Test sharing single arc..." )
    {
        double travel_time = 0;
        auto network = create_linear_network(0, &travel_time);
        
        info_t info;
        info.emplace_back("src", "dst", 1, 5+travel_time);
        info.emplace_back("src", "dst", 2, 4+travel_time);

        auto shipments = create_shipments(info);
        
        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.debugLevel(-1);

        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);
        if (!result->mSuccess) print_result(std::cout, *result);

        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }
    
    SECTION( "Test NOT sharing single arc..." )
    {
        double travel_time = 0;
        auto network = create_linear_network(0, &travel_time);
        
        info_t info;
        info.emplace_back("src", "dst", 1, 1+travel_time);
        info.emplace_back("src", "dst", 2, 2+travel_time);
        
        auto shipments = create_shipments(info);

		ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.quiteMode(false);
        ctsndp.debugLevel(-1);
        auto result = ctsndp.solve(std::move(network), std::move(shipments));

        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
        if (!result->mSuccess) print_result(std::cout, *result);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }
	
	SECTION( "Test sharing single arc with delay..." )
    {
        double travel_time = 0;
        auto network = create_linear_network(2, &travel_time);
        
        info_t info;
        info.emplace_back("src", "dst", 1, travel_time, 0.1);
        info.emplace_back("src", "dst", 2, 2+travel_time);
        
        auto shipments = create_shipments(info);
        
        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.verboseMode(false);
        ctsndp.debugLevel(-1);
		ctsndp.quiteMode(false);

        auto result = ctsndp.solve(std::move(network), std::move(shipments));

		PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
		REQUIRE(result->mIdealDelayCost == result->mDelayCost);
		REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
		REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
    }
    
    SECTION( "Test NO sharing single arc with delay..." )
    {
        double travel_time = 0;
        auto network = create_linear_network(0, &travel_time);
        
        info_t info;
        info.emplace_back("src", "dst", 1, travel_time, 10);
        info.emplace_back("src", "dst", 2, 2+travel_time);
        
        auto shipments = create_shipments(info);
        
        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.verboseMode(false);
        ctsndp.quiteMode(false);
        ctsndp.debugLevel(-1);
        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
		REQUIRE(result->mIdealDelayCost == result->mDelayCost);
		REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
		REQUIRE(result->mIdealVariableDelayCost == result->mVariableDelayCost);
		REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
    }
}


TEST_CASE( "Testing of sharing of multiple src/dst with common road networks", "[sharing middle arcs]" )
{
    cCTSNDP ctsndp;
    
    SECTION( "Test sharing two arcs..." )
    {
        double travel_time = 0;
        auto network = create_multiple_src_dst_linear_network(2, 3, 2, &travel_time);
        
        info_t info;
        info.emplace_back("src1", "dst1", 1, 5+travel_time);
        info.emplace_back("src2", "dst2", 2, 4+travel_time);
        
        auto shipments = create_shipments(info);
        
        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);

        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }
    
    SECTION( "Test NOT sharing arcs..." )
    {
        double travel_time = 0;
        auto network = create_multiple_src_dst_linear_network(2, 3, 2, &travel_time);

        info_t info;
        info.emplace_back("src1", "dst1", 1, 1+travel_time);
        info.emplace_back("src2", "dst2", 2, 2+travel_time);
        
        auto shipments = create_shipments(info);

		auto result = ctsndp.solve(std::move(network), std::move(shipments));

		PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }

	SECTION("Test NOT sharing arcs with incompatible commodities...")
	{
		double travel_time = 0;
		auto network = create_multiple_src_dst_linear_network(2, 3, 2, &travel_time);

		info_t info;
		info.emplace_back("src1", "dst1", 10, 10 + travel_time);
		info.emplace_back("src2", "dst2", 100, 100 + travel_time);

		auto shipments = create_shipments(info);

		auto result = ctsndp.solve(std::move(network), std::move(shipments));

		PRINT_RESULT(*result);
		REQUIRE(result->mSuccess);
		REQUIRE(result->mIdealFixedCost == result->mFixedCost);
		REQUIRE(result->mIdealVariableCost == result->mVariableCost);
		REQUIRE(result->mIdealTotalCost == result->mTotalCost);
	}

	SECTION( "Test sharing arcs with delay..." )
	{
		double travel_time = 0;
		auto network = create_multiple_src_dst_linear_network(2, 3, 2, &travel_time);

		info_t info;
		info.emplace_back("src1", "dst1", 1, travel_time, 1); // Make sure we use delay node
		info.emplace_back("src2", "dst2", 2, 2 + travel_time);
         
		auto shipments = create_shipments(info);

		auto result = ctsndp.solve(std::move(network), std::move(shipments));

		PRINT_RESULT(*result);
		REQUIRE(result->mSuccess);
		REQUIRE(result->mIdealDelayCost == result->mDelayCost);
		REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
		REQUIRE(result->mIdealVariableCost == result->mVariableCost);
		REQUIRE(result->mIdealVariableDelayCost == result->mVariableDelayCost);
		REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
	}
}


TEST_CASE( "Testing of sharing of two road networks", "[sharing linear arcs]" )
{
    cCTSNDP ctsndp;
    
    SECTION( "Test sharing two arcs..." )
    {
        double travel_time = 0;
        auto network = create_linear_network(1, &travel_time);
        
        info_t info;
        info.emplace_back("src", "dst", 1, 5+travel_time);
        info.emplace_back("src", "dst", 2, 4+travel_time);
        
        auto shipments = create_shipments(info);
        
        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }
    
    SECTION( "Test NOT sharing single arc..." )
    {
        double travel_time = 0;
        auto network = create_linear_network(1, &travel_time);
        
        info_t info;
        info.emplace_back("src", "dst", 1, 1+travel_time);
        info.emplace_back("src", "dst", 2, 2+travel_time);
        
        auto shipments = create_shipments(info);
 
        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.debugLevel(-1);

        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        ctsndp.debugLevel(-1);

        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }
    
     SECTION( "Test sharing single arc with delay..." )
     {
		 double travel_time = 0;
		 auto network = create_linear_network(1, &travel_time);

		 info_t info;
		 info.emplace_back("src", "dst", 1, travel_time, 1);
		 info.emplace_back("src", "dst", 2, 2 + travel_time);

		 auto shipments = create_shipments(info);

		 auto result = ctsndp.solve(std::move(network), std::move(shipments));
		 PRINT_RESULT(*result);
		 REQUIRE(result->mSuccess);
		 REQUIRE(result->mIdealDelayCost == result->mDelayCost);
		 REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
		 REQUIRE(result->mIdealVariableCost == result->mVariableCost);
		 REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
     }

	 SECTION("Test sharing single arc with delay penalty...")
	 {
		 double travel_time = 0;
		 auto network = create_linear_network(1, &travel_time);
		 for (auto& link : network->serviceLinks())
		 {
			 link->setCommodityBundleCapacity(1);
			 link->setCycleTime(5);
		 }

		 info_t info;
		 info.emplace_back("src", "dst", 1, travel_time, 1);
		 info.emplace_back("src", "dst", 1, travel_time, 1, 10);

		 auto shipments = create_shipments(info);

		 ctsndp.printPartiallyTimeExpandedNetwork(false);
		 ctsndp.debugLevel(-1);
		 ctsndp.printTimingInfo(false);
		 ctsndp.quiteMode(false);

/*
		 ctsndp.mLogPath = log_path;
		 ctsndp.mLogArcToLengthen = true;
		 ctsndp.mLogArcToShorten = true;
		 ctsndp.mLogPTEN = true;
		 ctsndp.mLogCommoditySchedules = true;
		 ctsndp.mLogConsolidations = true;
		 ctsndp.mLogCommodityPaths = true;
		 ctsndp.mLogUtilization = true;
		 ctsndp.mLogResults = true;
		 ctsndp.mLogLinkOvercapacity = true;
		 ctsndp.mLogProperties = true;
		 ctsndp.mLogLinkUtilization = true;
		 ctsndp.mLogProgress = true;
*/

		 ctsndp.setConsolidationTime(1);

		 auto result = ctsndp.solve(std::move(network), std::move(shipments));
		 PRINT_RESULT(*result);
		 print_result(std::cout, *result);
		 REQUIRE(result->mSuccess);
		 REQUIRE(result->mIdealDelayCost == result->mDelayCost);
		 REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
		 REQUIRE(result->mIdealVariableCost == result->mVariableCost);
		 REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
	 }
}

TEST_CASE( "Testing of capacity limited networks", "[capacity limited linear arcs]" )
{
    cCTSNDP ctsndp;
    
    SECTION( "Test capacity limited two node, single arc network..." )
    {
        double travel_time = 5;
        std::unique_ptr<cServiceNetwork> network(new cServiceNetwork);
        auto* src = network->addLocation("src");
        auto* dst = network->addLocation("dst");
        for (int i = 0; i < 1; ++i)
        {
            auto* link = network->addLink("link" + std::to_string(i), src, dst);
            link->setCapacity(1);
            link->setCommodityBundleCapacity(2);
            link->setFixedCost(static_cast<float>(1+50*i));
            link->setCommodityBundleCost(1);
            link->setTravelTime(5);
            link->setCycleTime(1);
        }

        info_t info;
        info.emplace_back("src", "dst", 1, travel_time+3);
        
        auto shipments = create_shipments(info);
        auto shipment = *(shipments->getShipments().begin());
        auto commodity = shipment->getCommodityBundle(0);

        for (int i = 1; i < 5; ++i)
        {
            cCommodityBundle* bundle = shipment->addCommodityBundle(commodity->getName() + std::to_string(i));
            
            if (commodity->hasDelayCost())
                bundle->setDelayCost(commodity->getDelayCost());
            
            if (commodity->hasMaxDelayTime())
                bundle->setMaxDelayTime(commodity->getMaxDelayTime());
        }

        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.debugLevel(-1);

		ctsndp.setConsolidationTime(1);
		auto result = ctsndp.solve(std::move(network), std::move(shipments));
        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
		// For some reason, the fixed costs suffer from very small floating point
		// errors.  The result is that they don't compare eventhough the difference
		// is 4 x 10^-6.
        REQUIRE(std::floor(result->mIdealFixedCost) == std::floor(result->mFixedCost));
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }

    SECTION( "Test capacity limited two node, two arc network..." )
    {
        double travel_time = 5;
        std::unique_ptr<cServiceNetwork> network(new cServiceNetwork);
        auto* src = network->addLocation("src");
        auto* node = network->addLocation("node");
        auto* dst = network->addLocation("dst");
        {
            auto* link = network->addLink("link0", src, node);
            link->setCapacity(100);
            link->setCommodityBundleCapacity(1);
            link->setFixedCost(1);
            link->setCommodityBundleCost(1);
            link->setTravelTime(travel_time);
            link->setCycleTime(1);
        }
        {
            auto* link = network->addLink("link1", node, dst);
            link->setCapacity(1);
            link->setCommodityBundleCapacity(2);
            link->setFixedCost(1);
            link->setCommodityBundleCost(1);
            link->setTravelTime(travel_time);
            link->setCycleTime(1);
        }

        info_t info;
        info.emplace_back("src", "dst", 1, 2*travel_time+3);
        
        auto shipments = create_shipments(info);
        auto shipment = *(shipments->getShipments().begin());
        auto commodity = shipment->getCommodityBundle(0);

        for (int i = 1; i < 5; ++i)
        {
            cCommodityBundle* bundle = shipment->addCommodityBundle(commodity->getName());
            
            if (commodity->hasDelayCost())
                bundle->setDelayCost(commodity->getDelayCost());
            
            if (commodity->hasMaxDelayTime())
                bundle->setMaxDelayTime(commodity->getMaxDelayTime());
        }

        ctsndp.printPartiallyTimeExpandedNetwork(false);
        ctsndp.debugLevel(-1);
        ctsndp.printTimingInfo(false);

/*
		ctsndp.mLogPath = log_path;
		ctsndp.mLogArcToLengthen = true;
		ctsndp.mLogPTEN = true;
		ctsndp.mLogCommoditySchedules = true;
		ctsndp.mLogConsolidations = true;
		ctsndp.mLogCommodityPaths = true;
		ctsndp.mLogUtilization = true;
		ctsndp.mLogResults = true;
		ctsndp.mLogLinkOvercapacity = true;
		ctsndp.mLogProperties = true;
		ctsndp.mLogLinkUtilization = true;
		ctsndp.mLogProgress = true;
*/

		ctsndp.setConsolidationTime(1);
		auto result = ctsndp.solve(std::move(network), std::move(shipments));

		PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost == result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost == result->mTotalCost);
    }
}

/*
TEST_CASE( "Testing of splitting commodities among two road networks", "[saturate linear arcs]" )
{
    cCTSNDP ctsndp;
    
    SECTION( "Test saturating two arcs..." )
    {
        double travel_time = 5;
        std::unique_ptr<cServiceNetwork> network(new cServiceNetwork);
        auto* src = network->addLocation("src");
        auto* dst = network->addLocation("dst");
        for (int i = 0; i < 1; ++i)
        {
            auto* link = network->addLink("link" + std::to_string(i), src, dst);
            link->setCapacity(1);
            link->setCommodityBundleCapacity(2);
            link->setFixedCost(1+50*i);
            link->setCommodityBundleCost(1);
            link->setTravelTime(5);
        }

        info_t info;
        info.emplace_back("src", "dst", 1, travel_time+3);
        
        auto shipments = create_shipments(info);
        auto shipment = *(shipments->getShipments().begin());
        auto commodity = shipment->getCommodityBundle(0);

        for (int i = 1; i < 5; ++i)
        {
            cCommodityBundle* bundle = shipment->addCommodityBundle(commodity->getName());
            
            if (commodity->hasDelayCost())
                bundle->setDelayCost(commodity->getDelayCost());
            
            if (commodity->hasMaxDelayTime())
                bundle->setMaxDelayTime(commodity->getMaxDelayTime());
        }

        ctsndp.printPartiallyTimeExpandedNetwork(true);
        ctsndp.debugLevel(5);

        auto result = ctsndp.solve(std::move(network), std::move(shipments));
        print_result(std::cout, *result);
        PRINT_RESULT(*result);
        REQUIRE(result->mSuccess);
        REQUIRE(result->mIdealFixedCost <= result->mFixedCost);
        REQUIRE(result->mIdealVariableCost == result->mVariableCost);
        REQUIRE(result->mIdealTotalCost <= result->mTotalCost);
    }
}
*/
