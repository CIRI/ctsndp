//
//  test_shipments.hpp
//  commodities_shared
//
//  Created by Brett Feddersen on 4/22/19.
//

#ifndef test_shipments_hpp
#define test_shipments_hpp

#include "../src/Commodities/CommodityShipment.hpp"
#include <memory>
#include <vector>

struct sShipmentInfo
{
    std::string sourceName;
    std::string destinationName;
    double earliestAvailableTime;
    double latestDeliveryTime;
    double delayCost;
	double delayPenaltyRate;
	double delayPenaltyLimit;
	double maxDelayTime;
    
    sShipmentInfo()
    :
        earliestAvailableTime(0), latestDeliveryTime(0),
        delayCost(-1), delayPenaltyRate(-1), maxDelayTime(-1)
    {}

    sShipmentInfo(const std::string& src, const std::string& dst,
                  double eat, double ldt, 
				  double delaycost = -1, 
				  double delaypenaltyrate = -1,
				  double delaypenaltylimit = 1000000,
		          double maxdelaytime = -1)
    :
        sourceName(src), destinationName(dst), earliestAvailableTime(eat),
        latestDeliveryTime(ldt), delayCost(delaycost), 
		delayPenaltyRate(delaypenaltyrate), delayPenaltyLimit(delaypenaltylimit),
		maxDelayTime(maxdelaytime)
    {}
};

typedef std::vector<sShipmentInfo> info_t;

/**
 *  Creates a unique smart pointer containing the nullptr.
 */
std::unique_ptr<cCommodityShipments> create_null_shipment();

/**
 *  Creates a cCommodityShipments container with no shipments
 */
std::unique_ptr<cCommodityShipments> create_empty_shipment();

/**
 *  Creates a cCommodityShipments container with no commodities
 */
std::unique_ptr<cCommodityShipments> create_empty_commodities();

/**
 *  Creates an empty (no nodes/arcs) service network
 */
std::unique_ptr<cCommodityShipments> create_shipments(const info_t& info);


#endif /* test_shipments_hpp */
