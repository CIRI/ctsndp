
from ServiceNetwork import ServiceNetwork
from algorithm1_solve_ctsndp import solve_ctsndp
import sys
import getopt


def main(argv):
    shipment_file = "../../data/dual_commodity_no_delay2.json"
    network_file = "../../data/dual_network.gsni"
    try:
        opts, args = getopt.getopt(argv, "hs:n:", ["shipment=", "network="])
    except getopt.GetoptError:
        print('ctsndp.py -s <shipmentfile> -n <networkfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('ctsndp.py -s <shipment_file> -n <network_file>')
            sys.exit()
        elif opt in ("-s", "--shipment"):
            shipment_file = arg
        elif opt in ("-n", "--network"):
            network_file = arg

    network = ServiceNetwork(shipment_file, network_file)
    solve_ctsndp(network)


if __name__ == '__main__':
    main(sys.argv[1:])
