# coding: utf-8

from commodities import CommodityBundle, CommodityBundles
from cptl.network import MulticommodityFlowNetwork


class ServiceNetwork(object):
    commodityBundles = None
    network = None

    def __init__(self, commoditiesFilePath, networkFilePath):
        self.commodityBundles = CommodityBundles.create(commoditiesFilePath)
        mcf = MulticommodityFlowNetwork()
        self.network = mcf.read(networkFilePath, False)

    def find_node(self, nodeName):
        for node in self.network.nodes():
            if nodeName == node:
                return node
        return None

    def nodes(self):
        return self.network.nodes()

    def edges(self):
        return self.network.edges()

    def commodities(self):
        return self.commodityBundles

    def find_edges_with_source(self, node):
        edges = list()
        for edge in self.network.edges():
            if edge[0] == node:
                edges.append(edge)
        return edges

    def get_travel_time(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:travel_time']

    def get_fixed_cost(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:fixed_cost']

    def get_per_unit_of_flow_cost(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:commodity_bundle_cost']

    def get_capacity(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:capacity']

    def get_commodity_bundle_capacity(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:commodity_bundle_capacity']
