# coding: utf-8

from PartiallyTimeExpandedNetwork import PartiallyTimeExpandedNetwork
import sys

"""
This is algorithm 2 listed on page 1311
Requires: Directed network 𝒟 = (𝒩, 𝒜), commodity set 𝒦
"""


def create_initial(service_network):
    partially_time_expanded_network = PartiallyTimeExpandedNetwork()
    # Line 1: for all commodities in K
    for commodity in service_network.commodities():
        # Line 2: Add node to Nt
        for source_name in commodity.sources:
            source_node = service_network.find_node(source_name)
            assert source_node is not None, "Unknown source name: %r" % source_name
            partially_time_expanded_network.add_node(source_node, commodity.eat, commodity)
        # Line 3
        for destination_name in commodity.destinations:
            destination_node = service_network.find_node(destination_name)
            assert destination_node is not None, "Unknown destination name: %r" % destination_name
            partially_time_expanded_network.add_node(destination_node, commodity.ldt, commodity)
    # line 5
    for node in service_network.nodes():
        partially_time_expanded_network.add_node(node, 0, None)
    # Line 8
    for source_node in partially_time_expanded_network.get_nodes():
        t = source_node.time
        # Line 9
        edges = service_network.find_edges_with_source(source_node.service_node)
        for edge in edges:
            t_prime = -1
            destination_node = None
            t_limit = t + service_network.get_travel_time(edge)
            # Line 10
            nodes = partially_time_expanded_network.find_nodes(edge[1])
            for node in nodes:
                if (node.time <= t_limit) and (node.time > t_prime):
                    t_prime = node.time
                    destination_node = node

            partially_time_expanded_network.add_arc(source_node, destination_node,
                                          service_network.get_capacity(edge),
                                          service_network.get_commodity_bundle_capacity(edge),
                                          service_network.get_fixed_cost(edge),
                                          service_network.get_per_unit_of_flow_cost(edge),
                                          service_network.get_travel_time(edge))
        # Line 12: Find smallest t_prime such that (i, t_time) is in Nt and t_prime > t
        service_nodes = partially_time_expanded_network.find_nodes(source_node.service_node)
        t_prime = sys.maxsize
        destination_node = None
        for node in service_nodes:
            if (node.time > t) and (node.time < t_prime):
                t_prime = node.time
                destination_node = node

        if destination_node is not None:
            partially_time_expanded_network.add_holdover_arc(source_node, destination_node)

    return partially_time_expanded_network
