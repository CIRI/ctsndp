# coding: utf-8

"""
This is algorithm 5 listed on page 1312
Requires: Node i in 𝒩; time point t_new in Ti with t_k < t_new < t_k_1
"""


def restore(partially_time_expanded_network, node, t_new):
    # We need to find the lower bound, t_k
    time_points = partially_time_expanded_network.get_time_points(node.get_service_node())
    t_k = 0.0
    for i in range(len(time_points)):
        if node.get_time() == time_points[i]:
            t_k = time_points[i]
            break

    # Line 1: For all ((i, t_k), (j, t)) in 𝒜t do
    source_node = partially_time_expanded_network.find_node(node.get_service_node(), t_k)
    arcs = partially_time_expanded_network.find_all_outflow_arcs_from_node(source_node)
    for arc in arcs:
        t = arc.get_destination().get_time()
        # Line 2: Set t_prime = arg max{ s in Tj | s <= t_new + tau_ij}
        t_prime = 0
        t_limit = t_new + arc.get_actual_travel_time()
        time_points = partially_time_expanded_network.get_time_points(arc.get_destination_service_node())
        for time_point in time_points:
            if (time_point <= t_limit) and (time_point > t_prime):
                t_prime = time_point
        # Line 3:
        if t_prime != t:
            # Line 4: Delete arc ((i, t_new), (j, t)) from 𝒜t
            src = partially_time_expanded_network.find_node(arc.get_source_service_node(), t_new)
            dest = partially_time_expanded_network.find_node(arc.get_destination_service_node(), t)
            arc = partially_time_expanded_network.find_arc(src, dest)
            partially_time_expanded_network.remove_arc(arc)
            # Line 4: Add arc ((i, t_new), (j, t_prime)) to 𝒜t
            dest = partially_time_expanded_network.find_node(arc.get_destination_service_node(), t_prime)
            partially_time_expanded_network.add_arc(src, dest, arc.capacity, arc.fixed_cost,
                                                    arc.per_unit_of_flow_cost, arc.actual_travel_time)

    # Line 7: For all ((j, t), (i, t_k)) in 𝒜t such that t + tau_ji => t_new do
    arcs = partially_time_expanded_network.find_all_inflow_arcs_to_node(source_node)
    for arc in arcs:
        t = arc.get_source().get_time()
        if t + arc.get_actual_travel_time() >= t_new:
            # Line 8: Delete arc ((j, t), (i, t_k)) from 𝒜t
            partially_time_expanded_network.remove_arc(arc)
            # Line 8: Add arc ((j, t), (i, t_new)) to 𝒜t
            dest = partially_time_expanded_network.find_node(arc.get_destination_service_node(), t_new)
            partially_time_expanded_network.add_arc(arc.get_source(), dest, arc.capacity, arc.fixed_cost,
                                                    arc.per_unit_of_flow_cost, arc.actual_travel_time)

    return
