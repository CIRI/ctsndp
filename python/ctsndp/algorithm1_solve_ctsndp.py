# coding: utf-8

from algorithm2_create_initial import create_initial
from algorithm3_lengthen_arc import lengthen_arc
from ServiceNetworkDesign import *

"""
This is algorithm 1 listed on page 1310
Requires: Flat network 𝒟 = (𝒩, 𝒜), commodity set 𝒦
"""


def solve_ctsndp(service_network):
    # Line 1: Create a partially time-expanded network 𝒟t satisfying properties 1-4 (pg 1308-1310)
    partially_time_expanded_network = create_initial(service_network)
    snd = SND.create(partially_time_expanded_network)

    solved = False
    while not solved:
        snd.reset_gurobi_engine()

        # Line 3: Solve SND(𝒟t)
        snd.minimize_fixed_costs()

        # Line 4: Determine whether the solution to SND(𝒟t) can be converted to feasible solution
        #           to CTSNDP with the same cost
        arcs_to_lengthen = snd.identify_arcs_to_lengthen()
        solved = len(arcs_to_lengthen) == 0

        if solved:
            # Line 6: Stop. The converted solution is optimal for CTSNDP
            snd.construct_feasible_solution()
            snd.print_to_screen()
        else:
            # Line 8: The solution to SND(𝒟t) must use at least one arc that is "too short."
            #           Refine the partially time-expanded network 𝒟t by correcting the length of at
            #           least one such arc, in the process adding at least one new time point to 𝒯i
            #           for some i in 𝒩
            lengthen_arc(partially_time_expanded_network, arcs_to_lengthen)


