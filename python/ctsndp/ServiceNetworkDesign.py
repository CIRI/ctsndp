# coding: utf-8

"""
Purpose of this module is to perform the service network design optimization

This is algorithm list on page 1307, 1312, and 1313
"""

from gurobipy import *


class SND(object):
    gurobi_MinimizeFixedCosts = None
    gurobi_IdentifyArcs = None
    gurobi_ConstructFeasibleSolution = None
    partiallyTimeExpandedNetwork = None

    #
    # Note: i refers to the source node of a arc, while j refers to the destination
    #

    # Model Variables
    q_k = None          # the quantity of commodity k to transport
    u_ij = None         # the unit capacity of the arc (the arc can hold u units of transportation)
    v_ij = None         # the variable capacity of the arc (a unit of transportation can hold v units of commodity)
    f_ij = None         # the fixed costs of utilizing the arc
    c_ij = None         # the variable cost of utilizing the arc
    tau_ij = None       # the actual travel time of a commodity utilizing the arc
    tau_bar_kij = None  # the travel time in, 𝒟, of a commodity, k, utilizing the arc
    cost = 0;

    # Decision Variables: minimize_fixed_costs
    y_ij = None         # the number of times arc (i,j) must be used to transport commodity k
    x_kij = None        # does commodity k use the arc (i,j) in its transport

    # Results from "minimize fixed costs" optimization
    J_ak = None         # the set of all pairs of commodities dispatched on an arc
    J_a = None          # the set of all arc on which more than one commodity is dispatched
    P_ki = None         # the path commodities through the service network
    P_ka = None         # the path commodities through the service network

    # Decision Variables: identify_arcs_to_lengthen
    gamma1_ki = None    # the dispatch time of commodity, k, at node i
    theta_kij = None    # the travel time of arc, (i, j) when taken by commodity, k
    sigma_kij = None    # is the arc allowed to be too short

    # Decision Variables: construct_feasible_solution
    gamma2_ki = None    # the dispatch time of commodity, k, at node i
    delta_kk_ij = None

    def __init__(self, partially_time_expanded_network):
        self.x_kij = dict()
        self.y_ij = dict()
        self.f_ij = dict()
        self.c_ij = dict()
        self.u_ij = dict()
        self.v_ij = dict()
        self.q_k = dict()
        self.tau_ij = dict()
        self.tau_bar_kij = dict()
        self.J_ak = dict()
        self.J_a = dict()
        self.P_ki = dict()
        self.P_ka = dict()
        self.gamma1_ki = dict()
        self.theta_kij = dict()
        self.sigma_kij = dict()
        self.gamma2_ki = dict()
        self.delta_kk_ij = dict()
        self.gurobi_MinimizeFixedCosts = Model("MinimizeFixedCosts")
        self.gurobi_IdentifyArcs = Model("IdentifyArcsToLengthen")
        self.gurobi_ConstructFeasibleSolution = Model("ConstructFeasibleSolution")
        self.partiallyTimeExpandedNetwork = partially_time_expanded_network

    @staticmethod
    def create(partially_time_expanded_network):
        snd = SND(partially_time_expanded_network)
        return snd;

    """
    Implementation of the algorithm list in section 3 on page 1307
    """
    def minimize_fixed_costs(self):
        arcs = self.partiallyTimeExpandedNetwork.get_arcs()
        holdover_arcs = self.partiallyTimeExpandedNetwork.get_holdover_arcs()
        for arc in arcs:
            flowVariableName = "y_ij:" + str(arc).replace(" ", "_")
            # The lower bound of zero enforces constraint 4 for the algorithm list on page 1307
            self.y_ij[arc] = self.gurobi_MinimizeFixedCosts.addVar(lb=0.0, vtype=GRB.INTEGER, name=flowVariableName)
            self.f_ij[arc] = arc.fixed_cost
            self.c_ij[arc] = arc.per_unit_of_flow_cost
            self.u_ij[arc] = arc.capacity
            self.v_ij[arc] = arc.per_unit_of_flow_capacity
            self.tau_ij[arc] = arc.get_actual_travel_time()

        for k in self.partiallyTimeExpandedNetwork.commodities:
            self.q_k[k] = k.quantity
            # Using the BINARY type enforces constraint 3 for the algorithm list on page 1307
            for arc in arcs:
                commodityFlowVariableName = "x_kij:" + str(k).replace(" ", "_") + "," + str(arc).replace(" ", "_")
                self.x_kij[k, arc] = self.gurobi_MinimizeFixedCosts.addVar(vtype=GRB.BINARY, name=commodityFlowVariableName)

            for arc in holdover_arcs:
                commodityFlowVariableName = "holdover_x_kij:" + str(k).replace(" ", "_") + "," + str(arc).replace(" ", "_")
                self.x_kij[k, arc] = self.gurobi_MinimizeFixedCosts.addVar(vtype=GRB.BINARY, name=commodityFlowVariableName)

        self.gurobi_MinimizeFixedCosts.update()

        # Add constraints
        self.__constraint1_ConservationOfFlow()
        self.__constraint2_EnsureSufficientCapacityPerFlow()
        self.__constraint5_TravelTime()

        fixedCostVars = []
        for arc in arcs:
            fixedCostVars.append(self.f_ij[arc]*self.y_ij[arc])

        variableCostVars = []
        for k in self.partiallyTimeExpandedNetwork.commodities:
            for arc in arcs:
                variableCostVars.append(self.c_ij[arc]*self.q_k[k]*self.x_kij[k, arc])


#        delayCostVars = []
#        for k in self.partiallyTimeExpandedNetwork.commodities:
#            for arc in arcs:
#                delayCostVars.append(self.__computeDelayCost(k))

        self.gurobi_MinimizeFixedCosts.setObjective(LinExpr(quicksum(fixedCostVars)
                                                            + quicksum(variableCostVars)
#                                                            + quicksum(delayCostVars)
                                                            ),
                                                    GRB.MINIMIZE)
        self.gurobi_MinimizeFixedCosts.optimize()

        if self.gurobi_MinimizeFixedCosts.status == GRB.Status.OPTIMAL:
            self.cost = self.gurobi_MinimizeFixedCosts.objVal

            # Find all pair of commodities sharing an arc
            self.__find_all_shared_arcs()
            self.__find_commodities_paths()

    """
    Implementation of the algorithm list in section 4.3 on page 1312
    """
    def identify_arcs_to_lengthen(self):
        for k in self.P_ki.keys():
            path = self.P_ki[k]
            for node in path:
                commodityDispatchTimeName = "gamma_ki:" + str(k).replace(" ", "_") + "," + str(node).replace(" ", "_")
                # The lower bound of zero enforces constraint 11 for the algorithm list on page 1312
                self.gamma1_ki[k, node] = self.gurobi_IdentifyArcs.addVar(lb=0.0, vtype=GRB.INTEGER, name=commodityDispatchTimeName)

        for k in self.P_ka.keys():
            path = self.P_ka[k]
            for arc in path:
                commodityTravelTimeName = "theta_kij:" + str(k).replace(" ", "_") + "," + str(arc).replace(" ", "_")
                self.theta_kij[k, arc] = self.gurobi_IdentifyArcs.addVar(lb=0.0, vtype=GRB.CONTINUOUS, name=commodityTravelTimeName)
                commodityTooShortName = "sigma_kij:" + str(k).replace(" ", "_") + "," + str(arc).replace(" ", "_")
                self.sigma_kij[k, arc] = self.gurobi_IdentifyArcs.addVar(vtype=GRB.BINARY, name=commodityTooShortName)
                tau_bar_kij = arc.get_destination_time() - arc.get_source_time()
                self.tau_bar_kij[k, arc] = tau_bar_kij

        self.gurobi_IdentifyArcs.update()

        # Add constraints
        self.__constraint6_CountArcsWithShortenTravelTimes()
        self.__constraint7_EnsureAllowableDispatchTimes()
        self.__constraint8_EnsureDispatchIsAfterAvailable()
        self.__constraint9_EnsureArrivalBeforeDue()
        self.__constraint10_EnsureConsolidations()
        self.__constraint12_EnsureTravelTimeGreaterThanShortenTime()

        allowArcTooShortVars = []
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            path = self.P_ka[k]
            n = len(path)
            for i in range(0, n-1):
                arc = path[i]
                allowArcTooShortVars.append(self.sigma_kij[k, arc])

        self.gurobi_IdentifyArcs.setObjective(LinExpr(quicksum(allowArcTooShortVars)), GRB.MINIMIZE)
        self.gurobi_IdentifyArcs.optimize()

        arc_to_length = list()
        if self.gurobi_IdentifyArcs.status == GRB.Status.OPTIMAL:
            if self.gurobi_IdentifyArcs.objVal == 0.0:
                return arc_to_length
            for k in self.partiallyTimeExpandedNetwork.get_commodities():
                path = self.P_ka[k]
                n = len(path)
                for i in range(0, n - 1):
                    arc = path[i]
                    test = self.sigma_kij[k, arc].x
                    if test:
                        arc_to_length.append(arc)

        return arc_to_length

    """
    Implementation of the algorithm list in section 4.4 on page 1313
    """
    def construct_feasible_solution(self):
        for k in self.P_ki.keys():
            path = self.P_ki[k]
            for node in path:
                commodityDispatchTimeName = "gamma_ki:" + str(k).replace(" ", "_") + "," + str(node).replace(" ", "_")
                # The lower bound of zero enforces constraint 18 for the algorithm list on page 1313
                self.gamma2_ki[k, node] = self.gurobi_ConstructFeasibleSolution.addVar(lb=0.0, vtype=GRB.INTEGER, name=commodityDispatchTimeName)

        for arc in self.J_a.keys():
            for k1, k2 in self.J_ak[arc]:
                deltaDispatchTimeName = "delta_kk_ij: (" + str(k1).replace(" ", "_") \
                                        + ", " + str(k2).replace(" ", "_") + ") "+ str(arc).replace(" ", "_")
                self.delta_kk_ij[k1, k2, arc] = self.gurobi_ConstructFeasibleSolution.addVar(vtype=GRB.CONTINUOUS,
                                                                               name=deltaDispatchTimeName)

        self.gurobi_ConstructFeasibleSolution.update()

        # Add constraints
        self.__constraint13_EnsureAllowableDispatchTimes()
        self.__constraint14_EnsureDispatchIsAfterAvailable()
        self.__constraint15_EnsureArrivalBeforeDue()
        self.__constraint16()
        self.__constraint17()

        dispatchMismatchVars = []
        for arc in self.J_a.keys():
            for k1, k2 in self.J_ak[arc]:
                dispatchMismatchVars.append(self.delta_kk_ij[k1, k2, arc])

        self.gurobi_ConstructFeasibleSolution.setObjective(LinExpr(quicksum(dispatchMismatchVars)), GRB.MINIMIZE)
        self.gurobi_ConstructFeasibleSolution.optimize()

        if self.gurobi_ConstructFeasibleSolution.status == GRB.Status.OPTIMAL:
            objVal = self.gurobi_ConstructFeasibleSolution.objVal
            if objVal == 0:
                return True

        return False

    def reset_gurobi_engine(self):
        self.gurobi_MinimizeFixedCosts.reset()
        self.gurobi_IdentifyArcs.reset()
        self.gurobi_ConstructFeasibleSolution.reset()

        # Remove all data stored
        self.x_kij = dict()
        self.y_ij = dict()
        self.f_ij = dict()
        self.c_ij = dict()
        self.u_ij = dict()
        self.q_k = dict()
        self.tau_ij = dict()
        self.tau_bar_kij = dict()
        self.J_ak = dict()
        self.J_a = dict()
        self.P_ki = dict()
        self.P_ka = dict()
        self.gamma1_ki = dict()
        self.theta_kij = dict()
        self.sigma_kij = dict()
        self.gamma2_ki = dict()
        self.delta_kk_ij = dict()

    def print_to_screen(self):
        print("")
        print("Cost: " + str(self.cost))
        for k in self.partiallyTimeExpandedNetwork.commodities:
            print("")
            print("Moving " + str(k) + ":")
            first_node = True
            arcs = self.P_ka[k]
            path_string = ""
            arrival_time = 0.0
            for arc in arcs:
                t = self.gamma2_ki[k, arc.get_source_service_node()].x
                if first_node:
                    path_string += arc.get_source_service_name()
                    path_string += " (eat: " + str(k.eat) + ", departs: " + str(t) + ")"
                    first_node = False
                else:
                    path_string += " (arrives: " + str(arrival_time) + ", departs: " + str(t) + ")"
                path_string += " -> "
                path_string += arc.get_destination_service_name()
                arrival_time = t + arc.get_actual_travel_time()
            arc = arcs[len(arcs)-1]
            t = self.gamma2_ki[k, arc.get_source_service_node()].x
            t += arc.get_actual_travel_time()
            path_string += " (arrives: " + str(t) + ", ldt: " + str(k.ldt) + ")"
            print(path_string)

    def print_debug_to_screen(self):
        arcs = self.partiallyTimeExpandedNetwork.get_arcs()
        holdover_arcs = self.partiallyTimeExpandedNetwork.get_holdover_arcs()
        for k in self.partiallyTimeExpandedNetwork.commodities:
            print("")
            print("Moving " + str(k) + ":")
            for arc in arcs:
                if self.x_kij[k, arc].x >= 1:
                    print(self.x_kij[k, arc].varName)
            for arc in holdover_arcs:
                if self.x_kij[k, arc].x >= 1:
                    print(self.x_kij[k, arc].varName)
        print("")
        print("The amount of transportation used:")
        for arc in arcs:
            if self.y_ij[arc].x > 0:
                print(self.y_ij[arc].varName + " " + str(self.y_ij[arc].x))

    #
    # Perform constraint 1 for the algorithm list on page 1307
    #
    def __constraint1_ConservationOfFlow(self):
        # for all commodities
        commodities = self.partiallyTimeExpandedNetwork.get_commodities()
        for commodity in commodities:
            source_nodes = commodity.getSourceLocations()
            destination_nodes = commodity.getDestinationLocations()

            for node in self.partiallyTimeExpandedNetwork.get_nodes():
                d = 0
                if node.commodity == commodity:
                    for source_node in source_nodes:
                        if node.service_node == source_node and node.time == commodity.eat:
                            d = 1
                            break
                    for destination_node in destination_nodes:
                        if node.service_node == destination_node and node.time == commodity.ldt:
                            d = -1
                            break

                outFlowVars = []
                inFlowVars = []
                for arc in self.partiallyTimeExpandedNetwork.get_arcs():
                    if arc.get_source_service_node() == node.service_node \
                            and arc.get_source_time() == node.time:
                        outFlowVars.append(self.x_kij[commodity, arc])
                    if arc.get_destination_service_node() == node.service_node \
                            and arc.get_destination_time() == node.time:
                        inFlowVars.append(self.x_kij[commodity, arc])

                for arc in self.partiallyTimeExpandedNetwork.get_holdover_arcs():
                    if arc.get_source_service_node() == node.service_node \
                            and arc.get_source_time() == node.time:
                        outFlowVars.append(self.x_kij[commodity, arc])
                    if arc.get_destination_service_node() == node.service_node\
                            and arc.get_destination_time() == node.time:
                        inFlowVars.append(self.x_kij[commodity, arc])

                lhs = LinExpr(quicksum(outFlowVars) - quicksum(inFlowVars))
                self.gurobi_MinimizeFixedCosts.addConstr(lhs, GRB.EQUAL, d, name=node.service_node)

        self.gurobi_MinimizeFixedCosts.update()

    #
    # Perform constraint 2 for the algorithm listed on page 1307
    #
    def __constraint2_EnsureSufficientCapacityPerFlow(self):
        # for all arcs(i, j) in 𝒜t
        for arc in self.partiallyTimeExpandedNetwork.get_arcs():
            # TERM: Sum over commodity k, q_k * x_kij
            lhs = LinExpr()
            for k in self.partiallyTimeExpandedNetwork.commodities:
                lhs += self.q_k[k] * self.x_kij[k, arc]

            # TERM: u_ij * y_ij
            rhs = LinExpr((self.u_ij[arc] * self.v_ij[arc]) * self.y_ij[arc])

            self.gurobi_MinimizeFixedCosts.addConstr(lhs, GRB.LESS_EQUAL, rhs)

        self.gurobi_MinimizeFixedCosts.update()

    #
    # Perform constraint 5, page 1311, for the algorithm listed on page 1307
    #
    def __constraint5_TravelTime(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            t = k.ldt
            t = t - k.eat

            # for all arcs(i, j) in 𝒜t
            lhs = LinExpr()
            for arc in self.partiallyTimeExpandedNetwork.get_arcs():
                # TERM: tau_ij * x_kij
                lhs += self.tau_ij[arc] * self.x_kij[k, arc]

            self.gurobi_MinimizeFixedCosts.addConstr(lhs, GRB.LESS_EQUAL, t)

        self.gurobi_MinimizeFixedCosts.update()

    #
    # Perform constraint 6 for the algorithm listed on page 1312
    #
    def __constraint6_CountArcsWithShortenTravelTimes(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            path = self.P_ka[k]

            # for all arcs(i, j) in 𝒜t
            for arc in path:
                # TERM: tau_ij * (1 - sigma_kij)
                rhs = LinExpr(self.tau_ij[arc] * (1 - self.sigma_kij[k, arc]))
                self.gurobi_IdentifyArcs.addConstr(self.theta_kij[k, arc], GRB.GREATER_EQUAL, rhs)

        self.gurobi_IdentifyArcs.update()

    #
    # Perform constraint 7 for the algorithm listed on page 1312
    #
    def __constraint7_EnsureAllowableDispatchTimes(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            path = self.P_ka[k]

            for arc in path:
                lhs = LinExpr(self.gamma1_ki[k, arc.get_source_service_node()] + self.theta_kij[k, arc])
                destination = arc.get_destination_service_node()
                self.gurobi_IdentifyArcs.addConstr(lhs, GRB.LESS_EQUAL, self.gamma1_ki[k, destination])

        self.gurobi_IdentifyArcs.update()

    #
    # Perform constraint 8 for the algorithm listed on page 1312
    #
    def __constraint8_EnsureDispatchIsAfterAvailable(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            path = self.P_ki[k]
            self.gurobi_IdentifyArcs.addConstr(k.eat, GRB.LESS_EQUAL, self.gamma1_ki[k, path[0]])

        self.gurobi_IdentifyArcs.update()

    #
    # Perform constraint 9 for the algorithm listed on page 1312
    #
    def __constraint9_EnsureArrivalBeforeDue(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            n = len(self.P_ka[k])
            arc = self.P_ka[k][n-1]
            lhs = LinExpr(self.gamma1_ki[k, arc.get_source_service_node()] + self.theta_kij[k, arc])
            self.gurobi_IdentifyArcs.addConstr(lhs, GRB.LESS_EQUAL, k.ldt)

        self.gurobi_IdentifyArcs.update()

    #
    # Perform constraint 10 for the algorithm listed on page 1312
    #
    def __constraint10_EnsureConsolidations(self):
        for arc in self.J_a.keys():
            for k1, k2 in self.J_ak[arc]:
                self.gurobi_IdentifyArcs.addConstr(self.gamma1_ki[k1, arc.get_source_service_node()],
                                                   GRB.EQUAL,
                                                   self.gamma1_ki[k2, arc.get_source_service_node()])
        self.gurobi_IdentifyArcs.update()

    #
    # Perform constraint 12 for the algorithm listed on page 1312
    #
    def __constraint12_EnsureTravelTimeGreaterThanShortenTime(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            path = self.P_ka[k]
            n = len(path)
            for i in range(0, n-1):
                arc = path[i]
                self.gurobi_IdentifyArcs.addConstr(self.theta_kij[k, arc],
                                                   GRB.GREATER_EQUAL,
                                                   self.tau_bar_kij[k, arc])

        self.gurobi_IdentifyArcs.update()

    #
    # Perform constraint 13 for the algorithm listed on page 1313
    #
    def __constraint13_EnsureAllowableDispatchTimes(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            n = len(self.P_ka[k])
            for i in range(n-1):
                arc = self.P_ka[k][i]
                lhs = LinExpr(self.gamma2_ki[k, arc.get_source_service_node()] + self.tau_ij[arc])
                self.gurobi_ConstructFeasibleSolution.addConstr(lhs,
                                                                GRB.LESS_EQUAL,
                                                                self.gamma2_ki[k, arc.get_destination_service_node()])

        self.gurobi_ConstructFeasibleSolution.update()


    #
    # Perform constraint 14 for the algorithm listed on page 1313
    #
    def __constraint14_EnsureDispatchIsAfterAvailable(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            path = self.P_ki[k]
            self.gurobi_ConstructFeasibleSolution.addConstr(k.eat, GRB.LESS_EQUAL, self.gamma2_ki[k, path[0]])

        self.gurobi_ConstructFeasibleSolution.update()

    #
    # Perform constraint 15 for the algorithm listed on page 1313
    #
    def __constraint15_EnsureArrivalBeforeDue(self):
        # for all commodities
        for k in self.partiallyTimeExpandedNetwork.get_commodities():
            n = len(self.P_ka[k])
            arc = self.P_ka[k][n-1]
            lhs = LinExpr(self.gamma2_ki[k, arc.get_source_service_node()] + self.tau_ij[arc])
            self.gurobi_ConstructFeasibleSolution.addConstr(lhs, GRB.LESS_EQUAL, k.ldt)

        self.gurobi_ConstructFeasibleSolution.update()

    #
    # Perform constraint 16 for the algorithm listed on page 1313
    #
    def __constraint16(self):
        for arc in self.J_a.keys():
            for k1, k2 in self.J_ak[arc]:
                rhs = LinExpr(self.gamma2_ki[k1, arc.get_source_service_node()]
                              - self.gamma2_ki[k2, arc.get_source_service_node()] )
                self.gurobi_ConstructFeasibleSolution.addConstr(self.delta_kk_ij[k1, k2, arc], GRB.GREATER_EQUAL, rhs)

        self.gurobi_ConstructFeasibleSolution.update()

    #
    # Perform constraint 17 for the algorithm listed on page 1313
    #
    def __constraint17(self):
        for arc in self.J_a.keys():
            for k1, k2 in self.J_ak[arc]:
                rhs = LinExpr(self.gamma2_ki[k2, arc.get_source_service_node()]
                              - self.gamma2_ki[k1, arc.get_source_service_node()] )
                self.gurobi_ConstructFeasibleSolution.addConstr(self.delta_kk_ij[k1, k2, arc], GRB.GREATER_EQUAL, rhs)

        self.gurobi_ConstructFeasibleSolution.update()

    def __computeDelayCost(self, commodity):
        t = 0
        for arc in self.partiallyTimeExpandedNetwork.get_arcs():
            if self.x_kij[commodity, arc] >= 1:
                t += arc.get_actual_travel_time()
        for arc in self.partiallyTimeExpandedNetwork.get_holdover_arcs():
            if self.x_kij[commodity, arc] >= 1:
                t += arc.get_actual_travel_time()
        if t > commodity.ldt:
            return commodity.delayCost
        return 0

    #
    #
    #
    def __find_all_shared_arcs(self):
        commodities = self.partiallyTimeExpandedNetwork.get_commodities()
        n = len(commodities)
        if n <= 1:
            return

        arcs = self.partiallyTimeExpandedNetwork.get_arcs()
        # holdover_arcs = self.timeExpandedNetwork.get_holdover_arcs()
        # We are looking for commodity pairs sharing an arc: Ja(k1,k2) page 1312
        for i in range(n-1):
            k1 = commodities[i]
            for arc in arcs:
                if self.x_kij[k1, arc].x >= 1:
                    # Look for another commodity sharing this arc
                    for j in range(i+1, n):
                        k2 = commodities[j]
                        if (k2, arc) in self.x_kij:
                            if self.x_kij[k2, arc].x >= 1:
                                if arc not in self.J_ak:
                                    self.J_ak[arc] = list()
                                self.J_ak[arc].append({k1, k2})
        # We are looking for all arcs carrying more than one commodity: Ja page 1312
        for k in commodities:
            for arc in arcs:
                if self.x_kij[k, arc].x >= 1:
                    if arc not in self.J_a:
                        self.J_a[arc] = 1
                    else:
                        self.J_a[arc] = self.J_a[arc] + 1
        # Remove all arc with count 1
        for arc in arcs:
            if arc in self.J_a:
                if self.J_a[arc] == 1:
                    del(self.J_a[arc])

    #
    # Find the path that commodity, k, follows from its source to its sink
    #
    def __find_commodities_paths(self):
        commodities = self.partiallyTimeExpandedNetwork.get_commodities()
        arcs = self.partiallyTimeExpandedNetwork.get_arcs()
        for k in commodities:
            sources = k.getSourceLocations()
            destinations = k.getDestinationLocations()
            for node in sources:
                path_complete = False
                while not path_complete:
                    old_node = node
                    for arc in arcs:
                        if node == arc.get_source_service_node() \
                                and self.x_kij[k, arc].x >= 1:
                            if k not in self.P_ki:
                                self.P_ki[k] = list()
                                self.P_ka[k] = list()
                            self.P_ki[k].append(arc.get_source_service_node())
                            self.P_ka[k].append(arc)
                            node = arc.get_destination_service_node()
                            if node in destinations:
                                self.P_ki[k].append(arc.get_destination_service_node())
                                path_complete = True
                            break
                    # Make sure we are advancing in our path!
                    if not path_complete and old_node == node:
                        if k in self.P_ki:
                            del(self.P_ki[k])
                            del(self.P_ka[k])
                        break


