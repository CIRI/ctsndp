# coding: utf-8

from PartiallyTimeExpandedNetwork import *

"""
This is algorithm 4 listed on page 1312
Requires: Node i in 𝒩; time point t_new in T with t_k < t_new < t_k+1
"""


def refine(partially_time_expanded_network, node, t_new):
    # Testing to see if the requirements are met!
    time_points = partially_time_expanded_network.get_time_points(node.get_service_node())
    t_k = 0.0
    t_k_1 = 0.0
    for i in range(len(time_points)-1):
        if node.get_time() == time_points[i]:
            t_k = time_points[i]
            t_k_1 = time_points[i+1]
            break

    if not (t_k < t_new < t_k_1):
        print("Requirement violation!")
        return

    # Line 1: Add node (i, t_new) to 𝒩t
    new_node = partially_time_expanded_network.add_node(node.get_service_node(), t_new, node.get_commodity())

    # Line 2: Delete are ((i, t_k), (i, t_k_1)) from 𝒜t
    source_node = partially_time_expanded_network.find_node(node.get_service_node(), t_k)
    dest_node = partially_time_expanded_network.find_node(node.get_service_node(), t_k_1)
    old_arc = partially_time_expanded_network.find_holdover_arc(source_node, dest_node)
    partially_time_expanded_network.remove_holdover_arc(old_arc)

    # Line 2: Add arcs ((i, t_k), (i, t_new)) and ((i, t_new), (i, t_k_1)) to 𝒜t
    partially_time_expanded_network.add_holdover_arc(source_node, new_node)
    partially_time_expanded_network.add_holdover_arc(new_node, dest_node)

    # Line 3: for all ((i, t_k), (j, t)) in 𝒜t
    arcs = partially_time_expanded_network.find_all_outflow_arcs_from_node(source_node, False)
    for arc in arcs:
        # Line 4: Add arc ((i, t_new), (j, t)) to 𝒜t
        partially_time_expanded_network.add_arc(new_node, arc.get_destination(),
                                                arc.capacity, arc.fixed_cost, arc.per_unit_of_flow_cost,
                                                arc.actual_travel_time)

    return

