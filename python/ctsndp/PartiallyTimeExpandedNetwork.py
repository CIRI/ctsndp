# coding: utf-8

import sys


class PartiallyTimeExpandedNode(object):
    service_node = None
    time = 0
    commodity = None

    def __init__(self, service_node, time, commodity):
        self.service_node = service_node
        self.time = time
        self.commodity = commodity

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.time == other.time \
                   and self.service_node == other.service_node \
                   and self.commodity == other.commodity
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        s = self.service_node
        s += "(t:" + str(self.time)
        s += ")"
        return s

    def __hash__(self):
        return hash((self.time, self.service_node, self.commodity))

    def get_name(self):
        return self.service_node.name

    def get_time(self):
        return self.time

    def get_commodity(self):
        return self.commodity

    def get_service_node(self):
        return self.service_node

    def equal(self, other):
        if isinstance(other, self.__class__):
            return self.time == other.time \
                   and self.service_node == other.service_node
        return False


class PartiallyTimeExpandedArc(object):
    source = None
    destination = None
    capacity = 0
    per_unit_of_flow_capacity = 1
    fixed_cost = 0
    per_unit_of_flow_cost = 0
    actual_travel_time = 0

    def __init__(self, source_node, destination_node,
                 capacity, per_unit_of_flow_capacity,
                 fixed_cost, per_unit_of_flow_cost,
                 travel_time):
        self.source = source_node
        self.destination = destination_node
        self.capacity = capacity
        if per_unit_of_flow_capacity >= 1:
            self.per_unit_of_flow_capacity = per_unit_of_flow_capacity
        self.fixed_cost = fixed_cost
        self.per_unit_of_flow_cost = per_unit_of_flow_cost
        self.actual_travel_time = travel_time

    def __str__(self):
        s = ""
        s += str(self.source)
        s += " -> "
        s += str(self.destination)
        return s

    def get_source(self):
        return self.source

    def get_source_service_node(self):
        return self.source.service_node

    def get_source_service_name(self):
        return str(self.source.service_node)

    def get_source_time(self):
        return self.source.time

    def get_destination(self):
        return self.destination

    def get_destination_service_node(self):
        return self.destination.service_node

    def get_destination_service_name(self):
        return str(self.destination.service_node)

    def get_destination_time(self):
        return self.destination.time

    def get_actual_travel_time(self):
        return self.actual_travel_time

    def get_total_capacity(self):
        return self.capacity * self.per_unit_of_flow_capacity


class PartiallyTimeExpandedNetwork(object):
    nodes = None
    arcs = None
    holdover_arcs = None
    time_points = None
    commodities = None

    def __init__(self):
        self.nodes = set()
        self.arcs = set()
        self.holdover_arcs = set()
        self.time_points = dict()
        self.commodities = list()

    def add_node(self, service_node, time, commodity):
        if service_node not in self.time_points:
            self.time_points[service_node] = list()
        self.time_points[service_node].append(time)
        self.time_points[service_node].sort()
        new_node = PartiallyTimeExpandedNode(service_node, time, commodity)
        self.nodes.add(new_node)
        if commodity is not None and commodity not in self.commodities:
            self.commodities.append(commodity)
            self.commodities.sort()
        return new_node

    def get_nodes(self):
        return self.nodes

    def find_node(self, service_node, time):
        for n in self.nodes:
            if n.service_node == service_node and n.time == time:
                return n
        return None

    def find_nodes(self, service_node):
        matching_nodes = list()
        for n in self.nodes:
            if n.service_node == service_node:
                matching_nodes.append(n)
        return matching_nodes

    def add_arc(self, source_node, destination_node,
                capacity, per_unit_of_flow_capacity,
                fixed_cost, per_unit_of_flow_cost,
                travel_time):
        if source_node is None or destination_node is None:
            return
        new_arc = PartiallyTimeExpandedArc(source_node, destination_node,
                                           capacity, per_unit_of_flow_capacity,
                                           fixed_cost, per_unit_of_flow_cost,
                                           travel_time)
        self.arcs.add(new_arc)
        return new_arc

    def add_holdover_arc(self, source_node, destination_node):
        if source_node is None or destination_node is None:
            return
        new_holdover_arc = PartiallyTimeExpandedArc(source_node, destination_node, sys.maxsize, 1, 0, 0, 0)
        self.holdover_arcs.add(new_holdover_arc)
        return new_holdover_arc

    def get_arcs(self):
        return self.arcs

    def get_holdover_arcs(self):
        return self.holdover_arcs

    def find_all_inflow_arcs_to_node(self, node, include_holdover_arcs=True):
        inflows = list()
        for a in self.arcs:
            if a.destination.service_node == node.service_node and a.destination.time == node.time:
                inflows.append(a)

        if include_holdover_arcs:
            for a in self.holdover_arcs:
                if a.destination.service_node == node.service_node and a.destination.time == node.time:
                    inflows.append(a)
        return inflows

    def find_all_outflow_arcs_from_node(self, node, include_holdover_arcs=True):
        outflows = list()
        for a in self.arcs:
            if a.source.service_node == node.service_node and a.source.time == node.time:
                outflows.append(a)

        if include_holdover_arcs:
            for a in self.holdover_arcs:
                if a.source.service_node == node.service_node and a.source.time == node.time:
                    outflows.append(a)
        return outflows

    def find_arc(self, source_node, dest_node):
        if source_node is None or dest_node is None:
            return None

        for a in self.arcs:
            if a.source.equal(source_node) and a.destination.equal(dest_node):
                return a
        return None

    def find_holdover_arc(self, source_node, dest_node):
        if source_node is None or dest_node is None:
            return None

        for a in self.holdover_arcs:
            if a.source.equal(source_node) and a.destination.equal(dest_node):
                return a
        return None

    def remove_arc(self, arc):
        if arc is not None:
            self.arcs.remove(arc)

    def remove_holdover_arc(self, arc):
        if arc is not None:
            self.holdover_arcs.remove(arc)

    def get_commodities(self):
        return self.commodities

    def get_time_points(self, service_node):
        if service_node in self.time_points:
            return self.time_points[service_node]
        return None
