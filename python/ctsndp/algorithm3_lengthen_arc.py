# coding: utf-8

from algorithm4_refine import refine
from algorithm5_restore import restore


"""
This is algorithm 3 listed on page 1312
Requires: Arc((i,t),(j.t')) in 𝒜t
"""


def lengthen_arc(partially_time_expanded_network, arcs):
    for arc in arcs:
        node = arc.get_destination()
        t_new = arc.get_source_time() + arc.get_actual_travel_time()
        refine(partially_time_expanded_network, node, t_new)
        restore(partially_time_expanded_network, node, t_new)

