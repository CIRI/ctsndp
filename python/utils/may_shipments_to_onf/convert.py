
import sys
import getopt
import json
import os.path

class onfCommodity(object):
    commodityType = None
    origin = None
    delay_cost = None
    quantity = None
    cargo_categories = None
    transportation_methods = None
    uuid = None

class onfShipment(object):
    EAT = 0
    LDT = 0
    source = None
    destination = None
    name = None
    uuid = None
    commodities = None

    def __init__(self):
        self.commodities = list()

    def addCommodity(self, commodity):
        self.commodities.append(commodity)

class onfEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, onfShipment):
            return {
                "onf:id": 1,
                "onf:name": obj.name,
                "onf:uuid": obj.uuid,
                "onf:source": obj.source,
                "onf:destination": obj.destination,
                "onf:EAT": obj.EAT,
                "onf:LDT": obj.LDT,
                "commodity_bundles": obj.commodities
            }
        elif isinstance(obj, onfCommodity):
            if obj.delay_cost is None:
                return {
                    "onf:type": obj.commodityType,
                    "onf:quantity": obj.quantity,
                    "onf:origin": obj.origin,
                    "onf:uuid": obj.uuid,
                    "cargo_categories": obj.cargo_categories,
                    "transportation_methods": obj.transportation_methods
                }
            return {
                "onf:type": obj.commodityType,
                "onf:quantity": obj.quantity,
                "onf:delay_cost": obj.delay_cost,
                "onf:origin": obj.origin,
                "onf:uuid": obj.uuid,
                "cargo_categories": obj.cargo_categories,
                "transportation_methods": obj.transportation_methods
            }
        return super(onfEncoder, self).default(obj)


def convert_shipment(output_dir, shipment_file, arrival_node, departure_node,
                    destination_node, shipment_uuid, shipper, time):
    shipments = dict()
    with open(shipment_file, "r") as read_file:
        data = json.load(read_file)
        commodities = data["commodities"]
        for commodity in commodities:
            EAT = commodity["EAT"]
            LDT = commodity["LDT"]
            name = commodity["name"]
            source = commodity["source"]
            destination = commodity["destination"]

            if (name, source, destination, EAT, LDT) in shipments:
                shipment = shipments[(name, source, destination, EAT, LDT)]

                k = onfCommodity()
                k.cargo_categories = commodity["cargo_categories"]
                k.commodityType = commodity["commodityGroup"]
                k.delay_cost = commodity["delay_cost"]
                k.uuid = commodity["group_uuid"]
                k.quantity = commodity["nTEU"]
                k.origin = commodity["origin"]
                k.transportation_methods = commodity["transportation_methods"]

                shipment.addCommodity(k)

            else:
                shipment = onfShipment()
                shipment.name = name
                shipment.source = source
                shipment.destination = destination
                shipment.EAT = EAT
                shipment.LDT = LDT
                shipment.uuid = shipment_uuid

                k = onfCommodity()
                k.cargo_categories = commodity["cargo_categories"]
                k.commodityType = commodity["commodityGroup"]
                k.delay_cost = commodity["delay_cost"]
                k.uuid = commodity["group_uuid"]
                k.quantity = commodity["nTEU"]
                k.origin = commodity["origin"]
                k.transportation_methods = commodity["transportation_methods"]

                shipment.addCommodity(k)

                shipments[(name, source, destination, EAT, LDT)] = shipment

        filename = os.path.splitext(os.path.basename(shipment_file))[0]

        onf = {"onf:shipments": list(shipments.values())}
        with open(output_dir+"/"+filename+".shpt", "w") as write_file:
            json.dump(onf, write_file, cls = onfEncoder, indent=2)


def convert_shipments(schedule_file, output_dir):
    path = os.path.dirname(schedule_file)
    with open(schedule_file, "r") as read_file:
        data = json.load(read_file)
        shipments = data["shipments"]
        for shipment in shipments:
            arrival_node = shipment["arrival_node"]
            departure_node = shipment["departure_node"]
            destination_node = shipment["destination_node"]
            shipment_file = shipment["shipment_file"]
            shipment_uuid = shipment["shipment_uuid"]
            shipper = shipment["shipper"]
            time = shipment["time"]
            convert_shipment(output_dir, path + "/" + shipment_file,
                arrival_node, departure_node, destination_node, shipment_uuid,
                shipper, time)

def print_help():
    print('')
    print('Convert the May json shipment data into onf format.')
    print('usage:')
    print('    convert.py -s <schedule file> -o <output folder>')
    print('')
    print('where options are:')
    print('    -o, --output <output folder>                The output folder to save the converted shipments.')
    print('')
    print('    -s, --schedule <Schedule Filename>          The filename defining the system schedule,')
    print('                                                includes service network and shipments. Only')
    print('                                                the shipments are converted.')
    print('')
    print('    -h                                          diplay usage information')
    print('')

def main(argv):
    schedule_file = ""
    output_dir = "."
    try:
        opts, args = getopt.getopt(argv, "hs:o:", ["schedule","output"])
    except getopt.GetoptError:
        print_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit(0)
        elif opt in ("-s", "--schedule"):
            schedule_file = arg
        elif opt in ("-o", "--output"):
            output_dir = arg
        else:
            print_help()
            sys.exit(0)
    if len(schedule_file) == 0:
        print_help()
        sys.exit(0)
    convert_shipments(schedule_file, output_dir)

if __name__ == '__main__':
    main(sys.argv[1:])