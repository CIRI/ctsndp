"""
Copyright (c) Gabriel A. Weaver, 2017-2018, All Rights Reserved

@author Gabe

Purpose of this module is to read and write a valid file
"""
from commodities import CommodityBundle, CommodityBundles
from networkx.readwrite import json_graph
import codecs
import csv
import json
import jsonschema
import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pickle
import subprocess
import sys

class InfrastructureNetwork(object):
    """
    A representation of a Infrastructure Network used to instantiate 
    a simulation
    """
    network = nx.MultiDiGraph()
    schemaFilePaths = ["../data/schema/cptl/infrastructureNetwork.schema.json"]

    @staticmethod
    def create(networkFilePath):
        result = InfrastructureNetwork()
        result.network = result.read(networkFilePath, validate=True)
        return result

    @staticmethod
    def validate(jsonGraph, schemaFilePaths):
        for schemaFilePath in schemaFilePaths:
            with open(schemaFilePath) as sfp:
                schema = json.load(sfp)
                try:
                    jsonschema.validate(jsonGraph, schema)
                except jsonschema.exceptions.ValidationError as ve:
                    sys.stderr.write(str(ve) + "\n")
                    sys.exit(-1)
            sfp.close()

    @staticmethod
    def formatLinks(jsonGraph):
        """
        Update the links so that they don't use strings but rather indices
        """
        nodeId2Idx = {}
        for idx, node in enumerate(jsonGraph["nodes"]):
            nodeId = node["id"]
            nodeId2Idx[ nodeId ] = idx

        for link in jsonGraph["links"]:
            sourceId = link["source"]
            sourceIdx = nodeId2Idx[ sourceId ] 
            link["onf:source_id"] = sourceId
            link["source"] = sourceIdx

            targetId = link["target"]
            targetIdx = nodeId2Idx[ targetId ]
            link["onf:target_id"] = targetId
            link["target"] = targetIdx
        return jsonGraph


    def read(self, filePath, validate=True):
        with open(filePath) as f:
            jsonGraph = json.load(f)

        if validate:
            InfrastructureNetwork.validate(jsonGraph, self.schemaFilePaths)
        #jsonGraph = InfrastructureNetwork.formatLinks(jsonGraph)
        return json_graph.node_link_graph(jsonGraph, directed=True)

    def write(self, filePath, validate=True):
        jsonGraph = json_graph.node_link_data(self.network)
        with open(filePath, "w") as out:
            out.write(json.dumps(jsonGraph, indent=4, sort_keys=True))
        out.close()


class LinkBasedQueueingNetwork(InfrastructureNetwork):
    """
    An infrastructure network which has two queues per link.  The formulation, based on 
      Newell's Kinematic Wave model, allows one to capture spillback and congestion within
      a transportation network.
    """
    events = None        # Agents' arrival and departures among queues
    agent_paths = None   # Paths taken by agents through the network (the optimal paths when loaded)

    deltaT = 1           # mesoscopic simulation time interval (per day)
    v_f = 1              # free-flow speed (forward wave speed)
    w = 1                # constant backward wave speed
    k = 1                # jam density

    length_l = None      # length per link
    nlanes_l = None      # number of lanes per link 
    l = None             # Dictionary from link name to index
    n = None             # Dictionary from node name to index
    entranceQs = None    # Entrance queues for each edge (|L| x |T|)
    exitQs = None        # Exit queues for each edge     (|L| x |T|)
    lengths = None       # Length of link in unit-lane miles (|L|)
    A = None             # Cumulative number of arriving vehicles ready to move into
                         #  link l at given time (|L| x |T|)
    V = None             # Cumulative number of departing vehicles waiting at the 
                         #  vertical queue of link l at time t (|L| x |T|)
    D = None   
    
    y_k_t = None         # Per-link flow (|L| x |T|)
    q_max_lt = None      # maximum flow rate on link l between time t-deltaT and time t
    cap_in_lt = None     # inflow capacity of link l between time t-deltaT and time t
    cap_out_lt = None    # outflow capacity of link l between time t-deltaT and time t
    fftt = None          # free-flow travel time on link l; length(l)/v_f  (|L| x |T|)
    bwtt = None          # backward wave travel time on link l; length(l)/w (|L|)
    T = 25               # total simulation time (28 days)
    inEdgeDemand = None  # inflow demand of node between time t-deltaT and time t (|N| x |T|)
    outEdgeDemand = None # outflow demand of node between time t-deltaT and time t (|N| x |T|)

    # Useful for disruptions
    service_times = None # Service time of a node at a given time (|N| x |T|)
    shipments = None     # List of shipments
    nonOptimalityThreshold = None  # If this many shipments are delayed, recompute paths
    nonOptimalDepartures = None #

    @staticmethod
    def create(networkFilePath, shipmentFilePath):
        result = LinkBasedQueueingNetwork()
        result.network = result.read(networkFilePath)
        result.instantiateShipments(shipmentFilePath)
        result.nonOptimalityThreshold = 10
        result.nonOptimalDepartures = 0
        result.instantiateLinkIdx()
        result.instantiateNodeIdx()
        result.instantiateLinkQs()
        result.instantiateCumulativeCounts()
        result.instantiateFlowAndCapacities()
        result.instantiateForwardBackwardWaveTravelTime()
        return result
    
    def instantiateShipments(self, shipmentFilePath):
        with open(shipmentFilePath) as json_data:
            self.shipments = json.load(json_data)

    def getLinkName(self, edge):
        sId = edge[2]["onf:source_id"].replace("md1_", "")
        tId = edge[2]["onf:target_id"].replace("md1_", "")
        return "-".join([sId,tId])

    def instantiateLinkIdx(self):
        lIdx = 0
        self.l = {}
        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            self.l[linkName] = lIdx
            lIdx += 1

    def instantiateNodeIdx(self):
        nIdx = 0
        self.n = {}
        for node in self.network.nodes_iter(data=True):
            nodeName = node[0]
            self.n[nodeName] = nIdx
            nIdx += 1

    def instantiateServiceTimes(self):
        N = self.network.number_of_nodes()
        self.service_times = np.zeros((N, self.T))

        for node in self.network.nodes_iter(data=True):
            nodeName = node[0]
            nIdx = self.n[nodeName]
            self.service_times[nIdx][:] = int(node[1]["onf:commodity_bundle_service_time"])

    """
    Instantiate the entrance and exit queues for each edge
      |L| x |T|
    """
    def instantiateLinkQs(self):
        L = self.network.number_of_edges()
        self.entranceQs = []
        self.exitQs = []

        for l in range(0,L):
            self.entranceQs.append([])
            self.exitQs.append([])
            for t in range(0, self.T): 
                entranceQ = []
                exitQ = []
                self.entranceQs[l].append(entranceQ)
                self.exitQs[l].append(exitQ)

    """
    Instantiate the cumulative number of vehicles arriving, waiting, departing at
      a given link and time
    """
    def instantiateCumulativeCounts(self):
        L = self.network.number_of_edges()
        self.A = np.zeros((L, self.T))
        self.V = np.zeros((L, self.T))
        self.D = np.zeros((L, self.T))

    def instantiateFlowAndCapacities(self):
        L = self.network.number_of_edges()
        self.q_max_lt = np.zeros((L, self.T))
        self.cap_in_lt = np.zeros((L, self.T))
        self.cap_out_lt = np.zeros((L, self.T))
        self.y_k_t = np.zeros((L, self.T))

        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            self.q_max_lt[lIdx][:] = edge[2]["onf:capacity"]

    def instantiateForwardBackwardWaveTravelTime(self):
        L = self.network.number_of_edges()
        self.fftt = np.zeros( (L, self.T) )
        self.bwtt = np.zeros(L)
        self.length_l = np.ones(L)      # Simplifying assumption for now
        self.nlanes_l = np.ones(L)
        
        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            self.fftt[lIdx][:] = edge[2]["onf:travel_time"]   # free-flow travel time
            self.bwtt[lIdx] = 1                            # backward wave travel time

    def writeCumulativeCounts(self, outFilePath):
        np.savetxt(outFilePath + ".A", self.A, delimiter=",")
        np.savetxt(outFilePath + ".V", self.V, delimiter=",")
        np.savetxt(outFilePath + ".D", self.D, delimiter=",")

    def loadCumulativeCounts(self, inFilePath):
        self.A = np.loadtxt(inFilePath + ".A", delimiter=",")
        self.V = np.loadtxt(inFilePath + ".V", delimiter=",")
        self.D = np.loadtxt(inFilePath + ".D", delimiter=",")
        
    def writeQueues(self, outFilePath):
        with open(outFilePath + ".entrance", 'wb') as outfile:
            pickle.dump(self.entranceQs, outfile)
        outfile.close()

        with open(outFilePath + ".exit", 'wb') as outfile:
            pickle.dump(self.exitQs, outfile)
        outfile.close()
        
    def loadQueues(self, inFilePath):
        with open(inFilePath + ".entrance", 'rb') as infile:
            self.entranceQs = pickle.load(infile)
        infile.close()

        with open(inFilePath + ".exit", 'rb') as infile:
            self.exitQs = pickle.load(infile)
        infile.close()
        
    def readAgentEvents(self, eventsFilePath):
        self.events = []
        
        eventsFile = open(eventsFilePath)
        eventList = eventsFile.readlines()
        for event in eventList:
            agentIdx, linkName, t, bType = event.strip('()\n').replace("'","").replace(" ","").split(",")
            eTuple = (agentIdx, linkName, int(t), bType)
            self.events.append(eTuple)
        eventsFile.close()

    def readAgentPaths(self, eventsFilePath):
        self.agent_paths = {}
        
        paths = {}

        eventsFile = open(eventsFilePath)
        eventsList = eventsFile.readlines()
        for event in eventsList:
            agentIdx, linkName, t, bType = event.strip('()\n').replace("'","").replace(" ","").split(",")
            eTuple = (agentIdx, linkName, int(t), bType)
            
            if not (agentIdx in paths):
                paths[agentIdx] = {}

            if not (linkName in paths[agentIdx]):
                paths[agentIdx][linkName] = []
            paths[agentIdx][linkName].append(int(t))

        for agentIdx in paths.keys():
            self.agent_paths[agentIdx] = []
            for linkName in paths[agentIdx]:
                timeInterval = paths[agentIdx][linkName]
                sTime = timeInterval[0]
                eTime = timeInterval[1]
                self.agent_paths[agentIdx].append( (linkName, sTime, eTime) )
                
        eventsFile.close()

    """
    Plot the space-time graph for a given set of agents
    """
    def generateSpaceTimeGraphForAgents(self, outputImageFilePath, agentIdxs):
        plt.figure(dpi=1200)
        commodityIds = list(set(list(map(lambda x: x[0], agentIdxs))))
        colors = iter(cm.rainbow(np.linspace(0,1, len(agentIdxs))))    

        fig, ax = plt.subplots()
        for agentIdx in agentIdxs:
            xs = []
            ys = []
            agentEvents = self.agent_paths[agentIdx]
            for e in agentEvents:
                # Of the form (linkName, timeIn, timeOut)
                sourceName = e[0].split("-")[0]
                sourceTime = e[1]
                targetName = e[0].split("-")[1]
                targetTime = e[2]
                
                ys.append( sourceName )
                xs.append( sourceTime )
                ys.append( targetName )
                xs.append( targetTime )

            ax.plot(xs, ys, 'b-', color = next(colors), label=agentIdx)
        
        legend = ax.legend(loc='upper right', shadow=True)
        for label in legend.get_texts():
            label.set_fontsize('large')
        plt.savefig(outputImageFilePath)
                        

    """
    Step 1:  Load the list of events to the appropriate entrance buffers
    """
    def loadEntranceBuffers(self, t):
        for agentIdx in self.agent_paths.keys():
            agentEvents = self.agent_paths[agentIdx]
            futureEvents = list(filter(lambda x: x[1] >= t, agentEvents))
            futureEvents.sort(key=lambda x: x[1])
            
            # Get the minimum next event
            supremumEvent = futureEvents.pop(0)
            linkName = supremumEvent[0]
            eventTime = supremumEvent[1]
            
            # Works until service time on node changes
            self.appendEntranceEvent(agentIdx, linkName, eventTime - 2, eventTime)
    """
    Step 2:  Estimate requested link inflow capacity

    cap^in(l,t) = min{q^max(l,t), A^max(l,t) - A(l, t-deltaT) }
    """
    def estimateRequestedLinkInflowCapacity(self, t):
        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            term1 = self.q_max_lt[lIdx][t]

            # Assume no vehicles arrived to link l @ time interval prior to 0
            term2_1 = 0
            term2_2 = 0

            if t > 0:               
                # NB:  We are computing max capacity
                dN = self.k * self.length_l[lIdx] * self.nlanes_l[lIdx]
                term2_1 = self.D[lIdx][int(t - self.bwtt[lIdx])] + dN
                term2_2 = self.A[lIdx][t-self.deltaT]
                term2 = term2_1 + term2_2
            elif t == 0:
                # assume all capacity is inflow capacity at t=0
                term2 = edge[2]["onf:capacity"]
                
            self.cap_in_lt[lIdx][t] = min(term1, term2)

    """
    Compute the maximum flow that can be (S)ent by an edge queue (ENB or EXB)
      in the interval between t and t+1.
    """
    def S(self, edgeI, bType, t):
        Q_I = edgeI[2]["onf:capacity"]
        n_I = None
        
        linkName = self.getLinkName(edgeI)
        lIdx = self.l[linkName]

        if "ENB" == bType:
            if t > 0:
                n_I = self.A[lIdx][t] - self.A[lIdx][t-1]
            else:
                n_I = self.A[lIdx][t]
        elif "EXB" == bType:
            if t > 0:
                n_I = self.V[lIdx][t] - self.V[lIdx][t-1] #len(self.exitQs[lIdx][0])
            else:
                n_I = self.V[lIdx][t]

        return min(Q_I, n_I)

    """
    Compute the maximum flow that can be (R)eceived by an edge queue (ENB or EXB)
      in the interval between t and t+1
    """
    def R(self, edgeI, bType, t):
        # The maximum number of vehicles that can flow into cell i when the clock
        #  advances from t to t+1
        Q_I = edgeI[2]["onf:capacity"]
        n_I = None
        
        linkName = self.getLinkName(edgeI)
        lIdx = self.l[linkName]

        # max vehicles that can be present in cell i at time t
        N_I = edgeI[2]["onf:capacity"]        
        d_I = 1  # could be improved
        
        if "ENB" == bType:
            if t > 0:
                n_I = self.A[lIdx][t] - self.A[lIdx][t-1]
            else:
                n_I = self.A[lIdx][t]
        elif "EXB" == bType:
            if t > 0:
                n_I = self.V[lIdx][t] - self.V[lIdx][t-1] #len(self.exitQs[lIdx][0])
            else:
                n_I = self.V[lIdx][t]
        
        return min(Q_I, d_I * (N_I - n_I))

    """
    Compute the flow for an ordinary link
    """
    def computeFlowForOrdinaryLink(self, edge, t):
        S_Bk = self.S(edge, "ENB", t)
        R_Ek = self.R(edge, "EXB", t)
        y_k = min(S_Bk, R_Ek)
        return y_k

    """
    Compute the flow for a merge
      assuming only two inbound edges
    """
    def computeFlowForMergeLinks(self, edge1, edge2, t):
        
        if edge1[1] != edge2[1]:
            raise Exception("Edges do not have same target node!")

        S_Bk = self.S(edge1, "ENB", t)
        S_Ck = self.S(edge2, "ENB", t)
        R_Ek1 = self.R(edge1, "EXB", t)
        R_Ek2 = self.R(edge2, "EXB", t)

        R_Ek = min(R_Ek1, R_Ek2)
        
        result = None
        if R_Ek > S_Bk + S_Ck:
            y_k = S_Bk
            y_ck = S_Ck
        else:
            p_k = 0.5
            p_ck = 0.5
            kFlowList = [S_Bk, R_Ek - S_Ck, p_k * R_Ek]
            ckFlowList = [S_Ck, R_Ek - S_Bk, p_ck * R_Ek]

            y_k = sorted(kFlowList)[1]
            y_ck = sorted(ckFlowList)[1]

        result = (y_k, y_ck)
        return result

    """
    Compute the flow for a diverge
      assuming only two outbound edges
    """
    def computeFlowForDivergeLinks(self, edge1, edge2, t):
        
        if edge1[0] != edge2[0]:
            raise Exception("Edges do not have same source node!")
        
        # NB:  B_Ek + B_Ck == 1
        B_Ek = 0.5
        B_Ck = 0.5

        S_Bk1 = self.S(edge1, "ENB", t)
        S_Bk2 = self.S(edge2, "ENB", t)
        S_Bk = S_Bk1 + S_Bk2
        
        R_Ek = self.R(edge1, "EXB", t)
        R_Ck = self.R(edge2, "EXB", t)

        y_Bk = min([S_Bk, R_Ek / B_Ek, R_Ck / B_Ck])

        y_k = min([S_Bk1, R_Ek]) #y_k = B_Ek * y_Bk
        y_ck = min([S_Bk2, R_Ck]) #y_ck = B_Ck * y_Bk

        result = (y_k, y_ck)
        return result

    """
    Step 2:  Estimate requested link inflow capacity in a more advanced manner.
    """
    def estimateRequestedLinkInflowCapacityDagzano(self, t):
        #  There are three cases:
        #    compute flow if a normal link, merge, or diverge
        #  Rethink cases for inflow v outflow conservation
        for node in self.network.nodes_iter(data=True):
            nodeName = node[0]
            inEdges = self.network.in_edges(nodeName, data=True)
            outEdges = self.network.out_edges(nodeName, data=True)

            if len(inEdges) == 0 and len(outEdges) == 1:
                # Just an ordinary link, start node
                edge = outEdges[0]
                linkName = self.getLinkName(edge)
                lIdx = self.l[linkName]

                self.y_k_t[lIdx][t] = self.computeFlowForOrdinaryLink(edge, t)
            elif len(inEdges) == 1 and len(outEdges) == 0:
                # Just an ordinary link, end node
                edge = inEdges[0]
                linkName = self.getLinkName(edge)
                lIdx = self.l[linkName]

                self.y_k_t[lIdx][t] = self.computeFlowForOrdinaryLink(edge, t)
            elif len(inEdges) <= 1 and len(outEdges) == 2:
                # Diverge from node 
                edge1 = outEdges[0]
                linkName1 = self.getLinkName(edge1)
                l1Idx = self.l[linkName1]

                edge2 = outEdges[1]
                linkName2 = self.getLinkName(edge2)
                l2Idx = self.l[linkName2]
                
                (self.y_k_t[l1Idx][t], self.y_k_t[l2Idx][t]) =\
                    self.computeFlowForDivergeLinks(edge1, edge2, t)
                
            elif len(inEdges) == 2 and len(outEdges) <= 1:
                # Merge into node
                edge1 = inEdges[0]
                linkName1 = self.getLinkName(edge1)
                l1Idx = self.l[linkName1]

                edge2 = inEdges[1]
                linkName2 = self.getLinkName(edge2)
                l2Idx = self.l[linkName2]
                
                (self.y_k_t[l1Idx][t], self.y_k_t[l2Idx][t]) =\
                    self.computeFlowForMergeLinks(edge1, edge2, t)
            
    """
    Step 3:  Synchronize capacity distribution at each bottleneck

       TODO:  Update the cumulative departure curve when agent moves 
               out of this link
    """
    def synchronizeCapacityDistribution(self, t):
        # initialize incoming demand for each node
        N = self.network.number_of_nodes()
        totalIncomingDemand = np.zeros(N)
        totalOutgoingCapacity = np.zeros(N)

        # update incoming demand for each node
        for node in self.network.nodes_iter(data=True):
            nodeName = node[0]
            nIdx = self.n[nodeName]

            # Total Incoming Demand += Outflow demand from link l
            inEdges = self.network.in_edges(nodeName, data=True)
            for edge in inEdges:
                linkName = self.getLinkName(edge)
                lIdx = self.l[linkName]

                estimatedInEdgeOutflow = 0
                serviceTime = int(self.service_times[nIdx][t]) #node[1]["onf:commodity_bundle_service_time"]
                if t - serviceTime - 1 >= 0:
                    estimatedInEdgeOutflow = self.V[lIdx][t - serviceTime] \
                        - self.V[lIdx][t - serviceTime - 1]
                
                totalIncomingDemand[nIdx] += estimatedInEdgeOutflow

            outEdges = self.network.out_edges(nodeName, data=True)
            for edge in outEdges:
                linkName = self.getLinkName(edge)
                lIdx = self.l[linkName]
                estimatedRequestedInflowCapacity = self.cap_in_lt[lIdx][t]
                estimatedInflowCapacity = max( 0, self.q_max_lt[lIdx][t] - estimatedRequestedInflowCapacity )
                totalOutgoingCapacity[nIdx] += estimatedInflowCapacity
            
            # If there is a bottleneck, distribute the inflow capacity
            #   for the outgoing link                 
            #   to the outflow capacity for each incoming link
            inEdges = self.network.in_edges(nodeName, data=True)
            for edge in inEdges:
                linkName = self.getLinkName(edge)
                lIdx = self.l[linkName]
                if totalIncomingDemand[nIdx] > totalOutgoingCapacity[nIdx]:
                    if t - serviceTime > 0:
                        self.cap_out_lt[lIdx][t - serviceTime] = totalOutgoingCapacity[nIdx]
                    else:
                        self.cap_out_lt[lIdx][0] = self.q_max_lt[lIdx][t] 
                else:
                    if t - serviceTime > 0:
                        self.cap_out_lt[lIdx][t - serviceTime] = self.q_max_lt[lIdx][t] - self.cap_in_lt[lIdx][t]
                    else:
                        self.cap_out_lt[lIdx][0] = self.q_max_lt[lIdx][t]
    """
    Step 4:  Node transfer from exit buffer to entrance buffer of connected links
    """
    def nodeTransfer(self, t):
        for node in self.network.nodes_iter(data=True):
            nodeName = node[0]
            nIdx = self.n[nodeName]

            nodeServiceTime = self.service_times[nIdx][t]

            inEdges = self.network.in_edges(nodeName, data=True)
            for edge in inEdges:
                linkName = self.getLinkName(edge)
                lInIdx = self.l[linkName]

                eventsToRequeue = []
                
                numExited = 0
                while (len(self.exitQs[lInIdx][t]) > 0):
#                        (numExited < self.cap_out_lt[lInIdx][t]):
                    vWaitingToExitEvent = self.exitQs[lInIdx][t].pop()

                    a = vWaitingToExitEvent[0]
                    l_in = linkName
                    t_v = vWaitingToExitEvent[2]

                    # Link transfer can only occur when have at least two edges
                    #  in the agent paths                        
                    if (not a in self.agent_paths) or (len(self.agent_paths[a]) == 0):
                        raise Exception("No paths for agent %s from link %s" % (a, linkName))
                    
                    # Agent can depart if time is past when it gets to link end
                    if t >= t_v:
                        exbLinkTime = self.agent_paths[a].pop(0)
                        exbLink = exbLinkTime[0]
                        
                        optimalLinkDepartureEvent = list(filter(lambda x: x[0] == a and x[1] == exbLink and x[3] == "EXB", self.events))[0]
                        optimalDepartureTime = optimalLinkDepartureEvent[2]

                        if t > optimalDepartureTime:
                            # Consider After a Certain Amount of Breaking, Re-Optimizing
                            print("[W] Agent %s departing %s at non-optimal time %d instead of %d" % \
                                      (a, exbLink,  t, optimalDepartureTime) )
                            self.nonOptimalDepartures += 1;

                        if len(self.agent_paths[a]) > 0:
                            enbLinkTime = self.agent_paths[a][0]
                            enbLink = enbLinkTime[0]

                            l_out = enbLink
                            lOutIdx = self.l[l_out]
                            self.D[lInIdx][t] += 1
                            numExited += 1

                            # At t, enter the node immediately upon exit
                            # At t+ service time, start to traverse next link
                            self.appendEntranceEvent(a, l_out, t, int(t+nodeServiceTime) )
                        else:
                            # we have reached the end destination
                            self.D[lInIdx][t] += 1
                            numExited += 1
                    else:
                        # the event's time has not yet come, add back to exitQ
                        eventsToRequeue.append(vWaitingToExitEvent)
                
                self.exitQs[lInIdx][t] += eventsToRequeue

                
    def checkRecompute(self, t):
        """
        Logic at end to trigger recomputation of optimal paths
        """
        
        # OPTIMALITY THRESHOLD CONDITION
        #if self.nonOptimalDepartures >= self.nonOptimalityThreshold:
        #    self.recomputeOptimalPaths(t)
        
        # CAPACITY THRESHOLD CONDITION        
        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            lCapacity = list(map(lambda x: x + 0.001, self.q_max_lt[lIdx]))
            lUsage = list(map(lambda x: len(x), self.exitQs[lIdx]))
            capacityPct = list(map(lambda x: x[0] / float(x[1]), zip(lUsage, lCapacity)))
            
            usageAtTime = capacityPct[t]
            if usageAtTime >= .75:
                print("Link %s is at %f capacity @t=%d!" % (linkName, usageAtTime, t))
                # Update the network capacity appropriately
                linkNamePcs = linkName.split("-")
                sourceName = "md1_" + linkNamePcs[0]
                targetName = "md1_" + linkNamePcs[1]
                self.network.edge[sourceName][targetName][0]["onf:capacity"] = 0
                self.network.edge[sourceName][targetName][0]["onf:commodity_bundle_capacity"] = 0
                self.recomputeOptimalPaths(t)

    def recomputeOptimalPaths(self, t):
        print("Recompute Optimal Paths @ t=%d!"% t)
        shipmentsDict = self.generateShipmentsJSON(t)
        networkDict = self.generateNetworkJSON(t)

        # Output Files for Current State of Disrupted System @ t
        dShipmentsFilePath = "/tmp/project7/" + "shipment.disrupted" + str(t) + ".json"
        dNetworkFilePath = "/tmp/project7/" + "network.disrupted" + str(t) + ".json"
        dPathOutFilePath = "/tmp/project7/" + "paths.disrupted" + str(t) + ".json"
        dOutputImageFilePath = "/tmp/project7/" + "plot-cumulative.disrupted" + str(t) + ".png"
        dOutputCapacityImageFilePath = "/tmp/project7/" + "plot-capacity.disrupted" + str(t) + ".png"
        dSpaceTimeOutputImageFilePath = "/tmp/project7/" + "plot-spacetime.disruptedNew" + str(t) + ".png"
        dCommodityEventsImageFilePath = "/tmp/project7/" + "plot-spacetime.disrupted" + str(t) + ".png"
        dCommodityEventsFilePath = "/tmp/project7/" + "events.disrupted" + str(t) + ".txt"
        dCumulativeCountsFilePath = "/tmp/project7/" + "counts-cumulative.disrupted" + str(t) + ".png"
        dQueueFilePath = "/tmp/project7/" + "queues.disrupted" + str(t) + ".txt"

        # Output Files for Updated State of Re-Optimized, Disrupted System @ t
        shipmentsFilePath = "/tmp/project7/" + "shipment.recomputed" + str(t) + ".json"
        networkFilePath = "/tmp/project7/" + "network.recomputed" + str(t) + ".json"
        childOutputImageFilePath = "/tmp/project7/" + "plot.cumulative.recomputed" + str(t) + ".png"

        with open(dNetworkFilePath, 'w') as outfile:
            json.dump(networkDict, outfile, indent=4)

        with open(dShipmentsFilePath, 'w') as outfile:
            json.dump(shipmentsDict, outfile, indent=4)
        
        self.generatePlots(dOutputImageFilePath)
        self.generateSpaceTimeGraphForAgents(dSpaceTimeOutputImageFilePath, ['11', '12'])
        self.generateCapacityPlots(dOutputCapacityImageFilePath)
        self.writeCumulativeCounts(dCumulativeCountsFilePath)
        self.writeQueues(dQueueFilePath)

        #mcf = MulticommodityFlowNetwork()
        #mcf.network = mcf.read(dNetworkFilePath)
        #commodityBundles = CommodityBundles.create(dShipmentsFilePath)

        # Run and block until finished
        subprocess.call(["mcf.py", "--paths", dPathOutFilePath, dNetworkFilePath, dShipmentsFilePath, "bullshit"])
        subprocess.call(["python", "src/plotSpaceTimeTrajectories.py", dPathOutFilePath, dCommodityEventsImageFilePath, dCommodityEventsFilePath ])
        subprocess.call(["lbq.py", "--recompute", "FALSE", "-q", dQueueFilePath, "-c", dCumulativeCountsFilePath, "-t", str(t), "--disruption", "FALSE", dNetworkFilePath, dShipmentsFilePath, dCommodityEventsFilePath, childOutputImageFilePath, networkFilePath, shipmentsFilePath])

        print("finished @ %d" % t)
        sys.exit(1)
        

    """
    Step 5:  Link traversal from entrance buffer to exit buffer of same link
    """
    def linkTraversal(self, t):

        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]

            eventsToRequeue = []
            while len(self.entranceQs[lIdx][t]) > 0:
                vArrivalEvent = self.entranceQs[lIdx][t].pop()
                a = vArrivalEvent[0]
                l = linkName
                t_a = vArrivalEvent[2]

                if t >= t_a:
                    t_v = int(t_a + self.fftt[lIdx][t])
                    self.handleWaitingToExitEvent(a, l, t, t_v)
                else:
                    eventsToRequeue.append(vArrivalEvent)

            self.entranceQs[lIdx][t] += eventsToRequeue

    """
    Create an entrance event
    """
    def appendEntranceEvent(self, a, linkName, t, eventTime):
        event = (a, linkName, eventTime, "ENB")
        lIdx = self.l[linkName]
        self.entranceQs[lIdx][t].append(event)
        self.A[lIdx][t] += 1

    """
    Handle an departure event
    + waiting to exit means in transit or at end of link
    """
    def handleWaitingToExitEvent(self, a, linkName, t, eventTime):
        event = (a, linkName, eventTime, "EXB")
        lIdx = self.l[linkName]
        self.exitQs[lIdx][t].append(event)
        self.V[lIdx][t] += 1

    """
    Print the entrance and exit queues
    """
    def printQueues(self, bType, t):
        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            if bType == "EXB":
                print("Waiting to Exit %s:" % (linkName))
                print( list(map(lambda y: sorted(list(map(lambda x: (int(x[0]), int(x[2])), y))), self.exitQs[lIdx][:]))[t] )
            else:
                print("Enter: %s" % (linkName))
                print( list(map(lambda y: sorted(list(map(lambda x: (int(x[0]), int(x[2])), y))), self.entranceQs[lIdx][:]))[t] )
    
    """
    From the entrance and exit queues, export the commodity paths
     through the network.
    """
    def computeCommodityPaths(self, tCurrent):
        result = []
        for t in range(0, self.T):
            for edge in self.network.edges_iter(data=True):
                linkName = self.getLinkName(edge)
                lIdx = self.l[linkName]

                # Enter the buffer and be serviced
                # Recover Source Node Enter (ENN), Source Node Exit/Link Enter (ENB) Events
                for (a, linkName, t_e, bType) in self.entranceQs[lIdx][t]:
                    if not ((a, edge[0], t_e - 2, "ENN") in result):     #  Hardcode service ti
                        result.append( (a, edge[0], t_e - 2, "ENN") )
                        result.append( (a, edge[0], t_e, "EXN") )        # ENB == EXN
                        result.append( (a, edge[0], t_e, "ENL") )
  
                # Recover the Link Exit (EXB) Event
                departureEvent = None
                for (a, linkName, t_e, bType) in self.exitQs[lIdx][t]:
                    if not ( (a, edge[1], t_e, "EXB") in result ):
                        # May have to be more clever if departure delayed beyond t_e
                        result.append( (a, edge[1], t_e, "EXL") )
                        result.append( (a, edge[1], t_e, "ENN") )

        # Return only unique entries
        result = list(set(result))
        return result

    """
    Generate Plots:  Create cumulative traffic count plots
    """
    def generatePlots(self, outputImageFilePath):
        plt.figure()
        fig, axs = plt.subplots(2,4)
        fig.tight_layout()
        fig.subplots_adjust(top=0.9)
        fig.suptitle("Per-Link Cumulative Event Counts Over Time")
        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            
            row = None
            col = None
            if lIdx > 3:
                row = 1
                col = lIdx % 4
            else:
                row = 0
                col = lIdx

            xs = range(0, self.T)
            ysA = self.A[lIdx]
            ysV = self.V[lIdx]
            ysD = self.D[lIdx]
            inCapacity = self.cap_in_lt[lIdx]
            outCapacity = self.cap_out_lt[lIdx]

            axs[ row, col ].set_title("Link " + linkName)
            axs[ row, col ].xaxis.set_ticks(np.arange(0, self.T, 5.0))
            axs[ row, col ].yaxis.set_ticks(np.arange(0, self.T, 2.0))
            axs[ row, col ].plot(xs, ysA, 'b', linestyle=':', label='ENN')
            axs[ row, col ].plot(xs, ysV, 'g', linestyle='--', label='ENL')
            axs[ row, col ].plot(xs, ysD, 'r', linestyle='-', label='EXL')

        axs[1, 3].axis('off')
        handles, labels = axs[ 0, 0].get_legend_handles_labels()
        fig.legend(handles, labels, loc='lower right')
        plt.savefig(outputImageFilePath)


    def generateCapacityPlots(self, outputImageFilePath):
        plt.figure()
        fig, axs = plt.subplots(2,4)
        fig.tight_layout()
        fig.subplots_adjust(top=0.9)
        fig.suptitle("Per-Link Capacity Utilization Vs. Time")
        locs = zip( [0,1,2,3,4,5,6,7],[(0,0),(0,1),(0,2),(0,3),(1,0),(1,1),(1,2),(1,3)] )

        for edge in self.network.edges_iter(data=True):
            linkName = self.getLinkName(edge)
            lIdx = self.l[linkName]
            
            row = None
            col = None
            if lIdx > 3:
                row = 1
                col = lIdx % 4
            else:
                row = 0
                col = lIdx

            xs = range(0, self.T)
            lCapacity = list(map(lambda x: x + 0.001, self.q_max_lt[lIdx]))
            lUsage = list(map(lambda x: len(x), self.exitQs[lIdx]))
            capacityPct = list(map(lambda x: x[0] / float(x[1]), zip(lUsage, lCapacity)))

            axs[ row, col ].set_title("Link " + linkName)
            axs[ row, col ].xaxis.set_ticks(np.arange(0, self.T, 5.0))
            axs[ row, col ].yaxis.set_ticks(np.arange(0, self.T, 0.2))
            axs[ row, col ].set_ylim(0, 1.1)
            axs[ row, col ].plot(xs, capacityPct, 'c', label='c')
        axs[1,3].axis('off')
        plt.savefig(outputImageFilePath)
        

    """
    Compute the latest node at which commodity k_u arrives by time t
    """
    def computeLatestNodeArrivalEvent(self, k_u, t):
        agentEvents = self.agent_paths[k_u]             # Optimal and actual paths may diverge
        commodityTraversingEdgeEvents = list(filter(lambda x: x[1] <= t and t <= x[2], agentEvents))        
        commodityDepartsLaterEvents = list(filter(lambda x: t < x[1], agentEvents))
        
        commodityLocationTime = None                    

        if len(commodityTraversingEdgeEvents) > 0:
            # Commodity is traversing an edge
            eventAtT = commodityTraversingEdgeEvents[0]
            if t == eventAtT[1]:
                # k_u leaves source node
                sourceId = eventAtT[0].split("-")[0]
                sourceTime = eventAtT[1]
                commodityLocationTime = (sourceId, sourceTime)
            elif t == eventAtT[2]:
                # k_u enters target node
                sourceId = eventAtT[0].split("-")[1]
                targetTime = eventAtT[2]
                commodityLocationTime = (targetId, targetTime)
            elif eventAtT[1] < t and t < eventAtT[2]:
                # k_u is in between, go back to when node first was added
                sourceId = eventAtT[0].split("-")[0]
                targetId = eventAtT[0].split("-")[1]
                sourceTime = eventAtT[1]
                commodityLocationTime = (sourceId, sourceTime)
            else:
                raise Exception("Commodity is chilling at a node")

        elif len(commodityDepartsLaterEvents) > 0:
            # There are departures awaiting later on t' > t
            c2L2 = list(filter(lambda x: t < x[1], agentEvents))
            c2L2.sort(key=lambda x: x[1])
            event = c2L2[0]
            sourceId = event[0].split("-")[0]
            sourceTime = event[1]
            commodityLocationTime = (sourceId, sourceTime)

        else:
            # Commodity chills out at a node
            c2L1 = list(filter(lambda x: t > x[2], agentEvents))
            if len(c2L1) != 0:
                c2L1.sort(key=lambda x: x[2])
                event = c2L1[0]
                targetId = event[0].split("-")[1]
                targetTime = event[2]
                commodityLocationTime = (targetId, targetTime)

        commodityLocationTime = ("md1_" + commodityLocationTime[0], commodityLocationTime[1])
        return commodityLocationTime

    def computeEAT(self, k_u, t):
        earliestLocationTime = self.computeLatestNodeArrivalEvent(k_u, t)
        earliestTime = earliestLocationTime[2]
        return earliestTime

    def computeSource(self, k_u, t):
        earliestLocationTime = self.computeSourceLocationTime(k_u, t)
        earliestLocation = earliestLocationTime[1]
        return earliestLocation

    def createCommodityBundle(self, k_u, t):
        commodityName = "arrival1_" + str(k_u)
        old_bundle = list(filter(lambda x: x["onf:name"] == commodityName, self.shipments["commodity_bundles"]))[0]

        latestNodeArrivalEvent = self.computeLatestNodeArrivalEvent(k_u, t)
        bundle = {}
        bundle["onf:delay_cost"] = old_bundle["onf:delay_cost"]
        bundle["onf:destinations"] = old_bundle["onf:destinations"]
        bundle["onf:EAT"] = latestNodeArrivalEvent[1]
        bundle["onf:LDT"] = old_bundle["onf:LDT"] * 2   # Double LDT
        bundle["onf:quantity"] = old_bundle["onf:quantity"]
        bundle["onf:id"] = old_bundle["onf:id"]
        bundle["onf:name"] = commodityName
        bundle["onf:sources"] = [ latestNodeArrivalEvent[0] ]
        bundle["onf:type"] = old_bundle["onf:type"]
        return bundle

    """
    Get the locations of the active commodities at time t
      Currently location is given by latest arrival

    NB:  Currently have hardcoded the destination to 'md_5'
    """
    def computeActiveCommodityLocations(self, t):
        # Filter out commodities that have arrived at their destination by time t
        commodityLocationTimes = {}

        for k_u in self.agent_paths.keys():
            commodityLatestLocationTime = self.computeLatestNodeArrivalEvent(k_u, t)
            commodityLocation = commodityLatestLocationTime[0]
            if commodityLocation != "md1_5":
                commodityLocationTimes[k_u] = commodityLatestLocationTime
        return commodityLocationTimes

    """
    Output Commodity Shipment at time t
    """
    def generateShipmentsJSON(self, t):
        shipmentDict = {}
        shipmentDict["commodity_bundles"] = []

        activeCommodityLocationTimes = self.computeActiveCommodityLocations(t)

        # Create commodity bundles for the remaining
        for k_u in activeCommodityLocationTimes.keys():
            commodityBundle = self.createCommodityBundle(k_u, t)
            shipmentDict["commodity_bundles"].append(commodityBundle)
        return shipmentDict

    """
    Output network 
    """
    def generateNetworkJSON(self, t):
        jsonGraph = json_graph.node_link_data(self.network)
        activeCommodityLocationTimes = self.computeActiveCommodityLocations(t)
        sourceNodes = []
        for k_u in activeCommodityLocationTimes.keys():
            currentLocation = activeCommodityLocationTimes[k_u][0]
            sourceNodes.append(currentLocation)
        sourceNodes = list(set(sourceNodes))
        
        for node in jsonGraph["nodes"]:
            nodeName = node["id"]
            nIdx = self.n[nodeName]

            # Update Service Time
            node["onf:commodity_bundle_service_time"] = int(self.service_times[nIdx][t])

            # Also need to update demand
            if nodeName in sourceNodes:
                node["onf:commodity_bundle_demand"] = 1
            elif nodeName == "md1_5":
                node["onf:commodity_bundle_demand"] = -1
            elif "onf:commodity_bundle_demand" in node:
                del node["onf:commodity_bundle_demand"] 
                
        return jsonGraph

                        
class MulticommodityFlowNetwork(InfrastructureNetwork):
    """
    An Infrastructure Network annotated with the requisite information necessary 
    to run a Multicommodity Network Flow analysis.
    """
    demand = None                   #Look up dictionary from node to per commodity demand
    commodityBundles = None
    schemaFilePaths = ["../data/schema/cptl/infrastructureNetwork.schema.json",\
                       "../data/schema/multicommodityFlowNetwork.schema.json"]

    @staticmethod    
    def create(iNetwork, commodityBundles):
        result = MulticommodityFlowNetwork()
        result.network = iNetwork.network
        result.commodityBundles = commodityBundles
        return result

    @staticmethod
    def convertDES(sourceGraph):
        sourceJSONGraph = json_graph.node_link_data(sourceGraph)
        targetJSONGraph = {}
        targetJSONGraph["directed"] = sourceJSONGraph["directed"]
        targetJSONGraph["graph"] = sourceJSONGraph["graph"]

        targetJSONGraph["links"] = map(MulticommodityFlowNetwork.convertDESLink, sourceJSONGraph["links"])
        targetJSONGraph["multigraph"] = sourceJSONGraph["multigraph"]
        targetJSONGraph["nodes"] = map(MulticommodityFlowNetwork.convertDESNode, sourceJSONGraph["nodes"])
        targetGraph = json_graph.node_link_graph(targetJSONGraph)
        return targetGraph

    def readAndConvert(self, sourceNetworkFilePath, sourceType, validate):
        if "DES" != sourceType:
            sys.stderr.write("Cannot convert from source other than DES input!")
            sys.exit(-1)
        
        desGraph = self.read(sourceNetworkFilePath, validate=False)
        onfGraph = MulticommodityFlowNetwork.convertDES(desGraph)
        onfGraphJSON = json_graph.node_link_data(onfGraph)
        if validate:
            InfrastructureNetwork.validate(onfGraphJSON, self.schemaFilePaths)
        self.network = onfGraph
    
    @staticmethod
    def convertDESLink(desLinkJSON):
        result = {}
        result["name"] = desLinkJSON["name"]
        result["source"] = desLinkJSON["source"]
        result["target"] = desLinkJSON["target"]
        result["onf:commodity_bundle_cost"] = desLinkJSON["cost"]
        result["onf:commodity_bundle_capacity"] = "?"
        result["onf:capacity"] = desLinkJSON["capacity"]
        result["onf:travel_time"] = desLinkJSON["travel_time"]
        return result
        
    @staticmethod
    def convertDESNode(desNodeJSON):
        result = {}
        result["id"] = desNodeJSON["id"]
        result["onf:commodity_bundle_demand"] = "?"
        result["onf:commodity_bundle_service_time"] = desNodeJSON["capacity"]
        return result
    
    @staticmethod
    def generateInputTables(networkFilePath):
        """
        Given some JSON for an MCF network, create an input table that 
        could be filled out via Excel.
        """
        with open(networkFilePath) as f:
            jsonGraph = json.load(f)

        # link CSV
        linkSheet = []  
        linkColumnNames = [ "name", "source", "target", "onf:commodity_bundle_cost", "onf:commodity_bundle_capacity",\
                            "onf:capacity", "onf:travel_time" ]        
        linkSheet.append( ",".join(linkColumnNames) )

        for link in jsonGraph["links"]:
            row = []
            for columnName in linkColumnNames:
                if columnName not in link:
                    row.append( "?" )
                else:
                    row.append( str(link[columnName]) )
            linkSheet.append( ",".join(row) )
            
        # node CSV
        nodeSheet = []
        nodeColumnNames = [ "id", "onf:commodity_bundle_service_time" ]
        nodeSheet.append( ",".join(nodeColumnNames) )
        
        for node in jsonGraph["nodes"]:
            row = []
            for columnName in nodeColumnNames:
                if columnName not in node:
                    row.append("?")
                else:
                    row.append( str(node[columnName]) )
            nodeSheet.append( ",".join(row) )

        return "\n".join(nodeSheet) + "\n".join(linkSheet)
    
    def readInputTables(self, nodeFilePath, edgeFilePath):
        """
        Given CSV files for an MCF network, create an JSON graph and 
          instantiate a MCF network
        """
        
        with codecs.open(nodeFilePath, 'r', 'utf-8') as nodeCSVFile:
            nodeReader = csv.DictReader( nodeCSVFile, dialect=csv.excel )
            for nodeData in nodeReader:

                nodeId = nodeData["id"]
                self.network.add_node(nodeId)

                if nodeData["onf:commodity_bundle_service_time"] != "":
                    self.network.node[nodeId]["onf:commodity_bundle_service_time"] = int(nodeData["onf:commodity_bundle_service_time"])
                if nodeData["onf:commodity_bundle_demand"] != "":
                    self.network.node[nodeId]["onf:commodity_bundle_demand"] = json.loads(nodeData["onf:commodity_bundle_demand"])
                if nodeData["onf:vehicle_demand"] != "":
                    self.network.node[nodeId]["onf:vehicle_demand"] = int(nodeData["onf:vehicle_demand"])
        
        with codecs.open(edgeFilePath, 'r', 'utf-8') as edgeCSVFile:
            edgeReader = csv.DictReader( edgeCSVFile, dialect=csv.excel )
            for edgeData in edgeReader:
                
                sourceId = edgeData["source"]
                targetId = edgeData["target"]

                self.network.add_edge( sourceId, targetId )
                
                sourceIdx = self.network.nodes().index(sourceId)
                targetIdx = self.network.nodes().index(targetId)
                edgeData["source"] = sourceIdx
                edgeData["target"] = targetIdx
                edgeName = " to ".join( [ sourceId, targetId ] )
                # Assume one directed edge

                self.network.edge[sourceId][targetId][0]["name"] = edgeName

                if edgeData["onf:capacity"] != "":
                    self.network.edge[sourceId][targetId][0]["onf:capacity"] = int(edgeData["onf:capacity"])
                if edgeData["onf:commodity_bundle_capacity"] != "":
                    self.network.edge[sourceId][targetId][0]["onf:commodity_bundle_capacity"] = int(edgeData["onf:commodity_bundle_capacity"])
                if edgeData["onf:commodity_bundle_cost"] != "":
                    self.network.edge[sourceId][targetId][0]["onf:commodity_bundle_cost"] = int(edgeData["onf:commodity_bundle_cost"])
                if edgeData["onf:travel_time"] != "":
                    self.network.edge[sourceId][targetId][0]["onf:travel_time"] = int(edgeData["onf:travel_time"])
                
        
