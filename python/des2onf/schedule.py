# coding: utf-8

"""
@author Brett Feddersen

Purpose of this module is to manage the schedule file provided by the DES
"""

import json
import jsonschema
import sys
import os
import datetime as dt

from disruption import Disruption
from shipment import Shipment
from service_network import ServiceNetwork
from jsonschema import validate

def sort_disruptions_by_time(disruption):
    return disruption.time_in_sim

class Schedule(object):

    schedule = None
    data_path = None
    start_time_utc = None
    start_time_of_work_day = None
    end_time_of_work_day = None
    disruptions = None
    shipments = None
    service_network_filename = None

    def __init__(self):
        self.disruptions = list()
        self.shipments = list()

    def read(self, filePath):
        self.data_path = os.path.dirname(filePath) + "/"
        with open(filePath) as f:
            self.schedule = json.load(f)
        try:
            with open("./schema/schedule.schema.json") as sfp:
                schema = json.load(sfp)

            validate(self.schedule, schema)

            d = self.schedule["disruptions"]
            for index, node in enumerate(d):
                self.disruptions.append(Disruption.create(node))
                index
            self.disruptions.sort(key = sort_disruptions_by_time)

            s = self.schedule["shipments"]
            for index, node in enumerate(s):
                self.shipments.append(Shipment.create(self.data_path, node))

            startTimeStr = self.schedule["start_time"]
            c =  startTimeStr[len(startTimeStr)-2]
            if c != ":":
                startTimeStr = startTimeStr[0:-2] + ":" + startTimeStr[-2:]
            self.start_time_utc = dt.datetime.fromisoformat(startTimeStr)
            self.start_time_of_work_day = dt.datetime.strptime(self.schedule["workday_start"], "%H:%M")
            self.end_time_of_work_day = dt.datetime.strptime(self.schedule["workday_end"], "%H:%M")
            self.service_network_filename = self.schedule["network"]
        except jsonschema.exceptions.ValidationError as ve:
            sys.stderr.write(str(ve) + "\n")

        f.close()
        sfp.close()

    def write(self, filePath):
        if self.schedule is None:
            self.schedule = dict()
            # Add disruptions
            self.schedule["disruptions"] = list()
            for disruption in self.disruptions:
                self.schedule["disruptions"].append(disruption.to_json())
            self.schedule["network"] = self.service_network_filename
            self.schedule["shipments"] = list()
            for shipment in self.shipments:
                self.schedule["shipments"].append(shipment.to_json())
            self.schedule["start_time"] = self.start_time_utc.isoformat(sep=" ")
            self.schedule["workday_start"] = self.start_time_of_work_day.strftime("%H:%M")
            self.schedule["workday_end"] = self.end_time_of_work_day.strftime("%H:%M")
        
        with open(filePath, 'w', encoding='utf-8') as f:
            json.dump(self.schedule, f, ensure_ascii=False, indent=4)
        f.close()

    def baseServiceNetworkName(self):
        return os.path.join(self.data_path, self.service_network_filename)

    def workdayStart(self):
        return self.start_time_of_work_day.time()
    
    def setWorkdayStart(self, start_time):
        self.start_time_of_work_day = start_time
    
    def workdayEnd(self):
        return self.end_time_of_work_day.time()

    def getServiceNetworkAtTime(self, t):
        if not self.disruptions or t < self.disruptions[0].time_in_sim:
            filename = self.data_path + self.baseServiceNetworkName()
            print("Using service network: " + filename)
            return ServiceNetwork.create(filename)

        for i in range(len(self.disruptions)):
            if t < self.disruptions[i].time_in_sim:
                # use i-1 to create service network
                filename = self.data_path + self.disruptions[i-1].service_network_filename
                print("Using service network: " + filename)
                return ServiceNetwork.create(filename)

        filename = self.data_path + self.disruptions[-1].service_network_filename
        print("Using service network: " + filename)
        return ServiceNetwork.create(filename)

    def addDisruption(self, disruption):
        self.disruptions.append(disruption)

    def addShipment(self, shipment):
        self.shipments.append(shipment)

    def findShipment(self, uuid):
        for shp in self.shipments:
            if shp.shipment_uuid == uuid:
                return shp
        return None
