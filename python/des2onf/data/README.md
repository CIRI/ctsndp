# three-paths-with-delay

A single container that is rerouted multiple times as it moves through the network.

Overview.

* When the container arrives at the Container Yard, it has three parallel paths which go through warehouse 1, warehouse 2, and warehouse 3, respectively. All paths are of the same type (Primary), thus the container is routed along the minimum cost path, which is through warehouse 1.
* Before the container moves through this path, the network is disrupted and the path has a status of Delayed. Thus, the container is rerouted along a Primary or Secondary path, which in this case is a Primary path through warehouse 2. This means the container back travels along its original path to the container yard and start moving to warehouse 2.
* Before the container moves through this path, another disruption occurs, which reroutes the container through the only non-delayed path available, which is through warehouse 3. Thus, the container once again back tracks to the Container Yard and moves along the warehouse 3 path.
* Before completing its travel along the warehouse 3 path, yet another disruption occurs so that all three of the parallel paths are now Delayed. Thus, the container finds the lowest cost path to travel. This last disruption also increases the cost of traveling along the warehouse 3, which means that the contain back tracks to the Contain Yard and travels through the warehouse 1 path, which is the lowest cost path even after incurring the charges of traveling backwards. Note that without increasing the cost of the warehouse 3 path in this example, the container would have continued along the warehouse 3 path to its destination.
* The container finally reaches its destination going through the Delayed warehouse 1 path.

_Containers_

|       Location       | time | cost | Ext. cost |
|----------------------|------|------|-----------|
| PEV                  |   20 |    2 |         0 |
| Seaway Inbound       |   45 |   10 |         0 |
| Berth                |   55 |    2 |         0 |
| Gantry Crane Offload |   80 |    1 |         0 |
| Loading Dock         |   90 |    1 |        38 |
| Berth Road Out       |  115 |    1 |         0 |
| Container Yard       |  125 |    5 |         0 |
| Local Road 1 Out     |  150 |    1 |         0 |
| Warehouse 1          |  160 |    2 |         0 |
| Local Road 1 In      |  185 |    1 |         0 |
| Container Yard       |  195 |    5 |         0 |
| Local Road 2 Out     |  220 |    2 |         0 |
| Warehouse 2          |  230 |    2 |         0 |
| Local Road 2 In      |  255 |    1 |         0 |
| Container Yard       |  265 |    5 |         0 |
| Local Road 3 Out     |  290 |    2 |         0 |
| Warehouse 3          |  300 |    2 |         0 |
| Local Road 3 In      |  325 |    1 |         0 |
| Container Yard       |  335 |    5 |         0 |
| Local Road 1 Out     |  360 |    1 |         0 |
| Warehouse 1          |  370 |    2 |         0 |
| Port Road 1 Out      |  395 |    1 |         0 |
| NHTSA Checkpoint     |  405 |    2 |         0 |
| External Road 1      |  430 |    1 |         0 |
| S Fl Groceries       |  440 |    2 |         0 |
| DEPARTED SIMULATION  |  465 |   60 |        38 |


_Shipments_

|       Location      | time | cost |
|---------------------|------|------|
| PEV                 |   20 |    2 |
| Seaway Inbound      |   45 |   10 |
| Berth               |   55 |    2 |
| Seaway Outbound     |   80 |   10 |
| PEV                 |  110 |    2 |
| DEPARTED SIMULATION |  135 |   26 | 