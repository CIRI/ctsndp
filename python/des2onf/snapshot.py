
from jsonschema import validate
import json
import jsonschema
import sys
import os
import datetime as dt

class CommodityLocation(object):

    destination = None
    group_uuid = None
    location = None
    name = None
    shipment_uuid = None
    shipper = None
    nTEU = 1
    shipperProcessed = False
    uuid = None

    def __init__(self):
        self.shipperProcessed = False

    @staticmethod
    def create(jsonNode):
        result = CommodityLocation()
        result.destination = jsonNode["destination"]
        result.group_uuid = jsonNode["group_uuid"]
        result.location = jsonNode["location"]
        result.name = jsonNode["name"]
        result.shipment_uuid = jsonNode["shipment_uuid"]
        result.shipper = jsonNode["shipper"]
        result.shipperProcessed = jsonNode["shipperProcessed"]
        result.uuid = jsonNode["uuid"]
        return result


class PendingShipments(object):
    destination = None
    group_uuid = None
    location = None
    name = None
    shipment_uuid = None
    shipper = None
    uuid = None
    arrival_time_in_sim = None

    @staticmethod
    def create(jsonNode):
        result = PendingShipments()
        result.destination = jsonNode["destination"]
        result.group_uuid = jsonNode["group_uuid"]
        result.location = jsonNode["location"]
        result.name = jsonNode["name"]
        result.shipment_uuid = jsonNode["shipment_uuid"]
        result.shipper = jsonNode["shipper"]
        result.uuid = jsonNode["uuid"]
        return result

    def to_json(self):
        jsonNode = {}
        jsonNode["destination"] = self.destination
        jsonNode["group_uuid"] = self.group_uuid
        jsonNode["location"] = self.location
        jsonNode["shipment_file"] = self.name
        jsonNode["shipment_uuid"] = self.shipment_uuid
        jsonNode["shipper"] = self.shipper
        jsonNode["uuid"] = self.uuid

        if self.arrival_time_in_sim is not None:
            jsonNode["time"] = self.arrival_time_in_sim.total_seconds() // 60

        return jsonNode


class Snapshot(object):

    snapshot = None
    commodities = list()
    pending_shipments = list()
    start_time_utc = None
    time_in_sim = dt.time(0,0)

    def __init__(self, filePath):
        self.read(filePath)

    def read(self, filePath):
        with open(filePath) as f:
            self.snapshot = json.load(f)
        try:
            with open("./schema/snapshot.schema.json") as sfp:
                schema = json.load(sfp)

            validate(self.snapshot, schema)

            c = self.snapshot["commodities"]
            for index, node in enumerate(c):
                self.commodities.append(CommodityLocation.create(node))
                index

            s = self.snapshot.get("shipments")
            if s is not None:
                for index, node in enumerate(s):
                    self.pending_shipments.append(PendingShipments.create(node))
                    index

            self.start_time_utc = dt.datetime.fromisoformat(self.snapshot["UtcTime"])
            t = int(self.snapshot["time"])

            d = t // 1440
            h = (t % 1440) // 60
            m = (t % 1440) % 60
            self.time_in_sim = dt.timedelta(days = d, hours = h, minutes = m)

        except jsonschema.exceptions.ValidationError as ve:
            sys.stderr.write(str(ve) + "\n")

        f.close()
        sfp.close()

    def time_in_sim_min(self):
        return self.time_in_sim.hour * 60 + self.time_in_sim.minute

    def get_commodities(self):
        return self.commodities

    def has_commodity(self, group_uuid):
        for commodity in self.commodities:
            if commodity.group_uuid == group_uuid:
                return True
        return False
