
from jsonschema import validate
import json
import jsonschema
import sys
import os
import datetime as dt

class Commodity(object):

    delayCost = None
    destination = None
    eat = None          # Earliest Available Time
    ldt = None          # Latest Delivery Time
    nTEU = 1
    name = None
    source = None
    group_uuid = None
    uuid = None
    shipper_name = None
    cargo_categories = list()
    transportation_methods = list()

    @staticmethod
    def create(jsonNode):
        result = Commodity()
        result.eat = jsonNode["EAT"]
        result.ldt = jsonNode["LDT"]
        result.cargo_categories = jsonNode["cargo_categories"]
        result.delayCost = jsonNode["delay_cost"]
        result.destination = jsonNode["destination"]
        result.group_uuid = jsonNode["group_uuid"]
        result.name = jsonNode["name"]
        if "uuid" in jsonNode:
            result.uuid = jsonNode["uuid"]
        result.source = jsonNode["source"]
        result.quantity = jsonNode["nTEU"]
        result.transportation_methods = jsonNode["transportation_methods"]
        return result

    def to_json(self):
        jsonNode = {}
        jsonNode["EAT"] = self.eat
        jsonNode["LDT"] = self.ldt
        jsonNode["cargo_categories"] = self.cargo_categories
        jsonNode["delay_cost"] = self.delayCost
        jsonNode["destination"] = self.destination
        jsonNode["group_uuid"] = self.group_uuid
        if self.uuid is not None:
            jsonNode["uuid"] = self.uuid
        jsonNode["name"] = self.name
        jsonNode["source"] = self.source
        jsonNode["transportation_methods"] = self.transportation_methods
        if self.shipper_name is not None:
            jsonNode["shipper"] = self.shipper_name
        jsonNode["nTEU"] = self.quantity
        return jsonNode


class Shipment(object):

    shipment_filename = None
    commodities = list()
    arrival_node_name = None
    departure_node_name = None
    destination_node_name = None
    shipment_uuid = None
    shipper_name = None
    arrival_time_in_sim = None

    def __init__(self):
        self.commodities = list()

    @staticmethod
    def create(data_path, jsonNode):
        result = Shipment()
        result.shipment_filename = os.path.join(data_path, jsonNode["shipment_file"])
        result.arrival_node_name = jsonNode["arrival_node"]
        result.departure_node_name = jsonNode["departure_node"]
        result.destination_node_name = jsonNode["destination_node"]
        result.shipment_uuid = jsonNode["shipment_uuid"]
        result.shipper_name = jsonNode["shipper"]
        t = int(jsonNode["time"])

        d = t // 1440
        h = (t % 1440) // 60
        m = (t % 1440) % 60
        result.arrival_time_in_sim = dt.timedelta(days = d, hours = h, minutes = m)
        return result


    def load_commodities(self):
        with open(self.shipment_filename) as f:
            list_of_commodities = json.load(f)
        try:
            with open("./schema/shipment.schema.json") as sfp:
                schema = json.load(sfp)

            validate(list_of_commodities, schema)

        except jsonschema.exceptions.ValidationError as ve:
            sys.stderr.write(str(ve) + "\n")

        f.close()
        sfp.close()

        for node in list_of_commodities['commodities']:
            self.commodities.append(Commodity.create(node))

    def addCommodity(self, commodity):
        self.commodities.append(commodity)

    def findCommodity(self, group_uuid, uuid):
        for commodity in self.commodities:
            if commodity.group_uuid == group_uuid and commodity.uuid == uuid:
                return commodity
        return None

    def write_shipment_file(self, outputDirPath):
        results = {}
        results["commodities"] = []
        for commodity in self.commodities:
            results["commodities"].append(commodity.to_json())

        jsonStr = json.dumps(results, sort_keys=True, indent=4)
        with open(outputDirPath + "/" + self.shipment_filename, "w") as f:
            f.write(jsonStr)
        f.close()

    def to_json(self):
        jsonNode = {"shipment_file": self.shipment_filename}
        jsonNode["arrival_node"] = self.arrival_node_name
        jsonNode["departure_node"] = self.departure_node_name
        jsonNode["destination_node"] = self.destination_node_name
        jsonNode["shipment_uuid"] = self.shipment_uuid
        if self.shipper_name is not None:
            jsonNode["shipper"] = self.shipper_name
        jsonNode["time"] = self.arrival_time_in_sim.total_seconds() // 60
        return jsonNode
