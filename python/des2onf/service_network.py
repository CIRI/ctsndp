# coding: utf-8

from cptl.network import MulticommodityFlowNetwork


class ServiceNetwork(object):
    network = None

    def __init__(self):
        self.network = MulticommodityFlowNetwork()

    def addNode(self, nodeName, **attr):
        if not self.network.has_node(nodeName):
            if len(attr) == 0:
                self.network.add_node(nodeName)
            else:
                self.network.add_node(nodeName, **attr)
        return nodeName

    def findNode(self, nodeName):
        for node in self.network.nodes():
            if nodeName == node:
                return node
        return None

    def getNodeAttributes(self, node):
        return self.network.nodes.get(node).copy()

    def copyNodeAttributes(self, node):
        return self.network.nodes.get(node).copy()

    def addEdge(self, src, dst, attr):
        self.network.add_edge(src, dst)
        edge_data = self.network.get_edge_data(src, dst, None)
        edge_data[0] = attr

    def findEdge(self, edgeName):
        for edge in self.network.edges():
            edge_data = self.network.get_edge_data(edge[0], edge[1], None)
            edge_attr = edge_data.get(0, None)
            if edgeName == edge_attr['name']:
                return edge
        return None

    def nodes(self):
        return self.network.nodes()

    def edges(self):
        return self.network.edges()

    def findEdgesWithSource(self, node):
        edges = list()
        for edge in self.network.edges():
            if edge[0] == node:
                edges.append(edge)
        return edges

    def getTravelTime(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['travel_time']

    def getFixedCost(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['cost']

    def get_per_unit_of_flow_cost(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:commodity_bundle_cost']

    def getCapacity(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['capacity']

    def getCommodityBundleCapacity(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        edge_attr = edge_data.get(0, None)
        return edge_attr['onf:commodity_bundle_capacity']

    def getEdgeAttributes(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        return edge_data.get(0, None).copy()

    def copyEdgeAttributes(self, edge):
        edge_data = self.network.get_edge_data(edge[0], edge[1], None)
        return edge_data.get(0, None).copy()

    @staticmethod
    def create(filePath):
        result = ServiceNetwork()
        result.read(filePath)
        return result

    def read(self, filePath):
        mcf = MulticommodityFlowNetwork()
        self.network = mcf.read(filePath, False)

    def write(self, filePath):
        mcf = MulticommodityFlowNetwork()
        mcf.network = self.network
        mcf.write(filePath, False)
