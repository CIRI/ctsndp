"""
Copyright (c) Gabriel A. Weaver, 2017-2018, All Rights Reserved

@author Gabe

Purpose of this module is to manage Commodities
"""
from jsonschema import validate
import json
import jsonschema
import sys


class CommodityBundle:
    """
     A group of commodities that share a common source and destination as
     well as arrival and departure time.  Commodity bundle type may be a
     single commodity or a mix of commodities.
    """
    delayCost = None
    destinations = None
    eat = None          # Earliest Available Time
    uuid = None
    ldt = None          # Latest Delivery Time
    name = None
    quantity = None
    sources = None
    type = None
    group_uuid = None
    cargo_categories = list()
    transportation_methods = list()

    def __init__(self):
        self.cargo_categories = list()
        self.transportation_methods = list()

    @staticmethod
    def create(bundleDict):
        result = CommodityBundle()
        result.uuid = bundleDict["onf:id"]
        result.delayCost = bundleDict["onf:delay_cost"]
        result.destinations = bundleDict["onf:destinations"]
        result.eat = bundleDict["onf:EAT"]
        result.ldt = bundleDict["onf:LDT"]
        result.name = bundleDict["onf:name"]
        result.quantity = bundleDict["onf:quantity"]
        result.sources = bundleDict["onf:sources"]
        result.type = bundleDict["onf:type"]
        return result

    """
    d_{k_u}:  Total quantity of commodity in the commodity set where
       k = name, u = shipmentNumber
    """

    def getCommodityQuantity(self):
        return self.quantity

    """
    O(k_u):  The set of commodity set source locations
    """

    def getSourceLocations(self):
        return self.sources

    """
    D(k_u):  The set of commodity set destination locations
    """

    def getDestinationLocations(self):
        return self.destinations

    def getKey(self):
        return self.name

    def __str__(self):
        return self.getKey()


class CommodityBundles(object):
    """
    A set of commodity bundles.  Often this corresponds to the set of
     TEUs on a vessel.
    """
    commodityBundles = None
    schemaFilePath = "../../data/schema/commodityBundles.schema.json"

    def __init__(self):
        self.commodityBundles = set([])

    @staticmethod
    def create(filePath):
        result = CommodityBundles()
        commodityBundlesDict = result.read(filePath)

        for commodityBundleDict in commodityBundlesDict["commodity_bundles"]:
            commodityBundle = CommodityBundle.create(commodityBundleDict)
            result.commodityBundles.add(commodityBundle)
        return result

    """
    d_k:  total quantity of commodity k
    """
    def get_commodity_quantity(self, k):
#        commodityBundles = [s for s in self.shipments if s.name == k]
        d_k = 0
        for commodityBundle in self.commodityBundles:
            d_k = d_k + commodityBundle.quantity

        return d_k

    def read(self, filePath):
        with open(filePath) as f:
            commodityBundlesDict = json.load(f)
        with open(self.schemaFilePath) as sfp:
            schema = json.load(sfp)

        try:
            validate(commodityBundlesDict, schema)
        except jsonschema.exceptions.ValidationError as ve:
            sys.stderr.write(str(ve) + "\n")

        f.close()
        sfp.close()

        return commodityBundlesDict

    def write(self, filePath):
        results = {}
        results["commodity_bundles"] = []
        for commodityBundle in self.commodityBundles:
            result = {}
            result["onf:delay_cost"] = commodityBundle.delayCost
            result["onf:destinations"] = commodityBundle.destinations
            result["onf:EAT"] = commodityBundle.eat
            result["onf:LDT"] = commodityBundle.ldt
            result["onf:quantity"] = commodityBundle.quantity
            result["onf:id"] = commodityBundle.uuid
            result["onf:name"] = commodityBundle.name
            result["onf:sources"] = commodityBundle.sources
            result["onf:type"] = commodityBundle.type
            results["commodity_bundles"].append(result)

        jsonStr = json.dumps(results, sort_keys=True, indent=4)
        with open(filePath, "w") as f:
            f.write(jsonStr)
        f.close()

    def __iter__(self):
        return self.commodityBundles.__iter__()

#    def __next__(self):
#        return self.commodityBundles.__next__()

    def __str__(self):
        result = []
        for commodityBundle in self.commodityBundles:
            result.append(str(commodityBundle))
        return "\n".join(result)



