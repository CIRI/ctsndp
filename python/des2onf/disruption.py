
import datetime as dt

class Disruption(object):

    service_network_filename = None
    time_in_sim = None

    def __init__(self):
        self.time_in_sim = dt.time(0, 0)

    @staticmethod
    def create(jsonNode):
        result = Disruption()
        result.from_json(jsonNode)
        return result

    def from_json(self, jsonNode):
        self.service_network_filename = jsonNode["network"]

        t = int(jsonNode["time"])
        d = t // 1440
        h = (t % 1440) // 60
        m = (t % 1440) % 60
        self.time_in_sim = dt.timedelta(days = d, hours = h, minutes = m)

    def to_json(self):
        jsonNode = {"network": self.service_network_filename}
        jsonNode["time"] = self.time_in_sim.total_seconds // 60
        return jsonNode

