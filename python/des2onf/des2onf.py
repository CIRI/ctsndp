#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from service_network import ServiceNetwork
from schedule import Schedule
from snapshot import Snapshot
from shipment import Shipment
from shipment import Commodity
from collections import defaultdict
import sys
import getopt
import datetime as dt
from datetime import timedelta
import os

def find_shipment(shipments, uuid):
    for s in shipments:
        if s.shipment_uuid == uuid:
            return s
    return None


def main(argv):
    schedule_file = None
    snapshot_file = None
    relative_time = False

    try:
        opts, args = getopt.getopt(argv, "hs:t:o:r", ["schedule", "snapshot"])
    except getopt.GetoptError:
        print('des2onf.py -s <schedule_file> -t <snapshot_file> [-r]')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('des2onf.py -s <schedule_file> -t <snapshot_file> [-r]')
            sys.exit()
        elif opt in ("-s", "--schedule"):
            schedule_file = arg
        elif opt in ("-t", "--snapshot"):
            snapshot_file = arg
        elif opt in ("-r"):
            relative_time = True

    if (len(opts) == 0) or (schedule_file is None) or (snapshot_file is None):
        print('des2onf.py -s <schedule_file> -t <snapshot_file> [-r]')
        sys.exit(2)

    schedule = Schedule()
    schedule.read(schedule_file)

    snapshot = Snapshot(snapshot_file)
    time_in_sim = snapshot.start_time_utc - schedule.start_time_utc
    time_in_sim_min = time_in_sim / dt.timedelta(minutes = 1)

    outputDirPath = snapshot_file.replace(".json", "")
    if not os.path.isdir(outputDirPath):
        os.mkdir(outputDirPath)

    #
    # The snapshot shipment data does not contain the time information.
    # We will pull it forward here.
    for pending in snapshot.pending_shipments:
        s = schedule.findShipment(pending.shipment_uuid)
        if s is not None:
            pending.arrival_time_in_sim = s.arrival_time_in_sim
    
    #
    # Building the new service network file.  We will need to put nodes into the active service
    # network where our commodities are located
    #

    # Build a list of all of the commodities that are still "in flight"
    # from the snapshot data.  Note: this will not be all the data we
    # need to build a new shipment file.  We will have to cross reference
    # the shipper uuid and group uuid with original shipment files to 
    # determine which commodities are still inflight.
    snapshot_commodities = defaultdict(list)

    network = schedule.getServiceNetworkAtTime(snapshot.time_in_sim)

    for commodity in snapshot.commodities:
        key = commodity.shipment_uuid + "::" + commodity.group_uuid

        edge = network.findEdge(commodity.location)

        if edge is None:
            # The commodity is at a node so we don't have to change the
            # service network
            node = network.findNode(commodity.location)
            if node is None:
                print("Unknown location: " + commodity.location)
                continue
            snapshot_commodities[key].append(commodity)
            continue

        # The commodity is in an edge, we will need to add a node and edge
        # in the service network
        dstNode = network.findNode(edge[1])
        newNodeName = key + "::" + commodity.uuid
        srcNode = network.findNode(edge[0])
        attr = network.copyNodeAttributes(srcNode)
        attr['nbr_containers'] = 1
        attr['cost'] = 0
        attr['extended_time_period'] = 0
        attr['extended_time_rate'] = 0
        attr['holding_time'] = 0
        attr['service_time'] = 0
        attr['storage'] = 0
        attr.pop('latitude')
        attr.pop('longitude')
        newNode = network.addNode(newNodeName, **attr)

        attr = network.copyEdgeAttributes(edge)
        attr['travel_time'] = attr['travel_time'] / 2
        attr['cost'] = attr['cost'] / 2
        attr['capacity'] = 1
        attr['name'] = commodity.name + " to " + dstNode
        attr.pop('extended_time_period')
        attr.pop('extended_time_rate')
        attr.pop('latitude')
        attr.pop('longitude')
        if 'cycle_time' in attr:
            attr.pop('cycle_time')
        network.addEdge(newNode, dstNode, attr)

        commodity.location = newNodeName
        snapshot_commodities[key].append(commodity)

    relativeOutputDirPath = "/".join(outputDirPath.split("/")[-1:])
    absoluteOutputDirPath = "/".join(outputDirPath.split("/")[:-1])

    filename = outputDirPath + "/onfServiceNetwork-"
    filename += str(time_in_sim_min)
    filename += ".json"
    network.write(filename)

    #
    # Building the new shipment files.
    #

    # Build a list of all the shipments that are "in flight"
    shipments_inflight = list()
    for shipment in schedule.shipments:
        is_pending = False
        for pending_shipment in snapshot.pending_shipments:
            if pending_shipment.shipment_uuid == shipment.shipment_uuid:
                if snapshot.has_commodity(pending_shipment.group_uuid):
                    snapshot.pending_shipments.remove(pending_shipment)
                else:
                    is_pending = True
                break
        if is_pending:
            continue
        shipments_inflight.append(shipment)


    # Build a list of all of the commodities that are still "in flight"
    # Here is where we do the cross referencing between the
    # shipment commodities and the commodities in the snapshot. 
    for inflight in shipments_inflight:
        inflight.shipper_name = None
        inflight.load_commodities()
        commodities_needing_expansion = set()
        commodities = list(inflight.commodities)
        for commodity in commodities:
            key = inflight.shipment_uuid + "::" + commodity.group_uuid
            ks = snapshot_commodities.get(key)

            # Safety check, should never really happen
            if ks is None:
                inflight.commodities.remove(commodity)
                continue
                
            found = False

            # Did the commodity already arrive at its destination?
            # Is it considered still active?
            if commodity.quantity > 1:
                for k in ks:
                    if k.name == commodity.name:
                        found = True
                        break
                    elif k.name.startswith(commodity.name):
                        commodities_needing_expansion.add(commodity)
                        found = True
                        break
            else:
                for k in ks:
                    if commodity.name == k.name:
                        found = True
                        break

            if not found:
                inflight.commodities.remove(commodity)
                continue

            # Ok, we have an active commodity, update its
            # information
            if commodity.eat < time_in_sim_min:
                if relative_time:
                    commodity.eat = 0
                    commodity.ldt = int(commodity.ldt - time_in_sim_min)
                else:
                    commodity.eat = int(time_in_sim_min)
            else:
                if relative_time:
                    commodity.eat = int(commodity.eat - time_in_sim_min)
                    commodity.ldt = int(commodity.ldt - time_in_sim_min)
 
            if commodity.eat < 0:
                commodity.eat = 0
            if commodity.ldt < 0:
                commodity.ldt = 0

            commodity.source = k.location
            if not k.shipperProcessed:
                commodity.shipper_name = k.shipper

        for commodity in commodities_needing_expansion:
            key = inflight.shipment_uuid + "::" + commodity.group_uuid
            ks = snapshot_commodities.get(key)

            for k in ks:
                if k.name.startswith(commodity.name):
                    result = Commodity()
                    result.name = k.name
                    result.eat = commodity.eat
                    result.ldt = commodity.ldt
                    result.cargo_categories = commodity.cargo_categories
                    result.delayCost = commodity.delayCost
                    result.destination = commodity.destination
                    result.group_uuid = commodity.group_uuid
                    result.uuid = k.uuid
                    result.source = commodity.source
                    result.quantity = 1
                    result.transportation_methods = commodity.transportation_methods
                    result.shipper_name = commodity.shipper_name
                    inflight.addCommodity(result)

            inflight.commodities.remove(commodity)
                   

    #
    # Building the new schedule file
    #
    new_schedule = Schedule()
    new_schedule.service_network_filename = "/".join(filename.split("/")[-2:])
    if relative_time:
        new_schedule.start_time_utc = snapshot.start_time_utc 
    else:
        new_schedule.start_time_utc = schedule.start_time_utc
     
    new_schedule.start_time_of_work_day = schedule.workdayStart()
    new_schedule.end_time_of_work_day = schedule.workdayEnd()
    for disruption in schedule.disruptions:
        if disruption.time_in_sim > snapshot.time_in_sim:
            if relative_time:
                disruption_time_utc = schedule.start_time_utc + timedelta(hours=disruption.time_in_day.hour, minutes=disruption.time_in_day.minute)
                disruption.time_in_sim = (disruption_time_utc - snapshot.time_in_sim).time()
            new_schedule.addDisruption(disruption)

    ## Add the inflight commodities
    n = 0
    for shipment in shipments_inflight:
        if len(shipment.commodities) == 0:
            continue

        shipment.shipment_filename = relativeOutputDirPath + "/onfShipment" + str(n)
        shipment.shipment_filename += "-" + str(time_in_sim_min) + ".json"
        n += 1

        if relative_time:
            shipment.arrival_time_in_sim = dt.timedelta(0)

        shipment.write_shipment_file("/".join(outputDirPath.split("/")[:-1]))
        new_schedule.addShipment(shipment)

    ## Pull the "pending shipments" forward
    for pending in snapshot.pending_shipments:
        pending.name = pending.name.replace("shipments/", "../flows/shipments/")
        s = schedule.findShipment(pending.shipment_uuid)
        if s is not None:
            if relative_time:
                pending.arrival_time_in_sim = pending.arrival_time_in_sim - snapshot.time_in_sim
                if pending.arrival_time_in_sim.total_seconds() < 0:
                    pending.arrival_time_in_sim = dt.timedelta(0)

        new_schedule.addShipment(pending)

    filename = outputDirPath.replace("Snapshot", "Schedule") 
    #filename += str(time_in_sim_min)
    filename += ".json"
    new_schedule.write(filename)


if __name__ == '__main__':
    main(sys.argv[1:])
