
\section{Capacity Constraints}

The interconnections (roads, waterways, etc...) in a service network often have some constraint on the carrying capacity of a given interconnection.  We will refer to the interconnections (roads, waterways, rail, etc..) as links within the service network.  In order to extend the paper \cite{doi:10.1287/opre.2017.1624} to account for capacity constraints of service links, we must define three terms:


\begin{enumerate}
\item Cycle Time: the amount of time needed before a new commodity can utilize the service link.  For example, a shipping lane and dock might only have space for a single ship.  The ship needs to enter the shipping land, dock, unload and clear the shipping lane before another ship can utilize the shipping lane and dock.  In this case the cycle time might be quite large.  In another example, trucks might be separated by a couple of minutes before entering a roadway to avoid congestion on the roadway.  In this case, the cycle time will be short.
\item Consolidation Time:  In the case where the cycle time is short, we will be adding a large number of arcs to the partially time-expanded network.  To reduce the number of arcs, we introduce the concept of consolidation time.  The consolidation time is used to group the arcs with a cycle time less than the consolidation time into a single arc with the corresponding capacity increase.  For example, if we have a cycle time of one and a consolidation time of ten, we will create a single arc with an increased capacity of ten (consolidation time divided by the cycle time).  Another arc will be added at the current time plus the consolidation time.
\item Hold Arc: The paper \cite{doi:10.1287/opre.2017.1624} defined the concept of holdover arcs ($tau_{ii} = 0$) to represent the storage of a commodity at a given node.  The commodity could be dispatched at any future time.  We introduce the concept of hold arcs that represent the storage of a commodity at a given node, but must stay at that node for a given amount of time, $tau_{ii} > 0$.  Hold arcs are added to $\mathscr{A}_\mathscr{T}$ as they are part of the total time it takes for the commodity to travel through the service network.
\end{enumerate}

In order to enforce capacity constraints on a given arc, we add the following constraint to the Minimum Cost Optimization problem given in section three.
\setcounter{equation}{0}
\renewcommand{\theequation}{2.\alph{equation}}
\begin{equation}
y_{ij}^{t\bar{t}} \leqslant w_{ij} \\
\qquad \forall \> ((i, t), (j, \bar{t})) \in \mathscr{A_T};
\end{equation}
where $w_{ij}$ is the maximum number of times the arc can be used before reaching its limit.

\renewcommand{\theequation}{\arabic{equation}}

To determine which links are over capacity, we follow the procedure:

\begin{enumerate}
	\item For all of the arcs in each commodity path in $P_k$, compute the total number of commodities utilizing each service link, $u_l$.
	\item Remove all links in $u_l$ where the total number of commodities utilizing the link is below or at the link capacity.
	\item The set, $u_l$, contains the total commodity usage as a function of the service link independent of time.  For all of the arcs in each commodity path in $P_k$ whose service link is in $u_l$, compute the number of commodities utilizing a link in a given time interval.
	\begin{enumerate}
		\item The length of the time interval is the maximum value of either the consolidation time or the link's cycle time.
		\item The start of the time interval is computed by using the dispatch times (see section 4.3 and 4.4) for a given arc.
		\item The utilization of the link is computed as the sum of commodities using arcs within the interval start time plus interval length    
	\end{enumerate}
	\item For all links still over capacity, use algorithm 6 to add arcs into the partially time-expanded network to increase network capacity.
\end{enumerate}

\setcounter{algorithm}{5}
\begin{algorithm}
\caption{(ENSURE-CAPACITY($(i, t),(j, t^{\prime})), consolidation\_time$)}
\label{alg:ensure_capacity}
\begin{algorithmic}[1]
\Require{Arc($(i, t),(i, t^{\prime})) \in \mathscr{A_T})$}
\State Find $t_{new}$ by finding the last outflow hold arc from $(i, t)$ 
\State Find $t_{step} = max(consolidation\_time, cycle\_time$ for $(i,j))$
\If {$(i, t_{new}) \notin \mathscr{N_T}$}
\State $(i, t')$ = REFINE-CAPACITY$((i, t), t_{new}, t_{step}, capacity)$
\State RESTORE$((i, t'), t_k, t_{new})$.
\EndIf
\For {i = 1 to utilization of $(i, j)$}
\State $t_{new} = t' + t_{step}$
\State $(i, t')$ = REFINE-CAPACITY$((i, t'), t_{new}, t_{step}, capacity)$
\State RESTORE$((i, t'), t_k, t_{new})$.
\EndFor
\end{algorithmic}
\end{algorithm}


\begin{algorithm}
\caption{(REFINE-CAPACITY($(i, t), t_{new}, t_{step}, capacity, (i, j)$)}
\label{alg:refine_capacity}
\begin{algorithmic}[1]
\Require{Node $i \in \mathscr{N}$; time point $t_{new}^i \in \mathscr{T}_i$ with $t_k^i < t_{new}^i$}; $(i, j) \in \mathscr{A}$
\If {$t_{new}^i > t_{k+1}^i$ and $(t_{new}^i - t_{k+1}^i) < t_{step}$}
\ForAll {$((i, t_{k+1}^i), (j, t)) \in \mathscr{A}_\mathscr{T}$}
\If {$((i, t_{k+1}^i), (j, t)) = (i,j)$}
\State Delete arc $((i, t_{k+1}^i), (j, t))$ from $\mathscr{A}_\mathscr{T}$
\EndIf
\EndFor
\EndIf
\State Add node $(i, t_{new}^i)$ to $\mathscr{N}_{\mathscr{T}}$;
\If {$((i, t_{k}^i), (i, t_{k+1}^i)) \in \mathscr{H_T}$}
\State Delete arc $((i, t_{k}^i), (i, _{k+1}^i))$ from $\mathscr{H_T}$
\State Add arc $((i , t_{new}^i), (i, t_{k+1}^i))$ to $\mathscr{H_T}$
\Else
\State Delete arc $((i, t_{k}^i), (i, _{k+1}^i))$ from $\mathscr{A_T}$
\State Add hold arc $((i , t_{new}^i), (i, t_{k+1}^i), t_{k+1} - t_{new})$ to $\mathscr{A_T}$
\EndIf
\State Delete arc $((i, t_k^i), (i, t_{k+1}^i))$ from $\mathscr{A_T}$
\If {$(t_{new}^i - t_{k}^i) <= t_{step}$}
\State Add hold arc $((i , t_{k}^i), (i, t_{new}^i), t_{new} - t_k)$ to $\mathscr{A_T}$
\Else
\State Add arc $((i , t_{k}^i), (i, t_{new}^i))$ to $\mathscr{H_T}$
\EndIf
\ForAll {$((i, t_k^i), (j, t)) \in \mathscr{A_T}$}
\State Add arc $((i, t_{new}^i), (j, t))$ to $\mathscr{A_T}$
\State Set $w_{ij}$ for arc $((i, t_{new}^i), (j, t))$
\EndFor
\State \Return $(i, t_{new}^i)$
\end{algorithmic}
\end{algorithm}



%\FloatBarrier
%\begin{equation*}
%Z = min\sum_{k \in \mathscr{K}}\sum_{j=1}^{|P_k|-1}\gamma_i^k - \alpha_{i_ji_{j+1}}^k
%\end{equation*}
%subject to
%\begin{equation}
%\gamma_i^k \geqslant \alpha_{i_ji_{j+1}}^k \qquad \forall \> k \in \mathscr{K}, j = 1,...,|P_k|-1;
%\end{equation}
%\begin{equation}
%\gamma_{i_j}^k + \tau_{ij} \leqslant l_k \qquad \forall \> k \in \mathscr{K}, j = 1,...,|P_k|-1;
%\end{equation}

