<a id="top"></a>

# BUILDING THE CTSNDP EXECUTABLE

These are the instructions on building the main CTSNDP executable and tests.  We use the cmake meta-build system to generate the build system for the target operating system.  CMake encourages the use of a side-by-side build structure.  In this way, the build, object, library and executable files are kept separate from the source files use to generate them.  A side-by-side build structure look like:

    .
    ├── build             # The location of the object, library and executables
    └── src               # the source code

Before we begin, make sure you have:

1. [CMake is installed on your system](cmake.md#top).
2. Built and installed all [third party libraries](build_third_party_libs.md#top).
3. Gurobi installed, an academic license is aquired and the GUROBI_HOME environment is set.

Here is a suggested procedure to clone, build and install the CTSNDP project.

1. Open terminal window
2. Create a directory named CIRI in your home directory if it does not already exist.

> mkdir CIRI

3. Change into the CIRI directory

> cd CIRI

4. Clone the project

> git clone https://gitlab.engr.illinois.edu/CIRI/ctsndp.git

5. Change into the newly created library directory

> cd ctsndp

6. Because we are using C++ and python in this project.  We have an additional sub-directory to transverse.  Change to the cpp directory.

> cd cpp

7. Create the build directory

> mkdir build

8. Change into the build directory

> cd build

**Windows** 

A special note to Windows users.  The default location to install the libraries is C:\Program Files\${PROJECT_NAME}.  Unfortunately, this directory is protected and Windows does not have a good sudo system in place.  If you installed the third party libraries in the directory C:\Libraries, you should add the following option when invoking cmake for the first time:
`-DCMAKE_PREFIX_PATH="C:\Libraries"`.

**CPlex** 

The default solver for the ctsndp application is Gurobi, but you can also build the application to use the CPlex solver.  Cmake will first try to find the installation location of CPlex. If cmake can't find CPlex, then cmake will try to find the Gurobi installation.  If you have both solvers install on your computer, you can force the choice of one or the other using `-D FORCE_GUROBI=ON` or `-D FORCE_CPLEX=ON`.

## Using Make

Make is the default build tool for many operating systems: Linux, MacOS, etc.  Make can also be use in Windows environments if Cygwin or MingW64/msys are install.

> cmake -G "Unix Makefiles" ..
> make

## Using Ninja

Ninja is a cross-platform build tool like make.  However, it does provide a faster build system then make does.  If you have ninja install, you can use:

> cmake -G "Ninja" ..
> ninja

If everything builds correctly, the executable will be located in the build/bin directory.

# Linking Error

The ctsndp application is a C++ based application using a third party solver, Gurobi or CPlex.  It is important in the linking process that all of the code conforms to the same application binary interface, ABI.  Read the documentation for the solver you are using to make sure you link to the correct version.
