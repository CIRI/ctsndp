<a id="top"></a>

# Command Line Options

| Option | Parameter | Description
| ------ | --------- | ---
**-d, --debug** | Debug Level | Debug level [0 to 5]. Five is the most information.  Also [see logging options](logging-options)
**-f, --format** | Output Format | The format of the route data, default is ONF. Current formats: DES, ONF, or BOTH
**-l, --log** | Log Filename | Log optimization data to file
**-m, --numeric** | Numeric Focus Level | The numeric focus level of the solvers [0 to 3]
**-n, --service_network** | Service Network Filename | The filename defining the service network
**-o, --output** | Output Filename | The output filename for the routes
**-r, --result** | Results Filename | The output filename for the results (same as ONF routes)
**-s, --shipments** | Shipments Filename | The filename defining the shipments occuring within the service network
**-t, --time** | Consolidation Time | Group capacity limited arcs **Required to enable link capacity constraints**
**-u, --schedule** | Schedule Filename | The filename defining the system schedule, includes service network and shipments
**-w, --write** | Debug Filename | Write debug information to file
**--delay_threshold** | Delay Threshold (%) | Sets the threshold at which delay nodes are added to the network: 0 - 100
**--time_limit** | Time Limit (sec) | Limits optimization time (in seconds)
**--mcp** | Tuning Parameters Filename | Tuning parameters for the Minimize Cost Model
**--iatlp** | Tuning Parameters Filename | Tuning parameters for the Identify Arcs To Lengthen Model
**--cfsp** | Tuning Parameters Filename | Tuning parameters for the Construct Feasible Solution Model
**--mdp** | Tuning Parameters Filename | Tuning parameters for the Minimize Delays Model

Note: for Gurobi, the tuning parameter filename must have a .prm extension!

# Command Line Switches

| Option | Description
| ------ | ---
**-c, --log_console** | Log optimization data to console
**-p, --print** | Print partially time expanded network
**-q, --quite** | Quite mode
**-v, --verbose** | Verbose mode
**--ignore_required_locations** | Ignore all required locations in the shipment files
**--no_delays** | Disable the ability of commoditities to arrive late
**--timing** | Show timing information
**--no_consolidations** | Do not consolidate any of the commodities
**--consolidate_by_time** | Consolidate commodities based an ideal arrival time (For Testing)
**--consolidate_by_capacity** | Consolidate commodities only if the arc has limited capacity (For Testing)
**--no_capacity_limits** | Do not limit the capacity of an arc
**--allow_invalid_expansion** | Allow an invalid expansion of the partially time-expanded network on capacity limits (For Testing)
**-?, -h, --help** | Show the list of command line options

## Logging Options

For logging options: [see log_files.md](log_files.md#top)
