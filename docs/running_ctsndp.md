<a id="top"></a>

# Running the CTSNDP Application

The CTSNDP Application is a command line application requiring command line parameters for proper operation.  For a general list of options, use

> ctsndp -h

The CTSNDP needs two JSON based input files, one file specifying the service network and the other file specifying the commodities that will flow on the service network.  The main output of the CTSNDP application is a JSON based routes file:

> ctsndp -n <service_network_filename> -s <shipment_filename> -o <routes_filename>
>
> For example:
> ctsndp -n network.gsni -s shipment.shpt -o ./onf.routes

The *-n* option is used to specify the service network, the *-s* is used to specify the shipment file, and *-o* is used to specify the output routes filename. The output "routes" file, is a JSON formatted file using an ONF based format for visualization.  The output format can be controlled using the *-f* option.  To use the "routes" file for the DES application use:

> ctsndp -n network.gsni -s shipment.shpt -f DES -o ./des.routes

Note the *-f DES* option.  You can use *-f BOTH* to get both formats.  The onf version will have an ONF extension, <routes_filename>.onf, and the DES version will have a des extension, <routes_filename>.des.

When used in conjunction with DES application, the input can be simpilified as the DES application uses a JSON based schedule file to specify the service network and shipments.

> ctsndp -u <schedule_filename> -o <routes_filename>
>
> For example:
> ctsndp -u schedule.json -f DES -o ./des.routes

Another very useful option is *-r*.  The *-r* is used to write out a human readable output file:

> ctsndp -u <schedule_filename> -o <routes_filename> -r <results_filename>
>
> For example:
> ctsndp -u schedule.json -f DES -o ./des.routes -r ./des.results

For a full list of command line options: [see command_line_parameters](command_line_parameters.md#top)

# Examples

You can find two examples in the data directory.  The first one is in `data/simple`, and the second one is in `data/multipath`.  Both examples have either a shell script or a batch file you can use to run them.  The examples also have an `<example>.expected.routes` file which is the expected output from running the example.
