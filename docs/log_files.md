<a id="top"></a>

# Log Files

All log files are in a human readable format.  Some log files are listed as having a **_#** in the file name.  The **#** is replaced with the number assigned to the pass through the solver.

# Logging Command Line Options

| Option | Parameter | Description
| ------ | --------- | ---
**--log_path** | Log Path | The location to write the log files

# Logging Command Line Switches

| Option | Description
| ------ | ---
**--log_results** | Log the results to file [results.txt](#results-log)
**--log_create** | Log the results to file [create_initial.txt](#create-initial-log)
**--log_pten** | Log the partially time expanded network to file [pten_#.txt](partially-time-expanded-network-log)
**--log_properties** | Log the property [1 thru 4] checks of the partially time expanded network to file [properties_#.txt](properties-log)
**--log_min_cost_violations** | Log the minimize cost optimization constraint violations to file [MinimizeCostsConstraintViolations_#.txt](minimize-costs-constraint-violations-log)
**--log_commodity_paths** | Log the commodity paths to file [MinimizeCosts_X_kij_#.txt](minimize-costs-x_kij-log)
**--log_utilization** | Log the arc utilization to file [MinimizeCosts_Y_ij_#.txt](minimize-costs-y_ij-log)
**--log_consolidations** | Log the commodity consolidations to file [consolidations_#.txt](consolidations-log)
**--log_identify_arcs_violations** | Log the identify arcs to lengthen optimization constraint violations to file []]()
**--log_arcs_to_lengthen** | Log the arcs to lengthen to file [arcs_to_lengthen_#.txt](arcs-to-lengthen-log)
**--log_identify_arcs_time_info** | Log the time info for the arcs to lengthen to file [IdentifyArcsTimeInfo_#.txt](identify-arcs-time-info-log)
**--logCommoditySchedules** | Log the commodity schedule to file [IdentifyArcsCommoditySchedule_#.txt](identify-arcs-commodity-schedule-log)
**--log_construct_solution_violations** | Log the construct solution optimization constraint violations to file [ConstructSolutionConstraintViolations_#.txt](construct-solution-constraint-violations-log)
**--logMinimizeDelayConstraintViolations** | Log the minimize delay optimization constraint violations to file [MinimizeDelayConstraintViolations_#.txt](minimize-delay-constraint-violations-log)
**--logArcToShorten** | Log the arcs to shorten to file [arcs_to_shorten_#.txt](arcs-to-shorten-log)
**--log_link_utilization** | Log the service link utilization info file [link_utilization_#.txt](link-utilization-log)
**--log_link_overcapacity** | Log the links in the service network that are over capacity to file [link_overcapacity_#.txt](link-over-capacity-log)
**--log_all** | Turns on all logging.  Be prepared for A LOT of files!

# Log File Formats

## Results Log

Repeated for all commodities:
> FullyQualifiedCommodityName (qty = #) :
>
> Path in the form: Node Name (arrives: #, departs: #) =={ Arc Name }==> Node Name (arrives: #, departs: #) =={ Arc Name }==> ...
>
> Share of Fixed cost = #, Variable cost = #, Delay Cost = #, Variable delay cost = #, Total cost = #

List of arcs:
> Arc Name: utilization = Number of times arc was used in the solution

Summation of Costs:
> Of the form: Computed total cost campared to the ideal minimal cost from the first optimization

## Create Initial Log

## Partially Time-Expanded Network Log

## Properties Log

## Minimize Costs Constraint Violations Log

## Minimize Costs X_kij Log

## Minimize Costs Y_ij Log

## Consolidations Log

## Arcs To Lengthen Log

## Identify Arcs Time Info Log

## Identify Arcs Commodity Schedule Log

## Construct Solution Constraint Violations Log

## Minimize Delay Constraint Violations Log

## Arcs To Shorten Log

## Link Utilization Log

## Link Over Capacity Log
