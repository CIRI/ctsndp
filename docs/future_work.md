<a id="top"></a>

# Future Work

## Required Locations

One of the requirements of the CTSNDP application is that certain commodities are required to flow through certain locations.  My first attempt was to force x_kij to one so that the commodity would be forced to incur that cost and the minimize cost optimization would choose that location as path of the path.  However, it does not scale.  I turned off the feature in main.cpp with the line:

```c++
bool ignore_required_locations = true;  // should be false!!
```

A better way to implement this feature will need to be found.  I should note that one of the things that should be looked into is determining which commodities should be consolidated in the case of required location.  To help in this area, I have implemented the variable mJ_ak, the paper uses Ja pg. 1312, as a map (dictionary) with the key being the arc and value being a vector of vector of commodities:

```c++
typedef std::vector<const cCommodityBundle*> commodities_t;
typedef std::map<arc_ptr_t, std::vector<commodities_t>> consolidation_groups_t;
```

In this way, commodity consolidation can be done in "groups" based on some feature to be determined in the future.

## Identify Arcs to Lengthen / Construct Feasible Solution

The flow of the program is to use the "Minimize Costs" optimization to find the path flow of the commodities through the service network which minimizes costs.  In a partially time-expanded network, some of the arcs can be too short.  The "Identify Arcs to Lengthen" MIP optimization will find the arcs that are too short.  The arcs are then lengthen to their true travel times and the flows are re-optimized.  Once there are no further arcs to lengthen, another optimization is called, "Construct Feasible Solution", and the final routes and dispatch times are determined.  The "Construct Feasible Solution" optimization is a linear problem, LP.

Another way to find a solution to the CTSNDP problem is to start with the "Minimize Cost" optimization to find the paths.  We can then use the "Identify Arcs to Lengthen" find all arcs that are too short.  Lengthen those arcs until there are no more "short" arcs and derive our solution.  Since "Identify Arcs to Lengthen" is an MIP, it is relatively fast, however, it can become infeasible.  At this point, we can use the more computationaly intensive "Construct Feasible Solution" LP optimization.  The "Construct Feasible Solution" optimization will also tell us which arcs need to be lengthen and the process starts over.

To use the second mode of operation, you will need to uncomment the follow define found in ```ctsndp_defs.hpp```:

```c++
//#define CONSTRUCT_FEASIBLE_SOLUTION_AS_BACKUP
```

## Link Capacities

I have put in some code for this, but a lot more work has to be put into this feature.  The thing to remember is that the partially time-expanded network uses arcs which represents a link (road, etc) in the service network at a given time.  One must collect the arc utilization to determine if the underlining link is over capacity.  The code already does this, but it can be confusing.  See code in ctsndp.cpp and algorithm 6.  Note: algorithm 7 is not use.
