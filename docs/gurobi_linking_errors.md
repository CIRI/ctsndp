<a id="top"></a>

# Gurobi Linking Errors

When building the CTSNDP application with Gurobi as the solver, you might run into linking errors at the end of the build process.  You might see something like:

```text
GurobiModel.cpp:(.text+0x5d6): undefined reference to `GRBModel::set(GRB_StringAttr, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)'
/usr/bin/ld: ../lib64/libctsndp.a(GurobiModel.cpp.o): in function `cGurobiModel::ComputeIIS[abi:cxx11]() [clone .cold]':
GurobiModel.cpp:(.text.unlikely+0x82): undefined reference to `GRBException::getMessage[abi:cxx11]() const'
collect2: error: ld returned 1 exit status
```

The source of the error is an incompatibility ABI issue between the Gurobi library and the CTSNDP application.  There are two methods to solve this issue.  The first method is to change the symbolic link to point to an updated library, and the second method is to build the library using the current C++ compiler.  The ABI incompatibility is with the static library: \*.a for linux/mac systems and \*.lib for Window systems.

## Changing the Symbolic Link

Gurobi ships with their library built with several different versions of the native system C++ compiler.

### Linux

The default compiler for linux based systems is gcc/g++.  Gurobi uses the following naming convention for their linux library: ```libgurobi_g++<gcc version number>.a```.  For example, ```libgurobi_g++4.2.a```.  Gurobi uses a symbolic link so that standard library name ```libgurobi_c++.a``` can be passed to the link and then redirected to the correct library for the gcc version.  The symbolic link and libraries are found in ```gurobi<version>/linux64/lib``` directory.  If you have linking issues, try updating the symbolic link to point to the latest gcc version.

> ln -sf libgurobi_g++\<gcc version number\>.a libgurobi_c++.a
>
> Example:<br/>
> ln -sf libgurobi_g++5.2.a libgurobi_c++.a

### Windows

The default compiler for Windows based systems is Visual Studio.  Gurobi uses the following naming convention for their Windows library: ```gurobi_c++<Runtime Library><Visual Studio Year>.lib```.  Visual Studio has four runtime library linkage options: multithreaded (mt), multithreaded debug (mtd), multithreaded dll (md), and multithreaded dll debug (mdd).  For example, the gurobi library build with Visual Studio 2017 will have the name ```gurobi_c++mt2017.lib```.  Windows 10 supports symbolic links and we will use a symbolic link for the release build, ```gurobi_c++.lib```, and another symbolic link for the debug build, ```gurobi_c++d.lib```.  The symbolic link and libraries are found in ```gurobi<version>/win64/lib``` directory.  If you have linking issues, try updating the symbolic link to point to the latest Visual Studio version.

Open a command prompt as administrator.  

> mklink gurobi_c++.lib gurobi_c++mt\<Visual Studio Year\>.lib<br/>
> mklink gurobi_c++d.lib gurobi_c++mtd\<Visual Studio Year\>.lib<br/>
>
> Example:<br/>
> mklink gurobi_c++.lib gurobi_c++mt2019.lib<br/>
> mklink gurobi_c++d.lib gurobi_c++mtd2019.lib

### MacOS

The default compiler for Mac systems is clang.  

<!-- Gurobi uses the following naming convention for their linux library: ```libgurobi_g++<gcc version number>.a```.  For example, libgurobi_g++4.2.a.  Gurobi uses a symbolic link so that standard library name ```libgurobi_c++.a``` can be passed to the link and then redirected to the correct library for the gcc version.  The symbolic link and libraries are found in ```gurobi<version>/linux64/lib``` directory.  If you have linking issues, try updating the symbolic link to point to the latest gcc version.
-->

> Not sure at this time???

## Building the Gurobi Library

Sometimes you are not using the standard compiler or there is an update to the compiler that breaks ABI compatibility.  In these cases, you will need to build the Gurobi library yourself.  Gurobi give you the source code to do this in the ```gurobi<version>/<os>/src``` directory.

### Systems with Make support (Linux, MacOS, MingW, etc)

### Visual Studio
