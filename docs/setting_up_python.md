<a id="top"></a>

# Setting Up Python

The Python portion of this project uses Python 2.7.  I use PyCharm as an IDE for this project.  One of the requirements is matplotlib.  On MacOS, there is a bug in creating the virtual environment with the matplotlib package.  Unfortunately, this means we can not use virtual environments for this project.

This project requires GUROBI to be installed.  GUROBI provides a utility, setup.py, to install the required components.

## PyCharm

If you are using PyCharm, open a terminal window (select the option at the bottom left of the main PyCharm window.)  Note: you want to use the terminal inside PyCharm so that the correct Python is used.  Change to where the GUROBI libraries are installed.

```
# For Mac:
cd /Libraries/gurobi801/mac64
```

Use the following command to install gurobi.py into the virtual environment:

```
python setup.py install
```

## Other Prerequisites

The following packages are also require: jsonschema, NetworkX, matplotlib.  These additional packages can be installed through PyCharm be going into the System/Preferences dialog, select "Project: python" | Project Interpreter, and selecting the "+" button, or using:

```
pip install jsonschema NetworkX matplotlib
```

