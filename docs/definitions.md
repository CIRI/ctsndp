<a id="top"></a>

# Definitions

## Commodity Bundle

A commodity bundle is a group of goods being shipped as one unit.  A commodity bundle is the "atomic" unit of a shipment and is an abstract idea, the exact meaning depends on your use case.  By atomic we mean that a commodity bundle can not be divided into smaller parts. Commodity bundles can take different paths through the service network, but are always part of a single shipment.  Here are some examples:

### Port Operator

If I am a port operator, I can think of "commodity bundles" as my shipping containers as I can't have part of a shipping container.  The shipping containers can be loaded onto different trucks, trains, or ships as the containers move through the port facility.

### Warehouse Operator

If I am a warehouse operator, I can think of "commodity bundles" as pallets of goods that I move around the warehouse or load onto trucks or trains.

## Shipment

A shipment is a collection of commodity bundles that have the following properties:

1 single source and a single destination.

## Time




