\section{Delays}

It is not uncommon for the shipment of commodities to have provisions for a commodity to arrive late to its destination.  The paper \cite{doi:10.1287/opre.2017.1624} defines the concept of holdover arcs.  
A holdover arc represents the storage of commodity at a given location with the service location.  Thus, the commodity can arrive early at a given location and depart at a later time.  
There are times when it would be beneficial for a commodity to arrive late to its destination because the consolidation of commodities could result in an overall lower cost. 
In order to support commodities arriving late, or delayed, we introduce the concept of delay nodes and delay arcs to form a delay path for a given set of commodities.

We define the following optional commodity attributes in order to support late arrivals: 

\begin{enumerate}
\item A fixed delay cost charged to the shipper, $delay\_cost_k$
\item A maximum amount of time the commodity is allowed to be late, $\Delta_k$
\item A delay penalty based on how late the commodity is multiplied by a delay penalty rate, $delay\_penalty\_rate_k$
\item An upper bound to the delay penalty, $delay_penalty\_limit_k$
\end{enumerate}

A commodity must have a delay cost and/or a delay penalty rate defined in order to be consider a delayed arrival. 


\section*{Delay Nodes}

A delay node is similar to other nodes within the partially time expanded network except they contain the set of commodities that are allowed to pass through the node.  Thus, a delay node is defined by:

($d_k$, t, $\mathscr{K}'$) where $d_k$ is the destination location of the commodity, t greater than the latest deliver time, $l_k$, for commodity $k$, and $\mathscr{K}'$ is the set of commodities that can through the delay node.

Note: all commodities in the set $\mathscr{K}'$ have the same $d_k$ and  $l_k$.  A delay node has a set of commodities associated with the node because we must restrict this portion of the partially expanded network to the commodities that can be delayed.  

\section*{Delay Arcs}

A delay arc is similar to other arcs within the partially time expanded network except that the arcs travel time is negative, and the source node of a delay arc must be a delay node.  Thus, a delay arc is defined by:

(($d_k$, $t$), (($d_k$, $t'$)), $\mathscr{K}'$) where $t > t'  \geqslant l_k$, and $\mathscr{K}'$ is the set of commodities that can utilizes the arc in optimizing the commodities routing.

\section*{Delay Path}

A delay path is the part of the partially time-expanded network made up of the delay nodes and delays arcs for a given set of commodities, $\mathscr{K}' \subset \mathscr{K}$.  The delay path starts at ($d_k$, $t_{max}$) and ends at ($d_k$, $ldt$) for $k \in \mathscr{K}'$.  It should be noted that a single destination ($d_k$, $ldt$) might have more than one delay arc due to different maximum delay times.  If a destination has more than one delay path than the following holds true for each commodity subset in the delay path, $\mathscr{K}' \cap \mathscr{K}'' = \emptyset$


%The paper \cite{doi:10.1287/opre.2017.1624} gives several optimizations in order to solve the continuous time service network design problem.  We introduce the concept of delay nodes and arcs in order to solve the problem with commodities arriving late. 
%
%The "The Continuous-Time Service Network Design Problem" paper makes use  A commodity is allowed to arrive late if and only if the commodity has late penalty, $p_k$, associated with it. A commodity that is allow to be late may also has a limit on how late it is allowed to be.  The maximum delay time is given by $\Delta_k$.  We introduce the concept of delay nodes and delay arcs.  A delay node is a node in the partially time expanded network at the commodity's destination location, $d_k$, with a time, $t_f$, greater than the latest deliver time, $l_k$, for commodity $k$.  A delay arc connects the delay node in the partially time expanded network to destination node, $(d_k, l_k)$, for the commodity.  The "travel time" assigned to the delay arc is equal to $l_k$ minus $t_f$ and will always be negative.  The negative travel times allows constraint five of the minimum cost problem to be satisfied.  

\FloatBarrier
\section*{Changes to the Creation of the Partially Time Expanded Network}

Let $\mathscr{L}$ be the subset of commodities, $\mathscr{K}$, that are allowed to arrive late.  Thus, algorithm~\ref{alg:create_initial} is extended to add delay nodes/arcs into the partially time-expanded network, see operations~\ref{alg:create_initial:op0} through ~\ref{alg:create_initial:op1}.

\setcounter{algorithm}{1}
\begin{algorithm}
\caption{(CREATE-INITIAL)}
\label{alg:create_initial}
\begin{algorithmic}[1]
\Require{Directed network $\mathscr{D} = (\mathscr{N}, \mathscr{A})$, commodity set $\mathscr{K}$}
\ForAll {$k \in \mathscr{K}$}
\State Add node$(o_k, e_k)$ to $\mathscr{N}_\mathscr{T}$
\State Add node$(d_k, l_k)$ to $\mathscr{N}_\mathscr{T}$
\EndFor
\ForAll {$u \in \mathscr{N}$}
\State Add node$(u, 0)$ to $\mathscr{N}_\mathscr{T}$
\EndFor
\ForAll {$(i, t) \in \mathscr{N}_\mathscr{T}$}
\ForAll {$(i, j) \in \mathscr{A}$}
\State Find largest $t'$ such that $(j, t') \in \mathscr{N_T}$ and 
$t'  \leqslant t + \tau_{ij}$ and add arc$((i, t), (j, t'))$ to $\mathscr{A_T}$
\EndFor
\State Find smallest $t'$ such that $(i, t') \in \mathscr{N}_\mathscr{T}$ and $t' > t$ and add arc$((i, t), (i, t'))$ to $\mathscr{H}_\mathscr{T}$
\EndFor
\ForAll {$k \in \mathscr{L}$} \label{alg:create_initial:op0}
\If {$k$ has maximum delay time}
\State $t_{max} = l_k + \Delta_{k}$
\Else
\State $t_{max} =$ MAX\_ALLOWED\_DELAY	\Comment{Largest allowed value in optimizer}
\EndIf
\If {delay node$(d_k, t_{max})$ exists}
\State Add k to $\mathscr{K}'$ for delay node$(d_k, t_{max})$
\Else
\State Add delay node$(d_k, t_{max}, k)$ to $\mathscr{N}_\mathscr{T}$
\ForAll {arc$((i, t),(d_k, l_k)) \in \mathscr{A}$}
\State Find largest $t'$ such that $(i, t') \in \mathscr{N_T}$ and add arc$((i, t'), (d_k, t_{max}))$ to $\mathscr{A_T}$
\EndFor
\EndIf
\If {delay arc$((d_k, t_{max}), (d_k, l_k)$ does not exists}
\State Add delay arc$((d_k, t_{max}), (d_k, l_k))$ to $\mathscr{A}_\mathscr{T}$
\EndIf
\EndFor \label{alg:create_initial:op1}
\end{algorithmic}
\end{algorithm}

\FloatBarrier
\section*{Changes to the Minimize Cost Optimization}

Delay arcs are a function of  $\mathscr{K}'$ and therefore make up pseudo arcs in the partially time-expanded network.

We define the following subsets of $\mathscr{A_T}$: 

\begin{enumerate}
\item Let $\mathscr{A'_T}$ be a subset of $\mathscr{A_T}$ where the travel time for an arc is positive, (travel arcs)
\item Let $\mathscr{A''_T}$ be a subset of $\mathscr{A_T}$ where the travel time for an arc is negative (delay arcs)
\item Let $\mathscr{A'''_T}$ be a subset of $\mathscr{A_T}$ where the source or destination node is a delay node
\end{enumerate}

\FloatBarrier
\section*{Minimum Cost Problem}
\setcounter{equation}{0}
\begin{equation*}
z(\mathscr{D_T}) = min\left\{\sum_{((i, t), (j, \bar{t})) \in \mathscr{A'_T}} f_{ij} \\
y_{ij}^{t\bar{t}} + \sum_{((i, t), (j, \bar{t})) \in \mathscr{A''_T}} \sum_{k \in \mathscr{K'}} f_{ij}^k y_{ij}^{kt\bar{t}}
 + \sum_{k \in \mathscr{K}}  \sum_{((i, t), (j, \bar{t})) \in \\ 
\mathscr{A}_\mathscr{T}} c_{ij}^k q_k x_{ij}^{kt\bar{t}}\right\}
\end{equation*}
subject to
%\begin{equation}
\begin{eqnarray}\nonumber
&& \sum_{((i, t), (j, \bar{t})) \in \mathscr{A_T} \cup \mathscr{H_T}}
 x_{ij}^{kt\bar{t}}
 - \sum_{((j, \bar{t}), (i, t)) \in \mathscr{A_T} \cup \mathscr{H_T}}
 x_{ji}^{k\bar{t}t}\\
&& =
\begin{cases}
1& (i, t) = (o_k, e_k), \\
-1& (i, t) = (d_k, l_k), \quad \forall \> k \in \mathscr{K}, (i, t) \in \mathscr{N_T}\\
0& otherwise;
\end{cases}
\end{eqnarray}
%\end{equation}
%\begin{equation}
\begin{eqnarray}\nonumber
&& \sum_{k \in \mathscr{K}} q_k x_{ij}^{kt\bar{t}} \leqslant u_{ij} y_{ij}^{t\bar{t}}
\qquad \forall \> ((i, t), (j, \bar{t})) \in \mathscr{A'_T}, and\\
&& \sum_{k \in \mathscr{K}} q_k x_{ij}^{kt\bar{t}} \leqslant  \sum_{k \in \mathscr{K'}} u_{ij}^k y_{ij}^{kt\bar{t}}
\qquad \forall \> ((i, t), (j, \bar{t})) \in \mathscr{A''_T};
\end{eqnarray}
%\end{equation}
\begin{equation}
x_{ij}^{kt\bar{t}} \in \{0, 1\} \qquad \forall \> k \in \mathscr{K}, \\
((i, t), (j, \bar{t})) \in \mathscr{A_T} \cup \mathscr{H_T};
\end{equation}
\begin{equation}
y_{ij}^{t\bar{t}} \in \mathbb{N}_{\geqslant 0} \\
\qquad \forall \> ((i, t), (j, \bar{t})) \in \mathscr{A_T};
\end{equation}
\begin{equation}
\sum_{((i, t), (j, t^\prime)) \in \mathscr{A_T}} {\tau}_{ij} x_{ij}^{k\tau^\prime} \leqslant l_k - e_k; \\
\qquad \forall \> ((i, t), (j, \bar{t})) \in \mathscr{A_T};
\end{equation}
\begin{equation}
x_{ij}^{kt^\prime} = 0 \qquad  \forall \> k \in \mathscr{K},  \forall \> ((i, t), (j, t^\prime)) \in \mathscr{A'''_T}  \quad where  \quad k \notin  \mathscr{K'};
%\text{k is NOT allowed to travel on }(i, j) \implies x_{ij}^{kt\bar{t}} = 0 \qquad 
%\forall \> k \in \mathscr{K}, (i, j) \in \mathscr{A}.
\end{equation}


\section*{Minimize Delay Optimization}

\FloatBarrier
\section*{Minimize Delay Problem}
In the partially time-expanded network, the delay path for a commodity might be too long.  If the delay path is too long, the delay penalty assigned to a commodity will be too high because the delay penalty is computed by multiplying the given delay penalty rate for the commodity by the length in time of the delay path.  Thus, the minimized cost is not optimal because the delay penalty is not optimal.  We will therefore define a minimize delay optimization to find the delay paths that can be shortened.  In the minimize delay problem we define the following variables:
\begin{enumerate}
\item $\gamma_{i}^{'k}$ is the dispatch time of commodity, k, at the node, i, where the arc ij ends at the beginning of the commodity's delay path (i is not a delay node while j is a delay node).  $\gamma_{i}^{'k}$ is determined by the previous optimizations defined in sections 4.3 or 4.4.
\item $\alpha_{j}^k$ is the lower bound of the delay time.  $\alpha_{j}^k$ is computed as $\gamma_{i}^{'k} + \tau_{ij}$
\item $\gamma_{j}^k$ is the optimization variable representing the minimum dispatch time of commodity, k, along its delay path.  It should be noted that the commodity, k, has reached its destination at this time, but this dispatch time is needed to determine if a delay arc needs to be shortened and reduce the delay penalty.
\end{enumerate}

The optimization problem becomes:

\setcounter{equation}{17}
\begin{equation*}
Z = min\sum_{k \in \mathscr{L}} (1 + delay\_penalty\_rate^k)*(\gamma_{j}^{k} - \alpha_{j}^k)
\end{equation*}
%\begin{equation}
%\end{equation}
\begin{equation}
\gamma_{j}^{k} \geqslant \alpha_{j}^k, \qquad \forall \> k \in \mathscr{L},
\end{equation}
\begin{equation}
\gamma_{j}^k \leqslant l_k + \Delta_k \\
\qquad \forall \> k \in \mathscr{L^{\prime}},
\end{equation}

We will shorten the delay path of of a commodity, k, when $\gamma_{j}^{k}$ is less then the commodity's delay path start time, node (j, t).  I am not sure this is really an optimization problem, but I kept it as such because I was not sure how it would develop.

\FloatBarrier
\section*{Shorten Delay Arc Algorithms}

\setcounter{algorithm}{7}
\begin{algorithm}
\caption{(SHORTEN-ARC($(i, t),(i, t^{\prime})), t_{new}^i$)}
\label{alg:shorten_arc}
\begin{algorithmic}[1]
\Require{Arc($(i, t),(i, t^{\prime})) \in \mathscr{A_\mathscr{T}})$}; time point $t_{new}^i \notin \mathscr{T}_i$ 
\State Find {$((i, t_{k+1}^i), (i, t_k^i)) \in \mathscr{A}_\mathscr{T}$ such that $t_k^i < t_{new}^i < t_{k+1}^i$}
\State REFINE\_DELAY$((i, t^{\prime}, \mathscr{K'}), t_k, t_{new}, t_{k+1})$
\State RESTORE\_DELAY$((i, t_k, \mathscr{K'}), t_{new})$.
\end{algorithmic}
\end{algorithm}



\begin{algorithm}
\caption{(REFINE\_DELAY($(i, t^{\prime}, \mathscr{K'})$)}
\label{alg:refine}
\begin{algorithmic}[1]
\Require{Delay Node $(i, t^{\prime}, \mathscr{K'}) \in \mathscr{N}_\mathscr{T}$; time point $t_{new}^i \notin \mathscr{T}_i$ with $t_k^i < t_{new}^i < t_{k+1}^i$}
\State Add delay node $(i, t_{new}^i, \mathscr{K'})$ to $\mathscr{N}_{\mathscr{T}}$;
\State Delete delay arc $((i, t_{k+1}^i), (i, t_k^i))$ from $\mathscr{A}_\mathscr{T}$
\State Add delay arcs $((i, t_{k+1}^i), (i, t_{new}^i))$ and $((i , t_{new}^i), (i, t_k^i))$ to $\mathscr{A}_\mathscr{T}$
\ForAll {$((j, t), (i,  t_{k+1}^i)) \in \mathscr{A}_\mathscr{T}$}
\State Add arc $((j, t), (i, t_{new}^i))$ to $\mathscr{A}_\mathscr{T}$
\EndFor
\end{algorithmic}
\end{algorithm}


\begin{algorithm}
\caption{(RESTORE\_DELAY($(i, t_k^i, \mathscr{K'})$)}
\label{alg:restore}
\begin{algorithmic}[1]
\Require{Delay Node ($i, t_k^i, \mathscr{K'}) \in \mathscr{N}_\mathscr{T}$; time point $t_{new}^i \in \mathscr{T}_i$ with $t_k^i < t_{new}^i$}
\ForAll {$((j, t), (i, t_k^i)) \in \mathscr{A}_\mathscr{T}$ such that $t + \tau_{ji} \geqslant t_{new}^i$}
\State Delete arc $((j, t), (i, t_k^i))$ from $\mathscr{A}_\mathscr{T}$
\EndFor
\end{algorithmic}
\end{algorithm}

