<a id="top"></a>

## Project: The Continuous-Time Service Network Design Project (CTSNDP)

The code is a reproduction of the algorithms givin in:

The Continuous-Time Service Network Design Problem
by
Natashia Boland, Mike Hewitt, Luke Marshall, Martin Savelsbergh
Operations Research 65(5):1303-1321. https://doi.org/10.1287/opre.2017.1624

## Overview

* [Cloning project](#Cloning-this-project)
* [Directory Structure](#Directory-Structure)
* [Documentation](#Documentation)
* [Third-party contributions](#third-party-Contributions)
* [Dependencies](#Dependencies)

## Requirements

The ctsndp project has both a Python and C++ implementation.  For the C++ implementation, it is assumed that the target system has a C++ compiler that supports at least the C++19 standard.  The project also using the CMake meta-build system to generate the build files.  As such, many build systems are supported: makefiles, ninja, Xcode, Visual Studio, etc.

CMake can be installed using your package manager or by visiting https://cmake.org

## Cloning this project

To clone this project to your local machine, use the follow command:

    git clone https://gitlab.engr.illinois.edu/CIRI/ctsndp.git

<!--
In general, updating the submodules will be not be needed, but if you wish to update them, use the following command:

    git submodule update --recursive

If you didn't use the --recursive option when first cloning the project, use the following commands to pull the submodules:

    git submodule init
    git submodule update --recursive
-->

## Directory Structure

### A typical top-level directory layout

    .
    ├── data                    # Various data files for testing
    ├── docs                    # Documentation and artwork files
    ├── cpp                     # Source files for the C++ code
    ├── python                  # Python source files
    ├── third_party             # Third party C++ libraries
    └── README.md

## Documentation

* [Definitions](docs/definitions.md#top)
* [Using CMake](docs/cmake.md#top)
* [Building Third Party Libraries](docs/build_third_party_libs.md#top)
* [Build Instructions](docs/build.md#top)
* [Run Instructions](docs/running_ctsndp.md)
* [Future Work](docs/future_work.md)

## Third-party Contributions

The follow support libraries are hosted on GitHub:

* The command line parser library comes from the catchorg/Clara project
* The json parser library comes from the nlohmann/json project
* The json schema validator comes from the pboettch/json-schema-validator project
* The Catch2 testing library comes from the catchorg/Catch2 project

Please see the "Building third party libraries".

## Dependencies

This project depends on the GUROBI optimization library.  You can download the optimizer here: https://www.gurobi.com/downloads/download-center.  You will need to request an Academic license.  After installation, it is important to set the GUROBI_HOME environment variable to the install directory.  CMake uses this information in its build process.  After GUROBI is installed you will have to license it for that computer/user.  At the command prompt use:

> grbgetkey xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

where xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx is the license key from GUROBI.

If you are having linking errors, please [see Gurobi Linking Errors.](docs/gurobi_linking_errors.md#top)
